﻿var app = window.app || {};
(function (app) {
    (function (Components) {
        (function (Agent) {
            var Artwork = (function () {
                function Artwork() {
                    this.model = {};
                    this.model.guid = ko.observable();
                    this.model.name = ko.observable();
                    this.model.imageUrl = ko.observable();
                }
                return Artwork;
            })();
            var ArtworksManager = (function () {
                function ArtworksManager() {
                    this.model = {};
                    this.model.artworks = ko.observableArray([]);
                }
                return ArtworksManager;
            })();
            Agent.ArtworksManager = ArtworksManager;
            var Controller = (function () {
                function Controller() {

                }
                return Controller;
            })();
        })(Components.Agent || (Components.Agent = {}));
    })(app.Components || (app.Components = {}));
})(app);