﻿var app = window.app || {};
(function (app) {
    var utils = app.Utils,
        fetch = utils.fetch,
        defined = utils.defined,
        select = utils.select,
        receive = utils.receive;
    var Managers = app.Managers;
    (function (Screens) {
        var toDate = Screens.toDate = function (milennium, proximity, year, month, day, hour, minute) {
            var parts = [];
            if (proximity) {
                switch (proximity) {
                    case "==": parts.push("on"); break;
                    case "c.":
                    case "c": parts.push("around"); break;
                    case "<": parts.push("before"); break;
                    case "<=": parts.push("before or on"); break;
                    case ">": parts.push("after"); break;
                    case ">=": parts.push("on or after"); break;
                    default: break;
                }
            }
            if (hour && minute) {
                parts.push("{hh}:{mm}".fmt({ hh: hour, mm: minute }));
            }
            if (day && month) {
                parts.push(day + " of");
            }
            if (month) {
                parts.push(Months(month - 1) + ",");
            }
            if (year) {
                parts.push(year);
            }
            if (milennium == "BC") {
                parts.push(milennium);
            }
            return parts.join(" ");
        };
        var Editor = (function () {
            function Editor(component) {
                this.API = app.API;
                this.model = {};
                this.manager = {};
                this.manager.people /*: PeopleManager */ = null;
                this.manager.places /*: PlacesManager */ = null;
                this.manager.artworks /*: ArtworksManager */ = null;
                this.manager.queries /*: QueriesManager */ = null;
                this.currentManager = ko.observable();
                this.currentManagerIsReady = ko.observable(false);
                this.currentManagerTemplate = ko.observable();
                this.setupEventHandlers();
            }
            Editor.prototype.setupEventHandlers = function () {
                receive("ManageAgent", function (person) {
                    var manager = new Managers.PeopleManager();
                    manager.personSelected(person);
                    this.switchManager(manager);
                }, this);
                receive("ManagePlace", function (place) {
                    var manager = new Managers.PlacesManager();
                    manager.entitySelected(place);
                    this.switchManager(manager);
                }, this);
                receive("ManageArtwork", function (thing) {
                    var manager = new Managers.ArtworksManager();
                    manager.thingSelected(thing);
                    this.switchManager(manager);
                }, this);
                receive("ManageCategory", function (category) {
                    var manager = new Managers.CategoriesManager();
                    manager.categorySelected(category);
                    this.switchManager(manager);
                }, this);
            };
            Editor.prototype.switchManager = function (manager) {
                this.currentManagerIsReady(false);
                this.currentManager(manager);
                this.currentManagerTemplate(manager.templateName);
                this.currentManagerIsReady(true);
            };
            Editor.prototype.personManagerClicked = function () {
                var manager = new Managers.PeopleManager();
                this.switchManager(manager);
            };
            Editor.prototype.placeManagerClicked = function () {
                var manager = new Managers.PlacesManager();
                this.switchManager(manager);
            };
            Editor.prototype.categoryManagerClicked = function () {
                var manager = new Managers.CategoriesManager();
                this.switchManager(manager);
            };
            Editor.prototype.thingManagerClicked = function () {
                var manager = new Managers.ArtworksManager();
                this.switchManager(manager);
            };
            Editor.prototype.queriesManagerClicked = function () {
                var manager = new Managers.QueriesManager();
                this.switchManager(manager);
            };
            Editor.prototype.tempNodeManagerClicked = function () {
                var manager = new Managers.QueriesManager();
                this.switchManager(manager);
            };
            return Editor;
        })();
        Screens.Editor = Editor;
        var Months = [
            "January", "February", "March", "April", "May", "June", "July", "August",
            "September", "October", "November", "December"
        ];
        var Created = (function () {
            function Created(constructor) {
                this.model = {};
                this.model.Artist = {};
                this.model.Fact = {};
                this.model.Artwork = {};
                this.model.Place = {};
                this.model.Patron = {};
                this.model.Entity = null;
                if (constructor) {
                    this.bindConstructor(constructor);
                }
                this.computed = {};
                this.computed.hasStart = ko.computed(function () {
                    var fact = this.model.Fact;
                    return fact.Milennium || fact.Proximity || fact.Year || fact.Month || fact.Day || fact.Hour || fact.Minute;
                }, this);
                this.computed.hasFinish = ko.computed(function () {
                    var fact = this.model.Fact;
                    return fact.MilenniumEnd || fact.ProximityEnd || fact.YearEnd || fact.MonthEnd || fact.DayEnd || fact.HourEnd || fact.MinuteEnd;
                }, this);
                this.computed.start = ko.computed(function () {
                    var fact = this.model.Fact;
                    return toDate(fact.Milennium, fact.Proximity, fact.Year, fact.Month, fact.Day, fact.Hour, fact.Minute);
                }, this);
                this.computed.finish = ko.computed(function () {
                    var fact = this.model.Fact;
                    return toDate(fact.MilenniumEnd, fact.ProximityEnd, fact.YearEnd, fact.MonthEnd, fact.DayEnd, fact.HourEnd, fact.MinuteEnd);
                }, this);
                this.computed.hasPatron = ko.computed(function () {
                    return this.model.Patron;
                }, this);
                this.computed.patron = ko.computed(function () {
                    if (!this.model.Patron) {
                        return null;
                    }
                    return this.model.Patron.Name;
                }, this);
            }
            Created.prototype.patronClicked = function () {
                alert("Going to patron " + this.model.Patron.Guid);
            }
            Created.prototype.placeClicked = function () {
                alert("Going to place " + this.model.Place.Guid);
            }
            Created.prototype.bindConstructor = function (constructor) {
                if (!constructor) {
                    return;
                }
                if (constructor.model) {
                    this.bind(constructor.model);
                }
            };
            Created.prototype.bind = function (model) {
                this.model = model;
            };
            return Created;
        })();
        var Artwork = (function () {
            function Artwork() {
                this.representations = ko.observableArray([]);
                this.model = {};
                this.model.Artist = ko.observable();
                this.model.Artwork = ko.observable();
                this.model.Fact = ko.observable();
                this.model.Place = ko.observable();
                this.setupData();
                this.computed = {};
                this.computed.hasRepresentations = ko.computed(function () {
                    return this.representations().length;
                }, this);
                this.computed.hasStart = ko.computed(function () {
                    var fact = this.model.Fact();
                    return fact.Milennium || fact.Proximity || fact.Year || fact.Month || fact.Day || fact.Hour || fact.Minute;
                }, this);
                this.computed.hasFinish = ko.computed(function () {
                    var fact = this.model.Fact();
                    return fact.MilenniumEnd || fact.ProximityEnd || fact.YearEnd || fact.MonthEnd || fact.DayEnd || fact.HourEnd || fact.MinuteEnd;
                }, this);
                this.computed.start = ko.computed(function () {
                    var fact = this.model.Fact();
                    return toDate(fact.Milennium, fact.Proximity, fact.Year, fact.Month, fact.Day, fact.Hour, fact.Minute);
                }, this);
                this.computed.finish = ko.computed(function () {
                    var fact = this.model.Fact();
                    return toDate(fact.MilenniumEnd, fact.ProximityEnd, fact.YearEnd, fact.MonthEnd, fact.DayEnd, fact.HourEnd, fact.MinuteEnd);
                }, this);
            }
            Artwork.prototype.setupData = function () {
                var model = fetch(function () { return app.Screens.Resources.Fact; });
                this.bind(model);
                var representations = fetch(function () { return app.Screens.Resources.Representations; }, []);
                this.representations(representations);
            };
            Artwork.prototype.bind = function (model) {
                if (!model) {
                    return;
                }
                if (defined(model.Artist)) this.model.Artist(model.Artist);
                if (defined(model.Artwork)) this.model.Artwork(model.Artwork);
                if (defined(model.Fact)) this.model.Fact(model.Fact);
                if (defined(model.Place)) this.model.Place(model.Place);
            };
            return Artwork;
        })();
        Screens.Artwork = Artwork;
        var Gallery = (function () {
            function Gallery(constructor) {
                this.API = app.API;
                this.person = ko.observable();
                this.categories = ko.observableArray([]);
                this.facts = {};
                this.facts.name = ko.observable();
                this.facts.imageUrl = ko.observable();
                this.facts.birth = ko.observable();
                this.facts.death = ko.observable();
                this.facts.created = ko.observableArray([]);
                this.facts.livedAt = ko.observableArray([]);
                this.computed = {};
                this.computed.hasDeath = ko.computed(function () {
                    return this.facts.death();
                }, this);
                this.computed.hasCategories = ko.computed(function () {
                    return this.categories().length;
                }, this);
                this.computed.hasBirth = ko.computed(function () {
                    return this.facts.birth();
                }, this);
                this.computed.hasLivedAt = ko.computed(function () {
                    return this.facts.livedAt().length;
                }, this);
                this.computed.hasCreated = ko.computed(function () {
                    return this.facts.created().length;
                }, this);
                this.computed.start = ko.computed(function () {

                }, this);
                this.computed.finish = ko.computed(function () {

                }, this);
                this.computed.patron = ko.computed(function () {

                }, this);
                this.setupData();
            }
            Gallery.prototype.bindConstructor = function (constructor) {
                if (!constructor) {
                    return;
                }
                if (constructor.categories) {
                    this.bindCategories(constructor.categories);
                }
                if (constructor.facts) {
                    this.bindFacts(constructor.facts);
                }
            };
            Gallery.prototype.bindCategories = function (categories) {
                if (!categories) {
                    return;
                }
                if (defined(categories)) this.categories(categories);
            };
            Gallery.prototype.bindFacts = function (facts) {
                if (!facts) {
                    return;
                }
                if (defined(facts.name)) this.facts.name(facts.name);
                if (defined(facts.imageUrl)) this.facts.imageUrl(facts.imageUrl);
                if (defined(facts.birth)) this.facts.birth(facts.birth);
                if (defined(facts.death)) this.facts.death(facts.death);
                if (defined(facts.created)) {
                    this.facts.created(select(facts.created, function (model) {
                        return new Created({
                            model: model
                        });
                    }));
                }
                if (defined(facts.livedAt)) this.facts.livedAt(facts.livedAt);
            };
            Gallery.prototype.setupData = function () {
                var name = fetch(function () { return app.Screens.Resources.Facts.Name; });
                var imageUrl = fetch(function () { return app.Screens.Resources.Facts.ImageUrl; });
                var birth = fetch(function () { return app.Screens.Resources.Facts.Birth; });
                var death = fetch(function () { return app.Screens.Resources.Facts.Death; });
                var created = fetch(function () { return app.Screens.Resources.Facts.Created; });
                var livedAt = fetch(function () { return app.Screens.Resources.Facts.LivedAt; });
                this.bindFacts({
                    name: name,
                    imageUrl: imageUrl,
                    birth: birth,
                    death: death,
                    created: created,
                    livedAt: livedAt
                });
                var categories = fetch(function () { return app.Screens.Resources.Categories; });
                this.bindCategories(categories);
                var person = fetch(function () { return app.Screens.Resources.Agent; });
                this.person(person);
            };
            Gallery.prototype.createdClicked = function (created) {
                var guid = created.model.Artwork.Guid;
                var url = "/editor/artwork?guid=" + guid;
                window.location.href = url;
            };
            Gallery.prototype.livedAtClicked = function (created) {

            };
            return Gallery;
        })();
        Screens.Gallery = Gallery;
    })(app.Screens = (app.Screens || {}));
})(app);