/*

    SlideOutNav
    Awkwardly created by Nick

    Initiate the plugin on the navigation element like so:
    $(".secondarynav").slideOutNav();

    Give the button that opens/closes the nav a class  
    of ".show-slideoutnav-button" and it'll do the rest.


    The following options are available:

        buttonEl:       string,     // Class of the element that shows/hides the nav
        leftOrRight:    string,     // "left" or "right"; which side the nav should slide out from
        minWidth:       string,     // Minimum width for it to work
        maxWidth:       string,     // Maximum width for it to work
        removeClass:    bool        // Whether or not to remove any existing classes on the nav you target


    Default options:

        buttonEl:       ".show-slideoutnav-button",
        leftOrRight:    "left",
        minWidth:       null,       // Works on any width
        maxWidth:       null,       // Works on any width
        removeClass:    true


*/

(function ( $ ) {
 
    $.fn.slideOutNav = function( options ) {

        var settings = $.extend({
                buttonEl: ".show-slideoutnav-button",
                leftOrRight: "left",
                minWidth: null,
                maxWidth: null,
                removeClass: true
            }, options ),
            navEl = this,
            buttonEl = $(settings.buttonEl),
            leftOrRight = settings.leftOrRight != "left" ? "right" : "left";

        var SlideOutNavUtils = (function() {
            return {

                Init: function() {

                    // Wrap everything within the <body> in a <div> if it doesn't alreadt exist
                    if ( !$(".slideoutnav-pushcontent").length ) $("body").wrapInner("<div class=\"slideoutnav-pushcontent\"></div>");

                    var slideOutNavConstructor = navEl.clone(),
                        pushContent = $(".slideoutnav-pushcontent"),
                        slideOutNavID = "slideoutnav-" + navEl.index();

                    // Remove nav's class if setting's set
                    if (settings.removeClass == true) slideOutNavConstructor.attr("class","");

                    // Add slideoutnav attributes and insert before content wrapper
                    // Give unique ID for button to be linked to through ARIA attribute
                    slideOutNavConstructor
                                        .addClass("slideoutnav slideoutnav-is-hidden slideoutnav-from-" + leftOrRight)
                                        .attr("id", slideOutNavID)
                                        .attr("aria-hidden", "true")
                                        .insertBefore(pushContent);

                    // Apply button attributes
                    buttonEl
                            .attr("aria-controls", slideOutNavID)
                            .attr("aria-expanded", "false");

                    this.CSS();
                    this.DocumentClick();

                },

                CSS: function() {
                    var head = $("head"),
                        CSS =
"<style> \n\
    .slideoutnav-pushcontent, \n\
    .slideoutnav { \n\
        transition: all .3s ease-out; \n\
        -webkit-backface-visibility: hidden; \n\
        overflow-x: hidden; \n\
        position: relative; \n\
    } \n\
    .slideoutnav-pushcontent { overflow: hidden; } \n\
    .slideoutnav { \n\
        display: block; \n\
        position: fixed; \n\
        height: 125%; \n\
        min-height: 125%; \n\
        top: 0; \n\
        overflow-y: auto; \n\
        overflow-x: hidden; \n\
        padding-bottom: 45%; \n\
    } \n\
    .slideoutnav-from-left { right: 100%; } \n\
    .slideoutnav-from-right { left: 100%; } \n\
</style>";
                    head.append(CSS);
                },

                DocumentClick: function() {
                    $(document).on("click", function(event) {

                        // If we haven't clicked within the nav itself
                        if(!$(event.target).closest(".slideoutnav").length) {

                            // If the nav is visible, hide it
                            if($(".slideoutnav").hasClass("slideoutnav-is-visible")) {
                                SlideOutNavUtils.HideNav();
                            }

                            // Otherwise if the nav inn't visible...
                            else {
                                // ...and we've clicked on the show/hide button, show the nav (if conditions are met)
                                if ($(event.target).closest(buttonEl).length) {
                                    SlideOutNavUtils.ButtonClick();
                                    event.preventDefault();
                                }
                            }

                        }

                    });  
                },

                UnwrapBody: function() {
                    if ( $(".slideoutnav-pushcontent").length ) $(".slideoutnav-pushcontent").children().first().unwrap();
                },

                // The width of our nav, positive or negative depending on left or right positioning
                TranslateDistance: function() {
                    var translateDistance = leftOrRight == "left" ? $(".slideoutnav").outerWidth() : -Math.abs($(".slideoutnav").outerWidth());
                    return translateDistance;
                },

                NavIsVisible: function() {
                    return $(".slideoutnav").hasClass("slideoutnav-is-visible");
                },

                ShowNav: function() {
                    var slideOutNav = $(".slideoutnav"),
                        pushContent = $(".slideoutnav-pushcontent");

                    // Show slideoutnav
                    slideOutNav
                            .addClass("slideoutnav-is-visible")
                            .removeClass("slideoutnav-is-hidden")
                            .attr("aria-hidden", "false")
                            .css({"-webkit-transform": "translateX(" + SlideOutNavUtils.TranslateDistance() + "px)",
                                "-moz-transform": "translateX(" + SlideOutNavUtils.TranslateDistance() + "px)",
                                "-ms-transform": "translateX(" + SlideOutNavUtils.TranslateDistance() + "px)",
                                "-o-transform": "translateX(" + SlideOutNavUtils.TranslateDistance() + "px)",
                                "transform": "translateX(" + SlideOutNavUtils.TranslateDistance() + "px)",
                                "z-index": "1"}); // (z-index fix for IOS visibility bug)

                    // Slide content across
                    pushContent
                            .css({"-webkit-transform": "translateX(" + SlideOutNavUtils.TranslateDistance() + "px)",
                                "-moz-transform": "translateX(" + SlideOutNavUtils.TranslateDistance() + "px)",
                                "-ms-transform": "translateX(" + SlideOutNavUtils.TranslateDistance() + "px)",
                                "-o-transform": "translateX(" + SlideOutNavUtils.TranslateDistance() + "px)",
                                "transform": "translateX(" + SlideOutNavUtils.TranslateDistance() + "px)"});

                    // Change button attributes
                    buttonEl
                            .addClass("is-active")
                            .attr("aria-expanded", "true");
                },

                HideNav: function() {
                    var slideOutNav = $(".slideoutnav"),
                        pushContent = $(".slideoutnav-pushcontent");

                    // Hide slideoutnav
                    slideOutNav
                            .addClass("slideoutnav-is-hidden")
                            .removeClass("slideoutnav-is-visible")
                            .attr("aria-hidden", "true")
                            .css({"-webkit-transform": "",
                                "-moz-transform": "",
                                "-ms-transform": "",
                                "-o-transform": "",
                                "transform": ""});

                    // Slide content back across
                    pushContent
                            .css({"-webkit-transform": "", 
                                "-moz-transform": "",
                                "-ms-transform": "",
                                "-o-transform": "",
                                "transform": ""});

                    // Change button attributes
                    buttonEl
                            .removeClass("is-active")
                            .attr("aria-expanded", "false");
                },

                // Shows/Hides nav if necessary conditions are met
                ButtonClick: function() {

                    // Neither minWidth nor maxWidth were set
                    if (settings.minWidth == null && settings.maxWidth == null) {
                        SlideOutNavUtils.ShowNav();
                    }

                    // Either or both minWidth or maxWidth was set
                    else {

                        // Only minWith was set
                        if (settings.minWidth != null && settings.maxWidth == null) {

                            // Show nav if minWidth is true
                            if ( MediaQueries.MinWidth(settings.minWidth) ) {
                                SlideOutNavUtils.ShowNav();
                            }

                            // Hide nav if minWidth is false after resize
                            $(window).afterResize(function() {
                                if ( !MediaQueries.MinWidth(settings.minWidth) ) {
                                    SlideOutNavUtils.HideNav();
                                }
                            }, true, 200);
                        }

                        // Only maxWidth was set
                        else if (settings.minWidth == null && settings.maxWidth != null) {

                            // Show nav if maxWidth is true
                            if ( MediaQueries.MaxWidth(settings.maxWidth) ) {
                                SlideOutNavUtils.ShowNav();
                            }

                            // Hide nav if maxWidth is false after resize
                            $(window).afterResize(function() {
                                if ( !MediaQueries.MaxWidth(settings.maxWidth) ) {
                                    SlideOutNavUtils.HideNav();
                                }
                            }, true, 200);
                        }

                        // Both minWidth and MaxWidth were set
                        else {

                            // Show nav if minWidth and maxWidth are both trye
                            if ( MediaQueries.MinWidth(settings.minWidth) && MediaQueries.MaxWidth(settings.maxWidth) ) {
                                SlideOutNavUtils.ShowNav();
                            }

                            // Hide nav if minWidth OR maxWidth is false after resize
                            $(window).afterResize(function() {
                                if ( !MediaQueries.MinWidth(settings.minWidth) || !MediaQueries.MaxWidth(settings.maxWidth) ) {
                                    SlideOutNavUtils.HideNav();
                                }
                            }, true, 200);
                        }

                    }
                    // END else minWidth or maxWidth was set 

                }

            }
        })();
        // End SlideOutNavUtils

        SlideOutNavUtils.Init();
         
    };
 
}( jQuery ));