﻿var app = window.app || {};
(function (app) {
    (function (Selectors) {
        var utils = app.Utils,
            PubSub = utils.PubSub,
            defined = utils.defined;
        var AgentSelector = (function () {
            function AgentSelector(component/*: IAgentSelectorComponent */) {
                this.API = app.API;
                this.model = {};
                this.model.name = ko.observable();
                this.model.person /*: Agent */ = ko.observable();
                this.resource = {};
                this.resource.people = ko.observableArray();
                this.pubsub = new PubSub();
                this.text = {};
                this.text.details = ko.observable();
                if (component) {
                    this.bindComponent(component);
                }
            }
            AgentSelector.prototype.find = function () {
                var name = this.model.name();
                API.findAgentByName({
                    data: {
                        name: name
                    },
                    success: function (response) {
                        this.resource.people(response.people);
                    }
                }, this);
            };
            AgentSelector.prototype.personSelected = function (person/*: Agent */) {
                this.model.name(person.name);
                this.model.person(person);
                this.resource.people([]);
                this.pubsub.publish("AgentSelected", person);
            };
            AgentSelector.prototype.unbind = function () {
                return {
                    name: this.model.name(),
                    person: this.model.person()
                };
            };
            AgentSelector.prototype.bindComponent = function (component) {
                if (!component) {
                    return;
                }
                if (component.model) {
                    this.bind(component.model);
                }
            };
            AgentSelector.prototype.bind = function (model) {
                if (!model) {
                    return;
                }
                if (defined(model.name)) this.model.name(model.name);
            };
            return AgentSelector;
        })();
        Selectors.AgentSelector = AgentSelector;
        var PlaceSelector = (function () {
            function PlaceSelector(component/*: IPlaceSelectorComponent */) {
                this.API = app.API;
                this.model = {};
                this.model.name = ko.observable();
                this.model.place /*: Place */ = ko.observable();
                this.resource = {};
                this.resource.places = ko.observableArray();
                this.pubsub = new PubSub();
                this.text = {};
                this.text.details = ko.observable();
                if (component) {
                    this.bindComponent(component);
                }
            }
            PlaceSelector.prototype.addClicked = function () {
                var name = this.model.name();
                this.API.createPlace({
                    data: {
                        name: name
                    },
                    success: function (response) {
                        this.placeSelected(response.Data);
                    }
                }, this);
            };
            PlaceSelector.prototype.findClicked = function () {
                var name = this.model.name();
                this.API.findPlaces({
                    data: {
                        name: name
                    },
                    success: function (response) {
                        this.resource.places(response.Data);
                    }
                }, this);
            };
            PlaceSelector.prototype.placeSelected = function (place/*: Place */) {
                this.model.name(place.Name);
                this.model.place(place);
                this.resource.places([]);
                this.pubsub.publish("PlaceSelected", place);
            };
            PlaceSelector.prototype.unbind = function () {
                return {
                    name: this.model.name(),
                    place: this.model.place()
                };
            };
            PlaceSelector.prototype.bindComponent = function (component) {
                if (!component) {
                    return;
                }
                if (component.model) {
                    this.bind(component.model);
                }
            };
            PlaceSelector.prototype.bind = function (model) {
                if (!model) {
                    return;
                }
                if (defined(model.name)) this.model.name(model.name);
            };
            return PlaceSelector;
        })();
        Selectors.PlaceSelector = PlaceSelector;
        var EventSelector = (function () {
            function EventSelector(component/*: IEventSelectorComponent */) {
                this.API = app.API;
                this.model = {};
                this.model.name = ko.observable();
                this.model.event /*: EventNode */ = ko.observable();
                this.resource = {};
                this.resource.events = ko.observableArray();
                this.pubsub = new PubSub();
                this.text = {};
                this.text.details = ko.observable();
                if (component) {
                    this.bindComponent(component);
                }
            }
            EventSelector.prototype.addClicked = function () {
                var name = this.model.name();
                this.API.createEvent({
                    data: {
                        name: name
                    },
                    success: function (response) {
                        this.eventSelected(response.Data);
                    }
                }, this);
            };
            EventSelector.prototype.findClicked = function () {
                var name = this.model.name();
                this.API.findEvents({
                    data: {
                        name: name
                    },
                    success: function (response) {
                        this.resource.events(response.Data);
                    }
                }, this);
            };
            EventSelector.prototype.eventSelected = function (event/*: EventNode */) {
                this.model.name(event.Name);
                this.model.event(event);
                this.resource.events([]);
                this.pubsub.publish("EventSelected", event);
            };
            EventSelector.prototype.unbind = function () {
                return {
                    name: this.model.name(),
                    event: this.model.event()
                };
            };
            EventSelector.prototype.bindComponent = function (component) {
                if (!component) {
                    return;
                }
                if (component.model) {
                    this.bind(component.model);
                }
            };
            EventSelector.prototype.bind = function (model) {
                if (!model) {
                    return;
                }
                if (defined(model.name)) this.model.name(model.name);
            };
            return EventSelector;
        })();
        Selectors.EventSelector = EventSelector;
        //to update
        var toStartDateSelectorModel = Selectors.toStartDateSelectorModel = function (fact) {
            return {
                proximity: fact.Proximity,
                milennium: fact.Milennium,
                year: fact.Year,
                month: fact.Month,
                day: fact.Day,
                hour: fact.Hour,
                minute: fact.Minute
            };
        }
        //to update
        var toEndDateSelectorModel = Selectors.toEndDateSelectorModel = function (fact) {
            return {
                proximity: fact.ProximityEnd,
                milennium: fact.MilenniumEnd,
                year: fact.YearEnd,
                month: fact.MonthEnd,
                day: fact.DayEnd,
                hour: fact.HourEnd,
                minute: fact.MinuteEnd
            };
        };
        var fromStartDateSelectorModel = Selectors.fromStartDateSelectorModel = function (date) {
            return {
                Proximity: date.proximity,
                Milennium: date.milennium,
                Year: date.year,
                Month: date.month,
                Day: date.day,
                Hour: date.hour,
                Minute: date.minute
            };
        };
        var fromEndDateSelectorModel = Selectors.fromEndDateSelectorModel = function (date) {
            return {
                ProximityEnd: date.proximity,
                MilenniumEnd: date.milennium,
                YearEnd: date.year,
                MonthEnd: date.month,
                DayEnd: date.day,
                HourEnd: date.hour,
                MinuteEnd: date.minute
            };
        };
        var DateSelector = (function () {
            function DateSelector(component) {
                this.templateName = "date-selector-template";
                this.model = {};
                this.model.proximity = ko.observable();
                this.model.milennium = ko.observable();
                this.model.year = ko.observable();
                this.model.month = ko.observable();
                this.model.day = ko.observable();
                this.model.hour = ko.observable();
                this.model.minute = ko.observable();
                this.proximities = [
                    { text: "on", value: "==" },
                    { text: "around", value: "c." },
                    { text: "before", value: "<" },
                    { text: "after", value: ">" },
                    { text: "on or before", value: "<=" },
                    { text: "on or after", value: ">=" },
                ];
                this.milennia = [
                    "AD", "BC"
                ];
                if (component) {
                    this.bind(component);
                }
            }
            DateSelector.prototype.bindComponent = function (component) {
                if (!component) {
                    return;
                }
                if (component.model) {
                    this.bind(component.model);
                }
            };
            DateSelector.prototype.bind = function (model) {
                if (!model) {
                    return;
                }
                if (defined(model.proximity)) this.model.proximity(model.proximity);
                if (defined(model.milennium)) this.model.milennium(model.milennium);
                if (defined(model.year)) this.model.year(model.year);
                if (defined(model.month)) this.model.month(model.month);
                if (defined(model.day)) this.model.day(model.day);
                if (defined(model.hour)) this.model.hour(model.hour);
                if (defined(model.minute)) this.model.minute(model.minute);
            };
            DateSelector.prototype.unbind = function () {
                return {
                    proximity: this.model.proximity(),
                    milennium: this.model.milennium(),
                    year: this.model.year(),
                    month: this.model.month(),
                    day: this.model.day(),
                    hour: this.model.hour(),
                    minute: this.model.minute()
                };
            };
            return DateSelector;
        })();
        Selectors.DateSelector = DateSelector;
    })(app.Selectors || (app.Selectors = {}));
})(app);