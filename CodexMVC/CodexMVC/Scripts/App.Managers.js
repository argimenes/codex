﻿var app = window.app || {};
(function (app) {
    var utils = app.Utils,
        broadcast = utils.broadcast;
    var Stacks = app.Stacks,
        Common = app.Common,
        Nodes=app.Nodes,
        Mode = Common.Mode;
    (function (Managers) {
        var TempNodesManager = (function () {
            function TempNodesManager() {
                this.templateName = "temp-nodes-manager-template";
                this.API = app.API;
                this.model = {};
                this.model.results = ko.observableArray([]);
                this.setupData();
            }
            TempNodesManager.prototype.setupData = function () {
                this.allTempNodes();
            };
            TempNodesManager.prototype.queryFactsClicked = function () {
                this.queryFacts();
            };
            /**
            interface ITempNode {
                Label: string;
                Guid: string;
                Name: string;
                ModifiedDate: string;
            }
            */
            TempNodesManager.prototype.entityClicked = function (node/*: ITempNode */) {
                var eventType = "Manage" + node.Label;
                broadcast(eventType, node.Guid);
            };
            TempNodesManager.prototype.allTempNodes = function () {
                this.API.allTempNodes({
                    success: function (response) {
                        this.model.results(response.Data);
                    }
                }, this);
            };
            return TempNodesManager;
        })();
        Managers.TempNodesManager = TempNodesManager;
        var QueriesManager = (function () {
            function QueriesManager() {
                this.templateName = "queries-manager-template";
                this.API = app.API;
                this.model = {};
                this.model.results = ko.observableArray([]);
            }
            QueriesManager.prototype.queryFactsClicked = function () {
                this.queryFacts();
            };
            QueriesManager.prototype.agentClicked = function (data) {
                broadcast("ManageAgent", data.Agent);
            };
            QueriesManager.prototype.factClicked = function (data) {
                broadcast("ManageFact", data.Fact);
            };
            QueriesManager.prototype.placeClicked = function (data) {
                broadcast("ManagePlace", data.Place);
            };
            QueriesManager.prototype.queryFacts = function () {
                this.API.queryFacts({
                    success: function (response) {
                        this.model.results(response.Data);
                    }
                }, this);
            };
            return QueriesManager;
        })();
        Managers.QueriesManager = QueriesManager;
        var ArtworksManager = (function () {
            function ArtworksManager() {
                this.templateName = "things-manager-template";
                this.mode = ko.observable(Mode.None);
                this.API = app.API;
                this.model = {};
                this.model.newArtwork = new Nodes.Artwork();
                this.things = ko.observableArray([]);
                this.editArtworkStack = ko.observable();
                this.editArtworkStackIsReady = ko.observable(false);
                this.state = {};
                this.state.isNone = ko.computed(function () {
                    return Mode.None == this.mode();
                }, this);
                this.state.isEdit = ko.computed(function () {
                    return Mode.Edit == this.mode();
                }, this);
                this.state.isNew = ko.computed(function () {
                    return Mode.New == this.mode();
                }, this);
                this.setupData();
            }
            ArtworksManager.prototype.setupData = function () {
                this.showAllArtworks();
            };
            ArtworksManager.prototype.switchArtworkStack = function (stack) {
                this.mode(Mode.Edit);
                this.editArtworkStackIsReady(false);
                this.editArtworkStack(stack);
                this.editArtworkStackIsReady(true);
            };
            ArtworksManager.prototype.showAllArtworks = function () {
                this.API.allArtworks({
                    success: function (response) {
                        this.things(response.Data);
                    }
                }, this);
            };
            ArtworksManager.prototype.addNewArtworkInitiated = function () {
                this.model.newArtwork = new Nodes.Artwork();
                this.mode(Mode.New);
            };
            ArtworksManager.prototype.saveArtworkClicked = function () {
                var model = this.model.newArtwork.unbind();
                this.API.saveArtwork({
                    data: {
                        name: model.name,
                        sex: model.sex
                    },
                    success: function (response) {
                        var thing = response.Data;
                        var stack = new Stacks.ArtworkStack({ model: { self: thing } });
                        this.switchArtworkStack(stack);
                    }
                }, this);
            };
            ArtworksManager.prototype.thingSelected = function (thing) {
                var stack = new Stacks.ArtworkStack({ model: { self: thing } });
                this.switchArtworkStack(stack);
            };
            return ArtworksManager;
        })();
        Managers.ArtworksManager = ArtworksManager;
        var CategoriesManager = (function () {
            function CategoriesManager() {
                this.templateName = "categories-manager-template";
                this.mode = ko.observable(Mode.None);
                this.API = app.API;
                this.model = {};
                this.model.parent = ko.observable();
                this.model.name = ko.observable();
                this.parentCategories = ko.observableArray([]);
                this.categories = ko.observableArray([]);
                this.editCategoryStack = ko.observable();
                this.editCategoryStackIsReady = ko.observable(false);
                this.state = {};
                this.state.isNone = ko.computed(function () {
                    return Mode.None == this.mode();
                }, this);
                this.state.isEdit = ko.computed(function () {
                    return Mode.Edit == this.mode();
                }, this);
                this.state.isNew = ko.computed(function () {
                    return Mode.New == this.mode();
                }, this);
                this.setupData();
            }
            CategoriesManager.prototype.addNewAgentClicked = function () {
                this.model.newCategory = new Nodes.Category();
                this.mode(Mode.New);
                this.parentCategories(this.categories());
            };
            CategoriesManager.prototype.switchCategoryStack = function (stack) {
                this.mode(Mode.Edit);
                this.editCategoryStackIsReady(false);
                this.editCategoryStack(stack);
                this.editCategoryStackIsReady(true);
            };
            CategoriesManager.prototype.setupData = function () {
                this.showAllCategories();
            };
            CategoriesManager.prototype.showAllCategories = function () {
                this.API.allCategories({
                    success: function (response) {
                        this.categories(response.Data);
                    }
                }, this);
            };
            CategoriesManager.prototype.createCategoryClicked = function () {
                this.API.createCategory({
                    data: {
                        name: this.model.name(),
                        parentCategoryGuid: this.model.parent()
                    },
                    success: function (response) {
                        this.model.name(null);
                        this.model.parent(null);
                        this.mode(Mode.None);
                    }
                }, this);
            };
            CategoriesManager.prototype.categorySelected = function (category) {
                var stack = new Stacks.CategoryStack({ model: { self: category } });
                this.switchCategoryStack(stack);
            };
            return CategoriesManager;
        })();
        Managers.CategoriesManager = CategoriesManager;
        var PlacesManager = (function () {
            function PlacesManager(component) {
                this.templateName = "places-manager-template";
                this.mode = ko.observable(Mode.None);
                this.API = app.API;
                this.model = {};
                this.model.newEntity = new Nodes.Place();
                this.entities = ko.observableArray([]);
                this.editStack = ko.observable();
                this.editStackIsReady = ko.observable(false);
                this.state = {};
                this.state.isNone = ko.computed(function () {
                    return Mode.None == this.mode();
                }, this);
                this.state.isEdit = ko.computed(function () {
                    return Mode.Edit == this.mode();
                }, this);
                this.state.isNew = ko.computed(function () {
                    return Mode.New == this.mode();
                }, this);
                this.setupData();
            }
            PlacesManager.prototype.setupData = function () {
                this.showAll();
            };
            PlacesManager.prototype.switchStack = function (stack) {
                this.mode(Mode.Edit);
                this.editStackIsReady(false);
                this.editStack(stack);
                this.editStackIsReady(true);
            };
            PlacesManager.prototype.showAll = function () {
                this.API.allPlaces({
                    success: function (response) {
                        this.entities(response.Data);
                    }
                }, this);
            };
            PlacesManager.prototype.addNewEntityClicked = function () {
                this.model.newEntity = new Nodes.Place();
                this.mode(Mode.New);
            };
            PlacesManager.prototype.saveEntityClicked = function () {
                var model = this.model.newEntity.unbind();
                this.API.savePlace({
                    data: {
                        name: model.name,
                        latitude: model.latitude,
                        longitude: model.longitude
                    },
                    success: function (response) {
                        var place = response.Data;
                        var stack = new Stacks.PlaceStack({ model: { self: place } });
                        this.switchStack(stack);
                    }
                }, this);
            };
            PlacesManager.prototype.entitySelected = function (place) {
                var stack = new Stacks.PlaceStack({ model: { self: place } });
                this.switchStack(stack);
            };
            return PlacesManager;
        })();
        Managers.PlacesManager = PlacesManager;
        var PeopleManager = (function () {
            function PeopleManager() {
                this.templateName = "people-manager-template";
                this.mode = ko.observable(Mode.None);
                this.API = app.API;
                this.model = {};
                this.model.newAgent = new Nodes.Agent();
                this.people = ko.observableArray([]);
                this.editAgentStack = ko.observable();
                this.editAgentStackIsReady = ko.observable(false);
                this.state = {};
                this.state.isNone = ko.computed(function () {
                    return Mode.None == this.mode();
                }, this);
                this.state.isEdit = ko.computed(function () {
                    return Mode.Edit == this.mode();
                }, this);
                this.state.isNew = ko.computed(function () {
                    return Mode.New == this.mode();
                }, this);
                this.setupData();
            }
            PeopleManager.prototype.setupData = function () {
                this.showAllPeople();
            };
            PeopleManager.prototype.switchAgentStack = function (stack) {
                this.mode(Mode.Edit);
                this.editAgentStackIsReady(false);
                this.editAgentStack(stack);
                this.editAgentStackIsReady(true);
            };
            PeopleManager.prototype.showAllPeople = function () {
                this.API.allPeople({
                    success: function (response) {
                        this.people(response.Data);
                    }
                }, this);
            };
            PeopleManager.prototype.addNewAgentInitiated = function () {
                this.model.newAgent = new Nodes.Agent();
                this.mode(Mode.New);
            };
            PeopleManager.prototype.saveAgentClicked = function () {
                var model = this.model.newAgent.unbind();
                this.API.saveAgent({
                    data: {
                        name: model.name,
                        sex: model.sex
                    },
                    success: function (response) {
                        var person = response.Data;
                        var stack = new Stacks.AgentStack({ model: { self: person } });
                        this.switchAgentStack(stack);
                    }
                }, this);
            };
            PeopleManager.prototype.personSelected = function (person) {
                var stack = new Stacks.AgentStack({ model: { self: person } });
                this.switchAgentStack(stack);
            };
            return PeopleManager;
        })();
        Managers.PeopleManager = PeopleManager;
    })(app.Managers || (app.Managers = {}));
})(app);