﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CodexMVC.Models
{
    public class ImportHistoricEventsModel
    {
        public int? Start { get; set; }
        public int? Rows { get; set; }
    }
}