﻿using Data.Entities;
using Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CodexMVC.Models
{
    public class ViewPermissionModel
    {
        public Permission Permission { get; set; }
        public List<Role> RolesAssociatedWith { get; set; }
    }

    public class EditRoleModel
    {
        public string RoleGuid { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }

    public class ViewUserModel
    {
        public User User { get; set; }
        public List<Role> Roles { get; set; }
    }

    public class ViewRoleModel
    {
        public Role Role { get; set; }
        public List<Permission> Permissions { get; set; }
    }

    public class AddRoleToUserModel
    {
        [Required]
        public string UserGuid { get; set; }

        [Required]
        public string RoleGuid { get; set; }

        [ReadOnly(true)]
        public List<System.Web.Mvc.SelectListItem> Roles { get; set; }

        public AddRoleToUserModel()
        {
            var userService = DependencyResolver.Current.GetService<IUserService>();
            Roles = userService.AllRoles().Select(x => new System.Web.Mvc.SelectListItem { Text = x.Name, Value = x.Guid }).ToList();
        }
    }

    public class AddPermissionToRoleModel
    {
        [Required]
        public string RoleGuid { get; set; }

        [Required]
        public string PermissionGuid { get; set; }

        [ReadOnly(true)]
        public List<System.Web.Mvc.SelectListItem> Permissions { get; set; }

        public AddPermissionToRoleModel()
        {
            var userService = DependencyResolver.Current.GetService<IUserService>();
            Permissions = userService.AllPermissions().Select(x => new System.Web.Mvc.SelectListItem { Text = x.Name, Value = x.Guid }).ToList();
        }
    }
    public class EditPermissionModel
    {
        public string PermissionGuid { get; set; }

        [Required]
        public string Name { get; set; }

        public string Description { get; set; }
    }
    public class EditUserModel
    {
        public string UserGuid { get; set; }

        [Required]
        [Display(Name = "First name")]
        public string FirstName { get; set; }

        [Required]
        [Display(Name = "Last name")]
        public string LastName { get; set; }

        [Required]
        public string Username { get; set; }

        [Required]
        public string Password { get; set; }
    }
    public class EditUserRoleModel
    {
        [Required]
        public string UserGuid { get; set; }

        [Required]
        public string RoleGuid { get; set; }

        [ReadOnly(true)]
        public readonly User User;

        [ReadOnly(true)]
        public readonly List<System.Web.Mvc.SelectListItem> Roles;

        public EditUserRoleModel()
        {
            var userService = DependencyResolver.Current.GetService<IUserService>();
            Roles = userService.AllRoles().Select(x => new System.Web.Mvc.SelectListItem { Text = x.Name, Value = x.Guid }).ToList();
        }

        public EditUserRoleModel(string userGuid)
        {
            var userService = DependencyResolver.Current.GetService<IUserService>();
            UserGuid = userGuid;
            User = userService.FindUserByGuid(userGuid);
        }
    }
}