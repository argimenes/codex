﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CodexMVC.Models
{
    public class EditVideo
    {
        public string Guid { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string Uri { get; set; }

        [Required]
        public string Description { get; set; }

        public string Source { get; set; }

        public string StartTime { get; set; }

        public string EndTime { get; set; }

        [Required]
        public int? Width { get; set; }

        [Required]
        public int? Height { get; set; }
    }
}