﻿using Data;
using Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CodexMVC.Models
{
    public class EditWithAgent : EditAgentRelation
    {
        public Agent WithAgent { get; set; }
        public string WithAgentGuid { get; set; }
        public List<SelectListItem> Agents { get; set; }
        public EditWithAgent()
        {
            var agentService = DependencyResolver.Current.GetService<IAgentService>();
            var agents = agentService.AllWithAttributes();
            Agents = agents
                .OrderBy(x => x.Agent.FullName)
                .Select(x => new SelectListItem
                {
                    Text = x.ToDisplay(),
                    Value = x.Agent.Guid
                })
                .ToList();
        }
    }
}