﻿using Data;
using Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CodexMVC.Models
{
    public class AddCitizenOf
    {
        public Agent Agent { get; set; }

        public string PlaceGuid { get; set; }

        public List<SelectListItem> Places { get; set; }

        public AddCitizenOf()
        {
            var placeService = DependencyResolver.Current.GetService<IPlaceService>();
            var places = placeService.AllWithContainers();
            Places = places.Select(x => new System.Web.Mvc.SelectListItem
            {
                Text = x.Place.Name + " (" + string.Join(" > ", x.Containers.Select(x2 => x2.Name).ToArray()) + ")",
                Value = x.Place.Guid
            })
            .ToList();
        }
    }
}