﻿using Data;
using Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CodexMVC.Models
{
    public class EditAgentRelationToOtherAgent
    {
        public Agent Agent { get; set; }
        public Agent OtherAgent { get; set; }
        public string RelationGuid { get; set; }
        public List<SelectListItem> Agents { get; set; }
        public EditAgentRelationToOtherAgent()
        {

        }
        public EditAgentRelationToOtherAgent(bool loadResource)
        {
            var agentService = DependencyResolver.Current.GetService<IAgentService>();
            var agents = agentService.AllWithAttributes();
            Agents = agents.Select(x => new SelectListItem { Text = x.ToDisplay(), Value = x.Agent.Guid })
                           .ToList();
        }
    }
}