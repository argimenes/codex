﻿using Data;
using Data.Entities;
using Services;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CodexMVC.Models
{
    public class EditWorkedOn : ActModel
    {

        public EditWorkedOn()
        {

        }
        /// <summary>
        /// If <c>true</c> will populate the resource lists used for data entry.
        /// </summary>
        /// <param name="loadResources"></param>
        public EditWorkedOn(bool loadResources)
            : base(loadResources)
        {

        }
    }
    
}