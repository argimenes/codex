﻿using CodexMVC.Helpers;
using Data;
using Data.Entities;
using Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CodexMVC.Models
{
    public class ArtefactRelatedToArtefact
    {
        public Artefact Artefact { get; set; }
        public Relation Relation { get; set; }
        public Artefact OtherArtefact { get; set; }
    }
    public class RelationHyperNodeModel
    {
        public RelationModel RelationModel { get; set; }
    }
    public class AgentRelationHyperNodeModel : RelationHyperNodeModel
    {
        public string Action { get; set; }
        public string Title { get; set; }
        public AgentToAgentRelationHyperNode Node { get; set; }
        public bool IsCurrent { get; set; }
        public List<SelectListItem> Agents { get; set; }
        public AgentRelationHyperNodeModel()
        {
            Agents = RazorCache.AgentService.AllWithAttributes();
        }
    }
    public abstract class AbstractRelationModel
    {
        public string Action { get; set; }
        public string Title { get; set; }
        public string AgentGuid { get; set; }
        public string PlaceGuid { get; set; }
        public string CategoryGuid { get; set; }
        public string ArtefactGuid { get; set; }
        public string OtherArtefactGuid { get; set; }
        /**
         * Resources
         * */
        [ReadOnly(true)]
        public List<SelectListItem> Places { get; set; }

        [ReadOnly(true)]
        public List<SelectListItem> Agents { get; set; }

        [ReadOnly(true)]
        public List<SelectListItem> Artefacts { get; set; }

        [ReadOnly(true)]
        public List<SelectListItem> Categories { get; set; }

        public AbstractRelationModel()
        {
            var places = PlaceServiceCache.AllWithContainers();
            Places = places
                .OrderBy(x => x.Place.Name)
                .Select(x => new SelectListItem
                {
                    Text = x.ToDisplay(),
                    Value = x.Place.Guid
                })
                .ToList();

            var categories = CategoryServiceCache.AllWithAncestors();
            Categories = categories
                .OrderBy(x => x.Child.Name)
                .Select(x => new SelectListItem { Text = x.ToDisplay(), Value = x.Child.Guid })
                .ToList();

            var artefacts = ArtefactServiceCache.AllMadeBy();
            Artefacts = artefacts
                .OrderBy(x => x.Artefact.Name)
                .Select(x => new SelectListItem { Text = x.ToDisplay(), Value = x.Artefact.Guid })
                .ToList();

            var agents = AgentServiceCache.AllWithAttributes();
            Agents = agents
                .OrderBy(x => x.Agent.FullName)
                .Select(x => new SelectListItem { Text = x.ToDisplay(), Value = x.Agent.Guid })
                .ToList();

            var blank = new SelectListItem { };
            Agents.Insert(0, blank);
            Artefacts.Insert(0, blank);
            Categories.Insert(0, blank);
            Places.Insert(0, blank);
        }
    }
    public abstract class AbstractRelationHyperNodeModel : AbstractRelationModel
    {
        public string Context { get; set; }
        public Relation Relation { get; set; }
        public string DocumentGuid { get; set; }
        public string ImageGuid { get; set; }
        public Range Range { get; set; }
        public Proximity Proximity { get; set; }
        public Proximity ProximityEnd { get; set; }

        /**
         * Resources
         * */
        [ReadOnly(true)]
        public List<SelectListItem> Images { get; set; }

        [ReadOnly(true)]
        public List<SelectListItem> Proximities { get; set; }

        [ReadOnly(true)]
        public List<SelectListItem> Milennia { get; set; }

        [ReadOnly(true)]
        public List<SelectListItem> Months { get; set; }

        [ReadOnly(true)]
        public List<SelectListItem> Documents { get; set; }

        public AbstractRelationHyperNodeModel()
        {
            var blank = new SelectListItem { };

            var documents = ActServiceCache.AllDocumentHyperNodes();
            Documents = documents
                .Select(x => new SelectListItem { Text = x.SelectListItemText, Value = x.Document.Guid })
                .ToList();
            
            Documents.Insert(0, blank);

            Proximities = new List<SelectListItem>
            {
                blank,
                new SelectListItem { Text = "exactly", Value = "==" },
                new SelectListItem { Text = "around", Value = "c." },
                new SelectListItem { Text = "before", Value = "<" },
                new SelectListItem { Text = "after", Value = ">" },
                new SelectListItem { Text = "on or before", Value = ">=" },
                new SelectListItem { Text = "on or after", Value = "<=" },
            };

            Milennia = new List<SelectListItem>
            {
                blank,
                new SelectListItem { Text = "AD", Value = "AD"},
                new SelectListItem { Text = "BC", Value = "BC"}
            };

            Months = new List<SelectListItem>
            {
                blank,
                new SelectListItem { Text="01 January", Value = "1" },
                new SelectListItem { Text="02 February", Value = "2" },
                new SelectListItem { Text="03 March", Value = "3" },
                new SelectListItem { Text="04 April", Value = "4" },
                new SelectListItem { Text="05 May", Value = "5" },
                new SelectListItem { Text="06 June", Value = "6" },
                new SelectListItem { Text="07 July", Value = "7" },
                new SelectListItem { Text="08 August", Value = "8" },
                new SelectListItem { Text="09 September", Value = "9" },
                new SelectListItem { Text="10 October", Value = "10" },
                new SelectListItem { Text="11 November", Value = "11" },
                new SelectListItem { Text="12 December", Value = "12" },
                new SelectListItem { Text="------------", Value = "" },
                new SelectListItem { Text="   Spring", Value = "13" },
                new SelectListItem { Text="   Summer", Value = "14" },
                new SelectListItem { Text="   Autumn", Value = "15" },
                new SelectListItem { Text="   Winter", Value = "16" },
                new SelectListItem { Text="------------", Value = "" },
                new SelectListItem { Text="   Early", Value = "17" },
                new SelectListItem { Text="   Late", Value = "18" }

            };
        }
    }
    public class GenericRelationModel<TSource, TDest> : AbstractRelationHyperNodeModel
        where TSource : IEntity
        where TDest : IEntity
    {
        public GenericRelationHyperNode<TSource, TDest> Node { get; set; }
    }
    public class IEntityRelationModel : GenericRelationModel<IEntity, IEntity>
    {

    }
    public class GenericRelationNodeModel<TSource, TDest> : AbstractRelationModel where TSource : IEntity
        where TDest : IEntity
    {
        public GenericRelationNode<TSource, TDest> Node { get; set; }
    }
    public class GenericRelationModelDouble<TSource, TDest, TDest2> : GenericRelationModel<TSource, TDest>
        where TSource : IEntity
        where TDest : IEntity
        where TDest2 : IEntity
    {
        public new GenericRelationHyperNodeDouble<TSource, TDest, TDest2> Node { get; set; }
    }
    public class ArtefactToEntityRelationModel : GenericRelationModel<Artefact, Entity>
    {
        public string OtherArtefactGuid { get; set; }
    }
    public class ArtefactToAgentRelationModel : GenericRelationModel<Artefact, Agent>
    {
    }
    public class AgentToAgentRelationModel : GenericRelationModel<Agent, Agent>
    {
    }
    public class ArtefactToPlaceRelationModel : GenericRelationModel<Artefact, Place>
    {
    }
    public class AgentToPlaceRelationModel : GenericRelationModel<Agent, Place>
    {
    }
    public class ArtefactToCategoryRelationModel : GenericRelationModel<Artefact,Category>
    {
    }
    public class AgentToCategoryRelationModel : GenericRelationModel<Agent,Category>
    {
    }
    public class ArtefactToArtefactRelationModel : GenericRelationModel<Artefact,Artefact>
    {
    }
    public class AgentToArtefactRelationModel : GenericRelationModel<Agent,Artefact>
    {
       
    }
    public abstract class EditAgentRelation
    {
        public string ActGuid { get; set; }
        public Agent Agent { get; set; }
        public string EditActionName { get; set; }
        public string EditActionText { get; set; }
    }
}