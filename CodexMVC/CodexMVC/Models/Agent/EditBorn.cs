﻿using Data;
using Data.Entities;
using Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CodexMVC.Models
{
    public class RelationModel
    {
        public Relation Relation { get; set; }
        public bool IsCurrent { get; set; }
        public Agent Agent { get; set; }
        /**
         * Resources
         */
        public List<SelectListItem> Proximities { get; set; }
        public List<SelectListItem> Milennia { get; set; }
        public RelationModel()
        {
            var blank = new SelectListItem { };
            Proximities = new List<SelectListItem>
            {
                blank,
                new SelectListItem { Text = "exactly", Value = "==" },
                new SelectListItem { Text = "around", Value = "c." },
                new SelectListItem { Text = "before", Value = "<" },
                new SelectListItem { Text = "after", Value = ">" },
                new SelectListItem { Text = "on or before", Value = ">=" },
                new SelectListItem { Text = "on or after", Value = "<=" },
            };
            Milennia = new List<SelectListItem>
            {
                blank,
                new SelectListItem { Text = "AD", Value = "AD"},
                new SelectListItem { Text = "BC", Value = "BC"}
            };
        }
    }

    public class ActModel
    {
        public Act Act { get; set; }
        public Agent Agent { get; set; }
        /**
         * Resources
         */
        public List<SelectListItem> Proximities { get; set; }
        public List<SelectListItem> Milennia { get; set; }
        public ActModel()
        {

        }
        /// <summary>
        /// If <c>true</c> will populate the resource lists used for data entry.
        /// </summary>
        /// <param name="loadResources"></param>
        public ActModel(bool loadResources)
        {
            if (loadResources)
            {
                var blank = new SelectListItem { };
                Proximities = new List<SelectListItem>
            {
                blank,
                new SelectListItem { Text = "exactly", Value = "==" },
                new SelectListItem { Text = "around", Value = "c." },
                new SelectListItem { Text = "before", Value = "<" },
                new SelectListItem { Text = "after", Value = ">" },
                new SelectListItem { Text = "on or before", Value = ">=" },
                new SelectListItem { Text = "on or after", Value = "<=" },
            };
                Milennia = new List<SelectListItem>
            {
                blank,
                new SelectListItem { Text = "AD", Value = "AD"},
                new SelectListItem { Text = "BC", Value = "BC"}
            };
            }
        }
    }

    public class EditBorn : ActModel
    {
        public EditBorn()
        {

        }
        /// <summary>
        /// If <c>true</c> will populate the resource lists used for data entry.
        /// </summary>
        /// <param name="loadResources"></param>
        public EditBorn(bool loadResources)
            : base(loadResources)
        {

        }
    }

    public class EditDied : ActModel
    {
        public EditDied()
        {

        }
        /// <summary>
        /// If <c>true</c> will populate the resource lists used for data entry.
        /// </summary>
        /// <param name="loadResources"></param>
        public EditDied(bool loadResources)
            : base(loadResources)
        {

        }
    }
}