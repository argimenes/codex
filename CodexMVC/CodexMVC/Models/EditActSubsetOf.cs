﻿using Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CodexMVC.Models
{
    public class EditActSubsetOf
    {
        public Act Act { get; set; }

        public string SupersetGuid { get; set; }

    }
}