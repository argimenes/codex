﻿using CodexMVC.Helpers;
using Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CodexMVC.Models
{

    public enum QueryEnum
    {
        AllAgentsSharingSameAttributes
    }
    public class QueryModel
    {
        public string CitizenOfGuid { get; set; }
        public string RepresentativeOf { get; set; }
        public string AgentGuid { get; set; }
        public string ArtefactIsAGuid { get; set; }
        public string PlaceGuid { get; set; }
        public string Preposition { get; set; }

        public List<SelectListItem> Agents { get; set; }
        public List<SelectListItem> Categories { get; set; }
        public List<SelectListItem> Places { get; set; }

        public List<SelectListItem> Prepositions { get; set; }

        public List<Services.ArtefactsMadeByResult> Results { get; set; }

        public QueryModel()
        {
            var blank = new SelectListItem { };

            Agents = RazorCache.AgentService.AllWithAttributes();
            Agents.Insert(0, blank);

            Categories = RazorCache.CategoryService.AllWithAncestors();
            Categories.Insert(0, blank);

            Places = RazorCache.PlaceService.AllWithContainers();
            Places.Insert(0, blank);

            Prepositions = new List<SelectListItem>();
            Prepositions.Add(blank);
            Prepositions.Add(new SelectListItem { Text = "by", Value = "by" });
            Prepositions.Add(new SelectListItem { Text = "by agent", Value = "by agent" });
            Prepositions.Add(new SelectListItem { Text = "in", Value = "in" });


        }
    }
    public class SearchQuery
    {
        public QueryEnum? Query { get; set; }
        public Dictionary<QueryEnum, string> Queries = new Dictionary<QueryEnum, string>();
        public List<SelectListItem> QueriesList = new List<System.Web.Mvc.SelectListItem>();
        public SearchQuery()
        {
            Queries.Add(QueryEnum.AllAgentsSharingSameAttributes, "All agents share the same attributes");
            QueriesList = Queries.Select(x => new SelectListItem { Text = x.Value, Value = x.Key.ToString() }).ToList();
            QueriesList.Insert(0, new SelectListItem());
        }
    }
}