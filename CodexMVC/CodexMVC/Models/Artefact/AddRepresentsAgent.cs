﻿using Data.Entities;
using Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CodexMVC.Models
{
    public class AddRepresentsAgent
    {
        public Artefact Artefact { get; set; }

        public string AgentGuid { get; set; }

        public List<SelectListItem> Agents { get; set; }

        public AddRepresentsAgent()
        {
            var agentService = DependencyResolver.Current.GetService<IAgentService>();
            Agents = agentService.AllWithAttributes()
                .Select(x => new SelectListItem
                {
                    Value = x.Agent.Guid,
                    Text = x.ToDisplay()
                }).ToList();
        }

    }
}