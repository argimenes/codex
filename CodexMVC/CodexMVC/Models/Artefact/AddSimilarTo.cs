﻿using Data.Entities;
using Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CodexMVC.Models
{
    public class AddSimilarTo
    {
        public Artefact Artefact { get; set; }

        public string OtherArtefactGuid { get; set; }

        public List<SelectListItem> Artefacts { get; set; }

        public AddSimilarTo()
        {
            var artefactService = DependencyResolver.Current.GetService<IArtefactService>();
            Artefacts = artefactService.All()
                .Select(x => new System.Web.Mvc.SelectListItem { Text = x.Name, Value = x.Guid })
                .ToList();
        }
    }
}