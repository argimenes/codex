﻿using Data;
using Data.Entities;
using Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CodexMVC.Models
{
    public class ViewArtefactText
    {
        public Text Text { get; set; }
        public Artefact Artefact { get; set; }
        public List<Relation<Text, Agent>> Authors { get; set; }
        public Relation<Text, Artefact> Citation { get; set; }
        public List<Relation<Text, Category>> MentionsByCategory { get; set; }
        public List<Relation<Text, Agent>> MentionsByAgent { get; set; }
        public List<Relation<Text, Artefact>> MentionsByArtefact { get; set; }
        public List<Relation<Text, Place>> MentionsByPlace { get; set; }
    }
    
}