﻿using Data.Entities;
using Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CodexMVC.Models
{
    public class AddExhibitedIn
    {
        public Artefact Artefact { get; set; }

        public string ExhibitionGuid { get; set; }

        public List<SelectListItem> Exhibitions { get; set; }

        public AddExhibitedIn()
        {
            var eventService = DependencyResolver.Current.GetService<IEventService>();
            var events = eventService.FindEventsThatAre(Label.Categories.Exhibition);
            Exhibitions = events.Select(x => new System.Web.Mvc.SelectListItem { Text = x.Name, Value = x.Guid }).ToList();
        }
    }
}