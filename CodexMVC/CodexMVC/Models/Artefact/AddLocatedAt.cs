﻿
using CodexMVC.Helpers;
using Data.Entities;
using Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CodexMVC.Models
{
    public class AddLocatedIn
    {
        public Artefact Artefact { get; set; }

        public string BuildingGuid { get; set; }

        public List<SelectListItem> Buildings { get; set; }

        public AddLocatedIn()
        {
            var buildings = ArtefactServiceCache.AllMadeBy();
            Buildings = buildings.Select(x => new SelectListItem { Text = x.ToDisplay(), Value = x.Artefact.Guid })
                           .ToList();
        }
    }

    public class AddOntologicalStateOf
    {
        public Artefact Artefact { get; set; }

        public string CategoryGuid { get; set; }

        public List<SelectListItem> Categories { get; set; }

        public AddOntologicalStateOf()
        {
            var categories = CategoryServiceCache.AllSubcategoriesOfOntologicalState();
            Categories = categories.Select(x => new SelectListItem { Text = x.ToDisplay(), Value = x.Child.Guid })
                           .ToList();
        }
    }

    public class AddLocatedAt
    {
        public Artefact Artefact { get; set; }

        public string PlaceGuid { get; set; }

        public List<SelectListItem> Places { get; set; }

        public AddLocatedAt()
        {
            var places = PlaceServiceCache.AllWithContainers();
            Places = places.Select(x => new SelectListItem { Text = x.ToDisplay(), Value = x.Place.Guid })
                           .ToList();
        }
    }
}