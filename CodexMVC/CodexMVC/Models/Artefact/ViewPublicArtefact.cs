﻿using Data.Entities;
using Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CodexMVC.Models
{
    public class ViewPublicArtefact
    {
        public ArtefactPublicOld Details { get; set; }
        public GenericRelationHyperNode<Artefact, Data.Agent> MadeBy { get; set; }
    }
}