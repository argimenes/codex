﻿using CodexMVC.Helpers;
using Data;
using Data.Entities;
using Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CodexMVC.Models
{
    public class ViewAuditTrail
    {
        public User User { get; set; }
        public IEnumerable<AuditEntityTuple> Trail { get; set; }
    }

    public class ViewSeen
    {
        public User User { get; set; }
        public IEnumerable<ArtistArtefactImageTuple> Artefacts { get; set; }
    }

    public class ViewItinerary
    {
        public User User { get; set; }
        public IEnumerable<ItineraryWithDaysAndUsers> ItineraryItems { get; set; }
    }

    public class AddItineraryModel
    {
        public string Name { get; set; }
    }

    public class AssistantModel
    {
        public List<string> Style { get; set; }
        public List<string> Place { get; set; }
        public List<string> Date { get; set; }

        public List<SelectListItem> Styles { get; set; }
        public List<SelectListItem> Places { get; set; }
        public List<SelectListItem> Dates { get; set; }

        public AssistantModel()
        {
            Styles = RazorHelper.SelectListOfDescendants(Label.Categories.ArtMovement);
            Places = new List<SelectListItem>();
            Dates = new List<SelectListItem>();
        }
    }
    public class ViewFavourites
    {
        public User User { get; set; }
        public IEnumerable<NewAct> NewActs { get; set; }
        public IEnumerable<AgentWithAttribute> Agents { get; set; }
        public IEnumerable<ArtistArtefactImageTuple> Artefacts { get; set; }
        public IEnumerable<ArtistArtefactImageTuple> ArtefactsSeen { get; set; }
        public IEnumerable<ArtistArtefactImageTuple> Similar { get; set; }
        public IEnumerable<ArtistArtefactImageTuple> Recommended { get; set; }
    }
}