﻿using Data.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CodexMVC.Models
{
    public class EditArtefactText
    {
        public string TextGuid { get; set; }

        public Artefact Artefact { get; set; }

        public string Name { get; set; }

        public string Subtitle { get; set; }

        public string Value { get; set; }
    }
}