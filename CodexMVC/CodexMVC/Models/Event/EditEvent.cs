﻿using Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CodexMVC.Models
{
    public class EditEvent
    {
        public Event Event { get; set; }
        public List<SelectListItem> Proximities { get; set; }
        public List<SelectListItem> Milennia { get; set; }
        public EditEvent()
        {
            var blank = new SelectListItem { };
            Proximities = new List<SelectListItem>
            {
                blank,
                new SelectListItem { Text = "exactly", Value = "==" },
                new SelectListItem { Text = "around", Value = "c." },
                new SelectListItem { Text = "before", Value = "<" },
                new SelectListItem { Text = "after", Value = ">" },
                new SelectListItem { Text = "on or before", Value = ">=" },
                new SelectListItem { Text = "on or after", Value = "<=" },
            };
            Milennia = new List<SelectListItem>
            {
                blank,
                new SelectListItem { Text = "AD", Value = "AD"},
                new SelectListItem { Text = "BC", Value = "BC"}
            };
        }
    }
}