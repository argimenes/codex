﻿using Data.Entities;
using Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CodexMVC.Models
{
    public class ViewEvent
    {
        public Event Self { get; set; }
        public Event Parent { get; set; }
        public List<Event> Children { get; set; }
        public List<EventAttributeHyperNode> Attributes = new List<EventAttributeHyperNode>();
    }
}