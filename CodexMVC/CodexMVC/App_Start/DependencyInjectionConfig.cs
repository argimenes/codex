﻿using Autofac;
using Autofac.Core;
using Autofac.Integration.Mvc;
using Neo4jClient;
using Newtonsoft.Json.Serialization;
using Services;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

namespace CodexMVC.App_Start
{
    public class DependencyInjectionConfig
    {
        public static void CreateAndRegisterContainer()
        {
            var builder = GetContainerBuilder();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(builder.Build()));
        }

        private static ContainerBuilder GetContainerBuilder()
        {
            var builder = new ContainerBuilder();

            RegisterMVCControllersWithBuilder(builder);
            RegisterServicesAssemblyWithBuilder(builder);

            return builder;
        }

        private static void RegisterServicesAssemblyWithBuilder(ContainerBuilder builder)
        {
            var servicesAssembly = typeof(AgentService).Assembly;
            var serviceTypes = GetConcreteTypesFromAssembly(servicesAssembly);

            var neo4jEndpoint = (string)ConfigurationManager.AppSettings["Codex.Neo4j.ConnectionString"];
            var neo4jUser = (string)ConfigurationManager.AppSettings["Codex.Neo4j.User"];
            var neo4jPassword = (string)ConfigurationManager.AppSettings["Codex.Neo4j.Password"];

            //builder.RegisterType<GraphClient>()
            //    .UsingConstructor(typeof(Uri), typeof(string), typeof(string))
            //    .WithParameters(new List<Parameter>() { new PositionalParameter(0, new Uri(neo4jEndpoint)), new PositionalParameter(1, neo4jUser), new PositionalParameter(2, neo4jPassword) })
            //    .OnActivated(x =>
            //    {
            //        // Glimpse.Neo4jClient.Plugin.RegisterGraphClient(x.Instance);
            //        x.Instance.JsonContractResolver = new CamelCasePropertyNamesContractResolver();
            //        // x.Instance.JsonConverters.Add(new ComplexPropertyConverter());
            //        x.Instance.Connect();
            //    })
            //    .OnRelease(x => x.Dispose())
            //    //.Keyed<GraphClient>("Data")
            //    .InstancePerRequest();

            builder.Register(x =>
                {
                    var client = new GraphClient(new Uri(neo4jEndpoint), neo4jUser, neo4jPassword);
                    //client.JsonContractResolver = new CamelCasePropertyNamesContractResolver();
                    client.Connect();
                    return client;
                })
                // .OnRelease(x => x.Dispose())
                .As<GraphClient>()
                .InstancePerRequest();

            builder.RegisterTypes(serviceTypes)
                   .AsImplementedInterfaces();
        }

        private static void RegisterMVCControllersWithBuilder(ContainerBuilder builder)
        {
            var controllerAssembly = typeof(MvcApplication).Assembly;
            builder.RegisterControllers(controllerAssembly);
        }

        private static Type[] GetConcreteTypesFromAssembly(Assembly assembly)
        {
            var types = (from t in assembly.GetTypes()
                         where false == t.IsInterface
                         && false == t.IsAbstract
                         && t.GetInterface("I" + t.Name, false) != null
                         select t).ToArray();
            return types;
        }
    }
}