﻿using CodexMVC.Models;
using Data;
using Data.Entities;
using Newtonsoft.Json;
using Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace CodexMVC.Helpers
{
    public static class ExtensionMethods
    {
        public static string RelativePath(this HttpServerUtility srv, string path, HttpRequest context)
        {
            return path.Replace(context.ServerVariables["APPL_PHYSICAL_PATH"], "~/").Replace(@"\", "/");
        }
        public static string DasCapital(this string value)
        {
            if (string.IsNullOrWhiteSpace(value))
            {
                return value;
            }
            if (value.Length == 1)
            {
                return value.ToUpper();
            }
            value = value.Trim();
            return value.Substring(0, 1).ToUpper() + value.Substring(1);
        }
    }
    public static class RazorHelper
    {
        public static string And(IEnumerable<string> value)
        {
            var list = value.ToList();
            if (list ==null || list.Count() == 0)
            {
                return "";
            }
            if (list.Count() == 1)
            {
                return list.First();
            }
            if (list.Count() == 2)
            {
                return list[0] + " and " + list[1];
            }
            return string.Join(", ", list.Take(list.Count() - 1)) + " and " + list.Last();
        }

        public static IEnumerable<HeatMapJson> GetTimelineHeatMap(IEnumerable<IGrouping<string, MarkerJson>> markers)
        {
            var heatmap = markers
                .Select(x => new HeatMapJson
                {
                    Weight = x.Select(x2 => x2.Content).Count(),
                    Latitude = x.First().Latitude,
                    Longitude = x.First().Longitude
                });
            return heatmap;
        }

        public static IEnumerable<IGrouping<string, MarkerJson>> GetTimelineMarkers(IEnumerable<NewAct> timeline)
        {
            var geoActs = new List<NewAct>();

            foreach (var item in timeline)
            {
                if (item.EntityImages.Any(x => x.Entity is IGeo))
                {
                    geoActs.Add(item);
                }
            }

            var markdown = new MarkdownDeep.Markdown();

            var markers = geoActs
                .Select(x =>
                {
                    var geo = x.EntityImages.FirstOrDefault(x2 => x2.Entity is IGeo).Entity as IGeo;
                    var result = new
                    {
                        Latitude = geo.Latitude,
                        Longitude = geo.Longitude,
                        Content = "<b>" + FactExtensions.ToDisplay(x.Act).DasCapital() + "</b>, " + RemoveOuterTag("p", markdown.Transform(x.ToHtml())),
                        Act = x,
                        ContainerName = "<h4>" + geo.Name + "</h4>"
                    };
                    return result;
                })
                .Where(x => x.Latitude.HasValue && x.Longitude.HasValue)
                .OrderBy(x => x.Act.Act.Year)
                .Select(x => new MarkerJson
                {
                    Latitude = x.Latitude.Value,
                    Longitude = x.Longitude.Value,
                    Content = x.Content,
                    ContainerName = x.ContainerName
                })
                .GroupBy(x => x.Latitude.ToString() + ":" + x.Longitude.ToString())
                ;

            return markers;
        }

        public static string RemoveOuterTag(string tag, string value)
        {
            var result = Regex.Match(value, "^<" + tag + ">(.*)</" + tag + ">").Groups[1].Value;
            return result;
        }

        public static IEnumerable<HeatMapJson> GetArtworksHeatMap(List<IGrouping<string, ArtworkMarkerJson>> markers)
        {
            var heatmap = markers
                .Select(x => new HeatMapJson
                {
                    Weight = x.First().Ratings.Sum(),
                    Latitude = x.First().Latitude,
                    Longitude = x.First().Longitude
                });
            return heatmap;
        }

        public static List<IGrouping<string,ArtworkMarkerJson>> GetArtworkMarkers(IEnumerable<ContainerWithArtefacts> containerWithArtefacts)
        {
            var artworks = containerWithArtefacts
                .SelectMany(x => x.Artworks.Select(x2 =>
            {
                var container = x.TopContainer != null ? x.TopContainer : x.Container;
                var lat = container != null && container.Latitude.HasValue ? container.Latitude.Value : x.Location.Latitude.Value;
                var lng = container != null && container.Longitude.HasValue ? container.Longitude.Value : x.Location.Longitude.Value;
                //var stars = string.Join("", Enumerable.Range(1, (x2.Artefact.Rating ?? 1)).Select(z => "<span class='glyphicon glyphicon-star' style='color: gold;'></span>"));
                return new Services.ArtefactMarker
                {
                    Artist = x2.Artist,
                    Artwork = x2.Artefact,
                    Latitude = lat,
                    Longitude = lng,
                    Content = "<a href=\"/Artefact/ViewDetail/" + x2.Artefact.Guid + "\">" + x2.Artefact.ToDisplay() + "</a> ",
                    ContainerName = container.ToDisplay().Replace("\"", "'") + " <a href='/Artefact/ViewDetail/" + container.Guid + "' class='btn btn-default btn-sm'>see all</a> <a href='//maps.google.com/maps?daddr=" + lat + "," + lng + "' class='btn btn-default btn-sm'>map</a>"
                };
            }))
            .Distinct(new Services.ArtefactMarkerComparer())
            .GroupBy(x => x.Latitude.ToString() + ":" + x.Longitude.ToString())
            .Select(x => new ArtworkMarkerJson
            {
                Latitude = x.First().Latitude,
                Longitude = x.First().Longitude,
                ContainerName = x.First().ContainerName,
                Content = string.Join("<br/>", x.Select(x2 => x2.Content)),
                Ratings = x.Select(x2 => (x2.Artist.Rating ?? 1) * (x2.Artwork.Rating ?? 1))
            })
            .GroupBy(x => x.Latitude.ToString() + ":" + x.Longitude.ToString())
            .ToList();

            //var markers = artworks
            //    .GroupBy(x => x.Latitude.ToString() + ":" + x.Longitude.ToString())
            //    .Select(x => new ArtworkMarkerJson
            //    {
            //        Latitude = x.First().Latitude,
            //        Longitude = x.First().Longitude,
            //        ContainerName = x.First().ContainerName,
            //        Content = x.First().Content,
            //        Rating = x.Sum(x2 => (x2.Artist.Rating ?? 0) * (x2.Artwork.Rating ?? 0))
            //        /**
            //         * , Artworks = x.Select(x2 => new {
            //            Guid = x2.Artwork.Guid,
            //            Name = x2.Artwork.Name,
            //            Rating = x2.Artwork.Rating
            //        }),
            //         */
            //    })
            //    .ToList();

            return artworks;
        }

        public static List<SelectListItem> SelectListOfDescendants(string descendantsOf)
        {
            var categories = DependencyResolver.Current.GetService<IAgentService>().AllSubcategoriesOf(descendantsOf);
            return categories
                .OrderBy(x => x.Child.Name)
                .Select(x => new SelectListItem { Text = x.ToDisplay(), Value = x.Child.Guid })
                .ToList();
        }
        public static List<SelectListItem> SelectListOfAgentsByType(string agentTypeName)
        {
            var collections = DependencyResolver.Current.GetService<IAgentService>().AllAgentsByType(agentTypeName);
            return collections
                .OrderBy(x => x.Agent.FullName)
                .Select(x => new SelectListItem { Text = x.ToDisplay(), Value = x.Agent.Guid })
                .ToList();
        }
        public static List<SelectListItem> SelectListOfArtefactsByType(string artefactTypeName)
        {
            var collections = DependencyResolver.Current.GetService<IAgentService>().AllArtefactsByType(artefactTypeName);
            return collections
                .OrderBy(x => x.Artefact.Name)
                .Select(x => new SelectListItem { Text = x.ToDisplay(), Value = x.Artefact.Guid })
                .ToList();
        }
        public static void AddToHistory(History item)
        {
            var history = GetHistory();
            if (history.Count() == 100)
            {
                history.RemoveRange(49, 50);
            }
            history.RemoveAll(x => x.EntityGuid == item.EntityGuid && x.EntityType == item.EntityType);
            history.Add(item);
        }
        public static List<History> GetHistory()
        {
            var history = HttpContext.Current.Session["History"] as List<History>;
            if (history == null)
            {
                history = new List<History>();
                HttpContext.Current.Session["History"] = history;
            }
            return history;
        }

        public static IEnumerable<string[]> GetContexts(AbstractRelationHyperNodeModel model)
        {
            if (model.Context == null || model.Context == "null")
            {
                return null;
            }
            var json = JsonConvert.DeserializeObject<dynamic[]>(model.Context);
            return json.Select((dynamic x) => new string[] { (string)x.Label, (string)x.Guid });
        }
        public static IEnumerable<string> GetPairs(string input)
        {
            if (input == null)
            {
                return new List<string> { };
            }

            var regex = new Regex(@"\[.*?\]");
            var matches = regex.Matches(input);
            return matches.Cast<Match>().Select(x => x.Value.Replace("[", "").Replace("]", ""));
        }
        public static MatchCollection GetMatches(string input)
        {
            var regex = new Regex(@"\[.*?\]");
            var matches = regex.Matches(input);
            return matches;
        }
        public static string FromRange(Range scope)
        {
            switch (scope)
            {
                case Range.Between: return ">-<";
                case Range.Continuous: return "-->";
                default: return "-->";
            }
        }
        public static Range ToRange(string scope)
        {
            switch (scope)
            {
                case ">-<": return Range.Between;
                case "-->": return Range.Continuous;
                default: return Range.Continuous;
            }
        }
        public static string FromProximity(Proximity proximity)
        {
            switch (proximity)
            {
                case Proximity.On: return "==";
                case Proximity.Around: return "c.";
                case Proximity.Before: return "<";
                case Proximity.After: return ">";
                case Proximity.OnOrAfter: return ">=";
                case Proximity.OnOrBefore: return "<=";
                default: return "==";
            }
        }
        public static Proximity ToProximity(string proximity)
        {
            switch (proximity)
            {
                case "==": return Proximity.On;
                case "c.": return Proximity.Around;
                case "<": return Proximity.Before;
                case ">": return Proximity.After;
                case "<=": return Proximity.OnOrBefore;
                case ">=": return Proximity.OnOrAfter;
                default: return Proximity.On;
            }
        }
        public static string Concat(IEnumerable<Category> list, bool prefixWithArticle = false)
        {
            if (list.Count() == 1)
            {
                return list.First().Name.ToLower();
            }
            var results = list.Select(x => x.Name.ToLower());
            var firstLetter = results.First().Substring(0, 1).ToLower();
            var vowels = new[] { "a", "e", "i", "o", "u" };
            var prefix = "a";
            if (vowels.Contains(firstLetter))
            {
                prefix = "an";
            }
            if (false == prefixWithArticle)
            {
                prefix = "";
            }
            return prefix + " " + string.Join(", ", results.Take(results.Count() - 1).ToArray()) + " and " + results.Last();
        }
        public static string NameOf(IEntity entity)
        {
            if (entity is Data.Agent)
            {
                return ((Data.Agent)entity).FullName2;
            }
            if (entity is Artefact)
            {
                return ((Artefact)entity).ToDisplay();
            }
            return entity.Name;
        }
        public static string ToShortDates(IDateRange start, IDateRange end)
        {
            if (start == null && end == null)
            {
                return "";
            }
            var results = new List<string>();
            var startDate = start != null && start.Year.HasValue ? start.Year.ToString() : "?";
            var endDate = end != null && end.Year.HasValue ? end.Year.ToString() : "?";
            return string.Format("({0}-{1})", startDate, endDate);
        }
        public static string ToHtml(Agent agent)
        {
            return string.Format("<a href='/Agent/ViewDetail/{0}'>{1}</a>", agent.Guid, agent.FullName2);
        }
        public static string ToHtml(Place place)
        {
            return string.Format("<a href='/Place/ViewPublic?placeGuid={0}'>{1}</a>", place.Guid, place.Name);
        }
        //public static string ToHtml(IActHyperNode node)
        //{
        //    if (node is MetActHyperNode)
        //    {
        //        return ToHtml((MetActHyperNode)node);
        //    }
        //    if (node is LivedAtHyperNode)
        //    {
        //        return ToHtml((LivedAtHyperNode)node);
        //    }
        //    if (node is DiedActHyperNode)
        //    {
        //        return ToHtml((DiedActHyperNode)node);
        //    }
        //    if (node is BornActHyperNode)
        //    {
        //        return ToHtml((BornActHyperNode)node);
        //    }
        //    if (node is CreatedHyperNode)
        //    {
        //        return ToHtml((CreatedHyperNode)node);
        //    }
        //    if (node is TravelledHyperNode)
        //    {
        //        return ToHtml((TravelledHyperNode)node);
        //    }
        //    return string.Empty;
        //}
        //public static string ToHtml(MetActHyperNode act)
        //{
        //    var results = new List<string>();
        //    results.Add("<a href=\"/Agent/ViewPublic?agentGuid=" + act.Agent.Guid + "\">" + act.Agent.FullName2 + "</a> met");
        //    if (act.Other != null)
        //    {
        //        results.Add("with <a href=\"/Agent/ViewPublic?agentGuid=" + act.Other.Guid + "\">" + act.Other.FullName2 + "</a>");
        //    }
        //    if (act.Place != null)
        //    {
        //        results.Add("in <a href=\"/Place/ViewPublic?placeGuid=" + act.Place.Guid + "\">" + act.Place.ToDisplay() + "</a>");
        //    }
        //    results.Add(act.Act.ToDisplay());
        //    return string.Join(" ", results.ToArray());
        //}
        //public static string ToHtml(LivedAtHyperNode act)
        //{
        //    var results = new List<string>();
        //    results.Add("<a href=\"/Agent/ViewPublic?agentGuid=" + act.Agent.Guid + "\">" + act.Agent.FullName2 + "</a> lived");
        //    if (act.Place != null)
        //    {
        //        results.Add("in <a href=\"/Place/ViewPublic?placeGuid=" + act.Place.Guid + "\">" + act.Place.ToDisplay() + "</a>");
        //    }
        //    results.Add(act.Act.ToDisplay());
        //    return string.Join(" ", results.ToArray());
        //}
        //public static string ToHtml(DiedActHyperNode act)
        //{
        //    var results = new List<string>();
        //    results.Add("<a href=\"/Agent/ViewPublic?agentGuid=" + act.Agent.Guid + "\">" + act.Agent.FullName2 + "</a> died");
        //    if (act.Place != null)
        //    {
        //        results.Add("in <a href=\"/Place/ViewPublic?placeGuid=" + act.Place.Guid + "\">" + act.Place.ToDisplay() + "</a>");
        //    }
        //    results.Add(act.Act.ToDisplay());
        //    return string.Join(" ", results.ToArray());
        //}
        //public static string ToHtml(BornActHyperNode act)
        //{
        //    var results = new List<string>();
        //    results.Add("<a href=\"/Agent/ViewPublic?agentGuid=" + act.Agent.Guid + "\">" + act.Agent.FullName2 + "</a> was born");
        //    if (act.Place != null)
        //    {
        //        results.Add("in <a href=\"/Place/ViewPublic?placeGuid=" + act.Place.Guid + "\">" + act.Place.ToDisplay() + "</a>");
        //    }
        //    results.Add(act.Act.ToDisplay());
        //    return string.Join(" ", results.ToArray());
        //}
        //public static string ToHtml(CreatedHyperNode act)
        //{
        //    var results = new List<string>();
        //    results.Add("<a href=\"/Agent/ViewPublic?agentGuid=" + act.Agent.Guid + "\">" + act.Agent.FullName2 + "</a> worked");
        //    if (act.Artwork != null)
        //    {
        //        results.Add("on <a href=\"/Artefact/ViewPublic?guid=" + act.Artwork.Guid + "\">" + act.Artwork.ToDisplay() + "</a>");
        //    }
        //    if (act.Place != null)
        //    {
        //        results.Add("in <a href=\"/Place/ViewPublic?placeGuid=" + act.Place.Guid + "\">" + act.Place.ToDisplay() + "</a>");
        //    }
        //    if (act.Patron != null)
        //    {
        //        results.Add("for <a href=\"/Agent/ViewPublic?agentGuid=" + act.Patron.Guid + "\">" + act.Patron.FullName2 + "</a>");
        //    }
        //    results.Add(act.Act.ToDisplay());
        //    return string.Join(" ", results.ToArray());
        //}
        //public static string ToHtml(TravelledHyperNode act)
        //{
        //    var results = new List<string>();
        //    results.Add("<a href=\"/Agent/ViewPublic?agentGuid=" + act.Agent.Guid + "\">" + act.Agent.FullName2 + "</a> travelled");
        //    if (act.From != null)
        //    {
        //        results.Add("from <a href=\"/Place/ViewPublic?placeGuid=" + act.From.Guid + "\">" + act.From.ToDisplay() + "</a>");
        //    }
        //    if (act.To != null)
        //    {
        //        results.Add("to <a href=\"/Place/ViewPublic?placeGuid=" + act.To.Guid + "\">" + act.To.ToDisplay() + "</a>");
        //    }
        //    results.Add(act.Act.ToDisplay());
        //    return string.Join(" ", results.ToArray());
        //}
    }
}