﻿using Data.Entities;
using Services;
using Services.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;
using System.Web;
using System.Web.Mvc;

namespace CodexMVC.Helpers
{
    public static class SessionCacheManager
    {

    }
    public static class Constants
    {
        public const int DefaultOutputCacheDuration = 300;
    }
    public static class PlaceServiceCache
    {
        private static T GetService<T>() where T : class
        {
            return DependencyResolver.Current.GetService<T>();
        }
        public static List<PlaceResult> AllWithContainers()
        {
            return Cache.GetFromOrAddToCache<List<PlaceResult>>(
                CacheKey.PlaceService.AllWithContainers,
                () => GetService<IPlaceService>().AllWithContainers()
                    .OrderBy(x => x.Place.Name)
                    .ToList());
        }
    }
    public static class ActServiceCache
    {
        private static T GetService<T>() where T : class
        {
            return DependencyResolver.Current.GetService<T>();
        }
        public static List<DocumentHyperNode> AllDocumentHyperNodes()
        {
            return Cache.GetFromOrAddToCache<List<DocumentHyperNode>>(
                CacheKey.ActService.AllDocumentHyperNodes,
                () => GetService<IActService>().AllDocumentHyperNodes()
                    .OrderBy(x => x.SelectListItemText)
                    .ToList());
        }
    }
    public static class AgentServiceCache
    {
        private static T GetService<T>() where T : class
        {
            return DependencyResolver.Current.GetService<T>();
        }
        public static List<AgentWithAttribute> AllWithAttributes()
        {
            return Cache.GetFromOrAddToCache<List<AgentWithAttribute>>(
                CacheKey.AgentService.AllWithAttributes,
                () => GetService<IAgentService>().AllWithAttributes()
                        .OrderBy(x => x.Agent.FullName)
                        .ToList());
        }

    }
    public static class ArtefactServiceCache
    {
        private static T GetService<T>() where T : class
        {
            return DependencyResolver.Current.GetService<T>();
        }
        public static List<ArtefactMadeByAgent> AllSources()
        {
            return Cache.GetFromOrAddToCache<List<ArtefactMadeByAgent>>(
                CacheKey.ArtefactService.AllSources,
                () => GetService<IArtefactService>().AllSources()
                    .OrderBy(x => x.Artefact.Name)
                    .ToList());
        }
        public static List<ArtefactMadeByAgent> AllMadeBy()
        {
            return Cache.GetFromOrAddToCache<List<ArtefactMadeByAgent>>(
                CacheKey.ArtefactService.AllMadeBy,
                () => GetService<IArtefactService>().AllMadeBy()
                    .OrderBy(x => x.Artefact.Name)
                    .ToList());
        }
    }
    public static class CategoryServiceCache 
    {
        private static T GetService<T>() where T : class
        {
            return DependencyResolver.Current.GetService<T>();
        }
        public static List<CategoryResult> AllWithAncestors()
        {
            return Cache.GetFromOrAddToCache<List<CategoryResult>>(
                CacheKey.CategoryService.AllWithAncestors,
                () => GetService<ICategoryService>().AllWithAncestors()
                        .OrderBy(x => x.Child.Name)
                        .ToList());
        }
        public static List<CategoryResult> AllSubcategoriesOfFamilialRelation()
        {
            return Cache.GetFromOrAddToCache<List<CategoryResult>>(
                CacheKey.CategoryService.AllSubcategoriesOfFamilialRelation,
                () => GetService<IAgentService>().AllSubcategoriesOf(Label.Categories.FamilialRelation)
                        .OrderBy(x => x.Child.Name)
                        .ToList());
        }
        public static List<CategoryResult> AllSubcategoriesOfOntologicalState()
        {
            return Cache.GetFromOrAddToCache<List<CategoryResult>>(
                CacheKey.CategoryService.AllSubcategoriesOfFamilialRelation,
                () => GetService<IAgentService>().AllSubcategoriesOf(Label.Categories.OntologicalState)
                        .OrderBy(x => x.Child.Name)
                        .ToList());
        }
        public static List<CategoryResult> AllSubcategoriesOfArtisticSubject()
        {
            return Cache.GetFromOrAddToCache<List<CategoryResult>>(
                CacheKey.CategoryService.AllSubcategoriesOfArtisticSubject,
                () => GetService<IAgentService>().AllSubcategoriesOf(Label.Categories.ArtisticSubject)
                        .OrderBy(x => x.Child.Name)
                        .ToList());
        }
        public static List<CategoryResult> AllSubcategoriesOfArtGenre()
        {
            return Cache.GetFromOrAddToCache<List<CategoryResult>>(
                CacheKey.CategoryService.AllSubcategoriesOfArtGenre,
                () => GetService<IAgentService>().AllSubcategoriesOf(Label.Categories.ArtisticGenre)
                        .OrderBy(x => x.Child.Name)
                        .ToList());
        }
        public static List<CategoryResult> AllSubcategoriesOfArtMovement()
        {
            return Cache.GetFromOrAddToCache<List<CategoryResult>>(
                CacheKey.CategoryService.AllSubcategoriesOfArtMovement,
                () => GetService<IAgentService>().AllSubcategoriesOf(Label.Categories.ArtMovement)
                        .OrderBy(x => x.Child.Name)
                        .ToList());
        }
        public static List<CategoryResult> AllSubcategoriesOfArtwork()
        {
            return Cache.GetFromOrAddToCache<List<CategoryResult>>(
                CacheKey.CategoryService.AllSubcategoriesOfArtwork,
                () => GetService<IAgentService>().AllSubcategoriesOf(Label.Categories.Creation)
                        .OrderBy(x => x.Child.Name)
                        .ToList());
        }
        public static IEnumerable<Category> Disciplines()
        {
            return Cache.GetFromOrAddToCache<IEnumerable<Category>>(
                CacheKey.CategoryService.Disciplines,
                () => GetService<IAgentService>().Disciplines()
                        .OrderBy(x => x.Name));
        }
    }
    public static class RazorCache
    {
        public static void AddToCache<T>(string key, T value)
        {
            MemoryCache.Default.Add(key, value, null);
        }
        public static T GetFromCache<T>(string key)
        {
            return (T)MemoryCache.Default.Get(key);
        }
        private static T GetService<T>() where T : class
        {
            return DependencyResolver.Current.GetService<T>();
        }
        public static T GetFromOrAddToCache<T>(string key, Func<T> getter)
        {
            if (GetFromCache<T>(key) == null)
            {
                AddToCache(key, getter());
            }
            return GetFromCache<T>(key);
        }
        public static class ResourceList
        {
            public static List<SelectListItem> CatalogueIdentPrefixes()
            {
                return new List<SelectListItem>
                {
                    new SelectListItem {},
                    new SelectListItem { Text = "Op", Value = "Op" },
                    new SelectListItem { Text = "K", Value = "K" },
                    new SelectListItem { Text = "BWV", Value = "BWV" },
                    new SelectListItem { Text = "H", Value = "H" },
                };
            }
        }
        public static class CategoryService
        {
            public static List<SelectListItem> AllWithAncestors()
            {
                return CategoryServiceCache.AllWithAncestors()
                            .Select(x => new SelectListItem { Text = x.ToDisplay(), Value = x.Child.Guid })
                            .ToList();
            }
            public static List<SelectListItem> AllSubcategoriesOfFamilialRelation()
            {
                return CategoryServiceCache.AllSubcategoriesOfFamilialRelation()
                            .Select(x => new SelectListItem { Text = x.Child.Name, Value = x.Child.Guid })
                            .ToList();
            }
            public static List<SelectListItem> AllSubcategoriesOfArtisticSubject()
            {
                return CategoryServiceCache.AllSubcategoriesOfArtisticSubject()
                            .Select(x => new SelectListItem { Text = x.Child.Name, Value = x.Child.Guid })
                            .ToList();
            }
            public static List<SelectListItem> AllSubcategoriesOfArtGenre()
            {
                return CategoryServiceCache.AllSubcategoriesOfArtGenre()
                            .Select(x => new SelectListItem { Text = x.Child.Name, Value = x.Child.Guid })
                            .ToList();
            }
            public static List<SelectListItem> AllSubcategoriesOfArtMovement()
            {
                return CategoryServiceCache.AllSubcategoriesOfArtMovement()
                            .Select(x => new SelectListItem { Text = x.Child.Name, Value = x.Child.Guid })
                            .ToList();
            }
            public static List<SelectListItem> AllSubcategoriesOfArtwork()
            {
                return CategoryServiceCache.AllSubcategoriesOfArtwork()
                            .Select(x => new SelectListItem { Text = x.Child.Name, Value = x.Child.Guid })
                            .ToList();
            }
            public static List<SelectListItem> Disciplines()
            {
                return CategoryServiceCache.Disciplines()
                            .Select(x => new SelectListItem { Text = x.Name, Value = x.Guid })
                            .ToList();
            }
        }
        public static class AgentService
        {
            public static List<SelectListItem> AllWithAttributes()
            {
                return AgentServiceCache.AllWithAttributes()
                            .Select(x => new SelectListItem { Text = x.ToDisplay(), Value = x.Agent.Guid })
                            .ToList();
            }

        }
        public static class ArtefactService
        {
            public static List<SelectListItem> AllMadeBy()
            {
                return ArtefactServiceCache.AllMadeBy()
                            .Select(x => new SelectListItem { Text = x.ToDisplay(), Value = x.Artefact.Guid })
                            .ToList();
            }
        }
        public static class PlaceService
        {
            public static List<SelectListItem> AllWithContainers()
            {
                return PlaceServiceCache.AllWithContainers()
                            .Select(x => new SelectListItem { Text = x.ToDisplay(), Value = x.Place.Guid })
                            .ToList();
            }
        }
    }
}