﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CodexMVC.Helpers
{
    /// <summary>
    /// simplified moonphases
    /// </summary>
    public enum MoonPhases
    {
        /// <summary>
        /// Newmoon phase.
        /// </summary>
        NewMooon = 0,
        /// <summary>
        /// Waxing crescent moon phase.
        /// </summary>
        WaxingCrescent = 1,
        /// <summary>
        /// First quarter phase.
        /// </summary>
        FirstQuarter = 2,
        /// <summary>
        /// Waxing gibbous moon phase.
        /// </summary>
        WaxingGibbous = 3,
        /// <summary>
        /// Fullmoon phase.
        /// </summary>
        FullMoon = 4,
        /// <summary>
        /// Waning gibbous moon phase.
        /// </summary>
        WaningGibbous = 5,
        /// <summary>
        /// Last quarter phase.
        /// </summary>
        LastQuarter = 6,
        /// <summary>
        /// Waning crescent moon phase.
        /// </summary>
        WaningCrescent = 7
    }

    /// <summary>
    /// Represents the simplified moonphases of a given date.
    /// </summary>
    public class Moon
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Moon"/> class.
        /// </summary>
        /// <param name="year">The year.</param>
        /// <param name="day">The day.</param>
        /// <param name="hour">The hour.</param>
        public static string Phase(int year, int month, int day)
        {
            var P2 = 3.14159 * 2;
            var YY = year - (Int32)((12 - month) / 10);
            var MM = month + 9;
            if (MM >= 12) { MM = MM - 12; }
            var K1 = (Int32)(365.25 * (YY + 4712));
            var K2 = (Int32)(30.6 * MM + .5);
            var K3 = (Int32)((Int32)((YY / 100) + 49) * .75) - 38;
            var J = K1 + K2 + day + 59;
            if (J > 2299160) { J = J - K3; }
            var V = (J - 2451550.1) / 29.530588853;
            V = V - (Int32)(V);
            if (V < 0) { V = V + 1; }
            var AG = V * 29.53;
            string phase;
            if ((AG > 27.6849270496875) || (AG <= 1.8456618033125))
            {
                phase = "a new moon";
            }
            else if ((AG > 1.8456618033125) && (AG <= 5.5369854099375))
            {
                phase = "waxing crescent";
            }
            else if ((AG > 5.5369854099375) && (AG <= 9.2283090165625))
            {
                phase = "in the first quarter";
            }
            else if ((AG > 9.2283090165625) && (AG <= 12.9196326231875))
            {
                phase = "waxing gibbous";
            }
            else if ((AG > 12.9196326231875) && (AG <= 16.6109562298125))
            {
                phase = "a full moon";
            }
            else if ((AG > 16.6109562298125) && (AG <= 20.3022798364375))
            {
                phase = "waning gibbous";
            }
            else if ((AG > 20.3022798364375) && (AG <= 23.9936034430625))
            {
                phase = "in the last quarter";
            }
            else if ((AG > 23.9936034430625) && (AG <= 27.6849270496875))
            {
                phase = "waning crescent";
            }
            else
            {
                phase = "a full moon";
            }
            return phase;
        }
    }

    /// <summary>
    /// A class that provides sun informations of a given day on a given location
    /// represented by latitude and longtitude.
    /// </summary>
    public class Sun
    {
        #region ctor

        /// <summary>
        /// Initializes a new instance of the <see cref="Sun"/> class.
        /// </summary>
        /// <param name="Latitude">The latitude.</param>
        /// <param name="Longtitude">The longtitude.</param>
        /// <param name="year">The year.</param>
        /// <param name="month">The month.</param>
        /// <param name="day">The day.</param>
        public Sun(double Latitude, double Longtitude, int year, int month, int day)
        {
            var PI = Math.PI;
            var DR = PI / 180;
            var RD = 1 / DR;
            var B5 = Latitude;
            var L5 = Longtitude;
            var H = 0;    // timezone UTC
            var Now = DateTime.Now;
            var M = month;
            var D = day;
            B5 = DR * B5;
            var N = (Int32)(275 * M / 9) - 2 * (Int32)((M + 9) / 12) + D - 30;
            var L0 = 4.8771 + .0172 * (N + .5 - L5 / 360);
            var C = .03342 * Math.Sin(L0 + 1.345);
            var C2 = RD * (Math.Atan(Math.Tan(L0 + C)) - Math.Atan(.9175 * Math.Tan(L0 + C)) - C);
            var SD = .3978 * Math.Sin(L0 + C);
            var CD = Math.Sqrt(1 - SD * SD);
            var SC = (SD * Math.Sin(B5) + .0145) / (Math.Cos(B5) * CD);

            if (Math.Abs(SC) <= 1)
            {
                // calculate sunrise 
                var C3 = RD * Math.Atan(SC / Math.Sqrt(1 - SC * SC));
                var R1 = 6 - H - (L5 + C2 + C3) / 15;
                var HR = (Int32)(R1);
                var MR = (Int32)((R1 - HR) * 60);
                Sunrise = new DateTime(year, month, day, HR, MR, 0);
                // calculate sunset
                var S1 = 18 - H - (L5 + C2 - C3) / 15;
                var HS = (Int32)(S1);
                var MS = (Int32)((S1 - HS) * 60);
                Sunset = new DateTime(year, month, day, HS, MS, 0);
            }
            else
            {
                if (SC > 1)
                {
                    // sun is up all day ...
                    // Set Sunset to be in the future ...
                    Sunset = new DateTime(Now.Year + 1, Now.Month, Now.Day, Now.Hour, Now.Minute, Now.Second);
                    // Set Sunrise to be in the past ...
                    Sunrise = new DateTime(Now.Year - 1, Now.Month, Now.Day, Now.Hour, Now.Minute - 1, Now.Second);
                }
                if (SC < -1)
                {
                    // sun is down all day ...
                    // Set Sunrise and Sunset to be in the future ...
                    Sunrise = new DateTime(Now.Year + 1, Now.Month, Now.Day, Now.Hour, Now.Minute, Now.Second);
                    Sunset = new DateTime(Now.Year + 1, Now.Month, Now.Day, Now.Hour, Now.Minute, Now.Second);
                }
            }
        }

        #endregion
    
        #region fields

        /// <summary>
        /// DateTime representation of the sunrise-timestamp of a given day on a given location.
        /// </summary>
        public DateTime Sunrise;

        /// <summary>
        /// DateTime representation of the sunset-timestamp of a given day on a given location.
        /// </summary>
        public DateTime Sunset;


        #endregion
    }
}