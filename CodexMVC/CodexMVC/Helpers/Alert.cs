﻿using Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CodexMVC.Helpers
{
    public class Alert
    {
        public ResultStatus Status { get; set; }
        public string Message { get; set; }
        public Exception Exception { get; set; }
    }
}