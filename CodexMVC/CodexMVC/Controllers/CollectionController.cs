﻿using CodexMVC.Helpers;
using CodexMVC.Models;
using Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CodexMVC.Controllers
{
    public class CollectionController : BaseController
    {
        private readonly IArtefactService artefactService;
        public CollectionController(
            IArtefactService _artefactService
            )
        {
            artefactService = _artefactService;
        }

        [HttpGet]
        //[OutputCache(Duration = Constants.DefaultOutputCacheDuration, VaryByParam = "mode")]
        public ActionResult List(string mode = null)
        {
            var collections = artefactService.AllCollectionsThatHaveArtworks();
            var model = new ViewCollectionList
            {
                Collections = collections
            };
            if (mode == null || mode == CollectionListMode.ByCity.ToString())
            {
                model.Mode = CollectionListMode.ByCity;
            }
            else if (mode == CollectionListMode.ByType.ToString())
            {
                model.Mode = CollectionListMode.ByType;
            }
            return View(model);
        }
    }
}