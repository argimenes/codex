﻿using CodexMVC.Models;
using Services;
using Services.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CodexMVC.Controllers
{
    [Authorize(Roles = Permissions.AnyPublicAction)]
    public class ItineraryController : BaseController
    {
        [HttpGet]
        public ActionResult Add()
        {
            return View();
        }

        public DateTimeOffset? ParseDate(string value)
        {
            var date = DateTime.MinValue;
            var parsed = DateTime.TryParse(value, out date);
            return parsed ? date : (DateTimeOffset?)null;
        }

        [HttpGet]
        public JsonResult ModalAddToNew(string name, string artefactGuid, string from, string to, int? duration)
        {
            var itinerary = userService.SaveOrUpdate(new Itinerary
            {
                Name = name,
                From = ParseDate(from),
                To = ParseDate(to)
            });
            var day = userService.SaveOrUpdate(new ItineraryDay());

            userService.HasItinerary(CurrentUser.Guid, itinerary.Guid);
            userService.HasDay(itinerary.Guid, day.Guid);
            userService.DayRefersTo(day.Guid, artefactGuid);

            Session["DefaultItineraryGuid"] = itinerary.Guid;

            return new JsonResult
            {
                Data = new
                {
                    Success = true
                },
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        [HttpGet]
        public JsonResult ModalAddToExisting(string itineraryGuid, string artefactGuid)
        {
            var itinerary = userService.FindItineraryWithDaysAndUsers(itineraryGuid);
            var day = itinerary.Days.LastOrDefault();

            if (day == null)
            {
                day = new DayArtefactsTuple
                {
                    Day = userService.SaveOrUpdate(new ItineraryDay())
                };
                userService.HasDay(itinerary.Itinerary.Guid, day.Day.Guid);
            }

            userService.DayRefersTo(day.Day.Guid, artefactGuid);

            Session["DefaultItineraryGuid"] = itinerary.Itinerary.Guid;

            return new JsonResult
            {
                Data = new
                {
                    Success = true
                },
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        [HttpGet]
        public ActionResult Edit(string itineraryGuid)
        {
            return View();
        }

        [HttpPost]
        public ActionResult Edit(AddItineraryModel model)
        {
            if (ModelState.IsValid)
            {

            }
            return View(model);
        }

        [HttpGet]
        public ActionResult ViewDetail(string guid)
        {
            var itinerary = userService.FindItineraryWithDaysAndUsers(guid);

            return View(itinerary);
        }

        [HttpGet]
        public ActionResult List()
        {
            var itineraries = userService.FindItinerariesForUser(CurrentUser.Guid);
            return View(itineraries);
        }
    }
}