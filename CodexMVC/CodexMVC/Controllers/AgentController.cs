﻿using CodexMVC.Helpers;
using CodexMVC.Models;
using Data;
using Data.Entities;
using Services;
using Services.Common;
using Services.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;

namespace CodexMVC.Controllers
{
    public class AgentController : BaseController
    {
        private readonly IAgentService agentService;
        private readonly IPlaceService placeService;
        private readonly IArtefactService artefactService;
        private readonly IResourceService resourceService;
        private readonly IActService actService;
        private readonly IRelationService relationService;
        private readonly ICommonService commonService;
        public AgentController(
            IAgentService _agentService,
            IPlaceService _placeService,
            IArtefactService _artefactService,
            IActService _actService,
            IResourceService _resourceService,
            IRelationService _relationService,
            ICategoryService _categoryService,
            ICommonService _commonService
        )
        {
            agentService = _agentService;
            placeService = _placeService;
            artefactService = _artefactService;
            resourceService = _resourceService;
            actService = _actService;
            relationService = _relationService;
            commonService = _commonService;
        }

        public async Task<ActionResult> Artist(string name)
        {
            var agent = agentService.FindAgentByUrlFriendlyName(name);
            if (agent == null)
            {
                return new HttpNotFoundResult();
            }
            return await ViewDetail(agent.Guid);
        }

        [HttpGet]
        public ActionResult Test()
        {
            return View();
        }

        [Authorize(Roles = Permissions.AnyPublicAction)]
        [HttpGet]
        public JsonResult Follow(string agentGuid)
        {
            userService.FollowAgent(CurrentUser.Guid, agentGuid);
            return new JsonResult
            {
                Data = new { Success = true },
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        [Authorize(Roles = Permissions.AnyPublicAction)]
        [HttpGet]
        public JsonResult Unfollow(string agentGuid)
        {
            userService.UnfollowAgent(CurrentUser.Guid, agentGuid);
            return new JsonResult
            {
                Data = new { Success = true },
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        [HttpGet]
        public ActionResult TimelineByYear(string year)
        {
            var timeline = actService.FindNewActsByDates(year, null);
            return PartialView("~/Views/Shared/Partials/TimelineByYear.cshtml", timeline);
        }
        [HttpPost]
        public async Task<ActionResult> CompareTimeline(string agent1, string agent2)
        {
            var agent1Timeline = await actService.FindActsByAgent(agent1);
            var agent2Timeline = await actService.FindActsByAgent(agent2);
            var years1 = agent1Timeline.Select(x => x.Act.TimelineDate());
            var years2 = agent2Timeline.Select(x => x.Act.TimelineDate());
            var years = years1.Concat(years2).Distinct().OrderBy(x => x);
            var timelines = new Timelines { Time = new Dictionary<string, List<Timeline>>() };
            var a1 = agentService.FindAgentByGuid(agent1);
            var a2 = agentService.FindAgentByGuid(agent2);
            foreach (var year in years)
            {
                var timeline1 = new Timeline
                {
                    Agent = a1,
                    NewActs = agent1Timeline.Where(x => x.Act.TimelineDate() == year).ToList()
                };
                var timeline2 = new Timeline
                {
                    Agent = a2,
                    NewActs = agent2Timeline.Where(x => x.Act.TimelineDate() == year).ToList()
                };
                timelines.Time.Add(year, new List<Timeline> { timeline1, timeline2 });
            }
            var model = new ViewCompareTimeline
            {
                Timelines = timelines
            };
            return View("CompareTimeline", model);
        }
        [HttpGet]
        public ActionResult Graph(string guid)
        {
            var model = new ViewAgentGraph
            {
                Agent = agentService.FindAgentByGuid(guid)
            };
            return View(model);
        }

        [HttpGet]
        public async Task<ActionResult> List(string mode = null)
        {
            var artists = await GetArtistList();

            var model = new ViewAgentList
            {
                Artists = artists
            };
            if (mode == null || mode == AgentListMode.ByArtist.ToString())
            {
                model.Mode = AgentListMode.ByArtist;
            }
            else if (mode == AgentListMode.ByCitizenship.ToString())
            {
                model.Mode = AgentListMode.ByCitizenship;
            }
            else if (mode == AgentListMode.ByRepresentativeOf.ToString())
            {
                model.Mode = AgentListMode.ByRepresentativeOf;
            }
            else if (mode == AgentListMode.ByAttribute.ToString())
            {
                model.Mode = AgentListMode.ByAttribute;
            }
            else if (mode == AgentListMode.ByDateOfBirth.ToString())
            {
                model.Mode = AgentListMode.ByDateOfBirth;
            }
            else if (mode == AgentListMode.ByDateOfDeath.ToString())
            {
                model.Mode = AgentListMode.ByDateOfDeath;
            }
            else if (mode == AgentListMode.ShowChart.ToString())
            {
                model.Mode = AgentListMode.ShowChart;
            }
            else
            {
                model.Mode = AgentListMode.Everyone;
            }
            return View(model);
        }

        private async Task<IEnumerable<ArtistsWhoHaveArtworks>> GetArtistList()
        {
            var artists = Cache.GetFromCache<IEnumerable<ArtistsWhoHaveArtworks>>("AgentController.GetArtistList");
            if (artists == null)
            {
                artists = await agentService.AllArtistsWhoHaveArtworks();
                Cache.AddToCache("AgentController.GetArtistList", artists);
            }
            return artists;
        }
        private List<AgentWithAttribute> GetList()
        {
            if (GetFromCache<List<AgentWithAttribute>>(CacheKey.AgentService.AllWithAttributes) == null)
            {
                var userGuid = CurrentUser != null ? CurrentUser.Guid : null;
                AddToCache(CacheKey.AgentService.AllWithAttributes, agentService.AllWithAttributesWithBookmarks(userGuid)
                .OrderBy(x => x.Agent.FullName)
                .ToList());
            }
            return GetFromCache<List<AgentWithAttribute>>(CacheKey.AgentService.AllWithAttributes);
        }

        [HttpGet]
        public async Task<ActionResult> VerticalTimeline(string guid)
        {
            var newActs = await actService.FindActsByAgent(guid);
            var model = new ViewAgentVerticalTimeline
            {
                Agent = agentService.FindAgentByGuid(guid),
                Timeline = newActs
            };
            return View(model);
        }

        [HttpGet]
        [OutputCache(
            Duration = Constants.DefaultOutputCacheDuration,
            Location = OutputCacheLocation.Client,
            NoStore = true,
            VaryByParam = "guid")]
        public async Task<ActionResult> ViewDetail(string guid)
        {
            if (null == guid)
            {
                return new HttpNotFoundResult();
            }
            var exists = agentService.AgentExists(guid);
            if (false == exists)
            {
                return new HttpNotFoundResult();
            }
            var model = await GetViewDetailModel(guid);

            if (CurrentUser != null)
            {
                model.UserAgentDetails = GetUserAgentDetails(guid);
            }

            AddToHistory(guid, model.Details);

            return View(model);
        }

        [HttpGet]
        public async Task<ActionResult> Timeline(string guid)
        {
            if (null == guid)
            {
                return new HttpNotFoundResult();
            }
            var exists = agentService.AgentExists(guid);
            if (false == exists)
            {
                return new HttpNotFoundResult();
            }
            var agentDetails = await GetViewDetailModel(guid);
            var model = new TimelineJS
            {
                Visitors = new IEntity[] { agentDetails.Agent.Agent },
                Name = agentDetails.Agent.Agent.FullName2,
                Timeline = agentDetails.NewActs
            };
            return View(model);
        }

        [HttpGet]
        public async Task<ActionResult> TimelineCompare(string artistGuid1, string artistGuid2)
        {
            if (null == artistGuid1 || artistGuid2 == null)
            {
                return new HttpNotFoundResult();
            }
            if (false == agentService.AgentExists(artistGuid1) || false == agentService.AgentExists(artistGuid2))
            {
                return new HttpNotFoundResult();
            }
            var details1 = await GetViewDetailModel(artistGuid1);
            var details2 = await GetViewDetailModel(artistGuid2);
            var timeline = details1.NewActs.Concat(details2.NewActs).ToList();
            var model = new TimelineJS
            {
                Visitors = new IEntity[] { details1.Agent.Agent, details2.Agent.Agent },
                Name = details1.Agent.Agent.FullName2 + " and " + details2.Agent.Agent.FullName2,
                Timeline = timeline
            };
            return View("Timeline", model);
        }

        private UserAgentDetails GetUserAgentDetails(string guid)
        {
            var key = "AgentController.UserAgentDetails[guid=" + guid + "]";
            var cache = Session[key] as SessionCache<UserAgentDetails>;
            if (cache == null || cache.HasExpired(1))
            {
                cache = new SessionCache<UserAgentDetails>();
                cache.Data = userService.FindUserAgentDetails(CurrentUser.Guid, guid);
                Session[key] = cache;
            }
            return cache.Data;
        }

        private async Task<ViewAgentPublic> GetViewDetailModel(string guid)
        {
            var KEY = "AgentController.GetViewDetailModel[guid=" + guid + "]";
            var data = Cache.GetFromCache<ViewAgentPublic>(KEY);
            if (data == null)
            {
                var details = agentService.FindPersonalDetailsAsync(guid);
                var outgoingConnections = agentService.FindOutgoingWithArtworksAsync(guid);
                var exhibitions = await agentService.FindExhibitionsOfAsync(guid);
                var tours = await agentService.FindToursCovering(guid);
                var containerWithArtefacts = await artefactService.FindContainerWithArtefactsByAgentAsync(guid);
                var agentWithAttributes = GetList().FirstOrDefault(x => x.Agent.Guid == guid);
                var artefacts = await artefactService.ArtefactsMadeBy(guid);
                var timeline = await GetAgentTimeline(guid);
                var timelineMarkers = RazorHelper.GetTimelineMarkers(timeline);
                var timelineHeatMap = RazorHelper.GetTimelineHeatMap(timelineMarkers);
                var artworkMarkers = RazorHelper.GetArtworkMarkers(containerWithArtefacts);
                var artworkHeatmap = RazorHelper.GetArtworksHeatMap(artworkMarkers);
                data = new ViewAgentPublic
                {
                    TimelineMarkers = timelineMarkers,
                    TimelineHeatMap = timelineHeatMap,
                    ArtworkHeatMap = artworkHeatmap,
                    ArtworkMarkers = artworkMarkers,
                    Exhibitions = exhibitions,
                    Tours = tours,
                    OutgoingConnexions = outgoingConnections,
                    Details = details,
                    NewActs = timeline,
                    ContainerWithArtefacts = containerWithArtefacts,
                    Agent = agentWithAttributes,
                    Artefacts = artefacts
                };
                Cache.AddToCache(KEY, data, Constants.DefaultOutputCacheDuration);
            }
            return data;
        }

        private static void AddToHistory(string guid, PersonalDetails details)
        {
            if (details == null)
            {
                return;
            }
            var item = new History
            {
                EntityGuid = guid,
                EntityType = typeof(Agent).Name,
                Date = DateTimeOffset.Now,
                Event = "<a href='/Agent/ViewDetail/" + guid + "'>" + details.Agent.FullName2 + "</a> <small>" + details.SubTitle() + "</small>"
            };
            RazorHelper.AddToHistory(item);
        }



    }
}