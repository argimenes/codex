﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CodexMVC.Controllers
{
    public class DataSetController : BaseController
    {
        [HttpGet]
        public async Task<JsonResult> ByDateRangeJson(int? start, int? end)
        {
            var data = await dataSetService.FindDataSetByDateRangeAsync(start, end);
            return new JsonResult
            {
                Data = data,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        [HttpGet]
        public async Task<ActionResult> ByDateRange(int? start, int? end)
        {
            var data = await dataSetService.FindDataSetByDateRangeAsync(start, end);
            return View("Index", data);
        }

        [HttpGet]
        public async Task<ActionResult> ByPlace(string guid)
        {
            var data = await dataSetService.FindDataSetByPlaceAsync(guid);
            return View("Index", data);
        }

        [HttpGet]
        public async Task<JsonResult> ByDataSet(string guid)
        {
            var data = await dataSetService.FindDataSetRecursiveAsync(guid);
            return Succeeded(data);
        }

        [HttpGet]
        public async Task<JsonResult> BySource(string guid)
        {
            throw new NotImplementedException();
        }

        [HttpGet]
        public async Task<ActionResult> Index(string guid)
        {
            var data = await dataSetService.FindDataSetRecursiveAsync(guid);
            return View("Index", data);
        }

        [HttpGet]
        public async Task<ActionResult> List()
        {
            var results = await dataSetService.GetListItemsAsync();
            return View(results.OrderBy(x => x.DataSet.Name).ToList());
        }
    }
}