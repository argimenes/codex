﻿using CodexMVC.Controllers;
using CodexMVC.Models;
using Data.Entities;
using Services;
using Services.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace CodexMVC.Areas.Admin.Controllers
{
    public class ArtefactComparer : EqualityComparer<Artefact>
    {
        public override bool Equals(Artefact x, Artefact y)
        {
            return x.Guid == y.Guid;
        }

        public override int GetHashCode(Artefact obj)
        {
            return base.GetHashCode();

        }
    }

    public class CategoryComparer : EqualityComparer<Category>
    {
        public override bool Equals(Category x, Category y)
        {
            return x.Guid == y.Guid;
        }

        public override int GetHashCode(Category obj)
        {
            return base.GetHashCode();

        }
    }
    [Authorize(Roles = Permissions.CanAccessAdmin)]
    public class QueryController : BaseController
    {
        private readonly IAgentService agentService;
        private readonly IArtefactService artefactService;
        public QueryController(
            IAgentService _agentService,
            IArtefactService _artefactService
        )
        {
            agentService = _agentService;
            artefactService = _artefactService;
        }

        [HttpGet]
        public ActionResult UniversalSearch()
        {
            var model = new UniversalSearchModel();
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> UniversalSearch(UniversalSearchModel model)
        {
            if (ModelState.IsValid)
            {
                var results = await agentService.UniversalSearch(model.Search);
                model.Results = results;
            }
            return View(model);
        }

        [HttpGet]
        public ActionResult ByAge()
        {
            var ages = artefactService.AllArtworksByAge();
            var model = new ByAgeModel
            {
                Result = ages
            };
            return View(model);
        }

        [HttpGet]
        public ActionResult ByExterior()
        {
            var model = new ByExteriorModel();
            return View(model);
        }
        [HttpPost]
        public ActionResult ByExterior(ByExteriorModel model)
        {
            if (ModelState.IsValid)
            {
                var artefacts = artefactService.FindByExteriorQuery(model.ArtworkTypeGuid, model.PlaceGuid);
                model.Artefacts = artefacts;
            }
            return View(model);
        }
        [HttpGet]
        public ActionResult ByMotif()
        {
            var model = new ByMotifModel();
            return View(model);
        }
        [HttpPost]
        public ActionResult ByMotif(ByMotifModel model)
        {
            if (ModelState.IsValid)
            {
                var artefacts = artefactService.FindByMotifQuery(model.ArtworkTypeGuid, model.MotifGuid, model.PlaceGuid);
                model.Artefacts = artefacts;
            }
            return View(model);
        }
        [HttpGet]
        public ActionResult ByAgentTypeRepresented()
        {
            var model = new ByAgentTypeRepresentedModel();
            return View(model);
        }
        [HttpPost]
        public ActionResult ByAgentTypeRepresented(ByAgentTypeRepresentedModel model)
        {
            if (ModelState.IsValid)
            {
                var artefacts = artefactService.FindByAgentTypeRepresented(model.ArtworkTypeGuid, model.AgentTypeGuid, model.PlaceGuid);
                model.Artefacts = artefacts;
            }
            return View(model);
        }
        [HttpGet]
        public ActionResult ByCategoryRepresented()
        {
            var model = new ByCategoryRepresentedModel();
            return View(model);
        }
        [HttpPost]
        public ActionResult ByCategoryRepresented(ByCategoryRepresentedModel model)
        {
            if (ModelState.IsValid)
            {
                var artefacts = artefactService.FindByObjectRepresented(model.ArtworkTypeGuid, model.CategoryGuid, model.PlaceGuid);
                model.Artefacts = artefacts;
            }
            return View(model);
        }
        [HttpGet]
        public ActionResult ByAgentRepresented()
        {
            var model = new ByAgentRepresentedModel();
            return View(model);
        }
        [HttpPost]
        public ActionResult ByAgentRepresented(ByAgentRepresentedModel model)
        {
            if (ModelState.IsValid)
            {
                var artefacts = artefactService.FindByAgentRepresentedQuery(model.ArtworkTypeGuid, model.AgentGuid, model.PlaceGuid);
                model.Artefacts = artefacts;
            }
            return View(model);
        }
        [HttpGet]
        public ActionResult BySubject()
        {
            var model = new BySubjectModel();
            return View(model);
        }
        [HttpPost]
        public ActionResult BySubject(BySubjectModel model)
        {
            if (ModelState.IsValid)
            {
                var artefacts = artefactService.FindBySubjectQuery(model.ArtworkTypeGuid, model.SubjectGuid, model.PlaceGuid);
                model.Artefacts = artefacts;
            }
            return View(model);
        }
        [HttpGet]
        public ActionResult ByAestheticConvention()
        {
            var model = new ByAestheticConventionModel();
            return View(model);
        }
        [HttpPost]
        public ActionResult ByAestheticConvention(ByAestheticConventionModel model)
        {
            if (ModelState.IsValid)
            {
                var artefacts = artefactService.FindByAestheticConvention(model.ArtworkTypeGuid, model.ConventionGuid, model.PlaceGuid);
                model.Artefacts = artefacts;
            }
            return View(model);
        }
        [HttpGet]
        public ActionResult ByGenre()
        {
            var model = new ByGenreModel();
            return View(model);
        }
        [HttpPost]
        public ActionResult ByGenre(ByGenreModel model)
        {
            if (ModelState.IsValid)
            {
                var artefacts = artefactService.FindByGenreQuery(model.ArtworkTypeGuid, model.GenreGuid, model.PlaceGuid);
                model.Artefacts = artefacts;
            }
            return View(model);
        }
        [HttpGet]
        public ActionResult ByMovement()
        {
            var model = new ByMovementModel();
            return View(model);
        }
        [HttpPost]
        public ActionResult ByMovement(ByMovementModel model)
        {
            if (ModelState.IsValid)
            {
                var artefacts = artefactService.FindByMovementQuery(model.ArtworkTypeGuid, model.MovementGuid, model.PlaceGuid);
                model.Artefacts = artefacts;
            }
            return View(model);
        }
        [HttpGet]
        public ActionResult ByAgent()
        {
            var model = new ByAgentModel();
            return View(model);
        }
        [HttpPost]
        public ActionResult ByAgent(ByAgentModel model)
        {
            if (ModelState.IsValid)
            {
                var artefacts = artefactService.FindByAgentQuery(model.ArtworkTypeGuid, model.ArtistGuid, model.PlaceGuid);
                model.Artefacts = artefacts;
            }
            return View(model);
        }
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public ActionResult Search()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Search(SearchQuery model)
        {
            return View();
        }
        [HttpGet]
        public ActionResult AllArtefactsSharingSameAttributes()
        {
            return View(new ViewAllArtefactsWithSharedAttributes());
        }
        [HttpGet]
        public ActionResult Find()
        {
            return View(new QueryModel());
        }
        [HttpPost]
        public ActionResult Find(QueryModel model)
        {
            var results = artefactService.SearchByQuery(new FindByQuery
            {
                AgentGuid = model.AgentGuid,
                ArtefactIsAGuid = model.ArtefactIsAGuid,
                CitizenOfGuid = model.CitizenOfGuid,
                PlaceGuid = model.PlaceGuid,
                Preposition = model.Preposition,
                RepresentativeOfGuid = model.RepresentativeOf
            });
            model.Results = results.ToList();
            return View("Find", model);
        }

        [HttpPost]
        public async Task<ActionResult> AllArtefactsSharingSameAttributes(ViewAllArtefactsWithSharedAttributes model)
        {
            if (ModelState.IsValid)
            {
                var tuples = await artefactService.FindSimilarAsync(model.ArtefactGuid);
                model.Similar = tuples.ToList();

                var artefactImage = artefactService.FindArtefactWithImage(model.ArtefactGuid);
                model.ArtefactImage = artefactImage;

                var group = artefactService.FindSimilarArtefactsByTaxonomy(model.ArtefactGuid, model.Separation)
                    .GroupBy(x => x.Artefact2.Guid)
                    .OrderBy(x => x.First().PathCost)
                    .ThenByDescending(x => x.First().Categories.Count())
                    .ThenBy(x => x.First().Artefact2.Name);
                model.AttributeGrouping = group;

                var usages = artefactService.FindSimilarArtefactsByUsage(model.ArtefactGuid, model.Separation)
                    .GroupBy(x => x.Artefact2.Guid)
                    .OrderBy(x => x.First().PathCost)
                    .ThenByDescending(x => x.First().Categories.Count())
                    .ThenBy(x => x.First().Artefact2.Name);
                model.UsagesGrouping = usages;

                var motifs = artefactService.FindSimilarArtefactsByMotif(model.ArtefactGuid, model.Separation)
                    .GroupBy(x => x.Artefact2.Guid)
                    .OrderBy(x => x.First().PathCost)
                    .ThenByDescending(x => x.First().Categories.Count())
                    .ThenBy(x => x.First().Artefact2.Name);
                model.MotifsGrouping = motifs;

                var representations = artefactService.FindSimilarArtefactsByRepresents(model.ArtefactGuid, model.Separation)
                    .GroupBy(x => x.Artefact2.Guid)
                    .OrderBy(x => x.First().PathCost)
                    .ThenByDescending(x => x.First().Entities.Count())
                    .ThenBy(x => x.First().Artefact2.Name);
                model.RepresentsGrouping = representations;

                var agentRepresentations = artefactService.FindSimilarArtefactsByRepresentsByAgent(model.ArtefactGuid, model.Separation)
                    .GroupBy(x => x.Artefact2.Guid)
                    .OrderBy(x => x.First().PathCost)
                    .ThenByDescending(x => x.First().Entities.Count())
                    .ThenBy(x => x.First().Artefact2.Name);
                model.RepresentsAgentsGrouping = agentRepresentations;

                var placeRepresentations = artefactService.FindSimilarArtefactsByRepresentsByPlace(model.ArtefactGuid, model.Separation)
                    .GroupBy(x => x.Artefact2.Guid)
                    .OrderBy(x => x.First().PathCost)
                    .ThenByDescending(x => x.First().Entities.Count())
                    .ThenBy(x => x.First().Artefact2.Name);
                model.RepresentsPlacesGrouping = placeRepresentations;

                model.Attributes = artefactService.FindOutgoingRelationships(model.ArtefactGuid);
            }
            return View(model);
        }
        [HttpGet]
        public ActionResult AllAgentsSharingSameAttributes()
        {
            return View(new ViewAllAgentsWithSharedAttributes());
        }
        [HttpPost]
        public ActionResult AllAgentsSharingSameAttributes(ViewAllAgentsWithSharedAttributes model)
        {
            if (ModelState.IsValid)
            {
                var results = agentService.FindAgentsWithSameAttributes(model.AgentGuid);
                model.Results = results;
            }
            return View(model);
        }
        public ActionResult NaturalLanguage()
        {
            return View();
        }
    }
}