﻿using CodexMVC.Controllers;
using CodexMVC.Models;
using Data.Entities;
using Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CodexMVC.Areas.Admin.Controllers
{
    public abstract class ArtefactBaseController : BaseController
    {
        protected readonly IActService actService;
        protected readonly IAgentService personService;
        protected readonly IPlaceService placeService;
        protected readonly IArtefactService artefactService;
        protected readonly IRelationService relationService;
        protected readonly IResourceService resourceService;
        protected readonly ITextService textService;
        protected readonly ICommonService commonService;

        public ArtefactBaseController(
            IAgentService _personService,
            IPlaceService _placeService,
            IArtefactService _artefactService,
            IResourceService _resourceService,
            IActService _actService,
            IRelationService _relationService,
            ITextService _textService,
            ICommonService _commonService
        )
        {
            personService = _personService;
            placeService = _placeService;
            artefactService = _artefactService;
            resourceService = _resourceService;
            actService = _actService;
            relationService = _relationService;
            textService = _textService;
            commonService = _commonService;
        }
        [HttpGet]
        public ActionResult AddLocatedAt(string artefactGuid)
        {
            return AddBasicRelation<Place>(artefactGuid, "EditLocatedAt", "is located at ...");
        }
        [HttpGet]
        public ActionResult EditLocatedAt(string artefactGuid, string hasRelationGuid)
        {
            return FindBasicRelation<Place>(artefactGuid, hasRelationGuid, "EditLocatedAt", "is located at ...", artefactService.FindLocatedAt);
        }
        [HttpPost]
        public ActionResult EditLocatedAt(GenericRelationNodeModel<Artefact, Place> model)
        {
            return SaveBasicRelation<Place>(model, artefactService.LocatedAt);
        }
        [HttpGet]
        public ActionResult AddLocatedIn(string artefactGuid)
        {
            return AddBasicRelation<Artefact>(artefactGuid, "EditLocatedIn", " is located in ...");
        }
        [HttpGet]
        public ActionResult EditLocatedIn(string artefactGuid, string toDestGuid)
        {
            return FindBasicRelation<Artefact>(artefactGuid, toDestGuid, "EditLocatedIn", " is located in ...", artefactService.FindLocatedIn);
        }
        [HttpPost]
        public ActionResult EditLocatedIn(GenericRelationNodeModel<Artefact, Artefact> model)
        {
            return SaveBasicRelation<Artefact>(model, artefactService.LocatedIn);
        }
        [HttpGet]
        public ActionResult AddIsA(string artefactGuid)
        {
            return AddRelation<Category>(artefactGuid, "EditIsA", "is a ...");
        }
        [HttpGet]
        public ActionResult EditIsA(string artefactGuid, string hasRelationGuid)
        {
            return FindRelation<Category>(artefactGuid, hasRelationGuid, "EditIsA", "is a ...", artefactService.FindIsA);
        }
        [HttpPost]
        public ActionResult EditIsA(GenericRelationModel<Artefact, Category> model)
        {
            return SaveRelation<Category>(model, artefactService.IsA);
        }
        [HttpGet]
        public ActionResult AddOntologicalStateOf(string artefactGuid)
        {
            return AddRelation<Category>(artefactGuid, "EditOntologicalStateOf", "has an ontological state of ...", Label.Categories.OntologicalState);
        }
        [HttpGet]
        public ActionResult EditOntologicalStateOf(string artefactGuid, string hasRelationGuid)
        {
            return FindRelation<Category>(artefactGuid, hasRelationGuid, "EditOntologicalStateOf", "has an ontological state of ...", artefactService.FindOntologicalStateOf, Label.Categories.OntologicalState);
        }
        [HttpPost]
        public ActionResult EditOntologicalStateOf(ArtefactToCategoryRelationModel model)
        {
            return SaveRelation<Category>(model, artefactService.OntologicalStateOf, Label.Categories.OntologicalState);
        }
        protected List<TDest> GetDestinationByRelationshipType<TDest>(IEnumerable<GenericRelationHyperNode<Artefact, IEntity>> nodes, string relationshipType)
            where TDest : class, IEntity
        {
            return nodes
                .Where(x => x.RelationToDestinationType == relationshipType)
                .Select(x => x.Destination)
                .Cast<TDest>()
                .ToList();
        }

        [HttpGet]
        protected ActionResult Delete(string artefactGuid)
        {
            var artefact = artefactService.FindArtefactByGuid(artefactGuid);
            var deleted = artefactService.Delete(artefactGuid);
            GeneralMessage = string.Format("Artefact '{0}' ({1}) and {2} relationship(s) deleted.", artefact.ToDisplay(), artefactGuid, deleted);
            return ToList();
        }
        [HttpGet]
        protected ActionResult DeleteAttribute(string relationGuid, string artefactGuid)
        {
            commonService.DeleteRelationHyperNode(relationGuid);
            return ToViewDetail(artefactGuid);
        }
        protected abstract ActionResult ToViewDetail(string guid);
        protected abstract ActionResult ToList();
        [HttpGet]
        protected ActionResult DeleteBasicRelationship(string relationshipType, string relationshipGuid, string artefactGuid)
        {
            commonService.DeleteRelationship(relationshipType, relationshipGuid);
            return ToViewDetail(artefactGuid);
        }

        protected ActionResult AddBasicRelation<TDest>(string artefactGuid, string action, string title)
            where TDest : class, IEntity
        {
            var artefact = artefactService.FindArtefactByGuid(artefactGuid);
            return AddRelationTest<Artefact, TDest>(artefact, action, title);
        }

        protected ActionResult AddRelation<TDest>(string artefactGuid, string action, string title, string descendantsOf = null)
            where TDest : class, IEntity
        {
            var artefact = artefactService.FindArtefactByGuid(artefactGuid);
            return AddRelation<Artefact, TDest>(artefact, action, title, descendantsOf);
        }

        protected ActionResult FindBasicRelation<TDest>(string artefactGuid, string toDestGuid, string action, string title, Func<string, string, GenericRelationNode<Artefact, TDest>> getter)
            where TDest : class, IEntity
        {
            return FindRelationTest<Artefact, TDest>(artefactGuid, toDestGuid, action, title, getter);
        }

        protected ActionResult FindRelation<TDest>(string artefactGuid, string hasRelationGuid, string action, string title, Func<string, string, GenericRelationHyperNode<Artefact, TDest>> getter, string descendantsOf = null)
            where TDest : class, IEntity
        {
            return FindRelation<Artefact, TDest>(artefactGuid, hasRelationGuid, action, title, getter, descendantsOf);
        }
        protected ActionResult SaveBasicRelation<TDest>(GenericRelationNodeModel<Artefact, TDest> model, Action<GenericRelationNode<Artefact, TDest>> updater)
            where TDest : class, IEntity
        {
            return SaveRelationTest<Artefact, TDest>(model, updater, artefactService.FindArtefactByGuid);
        }
        protected ActionResult SaveRelation<TDest>(GenericRelationModel<Artefact, TDest> model, Action<GenericRelationHyperNode<Artefact, TDest>> updater, string descendantsOf = null)
            where TDest : class, IEntity
        {
            return SaveRelation<Artefact, TDest>(model, updater, artefactService.FindArtefactByGuid, descendantsOf);
        }
    }
}