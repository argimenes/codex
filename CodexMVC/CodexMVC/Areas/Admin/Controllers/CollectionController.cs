﻿using CodexMVC.Controllers;
using CodexMVC.Helpers;
using CodexMVC.Models;
using Data.Entities;
using Services;
using Services.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace CodexMVC.Areas.Admin.Controllers
{
    public class CollectionController : ArtefactBaseController
    {
        public CollectionController(
            IAgentService _personService,
            IPlaceService _placeService,
            IArtefactService _artefactService,
            IResourceService _resourceService,
            IActService _actService,
            IRelationService _relationService,
            ITextService _textService,
            ICommonService _commonService)
            : base(_personService, _placeService, _artefactService, _resourceService, _actService, _relationService, _textService, _commonService)
        {

        }

        [HttpGet]
        public ActionResult CheckForDuplicates(string name)
        {
            var collections = artefactService.CheckForDuplicateCollections(name)
                .Select(x =>
                {
                    var text = x.Collection.ToDisplay();
                    var subtitle = "";
                    if (x.Attributes != null && x.Attributes.Any())
                    {
                        subtitle = string.Join(" and ", x.Attributes.Select(x2 => x2.Name.ToLower()));
                    }
                    if (x.Location != null)
                    {
                        subtitle += " in " + x.Location.Name;
                    }
                    if (subtitle.Length > 0)
                    {
                        subtitle = " <small>" + subtitle + "</small>";
                    }
                    return new
                        {
                            Text = text + subtitle,
                            Value = x.Collection.Guid
                        };
                });

            return new JsonResult
            {
                Data = new
                {
                    Collections = collections
                },
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        [HttpGet]
        public ActionResult Edit(string artefactGuid)
        {
            var artefact = artefactService.FindArtefactByGuid(artefactGuid);
            return View("Edit", new EditArtefactModel
            {
                ArtefactGuid = artefactGuid,
                Ordinal = artefact.Ordinal,
                Name = artefact.Name,
                CatalogueIdentPrefix = artefact.CatalogueIdentPrefix,
                CatalogueIdent = artefact.CatalogueIdent,
                Article = artefact.Article,
                Rating = artefact.Rating,
                TripAdvisorLocationId = artefact.TripAdvisorLocationId,
                Description = artefact.Description,
                Subtitle = artefact.Subtitle,
                Width = artefact.Width,
                Height = artefact.Height,
                Depth = artefact.Depth,
                Latitude = artefact.Latitude,
                Longitude = artefact.Longitude
            });
        }
        [HttpPost]
        public ActionResult Edit(EditArtefactModel model)
        {
            if (ModelState.IsValid)
            {
                var artefact = artefactService.FindArtefactByGuid(model.ArtefactGuid);
                artefact.Name = model.Name;
                artefact.Ordinal = model.Ordinal;
                artefact.TripAdvisorLocationId = model.TripAdvisorLocationId;
                artefact.CatalogueIdent = model.CatalogueIdent;
                artefact.CatalogueIdentPrefix = model.CatalogueIdentPrefix;
                artefact.Article = model.Article;
                artefact.Description = model.Description;
                artefact.Rating = model.Rating;
                artefact.Subtitle = model.Subtitle;
                artefact.Width = model.Width;
                artefact.Height = model.Height;
                artefact.Depth = model.Depth;
                artefact.Latitude = model.Latitude;
                artefact.Longitude = model.Longitude;
                artefactService.SaveOrUpdate(artefact);
                return ToViewDetail(model.ArtefactGuid);
            }
            return View(model);
        }
        protected override ActionResult ToList()
        {
            return RedirectToAction("List", "Collection");
        }
        protected override ActionResult ToViewDetail(string guid)
        {
            return RedirectToAction("ViewDetail", "Collection", new { guid });
        }
        [HttpGet]
        public ActionResult List(string mode = null)
        {
            var collections = artefactService.AllCollections();
            var model = new ViewCollectionList
            {
                Collections = collections
            };
            return View(model);
        }
        public ActionResult Add()
        {
            var empty = Enumerable.Range(1, 7).Select(x => (string)null).ToList();
            var model = new AddCollectionModel
            {
                OpenDayStart = empty,
                OpenDayEnd = empty
            };
            return View("Add", model);
        }
        [HttpPost]
        public ActionResult Add(AddCollectionModel model)
        {
            if (ModelState.IsValid)
            {
                var artefact = new Artefact();
                artefact.Created = DateTimeOffset.Now;
                artefact.Name = model.Name;
                artefact.Ordinal = model.Ordinal;
                artefact.Article = model.Article;
                artefact.Description = model.Description;
                artefact.Rating = model.Rating;
                artefact.Latitude = model.Latitude;
                artefact.Longitude = model.Longitude;
                artefact.TripAdvisorLocationId = model.TripAdvisorLocationId;
                var detail = new CollectionDetail();
                detail.Address = model.Address;
                detail.OpenDayEnd = model.OpenDayEnd.ToArray();
                detail.OpenDayStart = model.OpenDayStart.ToArray();
                detail.Address = model.Address;
                detail.Directions = model.Directions;
                detail.Email = model.Email;
                detail.Website = model.Website;
                detail.Phone = model.Phone;
                detail.PurchaseTicketUrl = model.PurchaseTicketUrl;
                detail.AffiliationCode = model.AffiliationCode;
                artefactService.SaveOrUpdate(artefact);
                SaveArtefactImage(model, artefact);
                artefactService.SaveOrUpdate(detail);
                artefactService.HasDetail(artefact.Guid, detail.Guid);
                if (false == string.IsNullOrEmpty(model.CategoryGuid))
                {
                    artefactService.IsA(new GenericRelationHyperNode<Artefact, Category>
                    {
                        Source = artefact,
                        Relation = new Relation
                        {
                            Primary = true
                        },
                        Destination = new Category
                        {
                            Guid = model.CategoryGuid
                        }
                    });
                }
                if (false == string.IsNullOrEmpty(model.LocatedInGuid))
                {
                    artefactService.LocatedIn(new GenericRelationNode<Artefact, Artefact>
                    {
                        Source = artefact,
                        Destination = new Artefact { Guid = model.LocatedInGuid }
                    });
                }
                if (false == string.IsNullOrEmpty(model.LocatedAtGuid))
                {
                    artefactService.LocatedAt(new GenericRelationHyperNode<Artefact, Place>
                    {
                        Source = artefact,
                        Relation = new Relation(),
                        Destination = new Place { Guid = model.LocatedAtGuid }
                    });
                }
                GeneralMessage = string.Format("Created art collection '{0}'.", model.Name);
                return ToViewDetail(artefact.Guid);
            }
            return View(model);
        }

        [HttpGet]
        [Authorize(Roles = Permissions.ViewArtefact)]
        public async Task<ActionResult> ViewDetail(string guid)
        {
            var node = new ViewCollection();
            node.Artefact = artefactService.FindArtefactByGuid(guid);
            node.NewActs = await actService.FindNewActsByArtefact(guid);
            node.Detail = artefactService.FindDetailForArtefact(guid);
            node.OutgoingRelations = commonService.FindOutgoingRelations<Artefact>(guid);
            node.IncomingRelations = commonService.FindIncomingRelations<Artefact>(guid);
            node.BasicOutgoingRelations = commonService.FindBasicOutgoingRelations<Artefact>(guid);
            node.BasicIncomingRelations = commonService.FindBasicIncomingRelations<Artefact>(guid);
            node.IsA = GetDestinationByRelationshipType<Category>(node.OutgoingRelations, Label.Relationship.IsA);
            node.Images = resourceService.FindArtefactImages(guid);
            return View("ViewDetail", node);
        }
    }
}