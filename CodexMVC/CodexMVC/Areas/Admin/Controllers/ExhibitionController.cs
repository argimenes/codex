﻿using CodexMVC.Controllers;
using CodexMVC.Models;
using Data.Entities;
using Services;
using Services.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CodexMVC.Areas.Admin.Controllers
{
    public class ExhibitionController : ArtefactBaseController
    {
        public ExhibitionController(
            IAgentService _personService,
            IPlaceService _placeService,
            IArtefactService _artefactService,
            IResourceService _resourceService,
            IActService _actService,
            IRelationService _relationService,
            ITextService _textService,
            ICommonService _commonService)
            : base(_personService, _placeService, _artefactService, _resourceService, _actService, _relationService, _textService, _commonService)
        {

        }
        [HttpGet]
        public ActionResult Edit(string artefactGuid)
        {
            var artefact = artefactService.FindArtefactByGuid(artefactGuid);
            return View("Edit", new EditArtefactModel
            {
                ArtefactGuid = artefactGuid,
                Ordinal = artefact.Ordinal,
                Name = artefact.Name,
                CatalogueIdentPrefix = artefact.CatalogueIdentPrefix,
                CatalogueIdent = artefact.CatalogueIdent,
                Article = artefact.Article,
                Rating = artefact.Rating,
                Description = artefact.Description,
                Subtitle = artefact.Subtitle,
                Width = artefact.Width,
                Height = artefact.Height,
                Depth = artefact.Depth,
                Latitude = artefact.Latitude,
                Longitude = artefact.Longitude
            });
        }
        [HttpPost]
        public ActionResult Edit(EditArtefactModel model)
        {
            if (ModelState.IsValid)
            {
                var artefact = artefactService.FindArtefactByGuid(model.ArtefactGuid);
                artefact.Name = model.Name;
                artefact.Ordinal = model.Ordinal;
                artefact.CatalogueIdent = model.CatalogueIdent;
                artefact.CatalogueIdentPrefix = model.CatalogueIdentPrefix;
                artefact.Article = model.Article;
                artefact.Description = model.Description;
                artefact.Rating = model.Rating;
                artefact.Subtitle = model.Subtitle;
                artefact.Width = model.Width;
                artefact.Height = model.Height;
                artefact.Depth = model.Depth;
                artefact.Latitude = model.Latitude;
                artefact.Longitude = model.Longitude;
                artefactService.SaveOrUpdate(artefact);
                return ToViewDetail(model.ArtefactGuid);
            }
            return View(model);
        }
        [HttpGet]
        public ActionResult AddTakesPlaceIn(string artefactGuid)
        {
            return AddRelation<Artefact>(artefactGuid, "EditTakesPlaceIn", " takes place in ...");
        }
        [HttpGet]
        public ActionResult EditTakesPlaceIn(string artefactGuid, string hasRelationGuid)
        {
            return FindBasicRelation<Artefact>(artefactGuid, hasRelationGuid, "EditTakesPlaceIn", " takes place in ...", artefactService.FindTakesPlaceIn);
        }
        [HttpPost]
        public ActionResult EditTakesPlaceIn(GenericRelationModel<Artefact, Artefact> model)
        {
            return SaveRelation<Artefact>(model, artefactService.TakesPlaceIn);
        }
        [HttpGet]
        public ActionResult AddExhibitedIn(string artefactGuid)
        {
            return AddRelation<Artefact>(artefactGuid, "EditExhibitedIn", "was in the exhibition ...");
        }
        [HttpGet]
        public ActionResult EditExhibitedIn(string artefactGuid, string hasRelationGuid)
        {
            return FindRelation<Artefact>(artefactGuid, hasRelationGuid, "EditExhibitedIn", "was in the exhibition ...", artefactService.FindExhibitedIn);
        }
        [HttpPost]
        public ActionResult EditExhibitedIn(ArtefactToArtefactRelationModel model)
        {
            return SaveRelation<Artefact>(model, artefactService.ExhibitedIn);
        }
        protected override ActionResult ToList()
        {
            return RedirectToAction("List", "Exhibition");
        }
        protected override ActionResult ToViewDetail(string guid)
        {
            return RedirectToAction("ViewDetail", "Exhibition", new { guid });
        }
        [HttpGet]
        public ActionResult List(string mode = null)
        {
            var exhibitions = artefactService.AllExhibitions();
            var model = new ViewExhibitionList
            {
                Exhibitions = exhibitions
            };
            return View(model);
        }
        public ActionResult Add()
        {
            var model = new AddExhibitionModel
            {

            };
            return View("Add", model);
        }
        [HttpPost]
        public ActionResult Add(AddExhibitionModel model)
        {
            if (ModelState.IsValid)
            {
                var exhibition = new Artefact();
                exhibition.Created = DateTimeOffset.Now;
                exhibition.Name = model.Name;
                exhibition.Article = model.Article;
                exhibition.Description = model.Description;
                exhibition.Rating = model.Rating;
                artefactService.SaveOrUpdate(exhibition);
                var detail = new EventDetail();
                detail.Url = model.Url;
                detail.DayStart = model.DayStart;
                detail.MonthStart = model.MonthStart;
                detail.YearStart = model.YearStart;
                detail.DayEnd = model.DayEnd;
                detail.MonthEnd = model.MonthEnd;
                detail.YearEnd = model.YearEnd;
                artefactService.SaveOrUpdate(detail);
                artefactService.HasEventDetail(exhibition.Guid, detail.Guid);
                var exhibitionType = categoryService.FindByName(Label.Categories.Exhibition);
                artefactService.IsA(new GenericRelationHyperNode<Artefact, Category>
                {
                    Source = exhibition,
                    Relation = new Relation
                    {
                        Primary = true
                    },
                    Destination = exhibitionType
                });
                if (false == string.IsNullOrEmpty(model.LocatedInGuid))
                {
                    artefactService.TakesPlaceIn(new GenericRelationNode<Artefact, Artefact>
                    {
                        Source = exhibition,
                        Destination = new Artefact
                        {
                            Guid = model.LocatedInGuid
                        }
                    });
                }
                if (false == string.IsNullOrEmpty(model.Artist1Guid))
                {
                    artefactService.ExhibitingIn(new GenericRelationHyperNode<Data.Agent, Artefact>
                    {
                        Source = new Data.Agent
                        {
                            Guid = model.Artist1Guid
                        },
                        Relation = new Relation
                        {
                            Primary = true
                        },
                        Destination = exhibition
                    });
                }
                if (false == string.IsNullOrEmpty(model.Artist2Guid))
                {
                    artefactService.ExhibitingIn(new GenericRelationHyperNode<Data.Agent, Artefact>
                    {
                        Source = new Data.Agent
                        {
                            Guid = model.Artist2Guid
                        },
                        Relation = new Relation
                        {
                            Primary = true
                        },
                        Destination = exhibition
                    });
                }
                if (false == string.IsNullOrEmpty(model.Artist3Guid))
                {
                    artefactService.ExhibitingIn(new GenericRelationHyperNode<Data.Agent, Artefact>
                    {
                        Source = new Data.Agent
                        {
                            Guid = model.Artist3Guid
                        },
                        Relation = new Relation
                        {
                            Primary = true
                        },
                        Destination = exhibition
                    });
                }
                if (false == string.IsNullOrEmpty(model.Artwork1Guid))
                {
                    artefactService.ExhibitedIn(new GenericRelationHyperNode<Artefact, Artefact>
                    {
                        Source = new Artefact
                        {
                            Guid = model.Artwork1Guid
                        },
                        Relation = new Relation
                        {
                            Primary = true
                        },
                        Destination = exhibition
                    });
                }
                if (false == string.IsNullOrEmpty(model.Artwork2Guid))
                {
                    artefactService.ExhibitedIn(new GenericRelationHyperNode<Artefact, Artefact>
                    {
                        Source = new Artefact
                        {
                            Guid = model.Artwork2Guid
                        },
                        Relation = new Relation
                        {
                            Primary = true
                        },
                        Destination = exhibition
                    });
                }
                if (false == string.IsNullOrEmpty(model.Artwork3Guid))
                {
                    artefactService.ExhibitedIn(new GenericRelationHyperNode<Artefact, Artefact>
                    {
                        Source = new Artefact
                        {
                            Guid = model.Artwork3Guid
                        },
                        Relation = new Relation
                        {
                            Primary = true
                        },
                        Destination = exhibition
                    });
                }
                if (false == string.IsNullOrEmpty(model.StyleGuid))
                {
                    artefactService.AssociatedWith(new GenericRelationHyperNode<Artefact, Category>
                    {
                        Source = exhibition,
                        Relation = new Relation
                        {
                            Primary = true
                        },
                        Destination = new Category
                        {
                            Guid = model.StyleGuid
                        }
                    });
                }
                if (false == string.IsNullOrEmpty(model.GenreGuid))
                {
                    artefactService.AssociatedWith(new GenericRelationHyperNode<Artefact, Category>
                    {
                        Source = exhibition,
                        Relation = new Relation
                        {
                            Primary = true
                        },
                        Destination = new Category
                        {
                            Guid = model.GenreGuid
                        }
                    });
                }
                GeneralMessage = string.Format("Created art exhibition '{0}'.", model.Name);
                return ToViewDetail(exhibition.Guid);
            }
            return View(model);
        }

        [HttpGet]
        [Authorize(Roles = Permissions.ViewArtefact)]
        public ActionResult ViewDetail(string guid)
        {
            var node = new ViewExhibition();
            node.Artefact = artefactService.FindArtefactByGuid(guid);
            node.Detail = artefactService.FindDetailForArtefact(guid);
            node.OutgoingRelations = commonService.FindOutgoingRelations<Artefact>(guid);
            node.IncomingRelations = commonService.FindIncomingRelations<Artefact>(guid);
            node.BasicOutgoingRelations = commonService.FindBasicOutgoingRelations<Artefact>(guid);
            node.BasicIncomingRelations = commonService.FindBasicIncomingRelations<Artefact>(guid);
            node.IsA = GetDestinationByRelationshipType<Category>(node.OutgoingRelations, Label.Relationship.IsA);
            node.Images = resourceService.FindArtefactImages(guid);
            return View("ViewDetail", node);
        }

    }
}