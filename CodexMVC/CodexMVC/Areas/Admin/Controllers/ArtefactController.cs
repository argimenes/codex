﻿using CodexMVC.Controllers;
using CodexMVC.Helpers;
using CodexMVC.Models;
using Data;
using Data.Entities;
using Neo4jClient;
using Newtonsoft.Json;
using Services;
using Services.Common;
using Services.Helpers;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace CodexMVC.Areas.Admin.Controllers
{
    [Authorize(Roles = Permissions.CanAccessAdmin)]
    public class ArtefactController : ArtefactBaseController
    {
        public ArtefactController(
            IAgentService _personService,
            IPlaceService _placeService,
            IArtefactService _artefactService,
            IResourceService _resourceService,
            IActService _actService,
            IRelationService _relationService,
            ITextService _textService,
            ICommonService _commonService)
            : base(_personService, _placeService, _artefactService, _resourceService, _actService, _relationService, _textService, _commonService)
        {

        }
        [HttpGet]
        public ActionResult AddAssociatedWith(string artefactGuid)
        {
            return AddRelation<Category>(artefactGuid, "EditAssociatedWith", " is associated with ...", Label.Categories.Interest);
        }
        [HttpGet]
        public ActionResult EditAssociatedWith(string artefactGuid, string hasRelationGuid)
        {
            return FindRelation<Category>(artefactGuid, hasRelationGuid, "EditAssociatedWith", " is associated with ...", artefactService.FindAssociatedWith, Label.Categories.Interest);
        }
        [HttpPost]
        public ActionResult EditAssociatedWith(GenericRelationModel<Artefact, Category> model)
        {
            return SaveRelation<Category>(model, artefactService.AssociatedWith, Label.Categories.Interest);
        }

        [HttpGet]
        public async Task<ActionResult> CheckForDuplicates(string name, string madeByGuid)
        {
            var artefacts = await artefactService.CheckForDuplicates(name, madeByGuid);
            return new JsonResult
            {
                Data = new
                {
                    Artefacts = artefacts.Where(x => x.Artefact != null && x.Image != null)
                },
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        [HttpGet]
        [Authorize(Roles = Permissions.AddContext)]
        public ActionResult AddContext(string artefactGuid)
        {
            var artefact = artefactService.FindArtefactByGuid(artefactGuid);
            var model = new EditContextModel
            {
                Node = new ContextHyperNode
                {
                    Context = new Context(),
                    Subject = new Category(),
                    Artefact = artefact,
                    Place = new Place()
                }
            };
            return View("EditContext", model);
        }
        [HttpGet]
        [Authorize(Roles = Permissions.EditContext)]
        public ActionResult EditContext(string artefactGuid, string contextGuid)
        {
            var node = artefactService.FindContext(contextGuid);
            var model = new EditContextModel
            {
                Node = node
            };
            return View("EditContext", model);
        }
        [HttpPost]
        [Authorize(Roles = Permissions.EditContext)]
        public ActionResult EditContext(EditContextModel model)
        {
            if (ModelState.IsValid)
            {
                artefactService.SaveOrUpdate(model.Node);
                return ToViewDetail(model.Node.Artefact.Guid);
            }
            return View("EditContext", model);
        }
        [HttpGet]
        [Authorize(Roles = Permissions.DeleteContext)]
        public ActionResult DeleteContext(string contextGuid, string artefactGuid)
        {
            artefactService.DeleteContext(contextGuid);
            return ToViewDetail(artefactGuid);
        }
        [HttpGet]
        [Authorize(Roles = Permissions.AddArtefactRelation)]
        public ActionResult AddManagedBy(string artefactGuid)
        {
            return AddRelation<Agent>(artefactGuid, "EditManagedBy", "work was managed by ...");
        }
        [HttpGet]
        [Authorize(Roles = Permissions.EditArtefactRelation)]
        public ActionResult EditManagedBy(string artefactGuid, string hasRelationGuid)
        {
            return FindRelation<Agent>(artefactGuid, hasRelationGuid, "EditManagedBy", "work was managed by ...", artefactService.FindManagedBy);
        }
        [HttpPost]
        [Authorize(Roles = Permissions.EditArtefactRelation)]
        public ActionResult EditManagedBy(GenericRelationModel<Artefact, Agent> model)
        {
            return SaveRelation<Agent>(model, artefactService.ManagedBy);
        }
        [HttpGet]
        [Authorize(Roles = Permissions.AddArtefactRelation)]
        public ActionResult AddDesignedBy(string artefactGuid)
        {
            return AddRelation<Agent>(artefactGuid, "EditDesignedBy", "was designed by ...");
        }
        [HttpGet]
        public ActionResult EditDesignedBy(string artefactGuid, string hasRelationGuid)
        {
            return FindRelation<Agent>(artefactGuid, hasRelationGuid, "EditDesignedBy", "was designed by ...", artefactService.FindDesignedBy);
        }
        [HttpPost]
        public ActionResult EditDesignedBy(GenericRelationModel<Artefact, Agent> model)
        {
            return SaveRelation<Agent>(model, artefactService.DesignedBy);
        }
        [HttpGet]
        [Authorize(Roles = Permissions.AddArtefactRelation)]
        public ActionResult AddRepresentativeOf(string artefactGuid)
        {
            return AddRelation<Category>(artefactGuid, "EditRepresentativeOf", "is representative of ...");
        }
        [HttpGet]
        public ActionResult EditRepresentativeOf(string artefactGuid, string hasRelationGuid)
        {
            return FindRelation<Category>(artefactGuid, hasRelationGuid, "EditRepresentativeOf", "is representative of ...", artefactService.FindRepresentativeOf);
        }
        [HttpPost]
        public ActionResult EditRepresentativeOf(GenericRelationModel<Artefact, Category> model)
        {
            return SaveRelation<Category>(model, artefactService.RepresentativeOf);
        }
        [HttpGet]
        [Authorize(Roles = Permissions.AddArtefactRelation)]
        public ActionResult AddScoredFor(string artefactGuid)
        {
            return AddRelation<Category>(artefactGuid, "EditScoredFor", "is scored for ...", Label.Categories.MusicalInstrument);
        }
        [HttpGet]
        public ActionResult EditScoredFor(string artefactGuid, string hasRelationGuid)
        {
            return FindRelation<Category>(artefactGuid, hasRelationGuid, "EditScoredFor", "is scored for ...", artefactService.FindScoredFor, Label.Categories.MusicalInstrument);
        }
        [HttpPost]
        public ActionResult EditScoredFor(GenericRelationModel<Artefact, Category> model)
        {
            return SaveRelation<Category>(model, artefactService.ScoredFor, Label.Categories.MusicalInstrument);
        }
        [HttpGet]
        [Authorize(Roles = Permissions.AddArtefactRelation)]
        public ActionResult AddHasPhysicalPropertyOf(string artefactGuid)
        {
            return AddRelation<Category>(artefactGuid, "EditHasPhysicalPropertyOf", " has the physical property of being ...", Label.Categories.PhysicalProperty);
        }
        [HttpGet]
        public ActionResult EditHasPhysicalPropertyOf(string artefactGuid, string hasRelationGuid)
        {
            return FindRelation<Category>(artefactGuid, hasRelationGuid, "EditHasPhysicalPropertyOf", " has the physical property of being ...", artefactService.FindHasPhysicalPropertyOf, Label.Categories.PhysicalProperty);
        }
        [HttpPost]
        public ActionResult EditHasPhysicalPropertyOf(GenericRelationModel<Artefact, Category> model)
        {
            return SaveRelation<Category>(model, artefactService.HasPhysicalPropertyOf, Label.Categories.PhysicalProperty);
        }

        [HttpGet]
        public ActionResult AddHasGenre(string artefactGuid)
        {
            return AddRelation<Category>(artefactGuid, "EditHasGenre", "belongs to the genre of ...", Label.Categories.ArtisticGenre);
        }
        [HttpGet]
        public ActionResult EditHasGenre(string artefactGuid, string hasRelationGuid)
        {
            return FindRelation<Category>(artefactGuid, hasRelationGuid, "EditHasGenre", "belongs to the genre of ...", artefactService.FindHasGenre, Label.Categories.ArtisticGenre);
        }
        [HttpPost]
        public ActionResult EditHasGenre(GenericRelationModel<Artefact, Category> model)
        {
            return SaveRelation<Category>(model, artefactService.HasGenre, Label.Categories.ArtisticGenre);
        }



        [HttpGet]
        public ActionResult AddSimilarTo(string artefactGuid)
        {
            return AddRelation<Artefact>(artefactGuid, "EditSimilarTo", "looks likes ...");
        }
        [HttpGet]
        public ActionResult EditSimilarTo(string artefactGuid, string hasRelationGuid)
        {
            return FindRelation<Artefact>(artefactGuid, hasRelationGuid, "EditSimilarTo", "looks like ...", artefactService.FindSimilarTo);
        }
        [HttpPost]
        public ActionResult EditSimilarTo(ArtefactToArtefactRelationModel model)
        {
            return SaveRelation<Artefact>(model, artefactService.SimilarTo);
        }
        [HttpGet]
        public ActionResult AddInfluenceOn(string artefactGuid)
        {
            return AddRelation<Artefact>(artefactGuid, "EditInfluenceOn", "influenced ...");
        }
        [HttpGet]
        public ActionResult EditInfluenceOn(string artefactGuid, string hasRelationGuid)
        {
            return FindRelation<Artefact>(artefactGuid, hasRelationGuid, "EditInfluenceOn", "influenced ...", artefactService.FindInfluenceOn);
        }
        [HttpPost]
        public ActionResult EditInfluenceOn(ArtefactToArtefactRelationModel model)
        {
            return SaveRelation<Artefact>(model, artefactService.InfluenceOn);
        }

        [HttpGet]
        public ActionResult AddCopyOf(string artefactGuid)
        {
            return AddRelation<Artefact>(artefactGuid, "EditCopyOf", "is a copy of ...");
        }
        [HttpGet]
        public ActionResult EditCopyOf(string artefactGuid, string hasRelationGuid)
        {
            return FindRelation<Artefact>(artefactGuid, hasRelationGuid, "EditCopyOf", "is a copy of ...", artefactService.FindCopyOf);
        }
        [HttpPost]
        public ActionResult EditCopyOf(ArtefactToArtefactRelationModel model)
        {
            return SaveRelation<Artefact>(model, artefactService.CopyOf);
        }
        [HttpGet]
        public ActionResult AddStudyFor(string artefactGuid)
        {
            return AddRelation<Artefact>(artefactGuid, "EditStudyFor", "is a study for ...");
        }
        [HttpGet]
        public ActionResult EditStudyFor(string artefactGuid, string hasRelationGuid)
        {
            return FindRelation<Artefact>(artefactGuid, hasRelationGuid, "EditStudyFor", "is a study for ...", artefactService.FindStudyFor);
        }
        [HttpPost]
        public ActionResult EditStudyFor(ArtefactToArtefactRelationModel model)
        {
            return SaveRelation<Artefact>(model, artefactService.StudyFor);
        }
        /**
         * Work in progress...
         */
        [HttpGet]
        public ActionResult AddRepresents(string artefactGuid)
        {
            return AddRelation<Entity>(artefactGuid, "EditRepresents", "represents ...");
        }
        [HttpGet]
        public ActionResult EditRepresents(string artefactGuid, string hasRelationGuid)
        {
            return FindRelation<Entity>(artefactGuid, hasRelationGuid, "EditRepresents", "represents ...", artefactService.FindRepresents);
        }
        [HttpPost]
        public ActionResult EditRepresents(GenericRelationModel<Artefact, Entity> model)
        {
            return SaveEntityRelation(model, artefactService.Represents);
            // return SaveRelation<Entity>(model, artefactService.Represents);
        }
        [HttpGet]
        public ActionResult AddPartOf(string artefactGuid)
        {
            return AddBasicRelation<Artefact>(artefactGuid, "EditPartOf", "is part of ...");
        }
        [HttpGet]
        public ActionResult EditPartOf(string artefactGuid, string hasRelationGuid)
        {
            return FindBasicRelation<Artefact>(artefactGuid, hasRelationGuid, "EditPartOf", "is part of ...", artefactService.FindPartOf);
        }
        [HttpPost]
        public ActionResult EditPartOf(GenericRelationNodeModel<Artefact, Artefact> model)
        {
            return SaveBasicRelation<Artefact>(model, artefactService.PartOf);
        }
        [HttpGet]
        public ActionResult AddHasFormalQuality(string artefactGuid)
        {
            return AddRelation<Category>(artefactGuid, "EditHasFormalQuality", "has the formal quality of ...", Label.Categories.AestheticConvention);
        }
        [HttpGet]
        public ActionResult EditHasFormalQuality(string artefactGuid, string hasRelationGuid)
        {
            return FindRelation<Category>(artefactGuid, hasRelationGuid, "EditHasFormalQuality", "has the formal quality of ...", artefactService.FindUses, Label.Categories.AestheticConvention);
        }
        [HttpPost]
        public ActionResult EditHasFormalQuality(ArtefactToCategoryRelationModel model)
        {
            return SaveRelation<Category>(model, artefactService.Uses, Label.Categories.AestheticConvention);
        }
        [HttpGet]
        public ActionResult AddHasSubject(string artefactGuid)
        {
            return AddRelation<Category>(artefactGuid, "EditHasSubject", "has the subject ...", Label.Categories.ArtisticSubject);
        }
        [HttpGet]
        public ActionResult EditHasSubject(string artefactGuid, string hasRelationGuid)
        {
            return FindRelation<Category>(artefactGuid, hasRelationGuid, "EditHasSubject", "has the subject ...", artefactService.FindArtefactHasSubject, Label.Categories.ArtisticSubject);
        }
        [HttpPost]
        public ActionResult EditHasSubject(GenericRelationModel<Artefact, Category> model)
        {
            return SaveRelation<Category>(model, artefactService.ArtefactHasSubject, Label.Categories.ArtisticSubject);
        }
        [HttpGet]
        public ActionResult AddHasMotif(string artefactGuid)
        {
            return AddRelation<Category>(artefactGuid, "EditHasMotif", "has a motif ...", Label.Categories.Motif);
        }
        [HttpGet]
        public ActionResult EditHasMotif(string artefactGuid, string hasRelationGuid)
        {
            return FindRelation<Category>(artefactGuid, hasRelationGuid, "EditHasMotif", "has a motif ...", artefactService.FindHasMotif, Label.Categories.Motif);
        }
        [HttpPost]
        public ActionResult EditHasMotif(GenericRelationModel<Artefact, Category> model)
        {
            return SaveRelation<Category>(model, artefactService.HasMotif, Label.Categories.Motif);
        }
        [HttpGet]
        public ActionResult AddCommissionedBy(string artefactGuid)
        {
            return AddRelation<Agent>(artefactGuid, "EditCommissionedBy", "was commissioned by ...");
        }
        [HttpGet]
        public ActionResult EditCommissionedBy(string artefactGuid, string hasRelationGuid)
        {
            return FindRelation<Agent>(artefactGuid, hasRelationGuid, "EditCommissionedBy", "was commissioned by ...", artefactService.FindCommissionedBy);
        }
        [HttpPost]
        public ActionResult EditCommissionedBy(ArtefactToAgentRelationModel model)
        {
            return SaveRelation<Agent>(model, artefactService.CommissionedBy);
        }
        [HttpGet]
        public ActionResult AddPublishedBy(string artefactGuid)
        {
            return AddRelation<Agent>(artefactGuid, "EditPublishedBy", "was published by ...");
        }
        [HttpGet]
        public ActionResult EditPublishedBy(string artefactGuid, string hasRelationGuid)
        {
            return FindRelation<Agent>(artefactGuid, hasRelationGuid, "EditPublishedBy", "was published by ...", artefactService.FindPublishedBy);
        }
        [HttpPost]
        public ActionResult EditPublishedBy(ArtefactToAgentRelationModel model)
        {
            return SaveRelation<Agent>(model, artefactService.PublishedBy);
        }
        [HttpGet]
        public ActionResult AddMadeWith(string artefactGuid)
        {
            return AddRelation<Category>(artefactGuid, "EditMadeWith", "was made with ...", Label.Categories.Implement);
        }
        [HttpGet]
        public ActionResult EditMadeWith(string artefactGuid, string hasRelationGuid)
        {
            return FindRelation<Category>(artefactGuid, hasRelationGuid, "EditMadeWith", "was made with ...", artefactService.FindMadeWith, Label.Categories.Implement);
        }
        [HttpPost]
        public ActionResult EditMadeWith(ArtefactToCategoryRelationModel model)
        {
            return SaveRelation<Category>(model, artefactService.MadeWith, Label.Categories.Implement);
        }
        [HttpGet]
        public ActionResult AddMadeFrom(string artefactGuid)
        {
            return AddRelation<Category>(artefactGuid, "EditMadeFrom", "is made from ...", Label.Categories.Substance);
        }
        [HttpGet]
        public ActionResult EditMadeFrom(string artefactGuid, string hasRelationGuid)
        {
            return FindRelation<Category>(artefactGuid, hasRelationGuid, "EditMadeFrom", "is made from ...", artefactService.FindMadeFrom, Label.Categories.Substance);
        }
        [HttpPost]
        public ActionResult EditMadeFrom(ArtefactToCategoryRelationModel model)
        {
            return SaveRelation<Category>(model, artefactService.MadeFrom, Label.Categories.Substance);
        }


        private ActionResult NewEntityRelation(string artefactGuid, string action, string title)
        {
            var agent = artefactService.FindArtefactByGuid(artefactGuid);
            var model = new ArtefactToEntityRelationModel
            {
                Action = action,
                Title = agent.Name + " " + title,
                Node = new ArtefactToEntityRelationHyperNode
                {
                    Source = agent,
                    Relation = new Relation()
                }
            };
            return View("EditRelatedToEntity", model);
        }

        private ActionResult FindEntityRelation(string artefactGuid, string hasRelationGuid, string action, string title, Func<string, string, ArtefactToEntityRelationHyperNode> getter)
        {
            var node = getter(artefactGuid, hasRelationGuid);
            var model = new ArtefactToEntityRelationModel
            {
                Action = action,
                Title = node.Source.Name + " " + title,
                Node = node,
                Relation = node.Relation
            };
            model.OtherArtefactGuid = null;
            SetGuid(model, node.DestinationType, node.Destination.Guid);
            return View("EditRelatedToEntity", model);
        }
        private void SetGuid(ArtefactToEntityRelationModel model, string entityType, string entityGuid)
        {
            if (entityType == Label.Entity.Agent)
            {
                model.AgentGuid = entityGuid;
            }
            else if (entityType == Label.Entity.Category)
            {
                model.CategoryGuid = entityGuid;
            }
            else if (entityType == Label.Entity.Artefact)
            {
                model.OtherArtefactGuid = entityGuid;
            }
            else if (entityType == Label.Entity.Place)
            {
                model.PlaceGuid = entityGuid;
            }
        }

        private string GetGuid(GenericRelationModel<Artefact, Entity> model)
        {
            if (false == string.IsNullOrEmpty(model.AgentGuid))
            {
                return model.AgentGuid;
            }
            if (false == string.IsNullOrEmpty(model.PlaceGuid))
            {
                return model.PlaceGuid;
            }
            if (false == string.IsNullOrEmpty(model.CategoryGuid))
            {
                return model.CategoryGuid;
            }
            if (false == string.IsNullOrEmpty(model.OtherArtefactGuid))
            {
                return model.OtherArtefactGuid;
            }
            return null;
        }
        private ActionResult SaveEntityRelation(GenericRelationModel<Artefact, Entity> model, Action<ArtefactToEntityRelationHyperNode> updater)
        {
            if (ModelState.IsValid)
            {
                var guid = GetGuid(model);
                var node = new ArtefactToEntityRelationHyperNode
                {
                    Source = model.Node.Source,
                    HasRelationGuid = model.Node.HasRelationGuid,
                    RelationToDestinationGuid = model.Node.RelationToDestinationGuid,
                    Relation = model.Relation,
                    Destination = new Entity { Guid = guid }
                };
                updater(node);
                return ToViewDetail(model.Node.Source.Guid);
            }
            model.Node.Source = artefactService.FindArtefactByGuid(model.Node.Source.Guid);
            return View("EditRelatedToEntity", model);
        }

        [HttpGet]
        public ActionResult AddOwnedBy(string artefactGuid)
        {
            return AddRelation<Agent>(artefactGuid, "EditOwnedBy", "is owned by ...");
        }
        [HttpGet]
        public ActionResult EditOwnedBy(string artefactGuid, string hasRelationGuid)
        {
            return FindRelation<Agent>(artefactGuid, hasRelationGuid, "EditOwnedBy", "is owned by ...", artefactService.FindOwnedBy);
        }
        [HttpPost]
        public ActionResult EditOwnedBy(GenericRelationModel<Artefact, Agent> model)
        {
            return SaveRelation<Agent>(model, artefactService.OwnedBy);
        }
        [HttpGet]
        public ActionResult AddAttributedTo(string artefactGuid)
        {
            return AddRelation<Agent>(artefactGuid, "EditAttributedTo", "was made by ...");
        }
        [HttpGet]
        public ActionResult EditAttributedTo(string artefactGuid, string hasRelationGuid)
        {
            return FindRelation<Agent>(artefactGuid, hasRelationGuid, "EditAttributedTo", "was made by  ...", artefactService.FindAttributedTo);
        }
        [HttpPost]
        public ActionResult EditAttributedTo(ArtefactToAgentRelationModel model)
        {
            return SaveRelation<Agent>(model, artefactService.AttributedTo);
        }
        [HttpGet]
        public ActionResult AddMadeBy(string artefactGuid)
        {
            return AddRelation<Agent>(artefactGuid, "EditMadeBy", "was made by ...");
        }
        [HttpGet]
        public ActionResult EditMadeBy(string artefactGuid, string hasRelationGuid)
        {
            return FindRelation<Agent>(artefactGuid, hasRelationGuid, "EditMadeBy", "was made by  ...", artefactService.FindMadeBy);
        }
        [HttpPost]
        public ActionResult EditMadeBy(ArtefactToAgentRelationModel model)
        {
            return SaveRelation<Agent>(model, artefactService.MadeBy);
        }
        [HttpGet]
        public ActionResult AddAlias(string artefactGuid)
        {
            var artefact = artefactService.FindArtefactByGuid(artefactGuid);
            var model = new EditArtefactAlias
            {
                Artefact = artefact
            };
            return View("EditAlias", model);
        }
        [HttpPost]
        public ActionResult EditAlias(EditArtefactAlias model)
        {
            artefactService.AddAlias(model.Artefact.Guid, model.Alias);
            return ToViewDetail(model.Artefact.Guid);
        }
        [HttpGet]
        public ActionResult List()
        {
            var all = artefactService.AllMadeBy().OrderBy(x => x.Artefact.Name).ToList();
            return View(all);
        }
        private List<ArtefactMadeByAgent> GetList()
        {
            var key = CacheKey.ArtefactService.AllMadeBy;
            if (GetFromCache<List<ArtefactMadeByAgent>>(key) == null)
            {
                AddToCache(key, artefactService.AllMadeBy().OrderBy(x => x.Artefact.Name).ToList());
            }
            return GetFromCache<List<ArtefactMadeByAgent>>(key);
        }
        [HttpGet]
        public ActionResult ViewPublic(string guid)
        {
            var details = artefactService.FindArtefactPublicOld(guid);
            var madeBy = artefactService.FindMadeBy(guid);
            var model = new ViewPublicArtefact
            {
                Details = details,
                MadeBy = madeBy
            };
            return View(model);
        }

        [HttpGet]
        public ActionResult AddDetail(string artefactGuid)
        {
            var empty = Enumerable.Range(1, 7).Select(x => (string)null).ToList();
            var model = new EditDetailModel
            {
                Artefact = artefactService.FindArtefactByGuid(artefactGuid),
                OpenDayStart = empty,
                OpenDayEnd = empty
            };
            return View("EditDetail", model);
        }
        [HttpGet]
        public ActionResult EditDetail(string detailGuid, string artefactGuid)
        {
            var artefact = artefactService.FindArtefactByGuid(artefactGuid);
            var detail = artefactService.FindDetail(detailGuid);
            var model = new EditDetailModel
            {
                Artefact = artefact,
                Address = detail.Address,
                AffiliationCode = detail.AffiliationCode,
                DetailGuid = detail.Guid,
                Directions = detail.Directions,
                Email = detail.Email,
                OpenDayEnd = detail.OpenDayEnd.ToList(),
                OpenDayStart = detail.OpenDayStart.ToList(),
                Phone = detail.Phone,
                PurchaseTicketUrl = detail.PurchaseTicketUrl,
                Website = detail.Website
            };
            return View("EditDetail", model);
        }
        [HttpPost]
        public ActionResult EditDetail(EditDetailModel model)
        {
            if (ModelState.IsValid)
            {
                var isNew = model.DetailGuid == null;
                var detail = false == isNew ? artefactService.FindDetail(model.DetailGuid) : new CollectionDetail();
                detail.Address = model.Address;
                detail.OpenDayEnd = model.OpenDayEnd.ToArray();
                detail.OpenDayStart = model.OpenDayStart.ToArray();
                detail.Address = model.Address;
                detail.Directions = model.Directions;
                detail.Email = model.Email;
                detail.Website = model.Website;
                detail.Phone = model.Phone;
                detail.PurchaseTicketUrl = model.PurchaseTicketUrl;
                detail.AffiliationCode = model.AffiliationCode;
                artefactService.SaveOrUpdate(detail);
                if (isNew)
                {
                    artefactService.HasDetail(model.Artefact.Guid, detail.Guid);
                }
                return ToViewDetail(model.Artefact.Guid);
            }
            return View(model);
        }
        public ActionResult AddBook()
        {
            var model = new AddBookModel
            {
                Range = Range.Continuous
            };
            return View("AddBook", model);
        }
        public ActionResult AddArtwork()
        {
            var model = new AddArtworkModel
            {
                PublicDomain = true,
                Range = Range.Continuous
            };
            return View("AddArtwork", model);
        }
        public ActionResult AddMusic()
        {
            var model = new AddMusicModel();
            return View("AddMusic", model);
        }
        [HttpGet]
        public ActionResult Edit(string artefactGuid)
        {
            var artefact = artefactService.FindArtefactByGuid(artefactGuid);
            return View("Edit", new EditArtefactModel
            {
                ArtefactGuid = artefactGuid,
                Ordinal = artefact.Ordinal,
                Name = artefact.Name,
                CatalogueIdentPrefix = artefact.CatalogueIdentPrefix,
                CatalogueIdent = artefact.CatalogueIdent,
                Article = artefact.Article,
                Rating = artefact.Rating,
                Description = artefact.Description,
                Subtitle = artefact.Subtitle,
                Width = artefact.Width,
                Height = artefact.Height,
                Depth = artefact.Depth,
                Latitude = artefact.Latitude,
                Longitude = artefact.Longitude
            });
        }
        [HttpPost]
        public ActionResult Edit(EditArtefactModel model)
        {
            if (ModelState.IsValid)
            {
                var artefact = artefactService.FindArtefactByGuid(model.ArtefactGuid);
                artefact.Name = model.Name;
                artefact.Ordinal = model.Ordinal;
                artefact.CatalogueIdent = model.CatalogueIdent;
                artefact.CatalogueIdentPrefix = model.CatalogueIdentPrefix;
                artefact.Article = model.Article;
                artefact.Description = model.Description;
                artefact.Rating = model.Rating;
                artefact.Subtitle = model.Subtitle;
                artefact.Width = model.Width;
                artefact.Height = model.Height;
                artefact.Depth = model.Depth;
                artefact.Latitude = model.Latitude;
                artefact.Longitude = model.Longitude;
                artefactService.SaveOrUpdate(artefact);
                return ToViewDetail(model.ArtefactGuid);
            }
            return View(model);
        }
        [HttpPost]
        public ActionResult AddMusic(AddMusicModel model)
        {
            if (ModelState.IsValid)
            {
                var artefact = new Artefact();
                artefact.Name = model.Name;
                artefact.Article = model.Article;
                artefact.Description = model.Description;
                artefact.Subtitle = model.Subtitle;
                artefact.CatalogueIdent = model.CatalogueIdent;
                artefactService.SaveOrUpdate(artefact);
                if (false == string.IsNullOrEmpty(model.CategoryGuid))
                {
                    var category = personService.FindCategoryByGuid(model.CategoryGuid);
                    artefactService.IsA(artefact, category);
                }
                if (false == string.IsNullOrEmpty(model.MadeByGuid))
                {
                    artefactService.MadeBy(new GenericRelationHyperNode<Artefact, Agent>
                    {
                        Source = artefact,
                        Relation = new Relation(),
                        Destination = new Agent { Guid = model.MadeByGuid }
                    });
                }
                if (false == string.IsNullOrEmpty(model.ScoredFor1Guid))
                {
                    artefactService.ScoredFor(new GenericRelationHyperNode<Artefact, Category>
                    {
                        Source = artefact,
                        Relation = new Relation(),
                        Destination = new Category { Guid = model.ScoredFor1Guid }
                    });
                }
                if (false == string.IsNullOrEmpty(model.ScoredFor2Guid))
                {
                    artefactService.ScoredFor(new GenericRelationHyperNode<Artefact, Category>
                    {
                        Source = artefact,
                        Relation = new Relation(),
                        Destination = new Category { Guid = model.ScoredFor2Guid }
                    });
                }
                if (false == string.IsNullOrEmpty(model.ScoredFor3Guid))
                {
                    artefactService.ScoredFor(new GenericRelationHyperNode<Artefact, Category>
                    {
                        Source = artefact,
                        Relation = new Relation(),
                        Destination = new Category { Guid = model.ScoredFor3Guid }
                    });
                }
                if (false == string.IsNullOrEmpty(model.ScoredFor4Guid))
                {
                    artefactService.ScoredFor(new GenericRelationHyperNode<Artefact, Category>
                    {
                        Source = artefact,
                        Relation = new Relation(),
                        Destination = new Category { Guid = model.ScoredFor4Guid }
                    });
                }
                GeneralMessage = string.Format("Created place '{0}'.", model.Name);
                return ToViewDetail(artefact.Guid);
            }
            return View(model);
        }
        private string GetProximity(string date)
        {
            var proximity = Proximity.On;
            if (false == string.IsNullOrWhiteSpace(date))
            {
                date = date.ToLower();
                if (date.Contains("before"))
                {
                    proximity = Proximity.Before;
                }
                if (date.Contains("after"))
                {
                    proximity = Proximity.After;
                }
                var circa = new string[] { "around", "circa", "c.", "c" };
                if (circa.Any(x => date.Contains(x)))
                {
                    proximity = Proximity.Around;
                }
            }
            return RazorHelper.FromProximity(proximity);
        }
        private int? GetYear(string date)
        {
            if (string.IsNullOrWhiteSpace(date))
            {
                return (int?)null;
            }
            var yearDigits = new String(date.Where(Char.IsDigit).ToArray());
            int year = 0;
            return int.TryParse(yearDigits, out year) ? year : (int?)null;
        }
        private string GetMilennium(string date)
        {
            var milennium = "AD";
            if (false == string.IsNullOrWhiteSpace(date))
            {
                if (date.Contains("BC"))
                {
                    milennium = "BC";
                }
            }
            return milennium;
        }
        private Relation GetMadeByDates(AddArtworkModel model)
        {
            var start = model.YearMadeStart;
            var end = model.YearMadeEnd;
            var result = new Relation
            {
                Primary = true,
                Range = RazorHelper.FromRange(model.Range),
                Proximity = GetProximity(start),
                Milennium = GetMilennium(start),
                Year = GetYear(start),
                ProximityEnd = GetProximity(end),
                MilenniumEnd = GetMilennium(end),
                YearEnd = GetYear(end)
            };
            return result;
        }

        [HttpPost]
        public ActionResult AddBook(AddBookModel model)
        {
            if (ModelState.IsValid && false == model.Refresh)
            {
                var artefact = new Artefact();
                artefact.Created = DateTimeOffset.Now;
                artefact.Name = model.Name;
                artefact.Article = model.Article;
                artefact.Description = model.Description;
                artefact.Subtitle = model.Subtitle;
                artefact.Rating = model.Rating;
                artefactService.SaveOrUpdate(artefact);
                if (false == string.IsNullOrEmpty(model.CategoryGuid))
                {
                    artefactService.IsA(new GenericRelationHyperNode<Artefact, Category>
                    {
                        Source = artefact,
                        Relation = new Relation
                        {
                            Primary = true
                        },
                        Destination = new Category
                        {
                            Guid = model.CategoryGuid
                        }
                    });
                }
                if (false == string.IsNullOrEmpty(model.AuthorOfGuid))
                {
                    artefactService.MadeBy(new GenericRelationHyperNode<Artefact, Agent>
                    {
                        Source = artefact,
                        Relation = new Relation
                        {
                            Year = false == string.IsNullOrEmpty(model.YearMadeStart) ? int.Parse(model.YearMadeStart) : (int?)null,
                        },
                        Destination = new Agent
                        {
                            Guid = model.AuthorOfGuid
                        }
                    });
                }
                if (false == string.IsNullOrEmpty(model.PublishedByGuid))
                {
                    artefactService.PublishedBy(new GenericRelationHyperNode<Artefact, Agent>
                    {
                        Source = artefact,
                        Relation = new Relation
                        {
                            Primary = true
                        },
                        Destination = new Agent
                        {
                            Guid = model.PublishedByGuid
                        }
                    });
                }
                if (false == string.IsNullOrEmpty(model.TranslatedByGuid))
                {
                    artefactService.TranslatedBy(new GenericRelationHyperNode<Artefact, Agent>
                    {
                        Source = artefact,
                        Relation = new Relation
                        {
                            Primary = false
                        },
                        Destination = new Agent
                        {
                            Guid = model.TranslatedByGuid
                        }
                    });
                }
                if (model.Clone)
                {
                    ScrubModel(model);
                    return View(model);
                }
                GeneralMessage = string.Format("Created book '{0}'.", model.Name);
                return ToViewDetail(artefact.Guid);
            }
            ModelState.Remove("Refresh");
            model.Refresh = false;
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> AddArtwork(AddArtworkModel model)
        {
            if (ModelState.IsValid && false == model.Refresh)
            {
                var artefact = new Artefact();
                artefact.Created = DateTimeOffset.Now;
                artefact.Name = model.Name;
                artefact.Ordinal = model.Ordinal;
                artefact.Article = model.Article;
                artefact.Description = model.Description;
                artefact.Subtitle = model.Subtitle;
                artefact.Width = model.Width;
                artefact.Height = model.Height;
                artefact.Depth = model.Depth;
                artefact.Rating = model.Rating;
                artefactService.SaveOrUpdate(artefact);
                await SaveArtefactImage(model, artefact);
                if (false == string.IsNullOrEmpty(model.CategoryGuid))
                {
                    artefactService.IsA(new GenericRelationHyperNode<Artefact, Category>
                    {
                        Source = artefact,
                        Relation = new Relation
                        {
                            Primary = true
                        },
                        Destination = new Category
                        {
                            Guid = model.CategoryGuid
                        }
                    });
                }
                if (false == string.IsNullOrEmpty(model.MadeByGuid))
                {
                    artefactService.MadeBy(new GenericRelationHyperNode<Artefact, Agent>
                    {
                        Source = artefact,
                        Relation = GetMadeByDates(model),
                        Destination = new Agent
                        {
                            Guid = model.MadeByGuid
                        }
                    });
                }
                if (false == string.IsNullOrEmpty(model.MadeFromFirstGuid))
                {
                    artefactService.MadeFrom(new GenericRelationHyperNode<Artefact, Category>
                    {
                        Source = artefact,
                        Relation = new Relation
                        {
                            Primary = true
                        },
                        Destination = new Category
                        {
                            Guid = model.MadeFromFirstGuid
                        }
                    });
                }
                if (false == string.IsNullOrEmpty(model.MadeFromSecondGuid))
                {
                    artefactService.MadeFrom(new GenericRelationHyperNode<Artefact, Category>
                    {
                        Source = artefact,
                        Relation = new Relation
                        {
                            Primary = false
                        },
                        Destination = new Category
                        {
                            Guid = model.MadeFromSecondGuid
                        }
                    });
                }
                if (false == string.IsNullOrEmpty(model.SubjectGuid))
                {
                    artefactService.ArtefactHasSubject(new GenericRelationHyperNode<Artefact, Category>
                    {
                        Source = artefact,
                        Relation = new Relation
                        {
                            Primary = true
                        },
                        Destination = new Category { Guid = model.SubjectGuid }
                    });
                }
                if (false == string.IsNullOrEmpty(model.GenreGuid))
                {
                    artefactService.HasGenre(new GenericRelationHyperNode<Artefact, Category>
                    {
                        Source = artefact,
                        Relation = new Relation
                        {
                            Primary = true
                        },
                        Destination = new Category { Guid = model.GenreGuid }
                    });
                }
                if (false == string.IsNullOrEmpty(model.LocatedInGuid))
                {
                    artefactService.LocatedIn(new GenericRelationNode<Artefact, Artefact>
                    {
                        Source = artefact,
                        Destination = new Artefact { Guid = model.LocatedInGuid }
                    });
                }
                if (false == string.IsNullOrEmpty(model.Represents1Guid))
                {
                    artefactService.Represents(new GenericRelationHyperNode<Artefact, Entity>
                    {
                        Source = artefact,
                        Relation = new Relation
                        {
                            Primary = true
                        },
                        Destination = new Entity { Guid = model.Represents1Guid }
                    });
                }
                if (false == string.IsNullOrEmpty(model.Represents2Guid))
                {
                    artefactService.Represents(new GenericRelationHyperNode<Artefact, Entity>
                    {
                        Source = artefact,
                        Relation = new Relation
                        {
                            Primary = true
                        },
                        Destination = new Entity { Guid = model.Represents2Guid }
                    });
                }
                //if (false == string.IsNullOrEmpty(model.Represents3Guid))
                //{
                //    artefactService.Represents(new GenericRelationHyperNode<Artefact, Entity>
                //    {
                //        Source = artefact,
                //        Relation = new Relation
                //        {
                //            Primary = true
                //        },
                //        Destination = new Entity { Guid = model.Represents3Guid }
                //    });
                //}
                if (model.Clone)
                {
                    ScrubModel(model);
                    return View(model);
                }
                GeneralMessage = string.Format("Created artwork '{0}'.", model.Name);
                return ToViewDetail(artefact.Guid);
            }
            ModelState.Remove("Refresh");
            model.Refresh = false;
            return View(model);
        }

        private void ScrubModel(AddBookModel model)
        {
            ModelState.Remove("Article");
            ModelState.Remove("Name");
            ModelState.Remove("Rating");
            ModelState.Remove("YearMadeStart");
            ModelState.Remove("YearMadeEnd");
            ModelState.Remove("Description");
            ModelState.Remove("AuthorOfGuid");
            ModelState.Remove("TranslatedByGuid");
            ModelState.Remove("PublishedByGuid");
            model.Article = null;
            model.Description = null;
            model.AuthorOfGuid = null;
            model.TranslatedByGuid = null;
            model.PublishedByGuid = null;
            model.Name = null;
            model.Rating = null;
            model.YearMadeEnd = null;
            model.YearMadeStart = null;
            model.Range = Range.Continuous;
        }

        private void ScrubModel(AddArtworkModel model)
        {
            ModelState.Remove("Article");
            ModelState.Remove("Name");
            ModelState.Remove("SubjectGuid");
            ModelState.Remove("Represents1Guid");
            ModelState.Remove("Represents2Guid");
            //ModelState.Remove("Represents3Guid");
            ModelState.Remove("Rating");
            ModelState.Remove("Ordinal");
            ModelState.Remove("ImageUrl");
            ModelState.Remove("LocatedInGuid");
            //ModelState.Remove("Clone");
            ModelState.Remove("YearMadeStart");
            ModelState.Remove("YearMadeEnd");
            ModelState.Remove("Height");
            ModelState.Remove("Width");
            ModelState.Remove("Depth");
            ModelState.Remove("SubjectGuid");
            ModelState.Remove("GenreGuid");
            ModelState.Remove("Range");
            model.Article = null;
            model.Name = null;
            model.SubjectGuid = null;
            model.Represents1Guid = null;
            model.Represents2Guid = null;
            //model.Represents3Guid = null;
            model.Rating = null;
            model.Ordinal = null;
            model.ImageUrl = null;
            model.LocatedInGuid = null;
            //model.Clone = false;
            model.YearMadeEnd = null;
            model.YearMadeStart = null;
            model.Height = null;
            model.Width = null;
            model.Depth = null;
            model.SubjectGuid = null;
            model.GenreGuid = null;
            model.Range = Range.Continuous;
        }

        [HttpGet]
        [Authorize(Roles = Permissions.ViewArtefact)]
        public async Task<ActionResult> ViewDetail(string guid)
        {
            if (guid == null)
            {
                return new HttpNotFoundResult();
            }
            var node = new ViewArtefact();
            node.Artefact = artefactService.FindArtefactByGuid(guid);
            if (node.Artefact == null)
            {
                return new HttpNotFoundResult();
            }
            node.Detail = artefactService.FindDetailForArtefact(guid);
            node.NewActs = await actService.FindNewActsByArtefact(guid);
            node.Contexts = artefactService.FindContextsByArtefact(guid);
            node.OutgoingRelations = commonService.FindOutgoingRelations<Artefact>(guid);
            node.IncomingRelations = commonService.FindIncomingRelations<Artefact>(guid);
            node.BasicOutgoingRelations = commonService.FindBasicOutgoingRelations<Artefact>(guid);
            node.BasicIncomingRelations = commonService.FindBasicIncomingRelations<Artefact>(guid);
            node.IsA = GetDestinationByRelationshipType<Category>(node.OutgoingRelations, Label.Relationship.IsA);
            node.MadeFrom = GetDestinationByRelationshipType<Category>(node.OutgoingRelations, Label.Relationship.MadeFrom);
            node.Images = resourceService.FindArtefactImages(guid);
            node.Texts = textService.FindTextsByArtefact(guid);
            return View("ViewDetail", node);
        }
        [HttpGet]
        [Authorize(Roles = Permissions.AddArtefactImage)]
        public ActionResult AddImage(string artworkGuid)
        {
            return View("EditImage", new EditEntityImage
            {
                EntityGuid = artworkGuid
            });
        }
        [HttpGet]
        [Authorize(Roles = Permissions.EditArtefactImage)]
        public ActionResult EditImage(string imageGuid)
        {
            var node = resourceService.FindArtefactImageHyperNode(imageGuid);
            return View("EditImage", new EditEntityImage
            {
                EntityGuid = node.Artefact.Guid,
                ImageNode = node.Image,
                ImageGuid = imageGuid,
                Name = node.Image.Name,
                Description = node.Image.Description,
                Uri = node.Image.Uri,
                Width = node.Image.Width,
                Height = node.Image.Height,
                Source = node.Image.Source,
                SourceUrl = node.Image.SourceUrl,
                PublicDomain = node.Image.PublicDomain ?? false,
                Primary = node.Image.Primary ?? false
            });
        }
        protected override ActionResult ToList()
        {
            return RedirectToAction("List", "Artefact");
        }
        protected override ActionResult ToViewDetail(string guid)
        {
            return RedirectToAction("ViewDetail", "Artefact", new { guid });
        }
        [HttpGet]
        [Authorize(Roles = Permissions.DeleteArtefactImage)]
        public ActionResult DeleteImage(string imageGuid, string artefactGuid)
        {
            resourceService.DeleteArtefactImageHyperNode(imageGuid);
            return ToViewDetail(artefactGuid);
        }
        [HttpPost]
        [Authorize(Roles = Permissions.EditArtefactImage)]
        public async Task<ActionResult> EditImage(EditEntityImage model)
        {
            if (ModelState.IsValid)
            {
                var isNew = model.ImageGuid == null;
                var image = isNew ? new Image() : resourceService.FindImageByGuid(model.ImageGuid);
                image.Name = model.Name;
                image.Description = model.Description;
                if (isNew)
                {
                    image.FileName = Path.GetFileName(model.Image.FileName);
                    image.Extension = GetFileExtension(model.Image);
                }
                image.Primary = model.Primary;
                image.PublicDomain = model.PublicDomain;
                image.Source = model.Source;
                image.SourceUrl = model.SourceUrl;
                resourceService.SaveOrUpdate(image);
                resourceService.HasArtefactImage(model.EntityGuid, image.Guid);
                if (isNew)
                {
                    await SaveFileAsync(model.Image, image.Guid, image.Extension);
                }
                return ToViewDetail(model.EntityGuid);
            }
            return View(model);
        }
    }
}