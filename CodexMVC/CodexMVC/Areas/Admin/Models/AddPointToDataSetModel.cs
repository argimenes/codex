﻿using Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CodexMVC.Areas.Admin.Models
{
    public class DataPointHyperNodeModel : DataPointHyperNode
    {
        public string ArrivedFromDataSetGuid { get; set; }
        public string AddingToDataSetGuid { get; set; }
    }

    public class AddPointToDataSetFromActModel
    {
        public string ActGuid { get; set; }
        public string DataSetGuid { get; set; }
        public string Citation { get; set; }
        public string StatedInGuid { get; set; }
        public string Milennium { get; set; }
        public int? Year { get; set; }
        public int? Month { get; set; }
        public int? Day { get; set; }
        public int? Hour { get; set; }
        public int? Minute { get; set; }
    }
}