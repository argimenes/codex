﻿using Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Data.Entities
{
    public abstract class Resource : Entity
    {
        public bool? Primary { get; set; }
        public string Uri { get; set; }
        public string Source { get; set; }
        public string SourceUrl { get; set; }
        public bool? PublicDomain { get; set; }
    }
}