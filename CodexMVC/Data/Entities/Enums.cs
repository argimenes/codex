﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Entities
{
    public enum Range
    {
        Between = 0,
        Continuous
    }
    public enum Proximity
    {
        On = 0,
        Around,
        Before,
        After,
        OnOrBefore,
        OnOrAfter
    }
}
