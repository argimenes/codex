﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Entities
{
    public class Video : Resource
    {
        public int? Height { get; set; }

        public int? Width { get; set; }

        public string StartTime { get; set; }

        public string EndTime { get; set; }

        /// <summary>
        /// Duration in seconds
        /// </summary>
        public double? Duration { get; set; }

        public Video()
        {
            Label = "Video";
        }
    }
}
