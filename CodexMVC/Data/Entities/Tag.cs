﻿using Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Entities
{
    public class Tag : Entity
    {
        public string ValueType { get; set; }
        public bool? IsTheme { get; set; }
    }
    public class TagValue : Entity
    {
        public string Value { get; set; }
    }
}
