﻿using Neo4jClient;
using Neo4jClient.Cypher;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Entities
{
    //public enum EntityState
    //{
    //    Added = 0,
    //    Modified,
    //    Unchanged,
    //    Deleted
    //}
    //public static class Constants
    //{
    //    public static class EntityState
    //    {
    //        public static string Added { get { return "Added"; } }
    //        public static string Modified { get { return "Modified"; } }
    //        public static string Deleted { get { return "Deleted"; } }
    //        public static string Unchanged { get { return "Unchanged"; } }
    //    }
    //}
    //public class RelationState
    //{
    //    public string Guid { get; set; }
    //    public string EntityState { get; set; }
    //    public RelationState()
    //    {
    //        EntityState = Constants.EntityState.Added;
    //    }
    //}

    public class Property : Entity
    {
        public string Value { get; set; }
    }
    public class AgentAttributeHyperNode
    {
        public Agent Agent { get; set; }
        public string EntityType { get; set; }
        public string Relationship { get; set; }
        public string RelationshipGuid { get; set; }
        public string HasRelationGuid { get; set; }
        public IEntity Entity { get; set; }
    }
    public interface IArtefactAttributeBaseHyperNode
    {
        Artefact Artefact { get; set; }
        Relationship Relationship { get; set; }
        string HasRelationGuid { get; set; }
    }
    public class ArtefactAttributeBaseHyperNode : IArtefactAttributeBaseHyperNode
    {
        public Artefact Artefact { get; set; }
        public Relationship Relationship { get; set; }
        public string HasRelationGuid { get; set; }
    }
    public class ArtefactAttributeArtefactHyperNode : ArtefactAttributeBaseHyperNode
    {
        public Artefact OtherArtefact { get; set; }
    }
    public class ArtefactAttributeAgentHyperNode : ArtefactAttributeBaseHyperNode
    {
        public Agent Agent { get; set; }
    }
    public class ArtefactAttributeCategoryHyperNode : ArtefactAttributeBaseHyperNode
    {
        public Category Category { get; set; }
    }
    public class ArtefactAttributePlaceHyperNode : ArtefactAttributeBaseHyperNode
    {
        public Place Place { get; set; }
    }
    public class ArtefactAttributeEventHyperNode : ArtefactAttributeBaseHyperNode
    {
        public Event Event { get; set; }
    }
    public class ArtefactAttributeHyperNode
    {
        public Artefact Artwork { get; set; }
        public string EntityType { get; set; }
        public string Relationship { get; set; }
        public string RelationshipGuid { get; set; }
        public Entity Entity { get; set; }
    }
    public class CategoryAttributeHyperNode
    {
        public Category Category { get; set; }
        public IEnumerable<string> EntityLabels { get; set; }
        public string EntityLabel { get; set; }
        public string Relationship { get; set; }
        public string RelationshipGuid { get; set; }
        public Entity Entity { get; set; }
    }
    public class PlaceAttributeHyperNode
    {
        public Place Place { get; set; }
        public IEnumerable<string> EntityLabels { get; set; }
        public string EntityLabel { get; set; }
        public string Relationship { get; set; }
        public string RelationshipGuid { get; set; }
        public string HasRelationGuid { get; set; }
        public Entity Entity { get; set; }
    }
    public class RepresentsHyperNode
    {
        public Entity Source { get; set; }
        public Entity Target { get; set; }
    }
    public class PlaceResult
    {
        public Place Place { get; set; }
        public IEnumerable<Place> Containers { get; set; }
        public string ToDisplay()
        {
            var results = new List<string>();
            results.Add(Place.Name);
            if (Containers.Any())
            {
                results.Add("(" + string.Join(" / ", Containers.Select(x => x.Name).ToArray()) + ")");
            }
            return string.Join(" ", results.ToArray());
        }
    }
    public class CategoryResult
    {
        public Category Child { get; set; }
        public Category Parent { get; set; }
        public IEnumerable<Category> Ancestors { get; set; }
        public string ToDisplay()
        {
            var results = new List<string>();
            results.Add(Child.Name);
            if (Ancestors.Any())
            {
                results.Add("(" + string.Join(" / ", Ancestors.Select(x => x.Name).ToArray()) + ")");
            }
            return string.Join(" ", results.ToArray());
        }
    }
}
