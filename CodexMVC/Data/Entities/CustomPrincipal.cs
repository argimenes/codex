﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace Data.Entities
{
    public interface ICustomPrincipal : IPrincipal
    {
        string IPAddress { get; set; }
        string FirstName { get; set; }
        bool StayLoggedIn { get; set; }
    }
    public class CustomPrincipal : GenericPrincipal, ICustomPrincipal
    {
        public CustomPrincipal(IIdentity identity, string[] roles) : base(identity, roles)
        {
            
        }
        public string Guid { get; set; }
        public string IPAddress { get; set; }
        public string FirstName { get; set; }
        public bool StayLoggedIn { get; set; }
        public string Permissions { get; set; }
    }
    public class CustomPrincipalSerialiseModel
    {
        public string Guid { get; set; }
        public string IPAddress { get; set; }
        public string Permissions { get; set; }
        public string FirstName { get; set; }
        public bool StayLoggedIn { get; set; }
    }
}
