﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Entities
{
    public class Context : Entity, IEntity
    {

    }
    public class ContextHyperNode
    {
        public Artefact Artefact { get; set; }
        public string HasContextGuid { get; set; }
        public Context Context { get; set; }
        public string ContextForGuid { get; set; }
        public Category Subject { get; set; }
        public string ContextAtGuid { get; set; }
        public Place Place { get; set; }
        public Image ArtefactImage { get; set; }
        public Agent MadeBy { get; set; }
    }
    public class CollectionDetail : Entity, IEntity
    {
        public string Address { get; set; }
        public string Directions { get; set; }
        public string Website { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string AffiliationCode { get; set; }
        public string PurchaseTicketUrl { get; set; }
        public string[] OpenDayStart { get; set; }
        public string[] OpenDayEnd { get; set; }
    }
    public class TourDetail : CollectionDetail
    {

    }
    public class EventDetail : Entity, IEntity
    {
        public string Url { get; set; }
        public int? DayStart { get; set; }
        public int? MonthStart { get; set; }
        public int? YearStart { get; set; }
        public int? DayEnd { get; set; }
        public int? MonthEnd { get; set; }
        public int? YearEnd { get; set; }
        public EventDetail()
        {
            Created = DateTimeOffset.Now;
        }
        public bool IsOnNow()
        {
            var end = End();
            return end == null || end.Value >= DateTimeOffset.Now.Date;
        }
        public bool Coming()
        {
            var start = Start();
            return start == null || start.Value > DateTimeOffset.Now.Date;
        }
        public bool IsFinishing()
        {
            var end = End();
            if (end == null)
            {
                return false;
            }
            var remaining = (end.Value - DateTimeOffset.Now).TotalDays;
            return remaining <= 7;
        }
        public bool IsOver()
        {
            var end = End();
            if (end == null)
            {
                return false;
            }
            return end.Value < DateTimeOffset.Now.Date;
        }
        public DateTime? Start()
        {
            if (false == YearStart.HasValue || false == MonthStart.HasValue || false == DayStart.HasValue)
            {
                return (DateTime?)null;
            }
            return new DateTime(YearStart.Value, MonthStart.Value, DayStart.Value);
        }
        public DateTime? End()
        {
            if (false == YearEnd.HasValue || false == MonthEnd.HasValue || false == DayEnd.HasValue)
            {
                return (DateTime?)null;
            }
            return new DateTime(YearEnd.Value, MonthEnd.Value, DayEnd.Value);
        }
        public string ToDates()
        {
            var start = new List<string>();

            if (YearStart.HasValue)
            {
                start.Add(YearStart.Value.ToString());
            }
            if (MonthStart.HasValue)
            {
                start.Add(MonthStart.Value.ToString());
            }
            if (DayStart.HasValue)
            {
                start.Add(DayStart.Value.ToString());
            }
            var end = new List<string>();
            if (YearEnd.HasValue)
            {
                end.Add(YearEnd.Value.ToString());
            }
            if (MonthEnd.HasValue)
            {
                end.Add(MonthEnd.Value.ToString());
            }
            if (DayEnd.HasValue)
            {
                end.Add(DayEnd.Value.ToString());
            }
            var result = new List<string>();
            if (YearStart.HasValue)
            {
                result.Add("from");
                result.Add(string.Join("-", start));
            }
            if (YearEnd.HasValue)
            {
                result.Add("to");
                result.Add(string.Join("-", end));
            }
            return string.Join(" ", result);
        }
    }
    public class Artefact : Entity, IEntity, IGeo
    {
        public decimal? Height { get; set; }
        public decimal? Width { get; set; }
        public decimal? Depth { get; set; }
        public string SizeUnit;
        public string Text { get; set; }
        public string CatalogueIdent { get; set; }
        public string CatalogueIdentPrefix { get; set; }
        public decimal? Latitude { get; set; }
        public decimal? Longitude { get; set; }
        public int? Ordinal { get; set; }
        public int? TripAdvisorLocationId { get; set; }

        public bool HasDimensions()
        {
            return Height.HasValue;
        }
        public string Cms()
        {
            var results = new List<string>();
            if (Height.HasValue)
            {
                results.Add(Height.Value.ToString());
            }
            if (Height.HasValue && Width.HasValue)
            {
                results.Add(Width.Value.ToString());
            }
            if (Height.HasValue && Width.HasValue && Depth.GetValueOrDefault() > 0)
            {
                results.Add(Depth.Value.ToString());
            }
            return string.Join(" x ", results);
        }
        public string Inches()
        {
            var results = new List<string>();
            if (Height.HasValue)
            {
                var height = Height.Value / 2.54m;
                results.Add(ToFraction(height));
            }
            if (Height.HasValue && Width.HasValue)
            {
                var width = Width.Value / 2.54m;
                results.Add(ToFraction(width));
            }
            if (Height.HasValue && Width.HasValue && Depth.GetValueOrDefault() > 0)
            {
                var depth = Depth.Value / 2.54m;
                results.Add(ToFraction(depth));
            }
            return string.Join(" x ", results);
        }
        private string ToFraction(decimal value)
        {
            var whole = (int)Math.Floor(value);
            var remainder = value - whole;
            int numerator = (int)(remainder * 1000) / 250;
            if (numerator == 0)
            {
                return whole.ToString();
            }
            if (numerator == 2)
            {
                return whole + "<sup>1/2</sup>";
            }
            return whole + "<sup>" + numerator + "/4</sup>";
        }
        public string ToDisplay()
        {
            if (string.IsNullOrEmpty(Article))
            {
                return Name;
            }
            return Article + " " + Name;
        }
        public Artefact()
        {
            // Created = DateTimeOffset.Now;
            Label = "Artefact";
        }
    }
    public class Exhibition : Artefact
    {

    }
}
