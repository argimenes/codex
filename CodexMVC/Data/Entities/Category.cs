﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Entities
{
    public class Category : Entity, IEntity
    {
        public string Adjective { get; set; }
        public bool Clade { get; set; }
        public Category()
        {
            Label = "Category";
        }
    }
    
}
