﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Entities
{
    public class Text : Entity, IEntity
    {
        public string Value { get; set; }
        public bool Primary { get; set; }
        public Text()
        {
            Label = Data.Entities.Label.Entity.Text;
        }
    }
}
