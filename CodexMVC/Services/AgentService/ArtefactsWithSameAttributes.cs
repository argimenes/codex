﻿using Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class ArtefactsWithSameAttributes
    {
        public Artefact Artefact1 { get; set; }
        public Artefact Artefact2 { get; set; }
        public IEnumerable<Category> Categories { get; set; }
        public IEnumerable<Entity> Entities { get; set; }
        public IEnumerable<string> EntityTypes { get; set; }
        public IEnumerable<string> RelationTypes { get; set; }
        public Image Image { get; set; }
        public long Total { get; set; }
        public long PathCost { get; set; }
        public decimal Rank
        {
            get
            {
                return (decimal)Categories.Count() / PathCost;
            }
        }
    }
}
