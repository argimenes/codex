﻿using Data;
using System.Collections.Generic;
using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class SocialHyperNode
    {
        public IEnumerable<Agent> FellowBenefactorsOf { get; set; }
        public IEnumerable<Agent> BenefactorsOf { get; set; }
        public IEnumerable<Agent> BenefactorsTo { get; set; }
        public IEnumerable<Agent> Teachers { get; set; }
        public IEnumerable<Agent> TeacherOfTeachers { get; set; }
        public IEnumerable<Agent> FellowStudents { get; set; }
        public IEnumerable<Agent> Students { get; set; }
        public IEnumerable<Agent> Friends { get; set; }
        public IEnumerable<Agent> Knows { get; set; }
        public IEnumerable<Agent> Adversaries { get; set; }
        public IEnumerable<Agent> FriendOfFriends { get; set; }
        public IEnumerable<Agent> MemberOf { get; set; }
        public IEnumerable<Agent> HasMember { get; set; }
    }
}
