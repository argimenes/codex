﻿using Data;
using Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class ArtefactPublicOld
    {
        public Artefact Artefact { get; set; }
        public IEnumerable<Image> Images { get; set; }
        public IEnumerable<Category> Attributes { get; set; }
        public IEnumerable<Agent> AgentsRepresented { get; set; }
        public IEnumerable<Artefact> ArtefactsRepresentingAgents { get; set; }
        public IEnumerable<Place> RepresentedPlaces { get; set; }
        public IEnumerable<Place> PlacesMatching { get; set; }
        public IEnumerable<ArtefactPlaceImageTuple> ArtefactByPlace { get; set; }
        public IEnumerable<Category> ThingsMatching { get; set; }
        public IEnumerable<Category> RepresentedCategories { get; set; }
        public IEnumerable<ArtefactCategoryImageTuple> ArtefactByCategory { get; set; }
        public IEnumerable<ArtefactAgentImageTuple> ArtefactByAgent { get; set; }
        public IEnumerable<Image> ArtefactsRepresentingThingsImages { get; set; }
        public IEnumerable<Artefact> ArtefactsRepresentingThings { get; set; }
        public IEnumerable<ArtefactMotifImageTuple> ArtefactByMotif { get; set; }
        public Agent MadeBy { get; set; }
        public Relation MadeByRelation { get; set; }
        public Place LocatedAt { get; set; }
        public Agent OwnedBy { get; set; }
        public IEnumerable<Category> MadeFrom { get; set; }
    }
}
