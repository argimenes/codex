﻿using Data;
using Data.Entities;
using log4net;
using Neo4jClient;
using Neo4jClient.Cypher;
using Newtonsoft.Json;
using Services.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class CharacterWithArtefacts
    {
        public Agent Character { get; set; }
        public IEnumerable<ArtefactAgentImageTuple> Artefacts { get; set; }
    }
    public class SubjectWithArtefacts
    {
        public Category Subject { get; set; }
        public IEnumerable<ArtefactAgentImageTuple> Artefacts { get; set; }
    }
    public class MotifWithArtefacts
    {
        public Category Motif { get; set; }
        public IEnumerable<ArtefactAgentImageTuple> Artefacts { get; set; }
    }
    public enum Day
    {
        Mon,
        Tue,
        Wed,
        Thu,
        Fri,
        Sat,
        Sun
    }
    public class ArtefactLocation
    {
        public IEnumerable<Artefact> Containers { get; set; }
        public IEnumerable<PlaceCategoryTuple> Inside { get; set; }
    }
    public class Commentary
    {
        public Agent Agent { get; set; }
        public IEnumerable<Category> Properties { get; set; }
        public Place CitizenOf { get; set; }
        public Category RepresentativeOf { get; set; }
        public IEnumerable<Document> Documents { get; set; }
    }
    public class ViewArtefactRepresents
    {
        public Agent Agent { get; set; }
        public Place Place { get; set; }
        public IEnumerable<ContainerWithArtefacts> ContainerWithArtefacts { get; set; }
    }
    public class ViewArtefactPublic
    {
        public UserArtefactDetails UserArtefactDetails { get; set; }
        public decimal? TotalUserRatings { get; set; }
        public int TotalUsersWhoRated { get; set; }
        public Artefact Artefact { get; set; }
        public CollectionDetail TopContainerDetail { get; set; }
        public bool Bookmarked { get; set; }
        public bool Seen { get; set; }
        public Category OntologicalState { get; set; }
        public IEnumerable<ArtefactImageTuple> SubArtefacts { get; set; }
        public IEnumerable<ContainerWithArtefacts> ContainerWithArtefacts { get; set; }
        public Image PrimaryImage { get; set; }
        public IEnumerable<Agent> Creators { get; set; }
        public IEnumerable<ArtefactImageTuple> WorksInSameCollection { get; set; }
        public IEnumerable<Artefact> Containers { get; set; }
        public IEnumerable<PlaceCategoryTuple> Places { get; set; }
        public IEnumerable<NewAct> Timeline { get; set; }
        public IEnumerable<NewAct> TimelineSameYear { get; set; }
        public IEnumerable<DocumentHyperNode> Documents { get; set; }
        public IEnumerable<Image> Images { get; set; }
        public Category Subject { get; set; }
        public IEnumerable<ArtefactImageTuple> SimilarTo { get; set; }
        public IEnumerable<TourDetails> Tours { get; set; }
        public IEnumerable<ExhibitionDetails> Exhibitions { get; set; }
        public IEnumerable<ExhibitionDetails> ExhibitionsIn { get; set; }
        public IEnumerable<ArtefactImageTuple> Recommended { get; set; }
        public IEnumerable<ArtefactImageTuple> SameSubject { get; set; }
        public IEnumerable<ArtistArtefactImageTuple> SameYear { get; set; }
        public IEnumerable<ArtistArtefactImageTuple> SameAge { get; set; }
        public IEnumerable<ArtefactImageTuple> SameAgents { get; set; }
        public IEnumerable<DocumentHyperNode> Commentary { get; set; }
        public Relation Made { get; set; }
        public Artefact CopyOf { get; set; }
        public Category ArtefactType { get; set; }
        public IEnumerable<Category> Materials { get; set; }
        public int? ArtistAge { get; set; }
        public IEnumerable<RelationAgentTuple> Represents { get; set; }
        public IEnumerable<RelationCategoryTuple> AestheticConventions { get; set; }

        public IEnumerable<AgentWithArtworkImageTuple> CharactersWithArtworks { get; set; }
        public IEnumerable<CategoryWithArtworkImageTuple> MotifsWithArtworks { get; set; }
        public IEnumerable<CategoryWithArtworkImageTuple> SubjectsWithArtworks { get; set; }
        public IEnumerable<CategoryWithArtworkImageTuple> TechniquesWithArtworks { get; set; }
    }
    public class RelationAgentTuple
    {
        public Relation Relation { get; set; }
        public Agent Agent { get; set; }
    }
    public class AgentWithArtworkImageTuple
    {
        public Agent Agent { get; set; }
        public IEnumerable<ArtefactImageTuple> Artworks { get; set; }
    }
    public class CategoryWithArtworkImageTuple
    {
        public Category Category { get; set; }
        public IEnumerable<ArtefactImageTuple> Artworks { get; set; }
    }
    public class RelationCategoryTuple
    {
        public Relation Relation { get; set; }
        public Category Category { get; set; }
    }
    /**
     * { Containers: collect(distinct(containers)), LocationWithType: collect(distint({ Location: inside, LocationType: insideType })) }
     */
    public interface IArtefactService : INeo4jClientService
    {
        ViewArtefactPublic FindArtefactDetailBasic(string artefactGuid);
        Task<IEnumerable<ExhibitionDetails>> FindExhibitionsOfAsync(string artefactGuid);
        IEnumerable<ArtistArtefactImageTuple> SimilarArtefactsByUser(string userGuid);
        IEnumerable<ArtistArtefactImageTuple> RecommendedArtefactsByUser(string userGuid);
        IEnumerable<ArtistArtefactImageTuple> FindBookmarkedArtefacts(string userGuid);
        ArtefactImageTuple FindArtefactWithImage(string artefactGuid);
        Task<IEnumerable<ArtefactImageTuple>> FindSimilarAsync(string artefactGuid);
        Task<IEnumerable<ArtefactsMadeByResult>> ArtefactsMadeByTest(string agentGuid);
        List<FindByCategoryAndAgentResult> FindByCategoryAndAgent(string agentGuid, string categoryGuid);
        Task<IEnumerable<ArtefactsMadeByResult>> ArtefactsMadeBy(string agentGuid);
        List<ArtefactMadeByAgent> AllMadeBy();
        List<ArtefactMadeByAgent> AllSources();
        List<ArtefactMadeByAgent> AllRepresentedArtefacts();
        List<Artefact> All();
        //void OwnedBy(Artefact artwork, Agent person);
        void MadeOf(Artefact artwork, params Category[] categories);
        List<object> FindOutgoingRelationships(string artefactGuid);
        List<object> FindIncomingRelationships(string artefactGuid);
        Artefact FindArtefactByGuid(string guid);
        void IsA(Artefact artefact, params Category[] categories);
        List<ArtefactsWithSameAttributes> FindSimilarArtefactsByTaxonomy(string artefactGuid, int? separation);
        List<ArtefactsWithSameAttributes> FindSimilarArtefactsByUsage(string artefactGuid, int? separation);
        List<ArtefactsWithSameAttributes> FindSimilarArtefactsByMotif(string artefactGuid, int? separation);
        List<ArtefactsWithSameAttributes> FindSimilarArtefactsByRepresentsByAgent(string artefactGuid, int? separation);
        List<ArtefactsWithSameAttributes> FindSimilarArtefactsByRepresents(string artefactGuid, int? separation);
        List<ArtefactsWithSameAttributes> FindSimilarArtefactsByRepresentsByPlace(string artefactGuid, int? separation);
        void Applies(string artefactGuid, string categoryGuid);
        void RepresentsPlace(string artefactGuid, string placeGuid);
        void RepresentsAgent(string artefactGuid, string agentGuid);
        void RepresentsCategory(string artefactGuid, string categoryGuid);
        void MadeFrom(string artefactGuid, string categoryGuid);
        void SimilarTo(string artefactGuid, string otherArtefactGuid);

        void LocatedAt(string artefactGuid, string placeGuid);
        void CommissionedBy(string artefactGuid, string agentGuid);
        void MadeWith(string artefactGuid, string implementGuid);
        void MadeBy(string artefactGuid, string agentGuid);
        void ExhibitedIn(string artefactGuid, string exhibitionGuid);
        /// <summary>
        /// Returns the created relationshipGuid.
        /// </summary>
        /// <param name="artefactGuid"></param>
        /// <param name="categoryGuid"></param>
        /// <param name="rank"></param>
        /// <returns></returns>
        string RecognisabilityByCategory(string artefactGuid, string categoryGuid, int rank);
        string RecognisabilityByAgent(string artefactGuid, string categoryGuid, int rank);
        Artefact SaveOrUpdate(Artefact artefact);
        CollectionDetail SaveOrUpdate(CollectionDetail detail);
        EventDetail SaveOrUpdate(EventDetail detail);
        //void HasSubject(string artefactGuid, string subjectGuid);
        List<ArtefactsRelatedToSimilarArtefacts> FindArtefactsRelatedToSimilarArtefacts(string artefactGuid, int? separation);
        void HasMotif(string artefactGuid, string motifGuid);
        List<ArtefactsRelatedByUsage> FindArtefactsRelatedByUsage(string artefactGuid);
        int Delete(string artefactGuid);

        void AddAlias(string p1, string p2);

        /**
         * Artefact to Entity
         */
        GenericRelationHyperNode<Artefact, Entity> FindRepresents(string artefactGuid, string hasRelationGuid);
        void Represents(GenericRelationHyperNode<Artefact, Entity> node);
        /**
         * Artefact to Artefact
         */
        GenericRelationNode<Artefact, Artefact> FindPartOf(string artefactGuid, string hasRelationGuid);
        void PartOf(GenericRelationNode<Artefact, Artefact> node);

        GenericRelationHyperNode<Artefact, Artefact> FindStudyFor(string artefactGuid, string hasRelationGuid);
        void StudyFor(GenericRelationHyperNode<Artefact, Artefact> node);

        GenericRelationHyperNode<Artefact, Artefact> FindCopyOf(string artefactGuid, string hasRelationGuid);
        void CopyOf(GenericRelationHyperNode<Artefact, Artefact> node);

        GenericRelationHyperNode<Artefact, Artefact> FindInfluenceOn(string artefactGuid, string hasRelationGuid);
        void InfluenceOn(GenericRelationHyperNode<Artefact, Artefact> node);
        GenericRelationHyperNode<Artefact, Artefact> FindSimilarTo(string artefactGuid, string hasRelationGuid);
        void SimilarTo(GenericRelationHyperNode<Artefact, Artefact> node);

        GenericRelationHyperNode<Artefact, Artefact> FindExhibitedIn(string artefactGuid, string hasRelationGuid);
        void ExhibitedIn(GenericRelationHyperNode<Artefact, Artefact> node);

        GenericRelationHyperNode<Artefact, Agent> FindCovers(string artefactGuid, string hasRelationGuid);
        void Covers(GenericRelationHyperNode<Artefact, Agent> node);

        GenericRelationHyperNode<Agent, Artefact> FindExhibitingIn(string artefactGuid, string hasRelationGuid);
        void ExhibitingIn(GenericRelationHyperNode<Agent, Artefact> node);

        GenericRelationNode<Artefact, Artefact> FindLocatedIn(string artefactGuid, string toDestGuid);
        void LocatedIn(GenericRelationNode<Artefact, Artefact> node);

        GenericRelationNode<Artefact, Artefact> FindTakesPlaceIn(string artefactGuid, string toDestGuid);
        void TakesPlaceIn(GenericRelationNode<Artefact, Artefact> node);

        //GenericRelationHyperNode<Artefact, Artefact> FindLocatedIn(string artefactGuid, string hasRelationGuid);
        //void LocatedIn(GenericRelationHyperNode<Artefact, Artefact> node);
        /**
         * Artefact to Place
         */
        GenericRelationNode<Artefact, Place> FindLocatedAt(string artefactGuid, string hasRelationGuid);
        void LocatedAt(GenericRelationNode<Artefact, Place> node);
        /**
         * Artefact to Agent
         */
        GenericRelationHyperNode<Artefact, Agent> FindCommissionedBy(string artefactGuid, string hasRelationGuid);
        void CommissionedBy(GenericRelationHyperNode<Artefact, Agent> node);
        GenericRelationHyperNode<Artefact, Agent> FindPublishedBy(string artefactGuid, string hasRelationGuid);
        void PublishedBy(GenericRelationHyperNode<Artefact, Agent> node);

        GenericRelationHyperNode<Artefact, Agent> FindTranslatedBy(string artefactGuid, string hasRelationGuid);
        void TranslatedBy(GenericRelationHyperNode<Artefact, Agent> node);

        GenericRelationHyperNode<Artefact, Agent> FindMadeBy(string artefactGuid, string hasRelationGuid = null);
        void MadeBy(GenericRelationHyperNode<Artefact, Agent> node);
        GenericRelationHyperNode<Artefact, Agent> FindAttributedTo(string artefactGuid, string hasRelationGuid = null);
        void AttributedTo(GenericRelationHyperNode<Artefact, Agent> node);

        GenericRelationHyperNode<Artefact, Agent> FindOwnedBy(string artefactGuid, string hasRelationGuid);
        void OwnedBy(GenericRelationHyperNode<Artefact, Agent> node);
        GenericRelationHyperNode<Artefact, Agent> FindDesignedBy(string artefactGuid, string hasRelationGuid);
        void DesignedBy(GenericRelationHyperNode<Artefact, Agent> node);
        GenericRelationHyperNode<Agent, Artefact> FindAppliesTo(string artefactGuid, string hasRelationGuid);
        GenericRelationHyperNode<Agent, Artefact> FindBuriedIn(string artefactGuid, string hasRelationGuid);
        GenericRelationHyperNode<Artefact, Agent> FindManagedBy(string artefactGuid, string hasRelationGuid);
        void BuriedIn(GenericRelationHyperNode<Agent, Artefact> node);
        void AppliesTo(GenericRelationHyperNode<Agent, Artefact> node);
        void ManagedBy(GenericRelationHyperNode<Artefact, Agent> node);
        /**
         * Artefact to Category
         */
        ArtefactToCategoryRelationHyperNode FindIsA(string artefactGuid, string hasRelationGuid);
        void IsA(GenericRelationHyperNode<Artefact, Category> node);
        ArtefactToCategoryRelationHyperNode FindOntologicalStateOf(string artefactGuid, string hasRelationGuid);
        void OntologicalStateOf(GenericRelationHyperNode<Artefact, Category> node);
        ArtefactToCategoryRelationHyperNode FindMadeFrom(string artefactGuid, string hasRelationGuid);
        void MadeFrom(GenericRelationHyperNode<Artefact, Category> node);
        ArtefactToCategoryRelationHyperNode FindMadeWith(string artefactGuid, string hasRelationGuid);
        void MadeWith(GenericRelationHyperNode<Artefact, Category> node);
        ArtefactToCategoryRelationHyperNode FindHasMotif(string artefactGuid, string hasRelationGuid);
        void HasMotif(GenericRelationHyperNode<Artefact, Category> node);
        GenericRelationHyperNode<Artefact, Category> FindArtefactHasSubject(string artefactGuid, string hasRelationGuid);
        void ArtefactHasSubject(GenericRelationHyperNode<Artefact, Category> node);
        ArtefactToCategoryRelationHyperNode FindUses(string artefactGuid, string hasRelationGuid);
        void Uses(GenericRelationHyperNode<Artefact, Category> node);

        GenericRelationHyperNode<Artefact, Artefact> FindTourFeatures(string artefactGuid, string hasRelationGuid);
        void TourFeatures(GenericRelationHyperNode<Artefact, Artefact> node);


        ArtefactToCategoryRelationHyperNode FindHasGenre(string artefactGuid, string hasRelationGuid);
        void HasGenre(GenericRelationHyperNode<Artefact, Category> node);
        ArtefactToCategoryRelationHyperNode FindHasPhysicalPropertyOf(string artefactGuid, string hasRelationGuid);
        void HasPhysicalPropertyOf(GenericRelationHyperNode<Artefact, Category> node);

        GenericRelationHyperNode<Artefact, Category> FindScoredFor(string artefactGuid, string hasRelationGuid);
        void ScoredFor(GenericRelationHyperNode<Artefact, Category> node);

        GenericRelationHyperNode<Artefact, Category> FindRepresentativeOf(string artefactGuid, string hasRelationGuid);
        void RepresentativeOf(GenericRelationHyperNode<Artefact, Category> node);

        GenericRelationHyperNode<Artefact, Category> FindAssociatedWith(string artefactGuid, string hasRelationGuid);
        void AssociatedWith(GenericRelationHyperNode<Artefact, Category> node);

        GenericRelationNode<Artefact, Artefact> FindStopsIn(string artefactGuid, string hasRelationGuid);
        void StopsIn(GenericRelationNode<Artefact, Artefact> node);

        GenericRelationNode<Artefact, Place> FindStopsAt(string artefactGuid, string hasRelationGuid);
        void StopsAt(GenericRelationNode<Artefact, Place> node);

        IEnumerable<ArtefactsMadeByResult> SearchBy(string artistName, string artefactTypeName);

        IEnumerable<ArtefactsMadeByResult> SearchByQuery(FindByQuery query);

        ContextHyperNode FindContext(string contextGuid);
        IEnumerable<ContextHyperNode> FindContextsByArtefact(string artefactGuid);
        void SaveOrUpdate(ContextHyperNode contextHyperNode);
        void DeleteContext(string contextGuid);

        List<ArtefactsMadeByResult> FindByAgentAndPlace(string agentGuid, string placeGuid);

        List<ArtefactsMadeByResult> FindByAgentQuery(string artworkTypeGuid, string artistGuid, string placeGuid);
        List<ArtefactsMadeByResult> FindByMovementQuery(string artworkTypeGuid, string movementGuid, string placeGuid);
        List<ArtefactsMadeByResult> FindByGenreQuery(string artworkTypeGuid, string genreGuid, string placeGuid);
        List<ArtefactsMadeByResult> FindByMotifQuery(string artworkTypeGuid, string genreGuid, string placeGuid);
        List<ArtefactsMadeByResult> FindByExteriorQuery(string artworkTypeGuid, string placeGuid);
        List<ArtefactsMadeByResult> FindBySubjectQuery(string artworkTypeGuid, string subjectGuid, string placeGuid);
        List<ArtefactsMadeByResult> FindByAestheticConvention(string artworkTypeGuid, string aestheticQualityGuid, string placeGuid);
        List<ArtefactsMadeByResult> FindByAgentRepresentedQuery(string artworkTypeGuid, string subjectGuid, string placeGuid);
        List<ArtefactsMadeByResult> FindByObjectRepresented(string artworkTypeGuid, string categoryGuid, string placeGuid);
        List<ArtefactsMadeByResult> FindByAgentTypeRepresented(string artworkTypeGuid, string agentTypeGuid, string placeGuid);

        bool ArtefactExists(string artefactGuid);
        Task<ViewArtefactPublic> FindArtefactDetailAsync(string artefactGuid);
        ArtefactPublicOld FindArtefactPublicOld(string guid);

        Task<IEnumerable<ContainerWithArtefacts>> FindContainerWithArtefactsByAgentAsync(string agentGuid);
        IEnumerable<ContainerWithArtefacts> FindContainerWithArtefactsBySubject(string subjectGuid);
        Task<IEnumerable<ContainerWithArtefacts>> FindContainerWithArtefactsByStyleAsync(string subjectGuid);
        IEnumerable<ContainerWithArtefacts> FindContainerWithArtefactsByMotif(string motifGuid);
        IEnumerable<ContainerWithArtefacts> FindContainerWithArtefactsByCharacter(string agentGuid);

        IEnumerable<ArtworksByAge> AllArtworksByAge();

        ViewArtefactRepresents CollectionsRepresentingAgent(string agentGuid, string placeGuid = null);

        IEnumerable<SubjectWithArtefacts> AllSubjectsWithArtefacts();
        IEnumerable<MotifWithArtefacts> AllMotifsWithArtefacts();
        IEnumerable<CharacterWithArtefacts> AllCharactersWithArtefacts();

        void HasDetail(string artefactGuid, string detailGuid);
        void HasEventDetail(string artefactGuid, string detailGuid);
        void DeleteDetail(string detailGuid);

        CollectionDetail FindDetailForArtefact(string artefactGuid);
        CollectionDetail FindDetail(string guid);

        IEnumerable<CollectionDetails> AllCollectionsThatHaveArtworks();
        IEnumerable<CollectionDetails> AllCollections();
        IEnumerable<ExhibitionDetails> AllExhibitions();
        IEnumerable<TourDetails> AllTours();

        IEnumerable<ArtistArtefactImageTuple> FindSeenArtefacts(string userGuid);

        ExhibitionDetails FindExhibition(string exhibitionGuid);

        TourDetails FindTour(string tourGuid);

        Task<IEnumerable<ArtefactImageTuple>> CheckForDuplicates(string name, string madeByGuid);

        IEnumerable<CollectionDetails> CheckForDuplicateCollections(string name);
        void UpdateArtefactPublicWithUserDetails(ViewArtefactPublic model, string userGuid);
    }

    public class CollectionDetails
    {
        public Artefact Collection { get; set; }
        public int ArtworkTotal { get; set; }
        public CollectionDetail Detail { get; set; }
        public IEnumerable<Category> Attributes { get; set; }
        public Place Location { get; set; }
        public IEnumerable<Place> Inside { get; set; }
        public IEnumerable<Category> RepresentativeOf { get; set; }
    }

    public class ExhibitionDetails
    {
        public bool Bookmarked { get; set; }
        public bool Seen { get; set; }
        public Artefact Exhibition { get; set; }
        public Artefact Collection { get; set; }
        public CollectionDetail CollectionDetail { get; set; }
        public EventDetail Detail { get; set; }
        public Place Location { get; set; }
        public IEnumerable<Place> Inside { get; set; }
        public IEnumerable<ArtistWithArtefactImage> Artworks { get; set; }
    }

    public class TourDetails
    {
        public bool Bookmarked { get; set; }
        public bool Seen { get; set; }
        public Artefact Tour { get; set; }
        public TourDetail Detail { get; set; }
        public Place Location { get; set; }
        public IEnumerable<Artefact> StopsIn { get; set; }
        public IEnumerable<Place> Inside { get; set; }
        public IEnumerable<ArtistWithArtefactImage> Artworks { get; set; }
    }

    public class ArtworksByAge
    {
        public int Age { get; set; }
        public Agent Artist { get; set; }
        public IEnumerable<ArtefactImageTuple> ArtefactImages { get; set; }
    }

    public class FindByQuery
    {
        public string CitizenOfGuid { get; set; }
        public string RepresentativeOfGuid { get; set; }
        public string AgentGuid { get; set; }
        public string ArtefactIsAGuid { get; set; }
        public string PlaceGuid { get; set; }
        public string Preposition { get; set; }
    }
    public class ArtefactService : IArtefactService
    {
        private readonly ILog Log = LogManager.GetLogger("DefaultLogger");
        public User CurrentUser { get; set; }
        private readonly GraphClient Client;
        private readonly ICommonService commonService;
        private readonly IActService actService;
        private readonly IRelationService relationService;
        public ArtefactService(
            GraphClient client,
            ICommonService _commonService,
            IRelationService _relationService,
            IActService _actService
        )
        {
            Client = client;
            commonService = _commonService;
            relationService = _relationService;
            actService = _actService;
        }

        public IEnumerable<CollectionDetails> CheckForDuplicateCollections(string name)
        {
            name = name.Trim();
            var search = "(?i).*" + name + ".*";

            var query = Client.Cypher
                .Match("(collectionType:Category { Name: {collectionType} })").WithParams(new { collectionType = Label.Categories.Collection })
                .Match("(collectionTypes:Category)-[:KIND_OF*0..]->(collectionType)")
                .Match("(collection:Artefact)-[:HAS_RELATION]->()-[:IS_A]->(collectionTypes)")
                .Where("collection.Name =~ '" + search + "'")
                .Match("(collection)-[:HAS_RELATION]->()-[:IS_A]->(attribute:Category)")
                .OptionalMatch("(collection)-[:LOCATED_AT]->(location:Place)")
                .With("{ Collection: collection, Attributes: collect(distinct(attribute)), Location: location  } as Collections")
                ;

            var results = query
                .Return(() => Return.As<CollectionDetails>("distinct Collections"))
                .Results
                .ToList();

            return results;
        }
        public async Task<IEnumerable<ArtefactImageTuple>> CheckForDuplicates(string artefactName, string madeByGuid)
        {
            artefactName = artefactName.Trim().Replace("'", "\'");
            var search = "(?i).*" + artefactName + ".*";

            var query = Client.Cypher
                .Match("(agent:Agent { Guid: {madeByGuid} })").WithParams(new { madeByGuid })
                .Match("(artefact:Artefact)-[:HAS_RELATION]->()-[:MADE_BY]->(agent)")
                .Where("artefact.Name =~ '" + search + "'")
                .OptionalMatch("(artefact)-[:HAS_IMAGE]->(image:Image { Primary: true })")
                ;

            var results = await query
                .Return(() => new ArtefactImageTuple
                {
                    Artefact = Return.As<Artefact>("artefact"),
                    Image = Return.As<Image>("image")
                })
                .ResultsAsync;

            return results;
        }

        public CollectionDetail FindDetail(string detailGuid)
        {
            var query = Client.Cypher
                .Match("(detail:Detail { Guid: {detailGuid} })").WithParams(new { detailGuid });
            var detail = query
                .Return(() => Return.As<CollectionDetail>("detail"))
                .Results
                .FirstOrDefault();
            return detail;
        }

        public CollectionDetail FindDetailForArtefact(string artefactGuid)
        {

            var query = Client.Cypher
                .Match("(artefact:Artefact { Guid: {artefactGuid} })").WithParams(new { artefactGuid })
                .Match("(artefact)-[:HAS_DETAIL]->(detail:Detail)");

            var detail = query
                .Return(() => Return.As<CollectionDetail>("detail"))
                .Results
                .FirstOrDefault();

            return detail;

        }

        public TourDetails FindTour(string tourGuid)
        {

            var query = Client.Cypher
                .Match("(tour:Artefact { Guid: {tourGuid} })").WithParams(new { tourGuid })
                .Match("(tour)-[:HAS_DETAIL]->(detail:Detail)")
                .OptionalMatch("(tour)-[:" + Label.Relationship.StopsIn + "]->(collection:Artefact)")
                .OptionalMatch("(tour)-[:" + Label.Relationship.StopsAt + "]->(location:Place)")
                // .OptionalMatch("(tour)-[:HAS_RELATION]->()-[:" + Label.Relationship.Covers + "]->(person:Agent)")
                .OptionalMatch("(tour)-[:HAS_RELATION]->()-[:" + Label.Relationship.TourFeatures + "]->(artefact:Artefact)")
                .OptionalMatch("(artefact)-[:HAS_RELATION]->()-[:MADE_BY]->(artist:Agent)")
                .OptionalMatch("(artefact)-[:HAS_IMAGE]->(image:Image { Primary: true })")
                .With("{ Artist: artist, ArtefactWithImages: collect(distinct { Artefact: artefact, Image: image }) } as Artworks, tour, detail, collection, location")
                ;

            var results = query
                .Return(() => new TourDetails
                {
                    Tour = Return.As<Artefact>("tour"),
                    Detail = Return.As<TourDetail>("detail"),
                    Location = Return.As<Place>("location"),
                    Artworks = Return.As<IEnumerable<ArtistWithArtefactImage>>("collect(distinct(Artworks))")
                })
                .Results
                .FirstOrDefault();

            return results;

        }

        public ExhibitionDetails FindExhibition(string exhibitionGuid)
        {

            var query = Client.Cypher
                .Match("(exhibition:Artefact { Guid: {exhibitionGuid} })").WithParams(new { exhibitionGuid })
                .Match("(exhibition)-[:HAS_DETAIL]->(detail:EventDetail)")
                .Match("(exhibition)-[:TAKES_PLACE_IN]->(collection:Artefact)")
                .Match("(collection)-[:LOCATED_AT]->(location:Place)-[:INSIDE_OF*1..]->(inside:Place)")
                .OptionalMatch("(collection)-[:HAS_DETAIL]->(collectionDetail:Detail)")
                .OptionalMatch("(artefact:Artefact)-[:HAS_RELATION]->()-[:EXHIBITED_IN]->(exhibition)")
                .OptionalMatch("(artefact)-[:HAS_RELATION]->()-[:MADE_BY]->(artist:Agent)")
                .OptionalMatch("(artefact)-[:HAS_IMAGE]->(image:Image { Primary: true })")
                .With("{ Artist: artist, ArtefactWithImages: collect(distinct { Artefact: artefact, Image: image }) } as Artworks, exhibition, detail, collection, location, inside, collectionDetail")
                ;

            var results = query
                .Return(() => new ExhibitionDetails
                {
                    Exhibition = Return.As<Artefact>("exhibition"),
                    Detail = Return.As<EventDetail>("detail"),
                    Collection = Return.As<Artefact>("collection"),
                    CollectionDetail = Return.As<CollectionDetail>("collectionDetail"),
                    Location = Return.As<Place>("location"),
                    Inside = Return.As<IEnumerable<Place>>("collect(distinct(inside))"),
                    Artworks = Return.As<IEnumerable<ArtistWithArtefactImage>>("collect(distinct(Artworks))")
                })
                .Results
                .FirstOrDefault();

            return results;

        }

        public IEnumerable<TourDetails> AllTours()
        {

            var query = Client.Cypher
               .Match("(tourType:Category { Name: {colName} })").WithParams(new { colName = Label.Categories.Tour })
               .Match("(tourTypes:Category)-[:KIND_OF*0..]->(tourType)")
               .Match("(tour:Artefact)-[:HAS_RELATION]->()-[:IS_A]->(tourTypes)")
               .Match("(tour)-[:HAS_DETAIL]->(detail:Detail)")
               .Match("(tour)-[:" + Label.Relationship.StopsAt + "]->(location:Place)-[:INSIDE_OF*1..]->(inside:Place)")
               ;

            var results = query
                .Return(() => new TourDetails
                {
                    Tour = Return.As<Artefact>("tour"),
                    Detail = Return.As<TourDetail>("detail"),
                    Location = Return.As<Place>("location"),
                    Inside = Return.As<IEnumerable<Place>>("collect(distinct(inside))")
                })
                .Results
                .ToList();

            return results;

        }

        public IEnumerable<ExhibitionDetails> AllExhibitions()
        {

            var query = Client.Cypher
                .Match("(exhibitionType:Category { Name: {colName} })").WithParams(new { colName = Label.Categories.Exhibition })
                .Match("(exhibitionTypes:Category)-[:KIND_OF*0..]->(exhibitionType)")
                .Match("(exhibition:Artefact)-[:HAS_RELATION]->()-[:IS_A]->(exhibitionTypes)")
                .Match("(exhibition)-[:HAS_DETAIL]->(detail:EventDetail)")
                .Match("(exhibition)-[:TAKES_PLACE_IN]->(collection:Artefact)")
                .Match("(collection)-[:LOCATED_AT]->(location:Place)-[:INSIDE_OF*1..]->(inside:Place)")
                ;

            var results = query
                .Return(() => new ExhibitionDetails
                {
                    Exhibition = Return.As<Artefact>("exhibition"),
                    Detail = Return.As<EventDetail>("detail"),
                    Collection = Return.As<Artefact>("collection"),
                    Location = Return.As<Place>("location"),
                    Inside = Return.As<IEnumerable<Place>>("collect(distinct(inside))")
                })
                .Results
                .ToList();

            return results;

        }

        public IEnumerable<CollectionDetails> AllCollections()
        {

            var query = Client.Cypher
                .Match("(col:Category { Name: {colName} })").WithParams(new { colName = Label.Categories.Collection })
                .Match("(cols:Category)-[:KIND_OF*0..]->(col)")
                .Match("(collection:Artefact)-[:HAS_RELATION]->()-[:IS_A]->(cols)")
                .Match("(collection)-[:HAS_RELATION]->()-[:IS_A]->(attributes:Category)")
                .Match("(collection)-[:LOCATED_AT]->(location:Place)-[:INSIDE_OF*1..]->(inside:Place)")
                ;

            var results = query
                .Return(() => new CollectionDetails
                {
                    Collection = Return.As<Artefact>("collection"),
                    Location = Return.As<Place>("location"),
                    Inside = Return.As<IEnumerable<Place>>("collect(distinct(inside))"),
                    Attributes = Return.As<IEnumerable<Category>>("collect(distinct(attributes))")
                })
                .Results
                .ToList();

            return results;

        }

        public IEnumerable<CollectionDetails> AllCollectionsThatHaveArtworks()
        {

            var query = Client.Cypher
                .Match("(col:Category { Name: {colName} })").WithParams(new { colName = Label.Categories.Collection })
                .Match("(cols:Category)-[:KIND_OF*0..]->(col)")
                .Match("(collection:Artefact)-[:HAS_RELATION]->()-[:IS_A]->(cols)")
                .Match("(collection)-[:HAS_RELATION]->()-[:IS_A]->(attributes:Category)")
                .Match("(collection)-[:LOCATED_AT]->(location:Place)-[:INSIDE_OF*1..]->(inside:Place)")
                .Match("artworks=(artefact:Artefact)-[:LOCATED_IN*0..]->(collection)")
                .Where("length(artworks) > 0")
                .Match("(artefact)-[:HAS_IMAGE]->(image:Image { Primary: true })")
                .Match("(location)-[:INSIDE_OF*0..]->(container:Place)")
                .OptionalMatch("(collection)-[:HAS_RELATION]->()-[:" + Label.Relationship.RepresentativeOf + "]->(representativeOf:Category)")
                .With("count(distinct(artefact)) as ArtworkTotal, collection, inside, attributes, representativeOf, location")
                ;

            var results = query
                .Return(() => new CollectionDetails
                {
                    Collection = Return.As<Artefact>("distinct(collection)"),
                    ArtworkTotal = Return.As<int>("ArtworkTotal"),
                    Location = Return.As<Place>("location"),
                    Inside = Return.As<IEnumerable<Place>>("collect(distinct(inside))"),
                    Attributes = Return.As<IEnumerable<Category>>("collect(distinct(attributes))"),
                    RepresentativeOf = Return.As<IEnumerable<Category>>("collect(distinct(representativeOf))")
                })
                .Results
                .ToList();

            return results;

        }

        public void DeleteDetail(string detailGuid)
        {

            Client.Cypher
                .Match("(detail:" + Label.Entity.Detail + " { Guid: {detailGuid} })").WithParams(new { detailGuid })
                .Match("(collection:" + Label.Entity.Artefact + ")-[r:" + Label.Relationship.HasDetail + "]->(detail)")
                .Delete("r, detail")
                .ExecuteWithoutResults();

        }

        public void HasEventDetail(string artefactGuid, string detailGuid)
        {

            commonService.CreateRelationship(Label.Entity.Artefact, artefactGuid, Label.Entity.EventDetail, detailGuid, Label.Relationship.HasDetail);

        }

        public void HasDetail(string artefactGuid, string detailGuid)
        {

            commonService.CreateRelationship(Label.Entity.Artefact, artefactGuid, Label.Entity.Detail, detailGuid, Label.Relationship.HasDetail);

        }

        public IEnumerable<CharacterWithArtefacts> AllCharactersWithArtefacts()
        {

            var query = Client.Cypher
                .Match("(artefact:Artefact)-[:HAS_RELATION]->()-[:REPRESENTS]->(character:Agent)")
                .Match("(artefact)-[:HAS_RELATION]->()-[:MADE_BY]->(artist:Agent)")
                .Match("(artefact)-[:HAS_IMAGE]->(image:Image { Primary: true })")
                ;

            var result = query
                .Return(() => new CharacterWithArtefacts
                {
                    Character = Return.As<Agent>("character"),
                    Artefacts = Return.As<IEnumerable<ArtefactAgentImageTuple>>("collect(distinct({ Artefact: artefact, Agent: artist, Image: image }))[0..5]")
                })
                .Results
                .ToList();

            return result;

        }

        public IEnumerable<MotifWithArtefacts> AllMotifsWithArtefacts()
        {

            var query = Client.Cypher
                .Match("(motif:Category)-[:KIND_OF*0..]->(motifs:Category { Name: {subjects} })").WithParams(new { subjects = Label.Categories.Motif })
                .Match("(artefact:Artefact)-[]->()-[:HAS_MOTIF]->(motif)")
                .Match("(artefact)-[]->()-[:MADE_BY]->(artist:Agent)")
                .Match("(artefact)-[:HAS_IMAGE]->(image:Image { Primary: true })")
                ;

            var result = query
                .Return(() => new MotifWithArtefacts
                {
                    Motif = Return.As<Category>("motif"),
                    Artefacts = Return.As<IEnumerable<ArtefactAgentImageTuple>>("collect(distinct({ Artefact: artefact, Agent: artist, Image: image }))[0..5]")
                })
                .Results
                .ToList();

            return result;

        }

        public IEnumerable<SubjectWithArtefacts> AllSubjectsWithArtefacts()
        {

            var query = Client.Cypher
                .Match("(subject:Category)-[:KIND_OF*0..]->(subjects:Category { Name: {subjects} })").WithParams(new { subjects = Label.Categories.ArtisticSubject })
                .Match("(artefact:Artefact)-[]->()-[:HAS_SUBJECT]->(subject)")
                .Match("(artefact)-[]->()-[:MADE_BY]->(artist:Agent)")
                .Match("(artefact)-[:HAS_IMAGE]->(image:Image { Primary: true })")
                ;

            var result = query
                .Return(() => new SubjectWithArtefacts
                {
                    Subject = Return.As<Category>("subject"),
                    Artefacts = Return.As<IEnumerable<ArtefactAgentImageTuple>>("collect(distinct({ Artefact: artefact, Agent: artist, Image: image }))[0..5]")
                })
                .Results
                .ToList();

            return result;

        }

        public IEnumerable<ArtworksByAge> AllArtworksByAge()
        {

            var query = Client.Cypher
                .Match("(artefact:Artefact)-[:HAS_RELATION]->(made:Relation)-[:MADE_BY]->(artist:Agent)")
                .Match("(artist)-[:HAS_RELATION]->(birth:Relation)-[:BORN_AT]->(:Place)")
                .Match("(artefact)-[:HAS_IMAGE]->(image:Image { Primary: true })")
                .With("(made.Year - birth.Year) as age,  artist, collect(distinct({ Artefact: artefact, Image: image })) as artefactImages")
                ;

            var results = query
                .Return(() => new ArtworksByAge
                {
                    Age = Return.As<int>("age"),
                    Artist = Return.As<Agent>("artist"),
                    ArtefactImages = Return.As<IEnumerable<ArtefactImageTuple>>("artefactImages")
                })
                 .OrderBy("age")
                .Results
                .ToList();

            return results;

        }

        public void UpdateArtefactPublicWithUserDetails(ViewArtefactPublic model, string userGuid)
        {
            var query = Client.Cypher
                .Match("(artefact:Artefact { Guid: {artefactGuid} })").WithParams(new { artefactGuid = model.Artefact.Guid })
                .OptionalMatch("bookmarked=(user:User { Guid: {userGuid} })-[:" + Label.Relationship.Bookmarked + "]->(artefact)").WithParams(new { userGuid })
                .OptionalMatch("seen=(user:User { Guid: {userGuid2} })-[:" + Label.Relationship.Saw + "]->(artefact)").WithParams(new { userGuid2 = userGuid })
                ;

            var result = query
                .Return(() => new
                {
                    Bookmarked = Return.As<bool>("length(bookmarked) > 0"),
                    Seen = Return.As<bool>("length(seen) > 0"),
                })
                .Results
                .FirstOrDefault();

            model.Bookmarked = result.Bookmarked;
            model.Seen = result.Seen;
        }

        public bool ArtefactExists(string artefactGuid)
        {
            var query = Client.Cypher
                .Match("(artefact:Artefact { Guid: {artefactGuid} })").WithParams(new { artefactGuid })
                ;

            var result = query
                .Return(() => Return.As<Artefact>("artefact"))
                .Results
                .FirstOrDefault()
                ;

            return result != null;
        }

        public ViewArtefactPublic FindArtefactDetailBasic(string artefactGuid)
        {

            var result = new ViewArtefactPublic();

            var detailsQuery = Client.Cypher
                .Match("(artefact:Artefact { Guid: {artefactGuid} })").WithParams(new { artefactGuid })
                .OptionalMatch("tcd=(artefact)-[:LOCATED_IN*0..]->(:Artefact)-[:HAS_DETAIL]->(topContainerDetail:Detail)")
                .OptionalMatch("(artefact)-[:HAS_DETAIL]->(detail:Detail)")
                .OptionalMatch("(artefact)-[:HAS_RELATION]->(made:Relation)-[:MADE_BY]->(artist:Agent)")
                .OptionalMatch("(artist)-[:HAS_RELATION]->(birth:Relation)-[:BORN_AT]->(:Place)")
                .OptionalMatch("(artefact)-[:HAS_IMAGE]->(image:Image)")
                .OptionalMatch("(artefact)-[:HAS_RELATION]->()-[:HAS_SUBJECT]->(subject:Category)")
                .OptionalMatch("(artefact)-[:HAS_RELATION]->()-[:IS_A]->(artefactType:Category)")
                .OptionalMatch("(artefact)-[:HAS_RELATION]->()-[:MADE_FROM]->(material:Category)")
            ;

            var detailsResults = detailsQuery
                .Return(() => new
                {
                    Artefact = Return.As<Artefact>("artefact"),
                    TopContainerDetail = Return.As<CollectionDetail>("topContainerDetail"),
                    HasTopContainerDetail = Return.As<bool>("length(tcd) > 0"),
                    Images = Return.As<IEnumerable<Image>>("collect(distinct(image))"),
                    Subject = Return.As<Category>("subject"),
                    Creators = Return.As<IEnumerable<Agent>>("collect(distinct(artist))"),
                    Made = Return.As<Relation>("made"),
                    Birth = Return.As<Relation>("birth"),
                    ArtefactType = Return.As<Category>("head(collect(artefactType))"),
                    Materials = Return.As<IEnumerable<Category>>("collect(distinct(material))"),
                })
                .Results;

            var details = detailsResults.FirstOrDefault();

            if (details != null)
            {
                result.Creators = details.Creators;
                result.Artefact = details.Artefact;
                result.Subject = details.Subject;
                result.Made = details.Made;
                result.ArtefactType = details.ArtefactType;
                result.Materials = details.Materials;

                if (details.Made != null && details.Made.Year.HasValue && details.Birth != null && details.Birth.Year.HasValue)
                {
                    result.ArtistAge = details.Made.Year.Value - details.Birth.Year.Value;
                }

                if (details.Images != null && details.Images.Any())
                {
                    result.Images = details.Images;
                    result.PrimaryImage = details.Images.FirstOrDefault(x => x.Primary ?? false) ?? details.Images.First();
                }

            }

            result.Containers = FindContainers(artefactGuid);
            result.Places = FindPlaces(artefactGuid);

            return result;
        }

        public async Task<ViewArtefactPublic> FindArtefactDetailAsync(string artefactGuid)
        {

            var result = new ViewArtefactPublic();

            var detailsQuery = Client.Cypher
                .Match("(artefact:Artefact { Guid: {artefactGuid} })").WithParams(new { artefactGuid })
                .OptionalMatch("tcd=(artefact)-[:LOCATED_IN*0..]->(:Artefact)-[:HAS_DETAIL]->(topContainerDetail:Detail)")
                .OptionalMatch("(artefact)-[:HAS_DETAIL]->(detail:Detail)")
                .OptionalMatch("(artefact)-[:HAS_RELATION]->(made:Relation)-[:MADE_BY]->(artist:Agent)")
                .OptionalMatch("(artist)-[:HAS_RELATION]->(birth:Relation)-[:BORN_AT]->(:Place)")
                .OptionalMatch("(artefact)-[:HAS_IMAGE]->(image:Image)")
                .OptionalMatch("(artefact)-[:HAS_RELATION]->()-[:HAS_SUBJECT]->(subject:Category)")
                .OptionalMatch("(artefact)-[:HAS_RELATION]->()-[:COPY_OF]->(copyOf:Artefact)")
                .OptionalMatch("(artefact)-[:HAS_RELATION]->()-[:" + Label.Relationship.OntologicalStateOf + "]->(ontologicalState:Category)")
                .OptionalMatch("(artefact)-[:HAS_RELATION]->()-[:IS_A]->(artefactType:Category)")
                .OptionalMatch("(artefact)-[:HAS_RELATION]->()-[:MADE_FROM]->(material:Category)")
                .OptionalMatch("(:" + Label.Entity.User + ")-[allRatings:" + Label.Relationship.Rates + "]->(artefact)")
            ;

            var detailsResults = await detailsQuery
                .Return(() => new
                {
                    Artefact = Return.As<Artefact>("artefact"),
                    TotalUsersWhoRated = Return.As<int>("count(allRatings)"),
                    TotalUserRatings = Return.As<decimal?>("sum(allRatings.Rating)"),
                    Detail = Return.As<CollectionDetail>("detail"),
                    TopContainerDetail = Return.As<CollectionDetail>("topContainerDetail"),
                    HasTopContainerDetail = Return.As<bool>("length(tcd) > 0"),
                    Images = Return.As<IEnumerable<Image>>("collect(distinct(image))"),
                    Subject = Return.As<Category>("subject"),
                    Made = Return.As<Relation>("made"),
                    Birth = Return.As<Relation>("birth"),
                    CopyOf = Return.As<Artefact>("copyOf"),
                    ArtefactType = Return.As<Category>("head(collect(artefactType))"),
                    Materials = Return.As<IEnumerable<Category>>("collect(distinct(material))"),
                    OntologicalState = Return.As<Category>("ontologicalState")
                })
                .ResultsAsync;

            var details = detailsResults.FirstOrDefault();

            if (details != null)
            {
                result.TotalUsersWhoRated = details.TotalUsersWhoRated;
                result.TotalUserRatings = details.TotalUserRatings;
                result.Artefact = details.Artefact;
                result.TopContainerDetail = details.HasTopContainerDetail ? details.TopContainerDetail : details.Detail;
                result.Subject = details.Subject;
                result.CopyOf = details.CopyOf;
                result.Made = details.Made;
                result.ArtefactType = details.ArtefactType;
                result.Materials = details.Materials;

                if (details.Made != null && details.Made.Year.HasValue && details.Birth != null && details.Birth.Year.HasValue)
                {
                    result.ArtistAge = details.Made.Year.Value - details.Birth.Year.Value;
                }

                if (details.Images != null && details.Images.Any())
                {
                    result.Images = details.Images;
                    result.PrimaryImage = details.Images.FirstOrDefault(x => x.Primary ?? false) ?? details.Images.First();
                }

            }

            result.SameSubject = await FindArtefactsWithSameSubjectAtSamePlaceAsync(artefactGuid); ;
            result.Commentary = await actService.FindDocumentHyperNodesByReferrerAsync(artefactGuid);
            result.WorksInSameCollection = await FindWorksByArtistInSameCollectionAsync(artefactGuid);
            result.Creators = await FindCreatorsAsync(artefactGuid);
            result.Containers = await FindContainersAsync(artefactGuid);
            result.Places = await FindPlacesAsync(artefactGuid);
            result.ContainerWithArtefacts = await FindContainerWithArtefactsAsync(artefactGuid);
            result.Timeline = await actService.FindNewActsByArtefact(artefactGuid);

            if (result.Made != null && result.Made.Year.HasValue)
            {
                result.TimelineSameYear =  actService.FindNewActsByDates(result.Made.Year.ToString(), null);
            }
            else
            {
                result.TimelineSameYear = new List<NewAct>();
            }

            result.Documents = await actService.FindDocumentHyperNodesByReferrerAsync(artefactGuid);
            result.CharactersWithArtworks = await FindCharactersForArtworkAsync(artefactGuid);
            result.TechniquesWithArtworks = await FindTechniquesForArtworkAsync(artefactGuid);
            result.SameYear = await FindAroundMadeDateAsync(artefactGuid);
            result.SameAge = await FindArtworksMadeAtSameAgeAsync(artefactGuid);
            result.SimilarTo = await FindSimilarAsync(artefactGuid);
            result.Exhibitions = await FindExhibitionsOfAsync(artefactGuid);
            result.ExhibitionsIn = await FindExhibitionsInAsync(artefactGuid);
            result.Tours = await FindToursOfAsync(artefactGuid); ;

            return result;

        }

        public async Task<IEnumerable<ExhibitionDetails>> FindExhibitionsInAsync(string collectionGuid)
        {

            var query = Client.Cypher
                .Match("(collection:Artefact { Guid: {collectionGuid} })").WithParams(new { collectionGuid })
                .Match("(collection)-[:LOCATED_AT]->(location:Place)-[:INSIDE_OF*1..]->(inside:Place)")
                .Match("(exhibition)-[:TAKES_PLACE_IN]->(collection)")
                .Match("(artefact:Artefact)-[:HAS_RELATION]->()-[:EXHIBITED_IN]->(exhibition)")
                .Match("(exhibition)-[:HAS_DETAIL]->(detail:EventDetail)")
                .OptionalMatch("(artefact:Artefact)-[:HAS_RELATION]->()-[:EXHIBITED_IN]->(exhibition)")
                .OptionalMatch("(artefact)-[:HAS_RELATION]->()-[:MADE_BY]->(artist:Agent)")
                .OptionalMatch("(artefact)-[:HAS_IMAGE]->(image:Image { Primary: true })")
                .With("{ Artist: artist, ArtefactWithImages: collect(distinct { Artefact: artefact, Image: image }) } as Artworks, exhibition, detail, collection, location, inside")
                ;

            var results = await query
                .Return(() => new ExhibitionDetails
                {
                    Exhibition = Return.As<Artefact>("exhibition"),
                    Detail = Return.As<EventDetail>("detail"),
                    Collection = Return.As<Artefact>("collection"),
                    Location = Return.As<Place>("location"),
                    Inside = Return.As<IEnumerable<Place>>("collect(distinct(inside))"),
                    Artworks = Return.As<IEnumerable<ArtistWithArtefactImage>>("collect(distinct(Artworks))")
                })
                .ResultsAsync;

            return results;

        }

        public IEnumerable<TourDetails> FindToursIn(string artefactGuid)
        {

            var query = Client.Cypher
                .Match("(artefact:Artefact { Guid: {artefactGuid} })").WithParams(new { artefactGuid })
                .Match("(tour:Artefact)-[:" + Label.Relationship.StopsIn + "]->(collection:Artefact)")
                .Match("(tour)-[:HAS_DETAIL]->(detail:Detail)")
                .OptionalMatch("(tour)-[:" + Label.Relationship.StopsAt + "]->(location:Place)")
                .OptionalMatch("(tour)-[:HAS_RELATION]->()-[:" + Label.Relationship.TourFeatures + "]->(artefact:Artefact)")
                .OptionalMatch("(tour)-[:HAS_RELATION]->()-[:" + Label.Relationship.Covers + "]->(person:Agent)")
                .OptionalMatch("(artefact)-[:HAS_RELATION]->()-[:MADE_BY]->(artist:Agent)")
                .OptionalMatch("(artefact)-[:HAS_IMAGE]->(image:Image { Primary: true })")
                .With("{ Artist: artist, ArtefactWithImages: collect(distinct { Artefact: artefact, Image: image }) } as Artworks, tour, detail, collection, location")
                ;

            var results = query
                .Return(() => new TourDetails
                {
                    Tour = Return.As<Artefact>("tour"),
                    Detail = Return.As<TourDetail>("detail"),
                    Location = Return.As<Place>("location"),
                    Artworks = Return.As<IEnumerable<ArtistWithArtefactImage>>("collect(distinct(Artworks))")
                })
                .Results
                .ToList();

            return results;

        }


        public async Task<IEnumerable<TourDetails>> FindToursOfAsync(string artefactGuid)
        {

            var query = Client.Cypher
                .Match("(artefact:Artefact { Guid: {artefactGuid} })").WithParams(new { artefactGuid })
                .Match("(tour:Artefact)-[:HAS_RELATION]->()-[:" + Label.Relationship.TourFeatures + "]->(artefact)")
                .Match("(tour)-[:HAS_DETAIL]->(detail:Detail)")
                .OptionalMatch("(tour)-[:" + Label.Relationship.StopsAt + "]->(location:Place)")
                .OptionalMatch("(tour)-[:" + Label.Relationship.StopsIn + "]->(collection:Artefact)")
                .OptionalMatch("(tour)-[:HAS_RELATION]->()-[:" + Label.Relationship.Covers + "]->(person:Agent)")
                .OptionalMatch("(artefact)-[:HAS_RELATION]->()-[:MADE_BY]->(artist:Agent)")
                .OptionalMatch("(artefact)-[:HAS_IMAGE]->(image:Image { Primary: true })")
                .With("{ Artist: artist, ArtefactWithImages: collect(distinct { Artefact: artefact, Image: image }) } as Artworks, tour, detail, collection, location")
                ;

            var results = await query
                .Return(() => new TourDetails
                {
                    Tour = Return.As<Artefact>("tour"),
                    Detail = Return.As<TourDetail>("detail"),
                    Location = Return.As<Place>("location"),
                    Artworks = Return.As<IEnumerable<ArtistWithArtefactImage>>("collect(distinct(Artworks))")
                })
                .ResultsAsync;

            return results;

        }

        public async Task<IEnumerable<ExhibitionDetails>> FindExhibitionsOfAsync(string artefactGuid)
        {

            var query = Client.Cypher
                .Match("(artefact:Artefact { Guid: {artefactGuid} })").WithParams(new { artefactGuid })
                .Match("(artefact)-[:HAS_RELATION]->()-[:EXHIBITED_IN]->(exhibition:Artefact)")
                .Match("(exhibition)-[:TAKES_PLACE_IN]->(collection:Artefact)")
                .Match("(collection)-[:LOCATED_AT]->(location:Place)-[:INSIDE_OF*1..]->(inside:Place)")
                .Match("(exhibition)-[:HAS_DETAIL]->(detail:EventDetail)")
                .OptionalMatch("(artefact:Artefact)-[:HAS_RELATION]->()-[:EXHIBITED_IN]->(exhibition)")
                .OptionalMatch("(artefact)-[:HAS_RELATION]->()-[:MADE_BY]->(artist:Agent)")
                .OptionalMatch("(artefact)-[:HAS_IMAGE]->(image:Image { Primary: true })")
                .With("{ Artist: artist, ArtefactWithImages: collect(distinct { Artefact: artefact, Image: image }) } as Artworks, exhibition, detail, collection, location, inside")
                ;

            var results = await query
                .Return(() => new ExhibitionDetails
                {
                    Exhibition = Return.As<Artefact>("exhibition"),
                    Detail = Return.As<EventDetail>("detail"),
                    Collection = Return.As<Artefact>("collection"),
                    Location = Return.As<Place>("location"),
                    Inside = Return.As<IEnumerable<Place>>("collect(distinct(inside))"),
                    Artworks = Return.As<IEnumerable<ArtistWithArtefactImage>>("collect(distinct(Artworks))")
                })
                .ResultsAsync;

            return results;

        }

        private async Task<IEnumerable<AgentWithArtworkImageTuple>> FindCharactersForArtworkAsync(string artefactGuid)
        {

            var query = Client.Cypher
                .Match("(artefact:Artefact { Guid: {artefactGuid} })").WithParams(new { artefactGuid })
                .Match("(subartefact)-[:LOCATED_IN*0..]->(artefact)")
                .Match("(subartefact)-[:HAS_RELATION]->()-[:" + Label.Relationship.Represents + "]->(character:Agent)")
                .Match("(other:Artefact)-[]->()-[:" + Label.Relationship.Represents + "]->(character)")
                .Where("other.Guid <> artefact.Guid")
                .Match("(other)-[:HAS_IMAGE]->(image:Image { Primary: true })")

                ;

            var results = await query
                .Return(() => new AgentWithArtworkImageTuple
                {
                    Agent = Return.As<Agent>("character"),
                    Artworks = Return.As<IEnumerable<ArtefactImageTuple>>("collect(distinct({ Artefact: other, Image: image }))")
                })
                .OrderBy("character.Name")
                .ResultsAsync
               ;

            return results;

        }

        private List<CategoryWithArtworkImageTuple> FindSubjectsForArtwork(string artefactGuid)
        {

            var represents = Client.Cypher
                .Match("(artefact:Artefact { Guid: {artefactGuid} })").WithParams(new { artefactGuid })
                .Match("(subartefact)-[:LOCATED_IN*0..]->(artefact)")
                .Match("(subartefact)-[:HAS_RELATION]->()-[:" + Label.Relationship.HasSubject + "]->(subject:Category)")
                .Match("(subjects:Category)-[:KIND_OF*0..]->(subject)")
                .Match("(other:Artefact)-[]->()-[:" + Label.Relationship.HasSubject + "]->(subjects)")
                .Where("other.Guid <> artefact.Guid")
                .Match("(other)-[:HAS_IMAGE]->(image:Image { Primary: true })")
                .Return(() => new CategoryWithArtworkImageTuple
                {
                    Category = Return.As<Category>("subjects"),
                    Artworks = Return.As<IEnumerable<ArtefactImageTuple>>("collect(distinct({ Artefact: other, Image: image }))")
                })
                .OrderBy("subjects.Name")
                .Results
                .ToList();

            return represents.ToList();

        }

        private List<CategoryWithArtworkImageTuple> FindMotifsForArtwork(string artefactGuid)
        {

            var represents = Client.Cypher
                .Match("(artefact:Artefact { Guid: {artefactGuid} })").WithParams(new { artefactGuid })
                .Match("(subartefact)-[:LOCATED_IN*0..]->(artefact)")
                .Match("(subartefact)-[:HAS_RELATION]->(:Relation { Primary: true })-[:" + Label.Relationship.HasMotif + "]->(motif:Category)")
                .Match("(motifs:Category)-[:KIND_OF*0..]->(motif)")
                .Match("(other:Artefact)-[]->(:Relation { Primary: true })-[:" + Label.Relationship.HasMotif + "]->(motifs)")
                .Where("other.Guid <> artefact.Guid")
                .Match("(other)-[:HAS_IMAGE]->(image:Image { Primary: true })")
                .Return(() => new CategoryWithArtworkImageTuple
                {
                    Category = Return.As<Category>("motifs"),
                    Artworks = Return.As<IEnumerable<ArtefactImageTuple>>("collect(distinct({ Artefact: other, Image: image }))")
                })
                .OrderBy("motifs.Name")
                .Results
                .ToList();

            return represents.ToList();

        }

        private async Task<IEnumerable<CategoryWithArtworkImageTuple>> FindTechniquesForArtworkAsync(string artefactGuid)
        {

            var query = Client.Cypher
                .Match("(artefact:Artefact { Guid: {artefactGuid} })").WithParams(new { artefactGuid })
                .Match("(subartefact)-[:LOCATED_IN*0..]->(artefact)")
                .Match("(subartefact)-[:HAS_RELATION]->(:Relation { Primary: true })-[:" + Label.Relationship.Uses + "]->(technique:Category)")
                .Match("(techniques:Category)-[:KIND_OF*0..]->(technique)")
                .Match("(other:Artefact)-[]->(:Relation { Primary: true })-[:" + Label.Relationship.Uses + "]->(techniques)")
                .Where("other.Guid <> artefact.Guid")
                .Match("(other)-[:HAS_IMAGE]->(image:Image { Primary: true })")
                ;

            var results = await query
                .Return(() => new CategoryWithArtworkImageTuple
                {
                    Category = Return.As<Category>("techniques"),
                    Artworks = Return.As<IEnumerable<ArtefactImageTuple>>("collect(distinct({ Artefact: other, Image: image }))")
                })
                .OrderBy("techniques.Name")
                .ResultsAsync;

            return results;

        }

        private List<RelationAgentTuple> FindArtworkRepresents(string artefactGuid)
        {

            var query = Client.Cypher
                .Match("(artefact:Artefact { Guid: {artefactGuid} })").WithParams(new { artefactGuid })
                .Match("(subartefact)-[:LOCATED_IN*0..]->(artefact)")
                .Match("(subartefact)-[:HAS_RELATION]->(represents:Relation)-[:REPRESENTS]->(agent:Agent)")
                ;

            var represents = query
                .Return(() => new RelationAgentTuple
                {
                    Relation = Return.As<Relation>("represents"),
                    Agent = Return.As<Agent>("agent")
                })
                .OrderBy("agent.FullName")
                .Results
                .Distinct(new RelationAgentTupleComparer())
                .ToList();

            return represents;
        }

        private async Task<IEnumerable<ArtefactImageTuple>> FindWorksByArtistInSameCollectionAsync(string artefactGuid)
        {

            var query = Client.Cypher
                .Match("(artefact:Artefact { Guid: {artefactGuid} })").WithParams(new { artefactGuid })
                .Match("(artefact)-[]->()-[:MADE_BY]->(artist:Agent)")
                .Match("(otherArtefact:Artefact)-[]->()-[:MADE_BY]->(artist:Agent)")
                .Match("(artefact)-[:LOCATED_IN]->(container:Artefact)")
                .Match("(otherArtefact)-[:LOCATED_IN]->(container)")
                .Match("(otherArtefact)-[:HAS_IMAGE]->(image:Image { Primary: true })")
                .Where("artefact.Guid <> otherArtefact.Guid")
                ;

            var results = await query
                .Return(() => new ArtefactImageTuple
                {
                    Artefact = Return.As<Artefact>("otherArtefact"),
                    Image = Return.As<Image>("image")
                })
                .ResultsAsync;

            return results;

        }

        private void FindOtherArtefactsWithTheSameAgents(string artefactGuid, ViewArtefactPublic result)
        {

            var query = Client.Cypher
                .Match("(artefact:Artefact { Guid: {artefactGuid} })").WithParams(new { artefactGuid })
                .Match("(artefact)-[]->()-[:REPRESENTS]->(agent:Agent)")
                .Match("(artefact2:Artefact)-[]->()-[:REPRESENTS]->(agent)")
                .Match("(artefact2)-[:HAS_IMAGE]->(image:Image { Primary: true })")
                .Where("artefact.Guid <> artefact2.Guid")
                ;

            var sameAgents = query
                .Return(() => new ArtefactImageTuple
                {
                    Artefact = Return.As<Artefact>("artefact2"),
                    Image = Return.As<Image>("image")
                })
                .Results
                .ToList();

            result.SameAgents = sameAgents;

        }

        private async Task<IEnumerable<ArtistArtefactImageTuple>> FindArtworksMadeAtSameAgeAsync(string artefactGuid)
        {

            var query = Client.Cypher
                .Match("(artefact:Artefact { Guid: {artefactGuid} })").WithParams(new { artefactGuid })
                .Match("(artefact)-[:HAS_RELATION]-(made:Relation)-[:MADE_BY]->(artist:Agent)")
                .Match("(artist)-[:HAS_RELATION]->(born:Relation)-[:BORN_AT]->(:Place)")
                .With("(made.Year - born.Year) as Age, artefact")
                .Match("(artefact2:Artefact)-[:HAS_RELATION]->(made2:Relation)-[:MADE_BY]->(artist:Agent)")
                .Match("(artist)-[:HAS_RELATION]->(born2:Relation)-[:BORN_AT]->(:Place)")
                .Where("artefact2.Guid <> artefact.Guid and Age = (made2.Year - born2.Year)")
                .Match("(artefact2)-[:HAS_IMAGE]->(image:Image { Primary: true })")
                ;

            var results = await query
                .Return(() => new ArtistArtefactImageTuple
                {
                    Artefact = Return.As<Artefact>("artefact2"),
                    Artist = Return.As<Agent>("artist"),
                    Image = Return.As<Image>("image")
                })
                .ResultsAsync;

            return results;

        }

        private async Task<IEnumerable<ArtistArtefactImageTuple>> FindAroundMadeDateAsync(string artefactGuid)
        {

            var query = Client.Cypher
                .Match("(artefact:Artefact { Guid: {artefactGuid} })").WithParams(new { artefactGuid })
                .Match("(artefact)-[:HAS_RELATION]-(made:Relation)-[:MADE_BY]->(:Agent)")
                .Match("(artefact2:Artefact)-[:HAS_RELATION]->(made2:Relation)-[:MADE_BY]->(artist:Agent)")
                .Where("artefact2.Guid <> artefact.Guid and made2.Year = made.Year")
                .Match("(artefact2)-[:HAS_IMAGE]->(image:Image { Primary: true })")
                ;

            var results = await query
                .Return(() => new ArtistArtefactImageTuple
                {
                    Artefact = Return.As<Artefact>("artefact2"),
                    Artist = Return.As<Agent>("artist"),
                    Image = Return.As<Image>("image")
                })
                .ResultsAsync;

            return results;

        }

        private IEnumerable<PlaceCategoryTuple> FindPlaces(string artefactGuid)
        {
            var query = Client.Cypher
                .Match("(artefact:Artefact { Guid: {artefactGuid} })").WithParams(new { artefactGuid })
                .OptionalMatch("atTopLevel=(artefact)-[:LOCATED_AT]->(location1:Place)")
                .OptionalMatch("(artefact)-[:LOCATED_IN*0..]->()-[:LOCATED_AT]->(location2:Place)")
                .OptionalMatch("(location1)-[:INSIDE_OF*0..]->(inside1:Place)")
                .OptionalMatch("(location2)-[:INSIDE_OF*0..]->(inside2:Place)")
                .OptionalMatch("(inside1)-[:IS_A]->(insideType1:Category)")
                .OptionalMatch("(inside2)-[:IS_A]->(insideType2:Category)")
                ;

            var results = query
                .Return(() => new
                {
                    Inside1 = Return.As<IEnumerable<PlaceCategoryTuple>>("collect(distinct({ Place: inside1, Category: insideType1 }))"),
                    Inside2 = Return.As<IEnumerable<PlaceCategoryTuple>>("collect(distinct({ Place: inside2, Category: insideType2 }))"),
                    AtTopLevel = Return.As<bool>("length(atTopLevel) > 0")
                })
                .Results;

            return results
                .AsParallel()
                .SelectMany(x => x.AtTopLevel ? x.Inside1 : x.Inside2)
                .Where(x => x.Place != null)
                .Distinct(new PlaceCategoryTupleComparer());
        }

        private async Task<IEnumerable<PlaceCategoryTuple>> FindPlacesAsync(string artefactGuid)
        {
            var query = Client.Cypher
                .Match("(artefact:Artefact { Guid: {artefactGuid} })").WithParams(new { artefactGuid })
                .OptionalMatch("atTopLevel=(artefact)-[:LOCATED_AT]->(location1:Place)")
                .OptionalMatch("(artefact)-[:LOCATED_IN*0..]->()-[:LOCATED_AT]->(location2:Place)")
                .OptionalMatch("(location1)-[:INSIDE_OF*0..]->(inside1:Place)")
                .OptionalMatch("(location2)-[:INSIDE_OF*0..]->(inside2:Place)")
                .OptionalMatch("(inside1)-[:IS_A]->(insideType1:Category)")
                .OptionalMatch("(inside2)-[:IS_A]->(insideType2:Category)")
                ;

            var results = await query
                .Return(() => new
                {
                    Inside1 = Return.As<IEnumerable<PlaceCategoryTuple>>("collect(distinct({ Place: inside1, Category: insideType1 }))"),
                    Inside2 = Return.As<IEnumerable<PlaceCategoryTuple>>("collect(distinct({ Place: inside2, Category: insideType2 }))"),
                    AtTopLevel = Return.As<bool>("length(atTopLevel) > 0")
                })
                .ResultsAsync;

            return results
                .AsParallel()
                .SelectMany(x => x.AtTopLevel ? x.Inside1 : x.Inside2)
                .Where(x => x.Place != null)
                .Distinct(new PlaceCategoryTupleComparer());
        }

        private IEnumerable<Artefact> FindContainers(string artefactGuid)
        {
            var query = Client.Cypher
                .Match("(artefact:Artefact { Guid: {artefactGuid} })").WithParams(new { artefactGuid })
                .Match("(artefact)-[:LOCATED_IN*0..]->(container:Artefact)")
                ;

            var results = query
                .Return(() => Return.As<Artefact>("container"))
                .Results;

            return results;
        }

        private async Task<IEnumerable<Artefact>> FindContainersAsync(string artefactGuid)
        {
            var query = Client.Cypher
                .Match("(artefact:Artefact { Guid: {artefactGuid} })").WithParams(new { artefactGuid })
                .Match("(artefact)-[:LOCATED_IN*0..]->(container:Artefact)")
                ;

            var results = await query
                .Return(() => Return.As<Artefact>("container"))
                .ResultsAsync;

            return results;
        }

        private async Task<IEnumerable<Agent>> FindCreatorsAsync(string artefactGuid)
        {
            var query = Client.Cypher
                .Match("(artefact:Artefact { Guid: {artefactGuid} })").WithParams(new { artefactGuid })
                .Match("(artefact)-[]->()-[:MADE_BY]->(agent:Agent)")
                ;

            var results = await query
                .Return(() => Return.As<Agent>("agent"))
                .ResultsAsync;

            return results;
        }

        private async Task<IEnumerable<ArtefactImageTuple>> FindArtefactsWithSameSubjectAtSamePlaceAsync(string artefactGuid)
        {
            var query = Client.Cypher
                .Match("(artefact:Artefact { Guid: {artefactGuid} })").WithParams(new { artefactGuid })
                .Match("(artefact)-[:LOCATED_IN*0..]->(:Artefact)-[:LOCATED_AT]->(location:Place)-[:INSIDE_OF*0..]->(inside:Place)")
                .Match("(inside)-[:IS_A]->(placeType:Category { Name: {placeType} })").WithParams(new { placeType = "City" })
                .Match("(artefact)-[]->()-[:HAS_SUBJECT]->(subject:Category)")
                .Match("(artefact2:Artefact)-[]->()-[:HAS_SUBJECT]->(subject)")
                .Match("(artefact2)-[:LOCATED_IN*0..]->()-[:LOCATED_AT]->(inside)")
                .Match("(artefact2)-[:HAS_IMAGE]->(image:Image { Primary: true })")
                .Where("artefact2.Guid <> artefact.Guid")
                ;

            var results = await query
                .Return(() => new ArtefactImageTuple
                {
                    Artefact = Return.As<Artefact>("artefact2"),
                    Image = Return.As<Image>("image")
                })
                .ResultsAsync
                ;

            return results;

        }

        public IEnumerable<ArtistArtefactImageTuple> RecommendedArtefactsByUser(string userGuid)
        {
            var query = Client.Cypher
                .Match("(user:User { Guid: {userGuid} })").WithParams(new { userGuid })
                .Match("(user)-[:BOOKMARKED]->(artefact:Artefact)")
                .Match("p=(otherUser:User)-[:BOOKMARKED]->(artefact)")
                .Match("(otherUser)-[:BOOKMARKED]->(otherArtefact:Artefact)")
                .Where("NOT((user)-[:BOOKMARKED]->(otherArtefact))")
                .Match("(otherArtefact)-[:HAS_RELATION]->()-[:MADE_BY]->(artist:Agent)")
                .Match("(otherArtefact)-[:HAS_IMAGE]->(image:Image { Primary: true })")
                .With("distinct(otherArtefact) as art, artist, image")
                ;
            var results = query
                .Return(() => new ArtistArtefactImageTuple
                {
                    Artist = Return.As<Agent>("artist"),
                    Artefact = Return.As<Artefact>("art"),
                    Image = Return.As<Image>("image")
                })
                .Results
                .Distinct()
                .ToList();
            return results;
        }

        public IEnumerable<ArtistArtefactImageTuple> SimilarArtefactsByUser(string userGuid)
        {
            var query = Client.Cypher
                .Match("(user:User { Guid: {userGuid} })").WithParams(new { userGuid })
                .Match("(user)-[:BOOKMARKED]->(artefact:Artefact)")
                .Match("(artefact)-[:HAS_RELATION]->(:Relation { Primary: true })-[:HAS_MOTIF|USES|HAS_GENRE]->(type:Category)")
                .Match("(subtypes:Category)-[:KIND_OF*0..]->(type)")
                .Match("p=(artefact2:Artefact)-[:HAS_RELATION]->(:Relation { Primary: true })-[:HAS_MOTIF|USES|HAS_GENRE]->(subtypes:Category)")
                .Where("NOT((user)-[:BOOKMARKED]->(artefact2) and length(p) <= 4) and artefact2.Rating >= 4")
                .With("count(subtypes) as SubtypeTotal, artefact2")
                .Match("(artefact2)-[:HAS_RELATION]->()-[:MADE_BY]->(artist:Agent)")
                .Match("(artefact2)-[:HAS_IMAGE]->(image:Image { Primary: true })")
                .Where("SubtypeTotal >= 2")
                .With("distinct(artefact2) as art, artist, image")
                ;
            var results = query
                .Return(() => new ArtistArtefactImageTuple
                {
                    Artist = Return.As<Agent>("artist"),
                    Artefact = Return.As<Artefact>("art"),
                    Image = Return.As<Image>("image")
                })
                .Results
                .Distinct()
                .ToList();
            return results;
        }

        public IEnumerable<ArtistArtefactImageTuple> FindSeenArtefacts(string userGuid)
        {
            var query = Client.Cypher
                .Match("(user:User { Guid: {userGuid} })").WithParams(new { userGuid })
                .Match("(user)-[:" + Label.Relationship.Saw + "]->(artefact:Artefact)")
                .OptionalMatch("(artefact)-[:LOCATED_IN*0..]->(topContainer:Artefact)-[:LOCATED_AT]->(location:Place)")
                .OptionalMatch("(artefact)-[:HAS_RELATION]->()-[:MADE_BY]->(artist:Agent)")
                .OptionalMatch("(artefact)-[:HAS_RELATION]->()-[:IS_A]->(artefactType:Category)")
                .OptionalMatch("(artefact)-[:HAS_IMAGE]->(image:Image { Primary: true })")
                ;
            var results = query
                .Return(() => new ArtistArtefactImageTuple
                {
                    Artefact = Return.As<Artefact>("artefact"),
                    TopContainer = Return.As<Artefact>("topContainer"),
                    Location = Return.As<Place>("location"),
                    Artist = Return.As<Agent>("head(collect(distinct(artist)))"),
                    Image = Return.As<Image>("head(collect(distinct(image)))"),
                    ArtefactType = Return.As<Category>("head(collect(distinct(artefactType)))")
                })
                .Results
                .ToList();
            return results;
        }

        public IEnumerable<ArtistArtefactImageTuple> FindBookmarkedArtefacts(string userGuid)
        {
            var query = Client.Cypher
                .Match("(user:User { Guid: {userGuid} })").WithParams(new { userGuid })
                .Match("(user)-[:" + Label.Relationship.Bookmarked + "]->(artefact:Artefact)")
                .OptionalMatch("(artefact)-[:LOCATED_IN*0..]->(topContainer:Artefact)-[:LOCATED_AT]->(location:Place)")
                .OptionalMatch("(artefact)-[:HAS_RELATION]->()-[:MADE_BY]->(artist:Agent)")
                .OptionalMatch("(artefact)-[:HAS_RELATION]->()-[:IS_A]->(artefactType:Category)")
                .OptionalMatch("(artefact)-[:HAS_IMAGE]->(image:Image { Primary: true })")
                ;
            var results = query
                .Return(() => new ArtistArtefactImageTuple
                {
                    Artefact = Return.As<Artefact>("artefact"),
                    //Container = Return.As<Artefact>("container"),
                    TopContainer = Return.As<Artefact>("topContainer"),
                    Location = Return.As<Place>("location"),
                    Artist = Return.As<Agent>("head(collect(distinct(artist)))"),
                    Image = Return.As<Image>("head(collect(distinct(image)))"),
                    ArtefactType = Return.As<Category>("head(collect(distinct(artefactType)))")
                })
                .Results
                .ToList();
            return results;
        }

        public async Task<IEnumerable<ContainerWithArtefacts>> FindContainerWithArtefactsAsync(string artefactGuid)
        {
            var query = Client.Cypher
                .Match("(artefact:Artefact { Guid: {artefactGuid} })").WithParams(new { artefactGuid })
                .Match("(subartefact:Artefact)-[:LOCATED_IN*0..]->(artefact)")
                .Match("(subartefact)-[:LOCATED_IN]->(container:Artefact)")
                .Match("p=(container)-[:LOCATED_IN*0..]->(artefact)")
                .Match("(subartefact)-[:HAS_RELATION]->()-[:MADE_BY]->(artist:Agent)")
                .Match("(subartefact)-[:HAS_IMAGE]->(image:Image { Primary: true })")
                .Match("(subartefact)-[:HAS_RELATION]->()-[:IS_A]->(artefactType:Category)")
                .OptionalMatch("(subartefact)-[:HAS_RELATION]->()-[:HAS_SUBJECT]->(subject:Category)")
                .OptionalMatch("(subartefact)-[:HAS_RELATION]->()-[:HAS_GENRE]->(genre:Category)")
                .OptionalMatch("(artist)-[:HAS_RELATION]->()-[:REPRESENTATIVE_OF]->(representativeOf:Category)")
                .With("container, collect(distinct({ Artefact: subartefact, ArtefactType: artefactType, Subject: subject, Genre: genre, Image: image, Artist: artist, ArtistRepresentativeOf: representativeOf })) as artworks, length(p) as Depth")
                // .Where("Depth <= 1") -- works in principle but we exclude artefacts without images, so some sub-artefacts get lost inadvertently
                ;

            var results = await query
                .Return(() => new ContainerWithArtefacts
                {
                    Container = Return.As<Artefact>("container"),
                    Depth = Return.As<int>("Depth"),
                    Artworks = Return.As<IEnumerable<ArtistArtefactImageTuple>>("artworks")
                })
                .OrderBy("Depth")
                .ResultsAsync;

            return results;
        }
        public ViewArtefactRepresents CollectionsRepresentingAgent(string agentGuid, string placeGuid = null)
        {
            var query = Client.Cypher
                .Match("(agent:Agent { Guid: {agentGuid} })").WithParams(new { agentGuid })
                .Match("(agents:Agent)-[:ASPECT_OF*0..]->(agent)")
                .Match("(artefact:Artefact)-[:HAS_RELATION]->()-[:REPRESENTS]->(agents)")
                .Match("(artefact)-[:HAS_RELATION]->()-[:MADE_BY]->(artist:Agent)");
            if (placeGuid != null)
            {
                query = query
                .Match("(place:Place { Guid: {placeGuid} })").WithParams(new { placeGuid })
                .Match("(places:Place)-[:INSIDE_OF*0..]->(place)")
                .Match("(collection:Artefact)-[:LOCATED_AT]->(places)");
            }
            else
            {
                query = query
                .Match("(collection:Artefact)-[:LOCATED_AT]->(places:Place)");
            }
            query = query
                .Match("(artefact)-[:LOCATED_IN*0..]->(collection)")
                .Match("(subartefact:Artefact)-[:LOCATED_IN*0..]->(artefact)")
                .Match("(subartefact)-[:HAS_IMAGE]->(image:Image { Primary: true })")
                .Match("(subartefact)-[:LOCATED_IN]->(container:Artefact)")
                .OptionalMatch("p=(container)-[:LOCATED_IN*0..]->(artefact)")
                .With("container, collect(distinct({ Artefact: subartefact, Image: image, Artist: artist })) as artworks, length(p) as Depth, places")
                ;
            var containerWithArtefacts = query
                .Return(() => new ContainerWithArtefacts
                {
                    Container = Return.As<Artefact>("container"),
                    Location = Return.As<Place>("places"),
                    Artworks = Return.As<IEnumerable<ArtistArtefactImageTuple>>("artworks")
                })
               .Results
               .ToList();
            var agent = Client.Cypher.Match("(agent:Agent { Guid: {agentGuid} })").WithParams(new { agentGuid }).Return(() => Return.As<Agent>("agent")).Results.FirstOrDefault();
            var result = new ViewArtefactRepresents
            {
                Agent = agent,
                ContainerWithArtefacts = containerWithArtefacts
            };
            if (placeGuid != null)
            {
                var place = Client.Cypher.Match("(place:Place { Guid: {placeGuid} })").WithParams(new { placeGuid }).Return(() => Return.As<Place>("place")).Results.FirstOrDefault();
                result.Place = place;
            }
            return result;
        }
        public IEnumerable<ContainerWithArtefacts> FindContainerWithArtefactsByCharacter(string agentGuid)
        {
            var result = Client.Cypher
                .Match("(character:Agent { Guid: {agentGuid} })").WithParams(new { agentGuid })
                .Match("(characters:Agent)-[:ASPECT_OF*0..]->(character)")
                .Match("(artefact:Artefact)-[]->()-[:REPRESENTS]->(characters)")
                .Match("(artefact)-[:HAS_RELATION]->()-[:MADE_BY]->(artist)")
                .Match("(subartefact:Artefact)-[:LOCATED_IN*0..]->(artefact)")
                .Match("(subartefact)-[:HAS_RELATION]->()-[:MADE_BY]->(artist)")
                .Match("(subartefact)-[:HAS_IMAGE]->(image:Image { Primary: true })")
                .Match("(subartefact)-[:LOCATED_IN]->(container:Artefact)")
                .OptionalMatch("(artefact)-[:HAS_RELATION]->()-[:HAS_SUBJECT]->(subject:Category)")
                .OptionalMatch("p=(container)-[:LOCATED_IN*0..]->(artefact)")
                .OptionalMatch("atTopLevel=(artefact)-[:LOCATED_AT]->(location1:Place)")
                .OptionalMatch("(artefact)-[:LOCATED_IN*0..]->(topContainer:Artefact)-[:LOCATED_AT]->(location2:Place)")
                .With("container, collect(distinct({ Artefact: subartefact, Subject: subject, Image: image, Artist: artist })) as artworks, length(p) as Depth, location1, location2, atTopLevel, topContainer")
                .Return(() => new
                {
                    Container = Return.As<Artefact>("container"),
                    Location1 = Return.As<Place>("location1"),
                    Location2 = Return.As<Place>("location2"),
                    AtTopLevel = Return.As<bool>("length(atTopLevel) > 0"),
                    TopContainer = Return.As<Artefact>("topContainer"),
                    Artworks = Return.As<IEnumerable<ArtistArtefactImageTuple>>("artworks")
                })
                .OrderBy("Depth")
                .Results
                .Select(x => new ContainerWithArtefacts
                {
                    Container = x.Container,
                    Artworks = x.Artworks,
                    Location = x.AtTopLevel ? x.Location1 : x.Location2,
                    TopContainer = false == x.AtTopLevel ? x.TopContainer : null
                })
                .ToList();
            return result;
        }
        public IEnumerable<ContainerWithArtefacts> FindContainerWithArtefactsByMotif(string motifGuid)
        {
            var result = Client.Cypher
                .Match("(motif:Category { Guid: {motifGuid} })").WithParams(new { motifGuid })
                .Match("(artefact:Artefact)-[:HAS_RELATION]->()-[:HAS_MOTIF*0..]->(motif)")
                .Match("(artefact)-[:HAS_RELATION]->()-[:MADE_BY]->(artist)")
                .Match("(subartefact:Artefact)-[:LOCATED_IN*0..]->(artefact)")
                .Match("(subartefact)-[:HAS_RELATION]->()-[:MADE_BY]->(artist)")
                .Match("(subartefact)-[:HAS_IMAGE]->(image:Image { Primary: true })")
                .Match("(subartefact)-[:LOCATED_IN]->(container:Artefact)")
                .OptionalMatch("p=(container)-[:LOCATED_IN*0..]->(artefact)")
                .OptionalMatch("atTopLevel=(artefact)-[:LOCATED_AT]->(location1:Place)")
                .OptionalMatch("(artefact)-[:LOCATED_IN*0..]->(topContainer:Artefact)-[:LOCATED_AT]->(location2:Place)")
                .With("container, collect(distinct({ Artefact: subartefact, Motif: motif, Image: image, Artist: artist })) as artworks, length(p) as Depth, location1, location2, atTopLevel, topContainer")
                .Return(() => new
                {
                    Container = Return.As<Artefact>("container"),
                    Location1 = Return.As<Place>("location1"),
                    Location2 = Return.As<Place>("location2"),
                    AtTopLevel = Return.As<bool>("length(atTopLevel) > 0"),
                    TopContainer = Return.As<Artefact>("topContainer"),
                    Artworks = Return.As<IEnumerable<ArtistArtefactImageTuple>>("artworks")
                })
                .OrderBy("Depth")
                .Results
                .Select(x => new ContainerWithArtefacts
                {
                    Container = x.Container,
                    Artworks = x.Artworks,
                    Location = x.AtTopLevel ? x.Location1 : x.Location2,
                    TopContainer = false == x.AtTopLevel ? x.TopContainer : null
                })
                .ToList();
            return result;
        }

        public async Task<IEnumerable<ContainerWithArtefacts>> FindContainerWithArtefactsByStyleAsync(string styleGuid)
        {

            var query1 = Client.Cypher
                .Match("(style:Category { Guid: {styleGuid} })").WithParams(new { styleGuid })
                .Match("(styles:Category)-[:KIND_OF*0..]->(style)")
                .Match("(artefact:Artefact)-[]->()-[:REPRESENTATIVE_OF]->(styles)")
                .Match("(artefact)-[:HAS_RELATION]->()-[:MADE_BY]->(artist:Agent)")
                .Match("(subartefact:Artefact)-[:LOCATED_IN*0..]->(artefact)")
                .Match("(subartefact)-[:HAS_RELATION]->()-[:MADE_BY]->(artist)")
                .Match("(subartefact)-[:HAS_IMAGE]->(image:Image { Primary: true })")
                .Match("(subartefact)-[:LOCATED_IN]->(container:Artefact)")
                .OptionalMatch("p=(container)-[:LOCATED_IN*0..]->(artefact)")
                .OptionalMatch("atTopLevel=(artefact)-[:LOCATED_AT]->(location1:Place)")
                .OptionalMatch("(artefact)-[:LOCATED_IN*0..]->(topContainer:Artefact)-[:LOCATED_AT]->(location2:Place)")
                .With("container, collect(distinct({ Artefact: subartefact, Image: image, Artist: artist })) as artworks, length(p) as Depth, location1, location2, atTopLevel, topContainer")
                ;
            var result1 = await query1
                .Return(() => new
                {
                    Container = Return.As<Artefact>("container"),
                    Location1 = Return.As<Place>("location1"),
                    Location2 = Return.As<Place>("location2"),
                    AtTopLevel = Return.As<bool>("length(atTopLevel) > 0"),
                    TopContainer = Return.As<Artefact>("topContainer"),
                    Artworks = Return.As<IEnumerable<ArtistArtefactImageTuple>>("artworks")
                })
                .OrderBy("Depth")
                .ResultsAsync;

            var result1a = result1
                .AsParallel()
                .Select(x => new ContainerWithArtefacts
                {
                    Container = x.Container,
                    Artworks = x.Artworks,
                    Location = x.AtTopLevel ? x.Location1 : x.Location2,
                    TopContainer = false == x.AtTopLevel ? x.TopContainer : null
                })
                .ToList();

            var query2 = Client.Cypher
                .Match("(style:Category { Guid: {styleGuid} })").WithParams(new { styleGuid })
                .Match("(styles:Category)-[:KIND_OF*0..]->(style)")
                .Match("(artist:Agent)-[]->()-[:REPRESENTATIVE_OF]->(styles)")
                .Match("(artefact)-[:HAS_RELATION]->()-[:MADE_BY]->(artist:Agent)")
                .Match("(subartefact:Artefact)-[:LOCATED_IN*0..]->(artefact)")
                .Match("(subartefact)-[:HAS_RELATION]->()-[:MADE_BY]->(artist)")
                .Match("(subartefact)-[:HAS_IMAGE]->(image:Image { Primary: true })")
                .Match("(subartefact)-[:LOCATED_IN]->(container:Artefact)")
                .OptionalMatch("p=(container)-[:LOCATED_IN*0..]->(artefact)")
                .OptionalMatch("atTopLevel=(artefact)-[:LOCATED_AT]->(location1:Place)")
                .OptionalMatch("(artefact)-[:LOCATED_IN*0..]->(topContainer:Artefact)-[:LOCATED_AT]->(location2:Place)")
                .With("container, collect(distinct({ Artefact: subartefact, Image: image, Artist: artist })) as artworks, length(p) as Depth, location1, location2, atTopLevel, topContainer")
                ;

            var result2 = await query2
                .Return(() => new
                {
                    Container = Return.As<Artefact>("container"),
                    Location1 = Return.As<Place>("location1"),
                    Location2 = Return.As<Place>("location2"),
                    AtTopLevel = Return.As<bool>("length(atTopLevel) > 0"),
                    TopContainer = Return.As<Artefact>("topContainer"),
                    Artworks = Return.As<IEnumerable<ArtistArtefactImageTuple>>("artworks")
                })
                .OrderBy("Depth")
                .ResultsAsync;

            var result2a = result2
                .AsParallel()
                .Select(x => new ContainerWithArtefacts
                {
                    Container = x.Container,
                    Artworks = x.Artworks,
                    Location = x.AtTopLevel ? x.Location1 : x.Location2,
                    TopContainer = false == x.AtTopLevel ? x.TopContainer : null
                })
                .ToList();

            var result = new List<ContainerWithArtefacts>();

            return result
                .Concat(result1a)
                .Concat(result2a);
        }

        public IEnumerable<ContainerWithArtefacts> FindContainerWithArtefactsBySubject(string subjectGuid)
        {
            var result = Client.Cypher
                .Match("(subject:Category { Guid: {subjectGuid} })").WithParams(new { subjectGuid })
                .Match("(artefact:Artefact)-[]->()-[:HAS_SUBJECT]->(subject)")
                .Match("(artefact)-[:HAS_RELATION]->()-[:MADE_BY]->(artist)")
                .Match("(subartefact:Artefact)-[:LOCATED_IN*0..]->(artefact)")
                .Match("(subartefact)-[:HAS_RELATION]->()-[:MADE_BY]->(artist)")
                .Match("(subartefact)-[:HAS_IMAGE]->(image:Image { Primary: true })")
                .Match("(subartefact)-[:LOCATED_IN]->(container:Artefact)")
                .OptionalMatch("p=(container)-[:LOCATED_IN*0..]->(artefact)")
                .OptionalMatch("atTopLevel=(artefact)-[:LOCATED_AT]->(location1:Place)")
                .OptionalMatch("(artefact)-[:LOCATED_IN*0..]->(topContainer:Artefact)-[:LOCATED_AT]->(location2:Place)")
                .With("container, collect(distinct({ Artefact: subartefact, Subject: subject, Image: image, Artist: artist })) as artworks, length(p) as Depth, location1, location2, atTopLevel, topContainer")
                .Return(() => new
                {
                    Container = Return.As<Artefact>("container"),
                    Location1 = Return.As<Place>("location1"),
                    Location2 = Return.As<Place>("location2"),
                    AtTopLevel = Return.As<bool>("length(atTopLevel) > 0"),
                    TopContainer = Return.As<Artefact>("topContainer"),
                    Artworks = Return.As<IEnumerable<ArtistArtefactImageTuple>>("artworks")
                })
                .OrderBy("Depth")
                .Results
                .Select(x => new ContainerWithArtefacts
                {
                    Container = x.Container,
                    Artworks = x.Artworks,
                    Location = x.AtTopLevel ? x.Location1 : x.Location2,
                    TopContainer = false == x.AtTopLevel ? x.TopContainer : null
                })
                .ToList();
            return result;
        }

        public async Task<IEnumerable<ContainerWithArtefacts>> FindContainerWithArtefactsByAgentAsync(string agentGuid)
        {
            var query = Client.Cypher
                .Match("(artist:Agent { Guid: {agentGuid} })").WithParams(new { agentGuid })
                .Match("(artefact:Artefact)-[:HAS_RELATION]->()-[:MADE_BY]->(artist)")
                .Match("(subartefact:Artefact)-[:LOCATED_IN*0..]->(artefact)")
                .Match("(subartefact)-[:HAS_RELATION]->()-[:IS_A]->(artefactType:Category)")
                .Match("(subartefact)-[:HAS_RELATION]->()-[:MADE_BY]->(artist)")
                .Match("(subartefact)-[:HAS_IMAGE]->(image:Image { Primary: true })")
                .Match("(subartefact)-[:LOCATED_IN]->(container:Artefact)")
                .OptionalMatch("p=(container)-[:LOCATED_IN*0..]->(artefact)")
                .OptionalMatch("atTopLevel=(artefact)-[:LOCATED_AT]->(location1:Place)")
                .OptionalMatch("(artefact)-[:LOCATED_IN*0..]->(topContainer:Artefact)-[:LOCATED_AT]->(location2:Place)")
                .OptionalMatch("(artefact)-[:HAS_RELATION]->()-[:HAS_SUBJECT]->(subject:Category)")
                .OptionalMatch("(artefact)-[:HAS_RELATION]->()-[:HAS_GENRE]->(genre:Category)")
                .With("container, collect(distinct({ Artefact: subartefact, ArtefactType: artefactType, Genre: genre, Subject: subject, Image: image, Artist: artist })) as artworks, length(p) as Depth, location1, location2, atTopLevel, topContainer")
                ;

            var result = await query
                .Return(() => new
                {
                    Container = Return.As<Artefact>("container"),
                    Location1 = Return.As<Place>("location1"),
                    Location2 = Return.As<Place>("location2"),
                    AtTopLevel = Return.As<bool>("length(atTopLevel) > 0"),
                    TopContainer = Return.As<Artefact>("topContainer"),
                    Artworks = Return.As<IEnumerable<ArtistArtefactImageTuple>>("artworks")
                })
                .OrderBy("Depth")
                .ResultsAsync
                ;

            return result
                .AsParallel()
                .Select(x => new ContainerWithArtefacts
                {
                    Container = x.Container,
                    Artworks = x.Artworks,
                    Location = x.AtTopLevel ? x.Location1 : x.Location2,
                    TopContainer = false == x.AtTopLevel ? x.TopContainer : null
                });
        }

        public void SaveOrUpdate(ContextHyperNode node)
        {
            var isNew = node.Context.Guid == null;
            commonService.SaveOrUpdate(node.Context, Label.Entity.Context);
            if (false == isNew)
            {
                DeleteContextRelations(node.Context.Guid);
            }
            // artefact-[:HAS_CONTEXT]->context
            commonService.CreateRelationship(Label.Entity.Artefact, node.Artefact.Guid, Label.Entity.Context, node.Context.Guid, Label.Relationship.HasContext);
            // context-[:CONTEXT_FOR]->subject
            commonService.CreateRelationship(Label.Entity.Context, node.Context.Guid, Label.Entity.Category, node.Subject.Guid, Label.Relationship.ContextFor);
            // context-[:CONTEXT_AT]->place
            commonService.CreateRelationship(Label.Entity.Context, node.Context.Guid, Label.Entity.Place, node.Place.Guid, Label.Relationship.ContextAt);
        }
        /// <summary>
        /// Before regenerating the relations we will want to blow them away.
        /// Note: prefer not to delete the Context node itself, though, as it has already been updated by now.
        /// This is essentially the groundwork for a 'hypernode update' operation rather than a 'delete hypernode' operation.
        /// </summary>
        /// <param name="node"></param>
        private void DeleteContextRelations(string contextGuid)
        {
            Client.Cypher.Match("(context:" + Label.Entity.Context + " { Guid: {contextGuid} })").WithParams(new { contextGuid })
                .Match("(entity)-[hasContext:" + Label.Relationship.HasContext + "]->(context)")
                .Match("(context)-[contextFor:" + Label.Relationship.ContextFor + "]->(subject:" + Label.Entity.Category + ")")
                .Match("(context)-[contextAt:" + Label.Relationship.ContextAt + "]->(place:" + Label.Entity.Place + ")")
                .Delete("hasContext, contextFor, contextAt")
                .ExecuteWithoutResults();
        }
        /// <summary>
        /// Deletes the context as well as its relations.
        /// </summary>
        /// <param name="contextGuid"></param>
        public void DeleteContext(string contextGuid)
        {
            Client.Cypher.Match("(context:" + Label.Entity.Context + " { Guid: {contextGuid} })").WithParams(new { contextGuid })
                .Match("(entity)-[hasContext:" + Label.Relationship.HasContext + "]->(context)")
                .Match("(context)-[contextFor:" + Label.Relationship.ContextFor + "]->(subject:" + Label.Entity.Category + ")")
                .Match("(context)-[contextAt:" + Label.Relationship.ContextAt + "]->(place:" + Label.Entity.Place + ")")
                .Delete("hasContext, contextFor, contextAt, context")
                .ExecuteWithoutResults();
        }
        public IEnumerable<ContextHyperNode> FindContextsByArtefact(string artefactGuid)
        {
            var query = Client.Cypher
                .Match("(artefact:" + Label.Entity.Artefact + " { Guid: {artefactGuid} })").WithParams(new { artefactGuid })
                .Match("(artefact)-[hasContext:" + Label.Relationship.HasContext + "]->(context:" + Label.Entity.Context + ")")
                .Match("(context)-[contextFor:" + Label.Relationship.ContextFor + "]->(subject:" + Label.Entity.Category + ")")
                .Match("(context)-[contextAt:" + Label.Relationship.ContextAt + "]->(place:" + Label.Entity.Place + ")");
            var results = query.Return(() => new ContextHyperNode
            {
                Context = Return.As<Context>("context"),
                Artefact = Return.As<Artefact>("artefact"),
                Subject = Return.As<Category>("subject"),
                Place = Return.As<Place>("place"),
                HasContextGuid = Return.As<string>("hasContext.Guid"),
                ContextAtGuid = Return.As<string>("contextAt.Guid"),
                ContextForGuid = Return.As<string>("contextFor.Guid")
            })
            .Results
            .ToList();
            return results;
        }
        public ContextHyperNode FindContext(string contextGuid)
        {
            var query = Client.Cypher
                .Match("(context:" + Label.Entity.Context + " { Guid: {contextGuid} })").WithParams(new { contextGuid })
                .Match("(artefact:" + Label.Entity.Artefact + ")-[hasContext:" + Label.Relationship.HasContext + "]->(context)")
                .Match("(context)-[contextFor:" + Label.Relationship.ContextFor + "]->(subject:" + Label.Entity.Category + ")")
                .Match("(context)-[contextAt:" + Label.Relationship.ContextAt + "]->(place:" + Label.Entity.Place + ")");
            var results = query.Return(() => new ContextHyperNode
            {
                Context = Return.As<Context>("context"),
                Artefact = Return.As<Artefact>("artefact"),
                Subject = Return.As<Category>("subject"),
                Place = Return.As<Place>("place"),
                HasContextGuid = Return.As<string>("hasContext.Guid"),
                ContextAtGuid = Return.As<string>("contextAt.Guid"),
                ContextForGuid = Return.As<string>("contextFor.Guid")
            })
            .Results
            .FirstOrDefault();
            return results;
        }
        public List<ArtefactsMadeByResult> FindByAgentTypeRepresented(string artworkTypeGuid, string agentTypeGuid, string placeGuid)
        {
            var query = Client.Cypher
                .Match("(agentType:" + Label.Entity.Category + " { Guid: {agentTypeGuid} })").WithParams(new { agentTypeGuid })
                .Match("(agentTypes:Category)-[:KIND_OF*0..]->(agentType)")
                .Match("(agents:Agent)-[:HAS_RELATION]->()-[:IS_A]->(agentTypes)")
                .Match("(artefact:" + Label.Entity.Artefact + ")-[:" + Label.Relationship.HasRelation + "]->()-[:" + Label.Relationship.Represents + "]->(agents)")
                .Match("(artefact)-[:" + Label.Relationship.HasRelation + "]->()-[:" + Label.Relationship.MadeBy + "|" + Label.Relationship.DesignedBy + "]->(agent:" + Label.Entity.Agent + ")")
                .Match("(artworkType:" + Label.Entity.Category + " { Guid: {artworkTypeGuid} })").WithParams(new { artworkTypeGuid })
                .Match("(artworkTypes:" + Label.Entity.Category + ")-[:" + Label.Relationship.KindOf + "*0..]->(artworkType)")
                .Match("(artefact)-[:" + Label.Relationship.HasRelation + "]->()-[:" + Label.Relationship.IsA + "]->(artworkTypes)")
                // .With("artefact, artworkType, artworkTypes, agent, agents, agentTypes")
                .Match("(place:" + Label.Entity.Place + " { Guid: {placeGuid} })").WithParams(new { placeGuid })
                .Match("(inside:" + Label.Entity.Place + ")-[:" + Label.Relationship.InsideOf + "*0..]->(place)")
                .Match("(artefact)-[:" + Label.Relationship.LocatedIn + "|" + Label.Relationship.PartOf + "*0..]->(locationArtefact)")
                .Match("(locationArtefact)-[:" + Label.Relationship.LocatedAt + "]->(inside)")
                .OptionalMatch("(artefact)-[:" + Label.Relationship.LocatedIn + "]->(location:" + Label.Entity.Artefact + ")")
                .OptionalMatch("(artefact)-[:" + Label.Relationship.HasImage + "]->(image:" + Label.Entity.Image + " { Primary: true })")
                ;
            var results = query
                .Return(() => new ArtefactsMadeByResult
                {
                    Artefact = Return.As<Artefact>("artefact"),
                    AgentsRepresented = Return.As<IEnumerable<AgentCategoryTuple>>("collect(distinct({ Agent: agents, Category: agentTypes }))"),
                    Image = Return.As<Image>("image"),
                    Owner = Return.As<Agent>("head(collect(agent))"),
                    Location = Return.As<Artefact>("location"),
                    Is = Return.As<IEnumerable<Category>>("collect(distinct(artworkTypes))"),
                    Place = Return.As<Place>("inside")
                })
                .Results
                .ToList();
            return results;
        }
        public List<ArtefactsMadeByResult> FindByObjectRepresented(string artworkTypeGuid, string categoryGuid, string placeGuid)
        {
            var query = Client.Cypher
                .Match("(categoryRepresented:" + Label.Entity.Category + " { Guid: {categoryGuid} })").WithParams(new { categoryGuid })
                .Match("(categories:Category)-[:KIND_OF*0..]->(categoryRepresented)")
                .Match("(artefact:" + Label.Entity.Artefact + ")-[:" + Label.Relationship.HasRelation + "]->()-[:" + Label.Relationship.Represents + "]->(categories)")
                .Match("(artefact)-[:" + Label.Relationship.HasRelation + "]->()-[:" + Label.Relationship.MadeBy + "|" + Label.Relationship.DesignedBy + "]->(agent:" + Label.Entity.Agent + ")")
                .Match("(artworkType:" + Label.Entity.Category + " { Guid: {artworkTypeGuid} })").WithParams(new { artworkTypeGuid })
                .Match("(artworkTypes:" + Label.Entity.Category + ")-[:" + Label.Relationship.KindOf + "*0..]->(artworkType)")
                .Match("(artefact)-[:" + Label.Relationship.HasRelation + "]->()-[:" + Label.Relationship.IsA + "]->(artworkTypes)")
                .With("artefact, artworkType, artworkTypes, agent")
                .Match("(place:" + Label.Entity.Place + " { Guid: {placeGuid} })").WithParams(new { placeGuid })
                .Match("(inside:" + Label.Entity.Place + ")-[:" + Label.Relationship.InsideOf + "*0..]->(place)")
                .Match("(artefact)-[:" + Label.Relationship.LocatedIn + "|" + Label.Relationship.PartOf + "*0..]->(locationArtefact)")
                .Match("(locationArtefact)-[:" + Label.Relationship.LocatedAt + "]->(inside)")
                .OptionalMatch("(artefact)-[:" + Label.Relationship.LocatedIn + "]->(location:" + Label.Entity.Artefact + ")")
                .OptionalMatch("(artefact)-[:" + Label.Relationship.HasImage + "]->(image:" + Label.Entity.Image + " { Primary: true })")
                ;
            var results = query
                .Return(() => new ArtefactsMadeByResult
                {
                    Artefact = Return.As<Artefact>("artefact"),
                    Image = Return.As<Image>("image"),
                    Owner = Return.As<Agent>("head(collect(agent))"),
                    Location = Return.As<Artefact>("location"),
                    Is = Return.As<IEnumerable<Category>>("collect(distinct(artworkTypes))"),
                    Place = Return.As<Place>("inside")
                })
                .Results
                .ToList();
            return results;
        }
        public List<ArtefactsMadeByResult> FindByAgentRepresentedQuery(string artworkTypeGuid, string agentGuid, string placeGuid)
        {
            var query = Client.Cypher
                .Match("(agentRepresented:" + Label.Entity.Agent + " { Guid: {agentGuid} })").WithParams(new { agentGuid })
                .Match("(artefact:" + Label.Entity.Artefact + ")-[:" + Label.Relationship.HasRelation + "]->()-[:" + Label.Relationship.Represents + "]->(agentRepresented)")
                .Match("(artefact)-[:" + Label.Relationship.HasRelation + "]->()-[:" + Label.Relationship.MadeBy + "|" + Label.Relationship.DesignedBy + "]->(agent:" + Label.Entity.Agent + ")")
                .Match("(artworkType:" + Label.Entity.Category + " { Guid: {artworkTypeGuid} })").WithParams(new { artworkTypeGuid })
                .Match("(artworkTypes:" + Label.Entity.Category + ")-[:" + Label.Relationship.KindOf + "*0..]->(artworkType)")
                .Match("(artefact)-[:" + Label.Relationship.HasRelation + "]->()-[:" + Label.Relationship.IsA + "]->(artworkTypes)")
                .With("artefact, artworkType, artworkTypes, agent")
                .Match("(place:" + Label.Entity.Place + " { Guid: {placeGuid} })").WithParams(new { placeGuid })
                .Match("(inside:" + Label.Entity.Place + ")-[:" + Label.Relationship.InsideOf + "*0..]->(place)")
                .Match("(artefact)-[:" + Label.Relationship.LocatedIn + "|" + Label.Relationship.PartOf + "*0..]->(locationArtefact)")
                .Match("(locationArtefact)-[:" + Label.Relationship.LocatedAt + "]->(inside)")
                .OptionalMatch("(artefact)-[:" + Label.Relationship.LocatedIn + "]->(location:" + Label.Entity.Artefact + ")")
                .OptionalMatch("(artefact)-[:" + Label.Relationship.HasImage + "]->(image:" + Label.Entity.Image + " { Primary: true })")
                ;
            var results = query
                .Return(() => new ArtefactsMadeByResult
                {
                    Artefact = Return.As<Artefact>("artefact"),
                    Image = Return.As<Image>("image"),
                    Owner = Return.As<Agent>("head(collect(agent))"),
                    Location = Return.As<Artefact>("location"),
                    Is = Return.As<IEnumerable<Category>>("collect(distinct(artworkTypes))"),
                    Place = Return.As<Place>("inside")
                })
                .Results
                .ToList();
            return results;
        }
        public List<ArtefactsMadeByResult> FindByAestheticConvention(string artworkTypeGuid, string aestheticQualityGuid, string placeGuid)
        {
            var query = Client.Cypher
                .Match("(quality:" + Label.Entity.Category + " { Guid: {aestheticQualityGuid} })").WithParams(new { aestheticQualityGuid })
                .Match("(qualities:" + Label.Entity.Category + ")-[:" + Label.Relationship.KindOf + "*0..]->(quality)")
                .Match("(artefact:" + Label.Entity.Artefact + ")-[:" + Label.Relationship.HasRelation + "]->()-[:" + Label.Relationship.Uses + "]->(qualities)")
                .Match("(artefact)-[:" + Label.Relationship.HasRelation + "]->()-[:" + Label.Relationship.MadeBy + "|" + Label.Relationship.DesignedBy + "]->(agent:" + Label.Entity.Agent + ")")
                .Match("(artworkType:" + Label.Entity.Category + " { Guid: {artworkTypeGuid} })").WithParams(new { artworkTypeGuid })
                .Match("(artworkTypes:" + Label.Entity.Category + ")-[:" + Label.Relationship.KindOf + "*0..]->(artworkType)")
                .Match("(artefact)-[:" + Label.Relationship.HasRelation + "]->()-[:" + Label.Relationship.IsA + "]->(artworkTypes)")
                .With("artefact, artworkType, artworkTypes, agent")
                .Match("(place:" + Label.Entity.Place + " { Guid: {placeGuid} })").WithParams(new { placeGuid })
                .Match("(inside:" + Label.Entity.Place + ")-[:" + Label.Relationship.InsideOf + "*0..]->(place)")
                .Match("(artefact)-[:" + Label.Relationship.LocatedIn + "|" + Label.Relationship.PartOf + "*0..]->(locationArtefact)")
                .Match("(locationArtefact)-[:" + Label.Relationship.LocatedAt + "]->(inside)")
                .OptionalMatch("(artefact)-[:" + Label.Relationship.LocatedIn + "]->(location:" + Label.Entity.Artefact + ")")
                .OptionalMatch("(artefact)-[:" + Label.Relationship.HasImage + "]->(image:" + Label.Entity.Image + " { Primary: true })")
                ;
            var results = query
                .Return(() => new ArtefactsMadeByResult
                {
                    Artefact = Return.As<Artefact>("artefact"),
                    Image = Return.As<Image>("image"),
                    Owner = Return.As<Agent>("agent"),
                    Location = Return.As<Artefact>("location"),
                    Is = Return.As<IEnumerable<Category>>("collect(distinct(artworkTypes))"),
                    Place = Return.As<Place>("inside")
                })
                .Results
                .ToList();
            return results;
        }
        public List<ArtefactsMadeByResult> FindBySubjectQuery(string artworkTypeGuid, string subjectGuid, string placeGuid)
        {
            return FindByCategoryQuery(artworkTypeGuid, placeGuid, subjectGuid, Label.Relationship.HasSubject);
            //var query = Client.Cypher
            //    .Match("(subject:" + Label.Entity.Category + " { Guid: {subjectGuid} })").WithParams(new { subjectGuid })
            //    .Match("(subjects:" + Label.Entity.Category + ")-[:" + Label.Relationship.KindOf + "*0..]->(subject)")
            //    .Match("(artefact:" + Label.Entity.Artefact + ")-[:" + Label.Relationship.HasRelation + "]->()-[:" + Label.Relationship.HasSubject + "]->(subjects)")
            //    .Match("(artefact)-[:" + Label.Relationship.HasRelation + "]->()-[:" + Label.Relationship.MadeBy + "|" + Label.Relationship.DesignedBy + "]->(agent:" + Label.Entity.Agent + ")")
            //    .Match("(artworkType:" + Label.Entity.Category + " { Guid: {artworkTypeGuid} })").WithParams(new { artworkTypeGuid })
            //    .Match("(artworkTypes:" + Label.Entity.Category + ")-[:" + Label.Relationship.KindOf + "*0..]->(artworkType)")
            //    .Match("(artefact)-[:" + Label.Relationship.HasRelation + "]->()-[:" + Label.Relationship.IsA + "]->(artworkTypes)")
            //    .With("artefact, artworkType, artworkTypes, agent")
            //    .Match("(place:" + Label.Entity.Place + " { Guid: {placeGuid} })").WithParams(new { placeGuid })
            //    .Match("(inside:" + Label.Entity.Place + ")-[:" + Label.Relationship.InsideOf + "*0..]->(place)")
            //    .Match("(artefact)-[:" + Label.Relationship.LocatedIn + "|" + Label.Relationship.PartOf + "*0..]->(locationArtefact)")
            //    .Match("(locationArtefact)-[:" + Label.Relationship.LocatedAt + "]->(inside)")
            //    .OptionalMatch("(artefact)-[:" + Label.Relationship.LocatedIn + "]->(location:" + Label.Entity.Artefact + ")")
            //    .OptionalMatch("(artefact)-[:" + Label.Relationship.HasImage + "]->(image:" + Label.Entity.Image + " { Primary: true })")
            //    ;
            //var results = query
            //    .Return(() => new ArtefactsMadeByResult
            //    {
            //        Artefact = Return.As<Artefact>("artefact"),
            //        Image = Return.As<Image>("image"),
            //        Owner = Return.As<Agent>("agent"),
            //        Location = Return.As<Artefact>("location"),
            //        Is = Return.As<IEnumerable<Category>>("collect(distinct(artworkTypes))"),
            //        Place = Return.As<Place>("inside")
            //    })
            //    .Results
            //    .ToList();
            //return results;
        }
        public List<ArtefactsMadeByResult> FindByMotifQuery(string artworkTypeGuid, string motifGuid, string placeGuid)
        {
            return FindByCategoryQuery(artworkTypeGuid, placeGuid, motifGuid, Label.Relationship.HasMotif);
        }
        public List<ArtefactsMadeByResult> FindByExteriorQuery(string artworkTypeGuid, string placeGuid)
        {
            var query = Client.Cypher
                .Match("(place:" + Label.Entity.Place + " { Guid: {placeGuid} })").WithParams(new { placeGuid })
                .Match("(inside:" + Label.Entity.Place + ")-[:INSIDE_OF*0..]->(place)")
                .Match("(exterior:" + Label.Entity.Category + " { Name: 'Exterior' })")
                .Match("(outside:" + Label.Entity.Artefact + ")-[:LOCATED_AT]->(inside)")
                .Match("(outside)-[]->()-[:HAS_PHYSICAL_PROPERTY_OF]->(exterior)")
                .Match("(artefact:" + Label.Entity.Artefact + ")-[:LOCATED_IN*0..]->(outside)")
                .Match("(artworkType:" + Label.Entity.Category + " { Guid: {artworkTypeGuid} })").WithParams(new { artworkTypeGuid })
                .Match("(artworkTypes:" + Label.Entity.Category + ")-[:" + Label.Relationship.KindOf + "*0..]->(artworkType)")
                .Match("(artefact)-[]->()-[:IS_A]->(artworkTypes)")
                .OptionalMatch("(artefact)-[]->()-[:MADE_BY]->(agent:Agent)")
                .OptionalMatch("(artefact)-[:HAS_IMAGE]->(image:Image)")
                ;
            var results = query
                .Return(() => new ArtefactsMadeByResult
                {
                    Artefact = Return.As<Artefact>("artefact"),
                    Image = Return.As<Image>("image"),
                    Owner = Return.As<Agent>("agent"),
                    Location = Return.As<Artefact>("outside"),
                    Is = Return.As<IEnumerable<Category>>("collect(distinct(artworkTypes))"),
                    Place = Return.As<Place>("inside")
                })
                .Results
                .ToList();
            return results;
        }
        private List<ArtefactsMadeByResult> FindByCategoryQuery(string artworkTypeGuid, string placeGuid, string categoryGuid, string relationshipType)
        {
            var query = Client.Cypher
                .Match("(category:" + Label.Entity.Category + " { Guid: {categoryGuid} })").WithParams(new { categoryGuid })
                .Match("(categories:" + Label.Entity.Category + ")-[:" + Label.Relationship.KindOf + "*0..]->(category)")
                .Match("(artefact:" + Label.Entity.Artefact + ")-[:" + Label.Relationship.HasRelation + "]->()-[:" + relationshipType + "]->(categories)")
                .Match("(artefact)-[:" + Label.Relationship.HasRelation + "]->()-[:" + Label.Relationship.MadeBy + "|" + Label.Relationship.DesignedBy + "]->(agent:" + Label.Entity.Agent + ")")
                .Match("(artworkType:" + Label.Entity.Category + " { Guid: {artworkTypeGuid} })").WithParams(new { artworkTypeGuid })
                .Match("(artworkTypes:" + Label.Entity.Category + ")-[:" + Label.Relationship.KindOf + "*0..]->(artworkType)")
                .Match("(artefact)-[:" + Label.Relationship.HasRelation + "]->()-[:" + Label.Relationship.IsA + "]->(artworkTypes)")
                .Match("(place:" + Label.Entity.Place + " { Guid: {placeGuid} })").WithParams(new { placeGuid })
                .Match("(inside:" + Label.Entity.Place + ")-[:" + Label.Relationship.InsideOf + "*0..]->(place)")
                .Match("(artefact)-[:" + Label.Relationship.LocatedIn + "|" + Label.Relationship.PartOf + "*0..]->(locationArtefact)")
                .Match("(locationArtefact)-[:" + Label.Relationship.LocatedAt + "]->(inside)")
                .OptionalMatch("(artefact)-[:" + Label.Relationship.LocatedIn + "]->(location:" + Label.Entity.Artefact + ")")
                .OptionalMatch("(artefact)-[:" + Label.Relationship.HasImage + "]->(image:" + Label.Entity.Image + " { Primary: true })")
                ;
            var results = query
                .Return(() => new ArtefactsMadeByResult
                {
                    Artefact = Return.As<Artefact>("artefact"),
                    Image = Return.As<Image>("image"),
                    Owner = Return.As<Agent>("agent"),
                    Location = Return.As<Artefact>("location"),
                    Is = Return.As<IEnumerable<Category>>("collect(distinct(artworkTypes))"),
                    CategoriesRepresented = Return.As<IEnumerable<Category>>("collect(distinct(categories))"),
                    Place = Return.As<Place>("inside")
                })
                .Results
                .ToList();
            return results;
        }
        public List<ArtefactsMadeByResult> FindByGenreQuery(string artworkTypeGuid, string genreGuid, string placeGuid)
        {
            var query = Client.Cypher
                .Match("(genre:" + Label.Entity.Category + " { Guid: {genreGuid} })").WithParams(new { genreGuid })
                .Match("(genres:" + Label.Entity.Category + ")-[:" + Label.Relationship.KindOf + "*0..]->(genre)")
                .Match("(artefact:" + Label.Entity.Artefact + ")-[:" + Label.Relationship.HasRelation + "]->()-[:" + Label.Relationship.HasGenre + "]->(genres)")
                .Match("(artefact)-[:" + Label.Relationship.HasRelation + "]->()-[:" + Label.Relationship.MadeBy + "|" + Label.Relationship.DesignedBy + "]->(agent:" + Label.Entity.Agent + ")")
                .Match("(artworkType:" + Label.Entity.Category + " { Guid: {artworkTypeGuid} })").WithParams(new { artworkTypeGuid })
                .Match("(artworkTypes:" + Label.Entity.Category + ")-[:" + Label.Relationship.KindOf + "*0..]->(artworkType)")
                .Match("(artefact)-[:" + Label.Relationship.HasRelation + "]->()-[:" + Label.Relationship.IsA + "]->(artworkTypes)")
                //.With("artefact, artworkType, artworkTypes, agent")
                .Match("(place:" + Label.Entity.Place + " { Guid: {placeGuid} })").WithParams(new { placeGuid })
                .Match("(inside:" + Label.Entity.Place + ")-[:" + Label.Relationship.InsideOf + "*0..]->(place)")
                .Match("(artefact)-[:" + Label.Relationship.LocatedIn + "|" + Label.Relationship.PartOf + "*0..]->(locationArtefact)")
                .Match("(locationArtefact)-[:" + Label.Relationship.LocatedAt + "]->(inside)")
                .OptionalMatch("(artefact)-[:" + Label.Relationship.LocatedIn + "]->(location:" + Label.Entity.Artefact + ")")
                .OptionalMatch("(artefact)-[:" + Label.Relationship.HasImage + "]->(image:" + Label.Entity.Image + " { Primary: true })")
                ;
            var results = query
                .Return(() => new ArtefactsMadeByResult
                {
                    Artefact = Return.As<Artefact>("artefact"),
                    Image = Return.As<Image>("image"),
                    Owner = Return.As<Agent>("agent"),
                    Location = Return.As<Artefact>("location"),
                    Is = Return.As<IEnumerable<Category>>("collect(distinct(artworkTypes))"),
                    CategoriesRepresented = Return.As<IEnumerable<Category>>("collect(distinct(genres))"),
                    Place = Return.As<Place>("inside")
                })
                .Results
                .ToList();
            return results;
        }
        public List<ArtefactsMadeByResult> FindByMovementQuery(string artworkTypeGuid, string movementGuid, string placeGuid)
        {
            var query = Client.Cypher
                .Match("(movement:" + Label.Entity.Category + " { Guid: {movementGuid} })").WithParams(new { movementGuid })
                .Match("(movements:" + Label.Entity.Category + ")-[:" + Label.Relationship.KindOf + "*0..]->(movement)")
                .Match("(agent:" + Label.Entity.Agent + ")-[:" + Label.Relationship.HasRelation + "]->()-[:" + Label.Relationship.RepresentativeOf + "]->(movements)")
                .Match("(artefact:" + Label.Entity.Artefact + ")-[:" + Label.Relationship.HasRelation + "]->()-[:" + Label.Relationship.MadeBy + "|" + Label.Relationship.DesignedBy + "]->(agent)")
                .Match("(artworkType:" + Label.Entity.Category + " { Guid: {artworkTypeGuid} })").WithParams(new { artworkTypeGuid })
                .Match("(artworkTypes:" + Label.Entity.Category + ")-[:" + Label.Relationship.KindOf + "*0..]->(artworkType)")
                .Match("(artefact)-[:" + Label.Relationship.HasRelation + "]->()-[:" + Label.Relationship.IsA + "]->(artworkTypes)")
                .With("artefact, artworkType, artworkTypes, agent")
                .Match("(place:" + Label.Entity.Place + " { Guid: {placeGuid} })").WithParams(new { placeGuid })
                .Match("(inside:" + Label.Entity.Place + ")-[:" + Label.Relationship.InsideOf + "*0..]->(place)")
                .Match("(artefact)-[:" + Label.Relationship.LocatedIn + "|" + Label.Relationship.PartOf + "*0..]->(locationArtefact)")
                .Match("(locationArtefact)-[:" + Label.Relationship.LocatedAt + "]->(inside)")
                //.OptionalMatch("(artefact)-[:LOCATED_AT]->(inside)")
                .OptionalMatch("(artefact)-[:" + Label.Relationship.LocatedIn + "]->(location:" + Label.Entity.Artefact + ")")
                .OptionalMatch("(artefact)-[:" + Label.Relationship.HasImage + "]->(image:" + Label.Entity.Image + " { Primary: true })")
                ;
            var results = query
                .Return(() => new ArtefactsMadeByResult
                {
                    Artefact = Return.As<Artefact>("artefact"),
                    Image = Return.As<Image>("image"),
                    Owner = Return.As<Agent>("agent"),
                    Location = Return.As<Artefact>("location"),
                    Is = Return.As<IEnumerable<Category>>("collect(distinct(artworkTypes))"),
                    Place = Return.As<Place>("inside")
                })
                .Results
                .ToList();
            return results;
        }
        public List<ArtefactsMadeByResult> FindByAgentQuery(string artworkTypeGuid, string artistGuid, string placeGuid)
        {
            var query = Client.Cypher
                .Match("(agent:" + Label.Entity.Agent + " { Guid: {artistGuid} })").WithParams(new { artistGuid })
                .Match("(artefact:" + Label.Entity.Artefact + ")-[:" + Label.Relationship.HasRelation + "]->()-[:" + Label.Relationship.MadeBy + "|" + Label.Relationship.DesignedBy + "]->(agent)")
                .Match("(artworkType:" + Label.Entity.Category + " { Guid: {artworkTypeGuid} })").WithParams(new { artworkTypeGuid })
                .Match("(artworkTypes:" + Label.Entity.Category + ")-[:" + Label.Relationship.KindOf + "*0..]->(artworkType)")
                .Match("(artefact)-[:" + Label.Relationship.HasRelation + "]->()-[:" + Label.Relationship.IsA + "]->(artworkTypes)")
                .With("artefact, artworkType, artworkTypes, agent")
                .Match("(place:" + Label.Entity.Place + " { Guid: {placeGuid} })").WithParams(new { placeGuid })
                .Match("(inside:" + Label.Entity.Place + ")-[:" + Label.Relationship.InsideOf + "*0..]->(place)")
                .Match("(artefact)-[:" + Label.Relationship.LocatedIn + "|" + Label.Relationship.PartOf + "*0..]->(locationArtefact)")
                .Match("(locationArtefact)-[:" + Label.Relationship.LocatedAt + "]->(inside)")
                .OptionalMatch("(artefact)-[:" + Label.Relationship.LocatedIn + "]->(location:" + Label.Entity.Artefact + ")")
                .OptionalMatch("(artefact)-[:" + Label.Relationship.HasImage + "]->(image:" + Label.Entity.Image + " { Primary: true })")
                ;
            var results = query
                .Return(() => new ArtefactsMadeByResult
                {
                    Artefact = Return.As<Artefact>("artefact"),
                    Image = Return.As<Image>("image"),
                    Owner = Return.As<Agent>("agent"),
                    Location = Return.As<Artefact>("location"),
                    Is = Return.As<IEnumerable<Category>>("collect(distinct(artworkTypes))"),
                    Place = Return.As<Place>("inside")
                })
                .Results
                .ToList();
            return results;
        }
        public List<ArtefactsMadeByResult> FindByAgentAndPlace(string agentGuid, string placeGuid)
        {
            var query = Client.Cypher
                .Match("(agent:" + Label.Entity.Agent + " { Guid: {agentGuid} })").WithParams(new { agentGuid })
                .Match("(place:" + Label.Entity.Place + " { Guid: {placeGuid} })").WithParams(new { placeGuid })
                .Match("(inside:" + Label.Entity.Place + ")-[:" + Label.Relationship.InsideOf + "*0..]->(place)")
                .Match("(artefact:" + Label.Entity.Artefact + ")-[:" + Label.Relationship.HasRelation + "]->()-[:" + Label.Relationship.MadeBy + "]->(agent)")
                .Match("(artefact)-[:" + Label.Relationship.HasRelation + "]->()-[:" + Label.Relationship.LocatedIn + "*0..]->(container:Artefact)-[:" + Label.Relationship.HasRelation + "]->()-[:" + Label.Relationship.LocatedAt + "*0..]->(inside)")
                .Match("(artefact)-[:" + Label.Relationship.HasRelation + "]->()-[:" + Label.Relationship.IsA + "]->(artefactType:" + Label.Entity.Category + ")")
                .OptionalMatch("(artefact)-[:" + Label.Relationship.HasRelation + "]->()-[:" + Label.Relationship.LocatedIn + "]->(location:" + Label.Entity.Artefact + ")")
                .OptionalMatch("(artefact)-[:" + Label.Relationship.HasImage + "]->(image:" + Label.Entity.Image + " { Primary: true })")
                ;
            var results = query
                .Return(() => new ArtefactsMadeByResult
                {
                    Artefact = Return.As<Artefact>("artefact"),
                    Image = Return.As<Image>("image"),
                    Owner = Return.As<Agent>("agent"),
                    Location = Return.As<Artefact>("location"),
                    Is = Return.As<IEnumerable<Category>>("collect(distinct(artefactType))")
                })
                .Results
                .ToList();
            return results;
        }
        public IEnumerable<ArtefactsMadeByResult> SearchByQuery(FindByQuery query)
        {
            if (query.Preposition == "by agent")
            {
                var results = Client.Cypher
                .Match("(agent:" + Label.Entity.Agent + " { Guid: {agentGuid} })").WithParams(new { agentGuid = query.AgentGuid })
                .Match("(ART:" + Label.Entity.Category + " { Guid: {artefactIsAGuid} })").WithParams(new { artefactIsAGuid = query.ArtefactIsAGuid })
                .Match("(artefactType:" + Label.Entity.Category + ")-[:" + Label.Relationship.KindOf + "*0..3]->(ART)")
                .Match("(artefact)-[:" + Label.Relationship.HasRelation + "]->()-[:" + Label.Relationship.IsA + "]->(artefactType)")
                .Match("(agent)<-[:" + Label.Relationship.MadeBy + "]-()<-[:" + Label.Relationship.HasRelation + "]-(artefact)")
                .OptionalMatch("(artefact)-[:" + Label.Relationship.HasImage + "]->(image:" + Label.Entity.Image + ")")
                .Return(() => new ArtefactsMadeByResult
                {
                    Owner = Return.As<Agent>("agent"),
                    Artefact = Return.As<Artefact>("artefact"),
                    Image = Return.As<Image>("image"),
                    Is = Return.As<IEnumerable<Category>>("collect(distinct(artefactType))")
                })
                .Results
                .ToList();
                return results;
            }
            else if (query.Preposition == "in")
            {
                var results = Client.Cypher
                .Match("(P:" + Label.Entity.Place + " { Guid: {placeGuid} })").WithParams(new { placeGuid = query.PlaceGuid })
                .Match("(AT:" + Label.Entity.Category + " { Guid: {artefactIsAGuid} })").WithParams(new { artefactIsAGuid = query.ArtefactIsAGuid })
                .Match("(place:" + Label.Entity.Place + ")-[:" + Label.Relationship.InsideOf + "*0..3]->(P)")
                .Match("(artefactType:" + Label.Entity.Category + ")-[:" + Label.Relationship.KindOf + "*0..3]->(AT)")
                .Match("(artefact)-[:" + Label.Relationship.HasRelation + "]->()-[:" + Label.Relationship.IsA + "]->(artefactType)")
                .Match("(agent:" + Label.Entity.Agent + ")-[:" + Label.Relationship.HasRelation + "]->()-[:" + Label.Relationship.CitizenOf + "]->(place)")
                .Match("(agent)<-[:" + Label.Relationship.MadeBy + "]-()<-[:" + Label.Relationship.HasRelation + "]-(artefact)")
                .OptionalMatch("(artefact)-[:" + Label.Relationship.HasImage + "]->(image:" + Label.Entity.Image + ")")
                .Return(() => new ArtefactsMadeByResult
                {
                    Owner = Return.As<Agent>("agent"),
                    Artefact = Return.As<Artefact>("artefact"),
                    Image = Return.As<Image>("image"),
                    Is = Return.As<IEnumerable<Category>>("collect(distinct(artefactType))")
                })
                .Results
                .ToList();
                return results;
            }
            else if (query.Preposition == "by")
            {
                var q = Client.Cypher
                    .Match("(PLACE:" + Label.Entity.Place + " { Guid: {citizenOfGuid} })").WithParams(new { citizenOfGuid = query.CitizenOfGuid })
                   .Match("(REPRESENTATIVE:" + Label.Entity.Category + " { Guid: {representativeOfGuid} })").WithParams(new { representativeOfGuid = query.RepresentativeOfGuid })
                   .Match("(TYPE:" + Label.Entity.Category + " { Guid: {artefactIsAGuid} })").WithParams(new { artefactIsAGuid = query.ArtefactIsAGuid })
                   .Match("(citizenOf:" + Label.Entity.Place + " )-[:" + Label.Relationship.InsideOf + "*0..3]->(PLACE)")
                   .Match("(representativeOf:" + Label.Entity.Category + " )-[:" + Label.Relationship.KindOf + "*0..3]->(REPRESENTATIVE)")
                   .Match("(artefactType:" + Label.Entity.Category + ")-[:" + Label.Relationship.KindOf + "*0..3]->(TYPE)")
                   .Match("(artefact:" + Label.Entity.Artefact + ")-[:" + Label.Relationship.HasRelation + "]->()-[:" + Label.Relationship.IsA + "]->(artefactType)")
                    .Match("(agent:" + Label.Entity.Agent + ")<-[:" + Label.Relationship.MadeBy + "]-()<-[:" + Label.Relationship.HasRelation + "]-(artefact)")
                   .Match("(agent)-[:" + Label.Relationship.HasRelation + "]->()-[:" + Label.Relationship.CitizenOf + "]->(citizenOf)")
                   .Match("(agent)-[:" + Label.Relationship.HasRelation + "]->()-[:" + Label.Relationship.RepresentativeOf + "]->(representativeOf)")
                   .OptionalMatch("(artefact)-[:" + Label.Relationship.HasImage + "]->(image:" + Label.Entity.Image + ")");
                var results = q
                   .Return(() => new ArtefactsMadeByResult
                   {
                       Owner = Return.As<Agent>("agent"),
                       Artefact = Return.As<Artefact>("artefact"),
                       Image = Return.As<Image>("image"),
                       Is = Return.As<IEnumerable<Category>>("collect(distinct(artefactType))")
                   })
                   .Results
                   .ToList();
                return results;
            }
            throw new NotImplementedException();

        }
        public IEnumerable<ArtefactsMadeByResult> SearchBy(string agentName, string artefactTypeName)
        {
            var results = Client.Cypher
                .Match("(agent:" + Label.Entity.Agent + ")").Where("agent.Name =~ '(?i)" + agentName + "'")
                .Match("(ART:" + Label.Entity.Category + ")").Where("ART.Name =~ '(?i)" + artefactTypeName + "' or ART.Plural =~ '(?i)" + artefactTypeName + "'")
                .Match("(artefactType:" + Label.Entity.Category + ")-[:" + Label.Relationship.KindOf + "*0..3]->(ART)")
                .Match("(artefact)-[:" + Label.Relationship.HasRelation + "]->()-[:" + Label.Relationship.IsA + "]->(artefactType)")
                .Match("(agent)<-[:" + Label.Relationship.MadeBy + "]-()<-[:" + Label.Relationship.HasRelation + "]-(artefact)")
                .OptionalMatch("(artefact)-[:" + Label.Relationship.HasImage + "]->(image:" + Label.Entity.Image + ")")
                .Return(() => new ArtefactsMadeByResult
                {
                    Owner = Return.As<Agent>("agent"),
                    Artefact = Return.As<Artefact>("artefact"),
                    Image = Return.As<Image>("image"),
                    Is = Return.As<IEnumerable<Category>>("collect(distinct(artefactType))")
                })
                .Results
                .ToList();
            return results;
        }
        public void OwnedBy(GenericRelationHyperNode<Artefact, Agent> node)
        {
            SaveOrUpdate(node, Label.Relationship.OwnedBy);
        }
        public void DesignedBy(GenericRelationHyperNode<Artefact, Agent> node)
        {
            SaveOrUpdate(node, Label.Relationship.DesignedBy);
        }
        public void BuriedIn(GenericRelationHyperNode<Agent, Artefact> node)
        {
            SaveOrUpdate(node, Label.Relationship.BuriedIn);
        }
        public void AppliesTo(GenericRelationHyperNode<Agent, Artefact> node)
        {
            SaveOrUpdate(node, Label.Relationship.AppliesTo);
        }
        public void ManagedBy(GenericRelationHyperNode<Artefact, Agent> node)
        {
            SaveOrUpdate(node, Label.Relationship.ManagedBy);
        }
        public void OntologicalStateOf(GenericRelationHyperNode<Artefact, Category> node)
        {
            SaveOrUpdate(node, Label.Relationship.OntologicalStateOf);
        }
        public void MadeFrom(GenericRelationHyperNode<Artefact, Category> node)
        {
            SaveOrUpdate(node, Label.Relationship.MadeFrom);
        }
        public void TranslatedBy(GenericRelationHyperNode<Artefact, Agent> node)
        {
            SaveOrUpdate(node, Label.Relationship.TranslatedBy);
        }
        public void PublishedBy(GenericRelationHyperNode<Artefact, Agent> node)
        {
            SaveOrUpdate(node, Label.Relationship.PublishedBy);
        }
        public void CommissionedBy(GenericRelationHyperNode<Artefact, Agent> node)
        {
            SaveOrUpdate(node, Label.Relationship.CommissionedBy);
        }
        public void MadeWith(GenericRelationHyperNode<Artefact, Category> node)
        {
            SaveOrUpdate(node, Label.Relationship.MadeWith);
        }
        public void HasMotif(GenericRelationHyperNode<Artefact, Category> node)
        {
            SaveOrUpdate(node, Label.Relationship.HasMotif);
        }
        public void ArtefactHasSubject(GenericRelationHyperNode<Artefact, Category> node)
        {
            SaveOrUpdate(node, Label.Relationship.HasSubject);
        }
        public void TourFeatures(GenericRelationHyperNode<Artefact, Artefact> node)
        {
            SaveOrUpdate(node, Label.Relationship.TourFeatures);
        }
        public void Uses(GenericRelationHyperNode<Artefact, Category> node)
        {
            SaveOrUpdate(node, Label.Relationship.Uses);
        }
        public void HasGenre(GenericRelationHyperNode<Artefact, Category> node)
        {
            SaveOrUpdate(node, Label.Relationship.HasGenre);
        }
        public void HasPhysicalPropertyOf(GenericRelationHyperNode<Artefact, Category> node)
        {
            SaveOrUpdate(node, Label.Relationship.HasPhysicalPropertyOf);
        }
        public void ScoredFor(GenericRelationHyperNode<Artefact, Category> node)
        {
            SaveOrUpdate(node, Label.Relationship.ScoredFor);
        }
        public void StopsIn(GenericRelationNode<Artefact, Artefact> node)
        {
            SaveOrUpdate(node, Label.Relationship.StopsIn);
        }
        public void StopsAt(GenericRelationNode<Artefact, Place> node)
        {
            SaveOrUpdate(node, Label.Relationship.StopsAt);
        }
        public void AssociatedWith(GenericRelationHyperNode<Artefact, Category> node)
        {
            SaveOrUpdate(node, Label.Relationship.AssociatedWith);
        }
        public void RepresentativeOf(GenericRelationHyperNode<Artefact, Category> node)
        {
            SaveOrUpdate(node, Label.Relationship.RepresentativeOf);
        }
        public void IsA(GenericRelationHyperNode<Artefact, Category> node)
        {
            SaveOrUpdate(node, Label.Relationship.IsA);
        }
        public void PartOf(GenericRelationNode<Artefact, Artefact> node)
        {
            SaveOrUpdate(node, Label.Relationship.PartOf);
        }
        public void StudyFor(GenericRelationHyperNode<Artefact, Artefact> node)
        {
            SaveOrUpdate(node, Label.Relationship.StudyFor);
        }

        public void CopyOf(GenericRelationHyperNode<Artefact, Artefact> node)
        {
            SaveOrUpdate(node, Label.Relationship.CopyOf);
        }
        public void InfluenceOn(GenericRelationHyperNode<Artefact, Artefact> node)
        {
            SaveOrUpdate(node, Label.Relationship.InfluenceOn);
        }
        public void SimilarTo(GenericRelationHyperNode<Artefact, Artefact> node)
        {
            SaveOrUpdate(node, Label.Relationship.SimilarTo);
        }
        public void ExhibitingIn(GenericRelationHyperNode<Agent, Artefact> node)
        {
            SaveOrUpdate(node, Label.Relationship.ExhibitingIn);
        }
        public void ExhibitedIn(GenericRelationHyperNode<Artefact, Artefact> node)
        {
            SaveOrUpdate(node, Label.Relationship.ExhibitedIn);
        }
        public void Covers(GenericRelationHyperNode<Artefact, Agent> node)
        {
            SaveOrUpdate(node, Label.Relationship.Covers);
        }
        public void LocatedIn(GenericRelationNode<Artefact, Artefact> node)
        {
            SaveOrUpdate(node, Label.Relationship.LocatedIn);
        }
        public void TakesPlaceIn(GenericRelationNode<Artefact, Artefact> node)
        {
            SaveOrUpdate(node, Label.Relationship.TakesPlaceIn);
        }
        public void LocatedAt(GenericRelationNode<Artefact, Place> node)
        {
            SaveOrUpdate(node, Label.Relationship.LocatedAt);
        }
        public void Represents(GenericRelationHyperNode<Artefact, Entity> node)
        {
            SaveOrUpdate(node, Label.Relationship.Represents);
        }
        public void MadeBy(GenericRelationHyperNode<Artefact, Agent> node)
        {
            SaveOrUpdate(node, Label.Relationship.MadeBy);
        }
        public void AttributedTo(GenericRelationHyperNode<Artefact, Agent> node)
        {
            SaveOrUpdate(node, Label.Relationship.AttributedTo);
        }
        /// <summary>
        /// Although the label and the type names are currently equivalent this could be rewritten
        /// to check the type and return the label constant explicitly.
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        public string GetNodeLabel<TEntity>() where TEntity : class, IEntity
        {
            return typeof(TEntity).UnderlyingSystemType.Name;
        }
        public void SaveOrUpdate<TSource, TDest>(GenericRelationNode<TSource, TDest> node, string relationToDestinationType, bool reverse = false)
            where TSource : class, IEntity
            where TDest : class, IEntity
        {
            var sourceType = GetNodeLabel<TSource>();
            var destType = GetNodeLabel<TDest>();
            var isNew = node.RelationToDestinationGuid == null;
            if (false == isNew)
            {
                relationService.DeleteRelationship(relationToDestinationType, node.RelationToDestinationGuid);
            }
            if (false == reverse)
            {
                relationService.CreateRelationship(sourceType, node.Source.Guid, destType, node.Destination.Guid, relationToDestinationType);
            }
            else
            {
                relationService.CreateRelationship(destType, node.Destination.Guid, sourceType, node.Source.Guid, relationToDestinationType);
            }
        }
        public void SaveOrUpdate<TSource, TDest>(GenericRelationHyperNode<TSource, TDest> node, string relationToDestinationType, bool reverse = false)
            where TSource : class, IEntity
            where TDest : class, IEntity
        {
            if (node.Relation == null)
            {
                node.Relation = new Relation();
            }
            var isNew = node.Relation.Guid == null;
            var sourceType = GetNodeLabel<TSource>();
            var destType = GetNodeLabel<TDest>();
            relationService.SaveOrUpdate(node.Relation);
            if (node.HasRelationGuid == null)
            {
                if (false == reverse)
                {
                    relationService.CreateRelationship(sourceType, node.Source.Guid, Label.Entity.Relation, node.Relation.Guid, Label.Relationship.HasRelation);
                }
                else
                {
                    relationService.CreateRelationship(destType, node.Destination.Guid, Label.Entity.Relation, node.Relation.Guid, Label.Relationship.HasRelation);
                }
            }
            if (node.RelationToDestinationGuid == null)
            {
                if (node.Destination.Guid != null)
                {
                    if (false == reverse)
                    {
                        relationService.CreateRelationship(Label.Entity.Relation, node.Relation.Guid, destType, node.Destination.Guid, relationToDestinationType);
                    }
                    else
                    {
                        relationService.CreateRelationship(Label.Entity.Relation, node.Relation.Guid, sourceType, node.Source.Guid, relationToDestinationType);
                    }
                }
            }
            else
            {
                relationService.DeleteRelationship(relationToDestinationType, node.RelationToDestinationGuid);
                if (false == reverse)
                {
                    if (node.Destination.Guid != null)
                    {
                        relationService.CreateRelationship(Label.Entity.Relation, node.Relation.Guid, destType, node.Destination.Guid, relationToDestinationType, node.RelationToDestinationGuid);
                    }
                }
                else
                {
                    relationService.CreateRelationship(Label.Entity.Relation, node.Relation.Guid, sourceType, node.Source.Guid, relationToDestinationType, node.RelationToDestinationGuid);
                }
            }
            UpdateReferrers(node, isNew);
        }
        public void SaveOrUpdate(ArtefactToCategoryRelationHyperNode node, string relationToDestinationType, bool reverse = false)
        {
            var isNew = node.Relation.Guid == null;
            relationService.SaveOrUpdate(node.Relation);
            if (node.HasRelationGuid == null)
            {
                if (false == reverse)
                {
                    relationService.CreateRelationship(Label.Entity.Artefact, node.Source.Guid, Label.Entity.Relation, node.Relation.Guid, Label.Relationship.HasRelation);
                }
                else
                {
                    relationService.CreateRelationship(Label.Entity.Category, node.Destination.Guid, Label.Entity.Relation, node.Relation.Guid, Label.Relationship.HasRelation);
                }
            }
            if (node.RelationToDestinationGuid == null)
            {
                if (node.Destination.Guid != null)
                {
                    if (false == reverse)
                    {
                        relationService.CreateRelationship(Label.Entity.Relation, node.Relation.Guid, Label.Entity.Category, node.Destination.Guid, relationToDestinationType);
                    }
                    else
                    {
                        relationService.CreateRelationship(Label.Entity.Relation, node.Relation.Guid, Label.Entity.Artefact, node.Source.Guid, relationToDestinationType);
                    }
                }
            }
            else
            {
                relationService.DeleteRelationship(relationToDestinationType, node.RelationToDestinationGuid);
                if (false == reverse)
                {
                    if (node.Destination.Guid != null)
                    {
                        relationService.CreateRelationship(Label.Entity.Relation, node.Relation.Guid, Label.Entity.Category, node.Destination.Guid, relationToDestinationType, node.RelationToDestinationGuid);
                    }
                }
                else
                {
                    relationService.CreateRelationship(Label.Entity.Relation, node.Relation.Guid, Label.Entity.Artefact, node.Source.Guid, relationToDestinationType, node.RelationToDestinationGuid);
                }
            }
            UpdateReferrers(node, isNew);
        }
        public void UpdateReferrers<TDest, TSource>(GenericRelationHyperNode<TDest, TSource> node, bool isNew)
            where TDest : IEntity
            where TSource : IEntity
        {
            if (false == isNew)
            {
                DeleteRelationRefersTo(node.Relation.Guid);
            }
            if (node.References != null)
            {
                foreach (var tuple in node.References)
                {
                    var type = tuple[0];
                    var guid = tuple[1];
                    commonService.CreateRelationship(Label.Entity.Relation, node.Relation.Guid, type, guid, Label.Relationship.RefersTo);
                }
            }
        }
        public void DeleteRelationRefersTo(string relationGuid)
        {
            DeleteEntityRefersTo(relationGuid, Label.Entity.Relation);
        }
        public void DeleteEntityRefersTo(string entityGuid, string entityType)
        {
            Client.Cypher
                .Match("(entity:" + entityType + " { Guid: {entityGuid} })-[r:" + Label.Relationship.RefersTo + "]->()")
                .WithParams(new
                {
                    entityGuid
                })
                .Delete("r")
                .ExecuteWithoutResults();
        }
        public void SaveOrUpdate(ArtefactToAgentRelationHyperNode node, string relationToDestinationType, bool reverse = false)
        {
            var isNew = node.Relation.Guid == null;
            relationService.SaveOrUpdate(node.Relation);
            if (node.HasRelationGuid == null)
            {
                if (false == reverse)
                {
                    relationService.CreateRelationship(Label.Entity.Artefact, node.Source.Guid, Label.Entity.Relation, node.Relation.Guid, Label.Relationship.HasRelation);
                }
                else
                {
                    relationService.CreateRelationship(Label.Entity.Agent, node.Destination.Guid, Label.Entity.Relation, node.Relation.Guid, Label.Relationship.HasRelation);
                }
            }
            if (node.RelationToDestinationGuid == null)
            {
                if (node.Destination.Guid != null)
                {
                    if (false == reverse)
                    {
                        relationService.CreateRelationship(Label.Entity.Relation, node.Relation.Guid, Label.Entity.Agent, node.Destination.Guid, relationToDestinationType);
                    }
                    else
                    {
                        relationService.CreateRelationship(Label.Entity.Relation, node.Relation.Guid, Label.Entity.Artefact, node.Source.Guid, relationToDestinationType);
                    }
                }
            }
            else
            {
                relationService.DeleteRelationship(relationToDestinationType, node.RelationToDestinationGuid);
                if (false == reverse)
                {
                    if (node.Destination.Guid != null)
                    {
                        relationService.CreateRelationship(Label.Entity.Relation, node.Relation.Guid, Label.Entity.Agent, node.Destination.Guid, relationToDestinationType, node.RelationToDestinationGuid);
                    }
                }
                else
                {
                    relationService.CreateRelationship(Label.Entity.Relation, node.Relation.Guid, Label.Entity.Artefact, node.Source.Guid, relationToDestinationType, node.RelationToDestinationGuid);
                }
            }
            UpdateReferrers(node, isNew);
        }
        public GenericRelationHyperNode<Artefact, Agent> FindAttributedTo(string artefactGuid, string hasRelationGuid = null)
        {
            return FindGenericRelation<Artefact, Agent>(artefactGuid, Label.Relationship.AttributedTo, hasRelationGuid);
        }
        public GenericRelationHyperNode<Artefact, Agent> FindMadeBy(string artefactGuid, string hasRelationGuid = null)
        {
            return FindGenericRelation<Artefact, Agent>(artefactGuid, Label.Relationship.MadeBy, hasRelationGuid);
        }
        public GenericRelationHyperNode<Artefact, Agent> FindOwnedBy(string artefactGuid, string hasRelationGuid = null)
        {
            return FindGenericRelation<Artefact, Agent>(artefactGuid, Label.Relationship.OwnedBy, hasRelationGuid);
        }
        public GenericRelationHyperNode<Artefact, Agent> FindDesignedBy(string artefactGuid, string hasRelationGuid)
        {
            return FindGenericRelation<Artefact, Agent>(artefactGuid, Label.Relationship.DesignedBy, hasRelationGuid);
        }
        public GenericRelationHyperNode<Agent, Artefact> FindBuriedIn(string artefactGuid, string hasRelationGuid)
        {
            return FindGenericRelation<Agent, Artefact>(artefactGuid, Label.Relationship.BuriedIn, hasRelationGuid);
        }
        public GenericRelationHyperNode<Agent, Artefact> FindAppliesTo(string artefactGuid, string hasRelationGuid)
        {
            return FindGenericRelation<Agent, Artefact>(artefactGuid, Label.Relationship.AppliesTo, hasRelationGuid);
        }
        public GenericRelationHyperNode<Artefact, Agent> FindManagedBy(string artefactGuid, string hasRelationGuid)
        {
            return FindGenericRelation<Artefact, Agent>(artefactGuid, Label.Relationship.ManagedBy, hasRelationGuid);
        }
        public GenericRelationHyperNode<Artefact, Agent> FindCommissionedBy(string artefactGuid, string hasRelationGuid)
        {
            return FindGenericRelation<Artefact, Agent>(artefactGuid, Label.Relationship.CommissionedBy, hasRelationGuid);
        }
        public GenericRelationHyperNode<Artefact, Agent> FindTranslatedBy(string artefactGuid, string hasRelationGuid)
        {
            return FindGenericRelation<Artefact, Agent>(artefactGuid, Label.Relationship.TranslatedBy, hasRelationGuid);
        }
        public GenericRelationHyperNode<Artefact, Agent> FindPublishedBy(string artefactGuid, string hasRelationGuid)
        {
            return FindGenericRelation<Artefact, Agent>(artefactGuid, Label.Relationship.PublishedBy, hasRelationGuid);
        }
        public ArtefactToCategoryRelationHyperNode FindHasMotif(string artefactGuid, string hasRelationGuid)
        {
            return FindArtefactToCategoryRelation(artefactGuid, Label.Relationship.HasMotif, hasRelationGuid);
        }
        public GenericRelationHyperNode<Artefact, Artefact> FindTourFeatures(string artefactGuid, string hasRelationGuid)
        {
            return FindGenericRelation<Artefact, Artefact>(artefactGuid, Label.Relationship.TourFeatures, hasRelationGuid);
        }
        public GenericRelationHyperNode<Artefact, Category> FindArtefactHasSubject(string artefactGuid, string hasRelationGuid)
        {
            return FindGenericRelation<Artefact, Category>(artefactGuid, Label.Relationship.HasSubject, hasRelationGuid);
        }
        public ArtefactToCategoryRelationHyperNode FindUses(string artefactGuid, string hasRelationGuid)
        {
            return FindArtefactToCategoryRelation(artefactGuid, Label.Relationship.Uses, hasRelationGuid);
        }
        public GenericRelationHyperNode<Artefact, Category> FindScoredFor(string artefactGuid, string hasRelationGuid)
        {
            return FindGenericRelation<Artefact, Category>(artefactGuid, Label.Relationship.ScoredFor, hasRelationGuid);
        }
        public GenericRelationNode<Artefact, Place> FindStopsAt(string artefactGuid, string hasRelationGuid)
        {
            return FindGenericRelation<Artefact, Place>(artefactGuid, Label.Relationship.StopsAt, hasRelationGuid);
        }
        public GenericRelationNode<Artefact, Artefact> FindStopsIn(string artefactGuid, string hasRelationGuid)
        {
            return FindGenericRelation<Artefact, Artefact>(artefactGuid, Label.Relationship.StopsIn, hasRelationGuid);
        }
        public GenericRelationHyperNode<Artefact, Category> FindAssociatedWith(string artefactGuid, string hasRelationGuid)
        {
            return FindGenericRelation<Artefact, Category>(artefactGuid, Label.Relationship.AssociatedWith, hasRelationGuid);
        }
        public GenericRelationHyperNode<Artefact, Category> FindRepresentativeOf(string artefactGuid, string hasRelationGuid)
        {
            return FindGenericRelation<Artefact, Category>(artefactGuid, Label.Relationship.RepresentativeOf, hasRelationGuid);
        }
        public ArtefactToCategoryRelationHyperNode FindHasGenre(string artefactGuid, string hasRelationGuid)
        {
            return FindArtefactToCategoryRelation(artefactGuid, Label.Relationship.HasGenre, hasRelationGuid);
        }
        public ArtefactToCategoryRelationHyperNode FindHasPhysicalPropertyOf(string artefactGuid, string hasRelationGuid)
        {
            return FindArtefactToCategoryRelation(artefactGuid, Label.Relationship.HasPhysicalPropertyOf, hasRelationGuid);
        }
        public ArtefactToCategoryRelationHyperNode FindMadeWith(string artefactGuid, string hasRelationGuid)
        {
            return FindArtefactToCategoryRelation(artefactGuid, Label.Relationship.MadeWith, hasRelationGuid);
        }
        public ArtefactToCategoryRelationHyperNode FindMadeFrom(string artefactGuid, string hasRelationGuid)
        {
            return FindArtefactToCategoryRelation(artefactGuid, Label.Relationship.MadeFrom, hasRelationGuid);
        }
        public ArtefactToCategoryRelationHyperNode FindOntologicalStateOf(string artefactGuid, string hasRelationGuid)
        {
            return FindArtefactToCategoryRelation(artefactGuid, Label.Relationship.OntologicalStateOf, hasRelationGuid);
        }
        public ArtefactToCategoryRelationHyperNode FindIsA(string artefactGuid, string hasRelationGuid)
        {
            return FindArtefactToCategoryRelation(artefactGuid, Label.Relationship.IsA, hasRelationGuid);
        }
        public GenericRelationNode<Artefact, Artefact> FindPartOf(string artefactGuid, string toDestGuid)
        {
            return commonService.FindGenericRelationNode<Artefact, Artefact>(artefactGuid, Label.Relationship.PartOf, toDestGuid);
        }
        public GenericRelationHyperNode<Artefact, Artefact> FindStudyFor(string artefactGuid, string hasRelationGuid)
        {
            return FindGenericRelation<Artefact, Artefact>(artefactGuid, Label.Relationship.StudyFor, hasRelationGuid);
        }
        public GenericRelationHyperNode<Artefact, Artefact> FindCopyOf(string artefactGuid, string hasRelationGuid)
        {
            return FindGenericRelation<Artefact, Artefact>(artefactGuid, Label.Relationship.CopyOf, hasRelationGuid);
        }
        //public void HasSubject(GenericRelationHyperNode<Act, Artefact> node)
        //{
        //    return FindGenericRelation<Act, Artefact>(artefactGuid, Label.Relationship.HasSubject, hasRelationGuid);
        //}
        public GenericRelationHyperNode<Artefact, Artefact> FindInfluenceOn(string artefactGuid, string hasRelationGuid)
        {
            return FindGenericRelation<Artefact, Artefact>(artefactGuid, Label.Relationship.InfluenceOn, hasRelationGuid);
        }
        public GenericRelationHyperNode<Artefact, Artefact> FindSimilarTo(string artefactGuid, string hasRelationGuid)
        {
            return FindGenericRelation<Artefact, Artefact>(artefactGuid, Label.Relationship.SimilarTo, hasRelationGuid);
        }
        public GenericRelationHyperNode<Agent, Artefact> FindExhibitingIn(string artefactGuid, string hasRelationGuid)
        {
            return FindGenericRelation<Agent, Artefact>(artefactGuid, Label.Relationship.ExhibitingIn, hasRelationGuid);
        }
        public GenericRelationHyperNode<Artefact, Agent> FindCovers(string artefactGuid, string hasRelationGuid)
        {
            return FindGenericRelation<Artefact, Agent>(artefactGuid, Label.Relationship.Covers, hasRelationGuid);
        }
        public GenericRelationHyperNode<Artefact, Artefact> FindExhibitedIn(string artefactGuid, string hasRelationGuid)
        {
            return FindGenericRelation<Artefact, Artefact>(artefactGuid, Label.Relationship.ExhibitedIn, hasRelationGuid);
        }
        public GenericRelationNode<Artefact, Artefact> FindTakesPlaceIn(string artefactGuid, string toDestGuid)
        {
            return commonService.FindGenericRelationNode<Artefact, Artefact>(artefactGuid, Label.Relationship.TakesPlaceIn, toDestGuid);
        }
        public GenericRelationNode<Artefact, Artefact> FindLocatedIn(string artefactGuid, string toDestGuid)
        {
            return commonService.FindGenericRelationNode<Artefact, Artefact>(artefactGuid, Label.Relationship.LocatedIn, toDestGuid);
        }
        public GenericRelationNode<Artefact, Place> FindLocatedAt(string artefactGuid, string toDestGuid)
        {
            return commonService.FindGenericRelationNode<Artefact, Place>(artefactGuid, Label.Relationship.LocatedAt, toDestGuid);
        }
        public GenericRelationHyperNode<Artefact, Entity> FindRepresents(string artefactGuid, string hasRelationGuid)
        {
            return FindGenericRelation<Artefact, Entity>(artefactGuid, Label.Relationship.Represents, hasRelationGuid);
        }
        public GenericRelationHyperNode<TSource, TDest> FindGenericRelation<TSource, TDest>(string agentGuid, string relationToDestinationType, string hasRelationGuid = null)
            where TSource : class, IEntity
            where TDest : class, IEntity
        {
            return commonService.FindGenericRelation<TSource, TDest>(agentGuid, relationToDestinationType, hasRelationGuid);
        }
        private ArtefactToCategoryRelationHyperNode FindArtefactToCategoryRelation(string agentGuid, string relationToDestinationType, string hasRelationGuid = null)
        {
            ICypherFluentQuery query;
            if (hasRelationGuid != null)
            {
                query = Client.Cypher
                       .Match("(source:" + Label.Entity.Artefact + " { Guid: {agentGuid} })-[:" + Label.Relationship.HasRelation + " { Guid: {hasRelationGuid} }]->(relation:" + Label.Entity.Relation + ")")
                       .OptionalMatch("(relation)-[toDest:" + relationToDestinationType + "]->(destination:" + Label.Entity.Category + ")")
                       .OptionalMatch("(relation)-[:" + Label.Relationship.RefersTo + "]->(entity)")
                       .WithParams(new
                       {
                           agentGuid,
                           hasRelationGuid
                       });
            }
            else
            {
                query = Client.Cypher
                       .Match("(source:" + Label.Entity.Artefact + " { Guid: {agentGuid} })-[:" + Label.Relationship.HasRelation + "]->(relation:" + Label.Entity.Relation + ")")
                       .OptionalMatch("(relation)-[toDest:" + relationToDestinationType + "]->(destination:" + Label.Entity.Category + ")")
                       .OptionalMatch("(relation)-[:" + Label.Relationship.RefersTo + "]->(entity)")
                       .WithParams(new
                       {
                           agentGuid
                       });
            }
            var result = query.Return((ICypherResultItem source, ICypherResultItem relation, ICypherResultItem destination) => new
                    {
                        Source = source.As<Artefact>(),
                        RelationToDestinationGuid = Return.As<string>("toDest.Guid"),
                        Relation = relation.As<Relation>(),
                        Destination = destination.As<Category>(),
                        Entities = Return.As<IEnumerable<EntityNode>>("collect({ Entity: entity, Type: head(labels(entity)) })")
                    })
                   .Results
                   .Select(x => new ArtefactToCategoryRelationHyperNode
                   {
                       Source = x.Source,
                       RelationToDestinationGuid = x.RelationToDestinationGuid,
                       Relation = x.Relation,
                       Destination = x.Destination,
                       Entities = x.Entities.Select(y => Helpers.Common.ToEntity(y.Type, y.Entity))
                   })
                   .FirstOrDefault();
            result.HasRelationGuid = hasRelationGuid;
            return result;
        }
        private ArtefactToAgentRelationHyperNode FindArtefactToAgentRelation(string agentGuid, string relationToDestinationType, string hasRelationGuid = null)
        {
            ICypherFluentQuery query;
            if (hasRelationGuid != null)
            {
                query = Client.Cypher
                       .Match("(source:" + Label.Entity.Artefact + " { Guid: {agentGuid} })-[:" + Label.Relationship.HasRelation + " { Guid: {hasRelationGuid} }]->(relation:" + Label.Entity.Relation + ")")
                       .OptionalMatch("(relation)-[toDest:" + relationToDestinationType + "]->(destination:" + Label.Entity.Agent + ")")
                       .OptionalMatch("(relation)-[:" + Label.Relationship.RefersTo + "]->(entity)")
                       .WithParams(new
                       {
                           agentGuid,
                           hasRelationGuid
                       });
            }
            else
            {
                query = Client.Cypher
                       .Match("(source:" + Label.Entity.Artefact + " { Guid: {agentGuid} })-[:" + Label.Relationship.HasRelation + "]->(relation:" + Label.Entity.Relation + ")")
                       .OptionalMatch("(relation)-[toDest:" + relationToDestinationType + "]->(destination:" + Label.Entity.Agent + ")")
                       .OptionalMatch("(relation)-[:" + Label.Relationship.RefersTo + "]->(entity)")
                       .WithParams(new
                       {
                           agentGuid
                       });
            }
            var result = query.Return((ICypherResultItem source, ICypherResultItem relation, ICypherResultItem destination) => new
                    {
                        Source = source.As<Artefact>(),
                        RelationToDestinationGuid = Return.As<string>("toDest.Guid"),
                        Relation = relation.As<Relation>(),
                        Destination = destination.As<Agent>(),
                        Entities = Return.As<IEnumerable<EntityNode>>("collect({ Entity: entity, Type: head(labels(entity)) })")
                    })
                   .Results
                   .Select(x => new ArtefactToAgentRelationHyperNode
                   {
                       Source = x.Source,
                       RelationToDestinationGuid = x.RelationToDestinationGuid,
                       Relation = x.Relation,
                       Destination = x.Destination,
                       Entities = x.Entities.Select(y => Helpers.Common.ToEntity(y.Type, y.Entity))
                   })
                   .FirstOrDefault();
            result.HasRelationGuid = hasRelationGuid;
            return result;
        }
        private ArtefactToEntityRelationHyperNode FindArtefactToEntityRelation(string agentGuid, string hasRelationGuid, string relationToDestinationType)
        {
            var result = Client.Cypher
                   .Match("(source:" + Label.Entity.Artefact + " { Guid: {agentGuid} })-[:" + Label.Relationship.HasRelation + " { Guid: {hasRelationGuid} }]->(relation:" + Label.Entity.Relation + ")")
                   .OptionalMatch("(relation)-[toDest:" + relationToDestinationType + "]->(destination)")
                   .WithParams(new
                   {
                       agentGuid,
                       hasRelationGuid
                   })
                   .Return((ICypherResultItem source, ICypherResultItem relation, ICypherResultItem destination) => new ArtefactToEntityRelationHyperNode
                   {
                       Source = source.As<Artefact>(),
                       RelationToDestinationGuid = Return.As<string>("toDest.Guid"),
                       Relation = relation.As<Relation>(),
                       Destination = destination.As<Entity>(),
                       DestinationType = Return.As<string>("head(labels(destination))")
                   })
                   .Results
                   .FirstOrDefault();
            result.HasRelationGuid = hasRelationGuid;
            return result;
        }
        private ArtefactToPlaceRelationHyperNode FindArtefactToPlaceRelation(string agentGuid, string hasRelationGuid, string relationToDestinationType)
        {
            var result = Client.Cypher
                   .Match("(source:" + Label.Entity.Artefact + " { Guid: {agentGuid} })-[:" + Label.Relationship.HasRelation + " { Guid: {hasRelationGuid} }]->(relation:" + Label.Entity.Relation + ")")
                   .OptionalMatch("(relation)-[toDest:" + relationToDestinationType + "]->(destination:" + Label.Entity.Place + ")")
                   .WithParams(new
                   {
                       agentGuid,
                       hasRelationGuid
                   })
                   .Return((ICypherResultItem source, ICypherResultItem relation, ICypherResultItem destination) => new ArtefactToPlaceRelationHyperNode
                   {
                       Source = source.As<Artefact>(),
                       RelationToDestinationGuid = Return.As<string>("toDest.Guid"),
                       Relation = relation.As<Relation>(),
                       Destination = destination.As<Place>()
                   })
                   .Results
                   .FirstOrDefault();
            result.HasRelationGuid = hasRelationGuid;
            return result;
        }
        private ArtefactToArtefactRelationHyperNode FindArtefactToArtefactRelation(string agentGuid, string hasRelationGuid, string relationToDestinationType)
        {
            var result = Client.Cypher
                   .Match("(source:" + Label.Entity.Artefact + " { Guid: {agentGuid} })-[:" + Label.Relationship.HasRelation + " { Guid: {hasRelationGuid} }]->(relation:" + Label.Entity.Relation + ")")
                   .OptionalMatch("(relation)-[toDest:" + relationToDestinationType + "]->(destination:" + Label.Entity.Artefact + ")")
                   .WithParams(new
                   {
                       agentGuid,
                       hasRelationGuid
                   })
                   .Return((ICypherResultItem source, ICypherResultItem relation, ICypherResultItem destination) => new ArtefactToArtefactRelationHyperNode
                   {
                       Source = source.As<Artefact>(),
                       RelationToDestinationGuid = Return.As<string>("toDest.Guid"),
                       Relation = relation.As<Relation>(),
                       Destination = destination.As<Artefact>()
                   })
                   .Results
                   .FirstOrDefault();
            result.HasRelationGuid = hasRelationGuid;
            return result;
        }
        public void SaveOrUpdate(ArtefactToEntityRelationHyperNode node, string relationToDestinationType, bool reverse = false)
        {
            commonService.SaveOrUpdate(node.Relation);
            if (node.HasRelationGuid == null)
            {
                if (false == reverse)
                {
                    commonService.CreateRelationship(
                        Label.Entity.Artefact, node.Source.Guid,
                        Label.Entity.Relation, node.Relation.Guid,
                        Label.Relationship.HasRelation);
                }
                else
                {
                    commonService.CreateRelationship(
                        Label.Entity.Artefact, node.Destination.Guid,
                        Label.Entity.Relation, node.Relation.Guid,
                        Label.Relationship.HasRelation);
                }
            }
            if (node.RelationToDestinationGuid == null)
            {
                if (node.Destination.Guid != null)
                {
                    if (false == reverse)
                    {
                        commonService.CreateRelationship(
                            Label.Entity.Relation, node.Relation.Guid,
                            null, node.Destination.Guid,
                            relationToDestinationType);
                    }
                    else
                    {
                        commonService.CreateRelationship(
                            Label.Entity.Relation, node.Relation.Guid,
                            null, node.Source.Guid,
                            relationToDestinationType);
                    }
                }
            }
            else
            {
                commonService.DeleteRelationship(relationToDestinationType, node.RelationToDestinationGuid);
                if (false == reverse)
                {
                    if (node.Destination.Guid != null)
                    {
                        commonService.CreateRelationship(
                            Label.Entity.Relation, node.Relation.Guid,
                            Label.Entity.Artefact, node.Destination.Guid,
                            relationToDestinationType, node.RelationToDestinationGuid);
                    }
                }
                else
                {
                    commonService.CreateRelationship(
                        Label.Entity.Relation, node.Relation.Guid,
                        Label.Entity.Artefact, node.Source.Guid,
                        relationToDestinationType, node.RelationToDestinationGuid);
                }
            }
        }
        //
        public void SaveOrUpdate(ArtefactToPlaceRelationHyperNode node, string relationToDestinationType, bool reverse = false)
        {
            commonService.SaveOrUpdate(node.Relation);
            if (node.HasRelationGuid == null)
            {
                if (false == reverse)
                {
                    commonService.CreateRelationship(
                        Label.Entity.Artefact, node.Source.Guid,
                        Label.Entity.Relation, node.Relation.Guid,
                        Label.Relationship.HasRelation);
                }
                else
                {
                    commonService.CreateRelationship(
                        Label.Entity.Artefact, node.Destination.Guid,
                        Label.Entity.Relation, node.Relation.Guid,
                        Label.Relationship.HasRelation);
                }
            }
            if (node.RelationToDestinationGuid == null)
            {
                if (node.Destination.Guid != null)
                {
                    if (false == reverse)
                    {
                        commonService.CreateRelationship(
                            Label.Entity.Relation, node.Relation.Guid,
                            Label.Entity.Place, node.Destination.Guid,
                            relationToDestinationType);
                    }
                    else
                    {
                        commonService.CreateRelationship(
                            Label.Entity.Relation, node.Relation.Guid,
                            Label.Entity.Place, node.Source.Guid,
                            relationToDestinationType);
                    }
                }
            }
            else
            {
                commonService.DeleteRelationship(relationToDestinationType, node.RelationToDestinationGuid);
                if (false == reverse)
                {
                    if (node.Destination.Guid != null)
                    {
                        commonService.CreateRelationship(
                            Label.Entity.Relation, node.Relation.Guid,
                            Label.Entity.Place, node.Destination.Guid,
                            relationToDestinationType, node.RelationToDestinationGuid);
                    }
                }
                else
                {
                    commonService.CreateRelationship(
                        Label.Entity.Relation, node.Relation.Guid,
                        Label.Entity.Place, node.Source.Guid,
                        relationToDestinationType, node.RelationToDestinationGuid);
                }
            }
        }
        public void SaveOrUpdate(ArtefactToArtefactRelationHyperNode node, string relationToDestinationType, bool reverse = false)
        {
            commonService.SaveOrUpdate(node.Relation);
            if (node.HasRelationGuid == null)
            {
                if (false == reverse)
                {
                    commonService.CreateRelationship(
                        Label.Entity.Artefact, node.Source.Guid,
                        Label.Entity.Relation, node.Relation.Guid,
                        Label.Relationship.HasRelation);
                }
                else
                {
                    commonService.CreateRelationship(
                        Label.Entity.Artefact, node.Destination.Guid,
                        Label.Entity.Relation, node.Relation.Guid,
                        Label.Relationship.HasRelation);
                }
            }
            if (node.RelationToDestinationGuid == null)
            {
                if (node.Destination.Guid != null)
                {
                    if (false == reverse)
                    {
                        commonService.CreateRelationship(
                            Label.Entity.Relation, node.Relation.Guid,
                            Label.Entity.Artefact, node.Destination.Guid,
                            relationToDestinationType);
                    }
                    else
                    {
                        commonService.CreateRelationship(
                            Label.Entity.Relation, node.Relation.Guid,
                            Label.Entity.Artefact, node.Source.Guid,
                            relationToDestinationType);
                    }
                }
            }
            else
            {
                commonService.DeleteRelationship(relationToDestinationType, node.RelationToDestinationGuid);
                if (false == reverse)
                {
                    if (node.Destination.Guid != null)
                    {
                        commonService.CreateRelationship(
                            Label.Entity.Relation, node.Relation.Guid,
                            Label.Entity.Artefact, node.Destination.Guid,
                            relationToDestinationType, node.RelationToDestinationGuid);
                    }
                }
                else
                {
                    commonService.CreateRelationship(
                        Label.Entity.Relation, node.Relation.Guid,
                        Label.Entity.Artefact, node.Source.Guid,
                        relationToDestinationType, node.RelationToDestinationGuid);
                }
            }
        }
        public void AddAlias(string artefactGuid, string alias)
        {
            var artefact = FindArtefactByGuid(artefactGuid);
            var aliases = (artefact.Aliases ?? new string[] { }).ToList();
            aliases.Add(alias);
            artefact.Aliases = aliases.ToArray();
            SaveOrUpdate(artefact);
        }
        public List<FindByCategoryAndAgentResult> FindByCategoryAndAgent(string agentGuid, string categoryGuid)
        {
            var results = Client.Cypher
                .Match("(category:" + Label.Entity.Category + " { Guid: {categoryGuid} })")
                .Match("(children)-[:" + Label.Relationship.KindOf + "*0..3]->(category)")
                .Match("(artefact:" + Label.Entity.Artefact + ")-[:" + Label.Relationship.HasRelation + "]->()-[:" + Label.Relationship.IsA + "]->(children)")
                .Match("(madeBy:" + Label.Entity.Agent + " { Guid: {agentGuid} })<-[:" + Label.Relationship.MadeBy + "]-()<-[:" + Label.Relationship.HasRelation + "]-(artefact)")
                .OptionalMatch("(artefact)-[:" + Label.Relationship.HasImage + "]->(image:" + Label.Entity.Image + ")")
                .WithParams(new
                {
                    agentGuid,
                    categoryGuid
                })
                .Return(() => new FindByCategoryAndAgentResult
                {
                    Artefact = Return.As<Artefact>("artefact"),
                    MadeBy = Return.As<Agent>("madeBy"),
                    Categories = Return.As<IEnumerable<Category>>("collect(distinct(children))"),
                    Image = Return.As<Image>("image")
                })
                .Results
                .ToList();
            return results;
        }
        /**
         * match (artist:Agent { Name: "Michelangelo" })
            match (artefact)-[:HAS_RELATION]->(:Relation)-[:MADE_BY]->(artist)
            optional match pa=(artefact)-[:HAS_RELATION]->(:Relation)-[:PART_OF]->(parentArtefact:Artefact)
            optional match ca=(childArtefact)-[:HAS_RELATION]->(:Relation)-[:PART_OF]->(artefact)
            optional match (artefact)-[:HAS_IMAGE]->(image:Image { Primary: true })
            with count(pa) as ParentCount, artefact, image, count(ca) as ChildCount
            where ParentCount = 0
            return artefact, image, ChildCount > 0 as HasChildren
         */
        public async Task<IEnumerable<ArtefactsMadeByResult>> ArtefactsMadeByTest(string agentGuid)
        {
            var query = Client.Cypher
                .Match("(artist:" + Label.Entity.Agent + " { Guid: {agentGuid} })").WithParams(new { agentGuid })
                .Match("(artefact:" + Label.Entity.Artefact + ")-[:" + Label.Relationship.HasRelation + "]->(:" + Label.Entity.Relation + ")-[:" + Label.Relationship.MadeBy + "]->(artist)")
                .OptionalMatch("pa=(artefact)-[:" + Label.Relationship.HasRelation + "]->(:" + Label.Entity.Relation + ")-[:" + Label.Relationship.PartOf + "]->(parentArtefact:" + Label.Entity.Artefact + ")")
                .OptionalMatch("ca=(childArtefact)-[:" + Label.Relationship.HasRelation + "]->(:" + Label.Entity.Relation + ")-[:" + Label.Relationship.PartOf + "]->(artefact)")
                .OptionalMatch("(artefact)-[:" + Label.Relationship.HasImage + "]->(image:" + Label.Entity.Image + " { Primary: true })")
                .OptionalMatch("(artefact)-[:" + Label.Relationship.HasRelation + "]->(:" + Label.Entity.Relation + ")-[:" + Label.Relationship.LocatedIn + "]->(location:" + Label.Entity.Artefact + ")")
                .OptionalMatch("(artefact)-[:" + Label.Relationship.HasRelation + "]->(:" + Label.Entity.Relation + ")-[:" + Label.Relationship.IsA + "]->(category:" + Label.Entity.Category + ")")
                .With("count(pa) as ParentCount, artefact, image, count(ca) as ChildCount, category, location")
                .Where("ParentCount = 0");

            var results = await
                    query.Return(() => new ArtefactsMadeByResult
                    {
                        Artefact = Return.As<Artefact>("artefact"),
                        Is = Return.As<IEnumerable<Category>>("collect(distinct(category))"),
                        Image = Return.As<Image>("image"),
                        Location = Return.As<Artefact>("head(collect(location))"),
                        HasChildren = Return.As<bool>("ChildCount > 0")
                    })
                    .ResultsAsync;

            return results;
        }

        public async Task<IEnumerable<ArtefactsMadeByResult>> ArtefactsMadeBy(string agentGuid)
        {
            var query = Client.Cypher
                .Match("(agent:" + Label.Entity.Agent + " { Guid: {agentGuid} })").WithParams(new { agentGuid })
                .Match("(artefact:Artefact)-[:" + Label.Relationship.HasRelation + "]->(:" + Label.Entity.Relation + ")-[:" + Label.Relationship.MadeBy + "]->(agent)")
                .OptionalMatch("(artefact)-[:LOCATED_IN*0..]->(location:Artefact)-[:LOCATED_AT]->(place:Place)")
                .OptionalMatch("(artefact)-[:" + Label.Relationship.HasImage + "]->(image:" + Label.Entity.Image + " { Primary: true })")
                .OptionalMatch("(artefact)-[:" + Label.Relationship.HasRelation + "]->(:" + Label.Entity.Relation + ")-[:" + Label.Relationship.OwnedBy + "]->(owner:" + Label.Entity.Agent + ")")
                .OptionalMatch("(artefact)-[:" + Label.Relationship.HasRelation + "]->(:" + Label.Entity.Relation + ")-[:" + Label.Relationship.IsA + "]->(is:Category)")
                ;

            var results = await query
                .Return(() => new ArtefactsMadeByResult
                {
                    Artefact = Return.As<Artefact>("artefact"),
                    Is = Return.As<IEnumerable<Category>>("collect(distinct(is))"),
                    Image = Return.As<Image>("image"),
                    Owner = Return.As<Agent>("head(collect(owner))"),
                    Location = Return.As<Artefact>("location"),
                    Place = Return.As<Place>("place")
                })
                .ResultsAsync;

            return results;
        }

        public int Delete(string artefactGuid)
        {
            var deletedRelation = Client.Cypher
                .Match("(artefact:" + Label.Entity.Artefact + " { Guid: {artefactGuid} })<-[r2]-(R:" + Label.Entity.Relation + ")<-[r1:" + Label.Relationship.HasRelation + "]-(agent:" + Label.Entity.Agent + ")")
                .WithParams(new
                {
                    artefactGuid
                })
                .Delete("r1, r2, R")
                .Return(() => Return.As<int>("count(r1) + count(r2) + count(R)"))
                .Results
                .FirstOrDefault();
            var deleted = Client.Cypher
                .Match("(artefact:" + Label.Entity.Artefact + " { Guid: {artefactGuid} })-[r]-()")
                .WithParams(new
                {
                    artefactGuid
                })
                .Delete("r, artefact")
                .Return(() => Return.As<int>("count(r)"))
                .Results
                .FirstOrDefault();
            return deletedRelation + deleted;
        }
        public List<ArtefactMadeByAgent> AllRepresentedArtefacts()
        {
            var results = Client.Cypher
                .Match("(artefact:" + Label.Entity.Artefact + ")")
                .Match("(artefact)-[:" + Label.Relationship.HasRelation + "]->(:" + Label.Entity.Relation + ")-[:" + Label.Relationship.MadeBy + "]->(agent)")
                .Match("(artefact)-[:" + Label.Relationship.HasRelation + "]->(:" + Label.Entity.Relation + ")-[:" + Label.Relationship.IsA + "]->(category:" + Label.Entity.Category + ")")
                .Match("(artefact)-[:HAS_IMAGE]->(image:Image)")
                .Return((ICypherResultItem artefact, ICypherResultItem agent) => new ArtefactMadeByAgent
                {
                    Artefact = artefact.As<Artefact>(),
                    Agents = Return.As<IEnumerable<Agent>>("collect(distinct(agent))"),
                    IsA = Return.As<IEnumerable<Category>>("collect(distinct(category))")
                })
                .Results
                .ToList();
            return results;
        }

        public List<ArtefactMadeByAgent> AllSources()
        {
            var results = Client.Cypher
               .Match("(reference:Category { Name: {referenceName} })").WithParams(new { referenceName = Label.Categories.Reference })
               .Match("(references:Category)-[:KIND_OF*0..]->(reference)")
               .Match("(artefact:" + Label.Entity.Artefact + ")-[:HAS_RELATION]->()-[:IS_A]->(references)")
               .OptionalMatch("(artefact)-[:" + Label.Relationship.HasRelation + "]->(madeBy:" + Label.Entity.Relation + ")-[:" + Label.Relationship.MadeBy + "]->(agent)")
               .OptionalMatch("(artefact)-[:" + Label.Relationship.HasRelation + "]->(:" + Label.Entity.Relation + ")-[:" + Label.Relationship.IsA + "]->(category:" + Label.Entity.Category + ")")
               .OptionalMatch("(artefact)-[:LOCATED_AT]->(location:Place)")
               .Return((ICypherResultItem artefact, ICypherResultItem agent) => new ArtefactMadeByAgent
               {
                   Artefact = artefact.As<Artefact>(),
                   MadeBy = Return.As<Relation>("madeBy"),
                   Agents = Return.As<IEnumerable<Agent>>("collect(agent)"),
                   IsA = Return.As<IEnumerable<Category>>("collect(distinct(category))"),
                   Location = Return.As<Place>("location")
               })
               .Results
               .ToList();
            return results;
        }

        public List<ArtefactMadeByAgent> AllMadeBy()
        {
            var results = Client.Cypher
                .Match("(artefact:" + Label.Entity.Artefact + ")")
                .OptionalMatch("(artefact)-[:" + Label.Relationship.HasRelation + "]->(madeBy:" + Label.Entity.Relation + ")-[:" + Label.Relationship.MadeBy + "]->(agent)")
                .OptionalMatch("(artefact)-[:" + Label.Relationship.HasRelation + "]->(:" + Label.Entity.Relation + ")-[:" + Label.Relationship.IsA + "]->(category:" + Label.Entity.Category + ")")
                .OptionalMatch("(artefact)-[:LOCATED_AT]->(location:Place)")
                .Return((ICypherResultItem artefact, ICypherResultItem agent) => new ArtefactMadeByAgent
                {
                    Artefact = artefact.As<Artefact>(),
                    MadeBy = Return.As<Relation>("madeBy"),
                    Agents = Return.As<IEnumerable<Agent>>("collect(agent)"),
                    IsA = Return.As<IEnumerable<Category>>("collect(distinct(category))"),
                    Location = Return.As<Place>("location")
                })
                .Results
                .ToList();
            return results;
        }
        public void SetCurrentUser(User user)
        {
            CurrentUser = user;
        }
        public EventDetail SaveOrUpdate(EventDetail detail)
        {
            if (detail.Guid == null)
            {
                detail.Guid = Guid.NewGuid().ToString();
            }
            Client.Cypher
                .Merge("(detail:" + Label.Entity.EventDetail + " { Guid: {guid} })")
                .OnCreate().Set("detail = {detail}")
                .OnMatch().Set("detail = {detail}")
                .WithParams(new
                {
                    guid = detail.Guid,
                    detail
                })
                .ExecuteWithoutResults();
            return detail;
        }
        public CollectionDetail SaveOrUpdate(CollectionDetail detail)
        {
            if (detail.Guid == null)
            {
                detail.Guid = Guid.NewGuid().ToString();
            }
            Client.Cypher
                .Merge("(detail:" + Label.Entity.Detail + " { Guid: {guid} })")
                .OnCreate().Set("detail = {detail}")
                .OnMatch().Set("detail = {detail}")
                .WithParams(new
                {
                    guid = detail.Guid,
                    detail
                })
                .ExecuteWithoutResults();
            return detail;
        }
        public Artefact SaveOrUpdate(Artefact artefact)
        {
            commonService.SaveOrUpdate(artefact, Label.Entity.Artefact);
            Cache.RemoveFromCache(CacheKey.ArtefactService.AllMadeBy);
            return artefact;
        }
        public string Recognisability(string artefactGuid, string entityGuid, string entityType, int rank)
        {
            var artefact = FindArtefactByGuid(artefactGuid);
            var relation = HasRelation(artefact, CreateRelation());
            var relationshipGuid = Guid.NewGuid().ToString();
            Client.Cypher
                .Match(
                    "(relation:" + Label.Entity.Relation + " { Guid: {relationGuid} })",
                    "(entity:" + entityType + " { Guid: {entityGuid} })")
                .WithParams(new
                {
                    relationGuid = relation.Guid,
                    entityGuid
                })
                .CreateUnique("relation-[:" + Label.Relationship.Recognisability + " { Guid: {relationshipGuid}, Rank: {rank} }]->entity")
                .WithParams(new
                {
                    relationshipGuid,
                    rank
                })
                .ExecuteWithoutResults();
            return relationshipGuid;
        }
        public string RecognisabilityByAgent(string artefactGuid, string agentGuid, int rank)
        {
            return Recognisability(artefactGuid, agentGuid, Label.Entity.Agent, rank);
        }
        public string RecognisabilityByCategory(string artefactGuid, string categoryGuid, int rank)
        {
            return Recognisability(artefactGuid, categoryGuid, Label.Entity.Category, rank);
        }
        public void Applies(string artefactGuid, string categoryGuid)
        {
            var artefact = FindArtefactByGuid(artefactGuid);
            var relation = HasRelation(artefact, CreateRelation());
            Client.Cypher
                .Match(
                    "(relation:" + Label.Entity.Relation + " { Guid: {relationGuid} })",
                    "(cat:" + Label.Entity.Category + " { Guid: {categoryGuid} })")
                .WithParams(new
                {
                    relationGuid = relation.Guid,
                    categoryGuid
                })
                .CreateUnique("relation-[:" + Label.Relationship.Uses + " { Guid: {guid} }]->cat")
                .WithParams(new
                {
                    guid = Guid.NewGuid().ToString()
                })
                .ExecuteWithoutResults();
        }
        public void RepresentsPlace(string artefactGuid, string placeGuid)
        {
            var artefact = FindArtefactByGuid(artefactGuid);
            CreateRelationship(artefact, placeGuid, Label.Entity.Place, Label.Relationship.Represents);
        }
        public void RepresentsAgent(string artefactGuid, string agentGuid)
        {
            var artefact = FindArtefactByGuid(artefactGuid);
            CreateRelationship(artefact, agentGuid, Label.Entity.Agent, Label.Relationship.Represents);
        }
        public void RepresentsCategory(string artefactGuid, string categoryGuid)
        {
            var artefact = FindArtefactByGuid(artefactGuid);
            CreateRelationship(artefact, categoryGuid, Label.Entity.Category, Label.Relationship.Represents);
        }
        public void MadeFrom(string artefactGuid, string categoryGuid)
        {
            var artefact = FindArtefactByGuid(artefactGuid);
            CreateRelationship(artefact, categoryGuid, Label.Entity.Category, Label.Relationship.MadeFrom);
        }
        public void CommissionedBy(string artefactGuid, string agentGuid)
        {
            var artefact = FindArtefactByGuid(artefactGuid);
            CreateRelationship(artefact, agentGuid, Label.Entity.Agent, Label.Relationship.CommissionedBy);
        }
        public void ExhibitedIn(string artefactGuid, string exhibitionGuid)
        {
            var artefact = FindArtefactByGuid(artefactGuid);
            CreateRelationship(artefact, exhibitionGuid, Label.Entity.Event, Label.Relationship.ExhibitedIn);
        }
        //public void HasSubject(string artefactGuid, string subjectGuid)
        //{
        //    var artefact = FindArtefactByGuid(artefactGuid);
        //    CreateRelationship(artefact, subjectGuid, Label.Entity.Category, Label.Relationship.HasSubject);
        //}
        public void HasMotif(string artefactGuid, string motifGuid)
        {
            var artefact = FindArtefactByGuid(artefactGuid);
            CreateRelationship(artefact, motifGuid, Label.Entity.Category, Label.Relationship.HasMotif);
        }
        public void MadeBy(string artefactGuid, string agentGuid)
        {
            var artefact = FindArtefactByGuid(artefactGuid);
            CreateRelationship(artefact, agentGuid, Label.Entity.Agent, Label.Relationship.MadeBy);
        }
        public void MadeWith(string artefactGuid, string implementGuid)
        {
            var artefact = FindArtefactByGuid(artefactGuid);
            CreateRelationship(artefact, implementGuid, Label.Entity.Category, Label.Relationship.MadeWith);
        }
        public void LocatedAt(string artefactGuid, string placeGuid)
        {
            var artefact = FindArtefactByGuid(artefactGuid);
            CreateRelationship(artefact, placeGuid, Label.Entity.Place, Label.Relationship.LocatedAt);
        }
        public void LocatedIn(string artefactGuid, string buildingGuid)
        {
            var artefact = FindArtefactByGuid(artefactGuid);
            CreateRelationship(artefact, buildingGuid, Label.Entity.Artefact, Label.Relationship.LocatedIn);
        }
        public void OntologicalStateOf(string artefactGuid, string categoryGuid)
        {
            var artefact = FindArtefactByGuid(artefactGuid);
            CreateRelationship(artefact, categoryGuid, Label.Entity.Category, Label.Relationship.OntologicalStateOf);
        }
        //public void OwnedBy(string artefactGuid, string ownerGuid)
        //{
        //    var artefact = FindArtefactByGuid(artefactGuid);
        //    CreateRelationship(artefact, ownerGuid, Label.Entity.Agent, Label.Relationship.OwnedBy);
        //    Helpers.Common.RemoveFromCache(CacheKey.ArtefactService.AllMadeBy);
        //}
        public void SimilarTo(string artefactGuid, string otherArtefactGuid)
        {
            var artefact = FindArtefactByGuid(artefactGuid);
            CreateRelationship(artefact, otherArtefactGuid, Label.Entity.Artefact, Label.Relationship.SimilarTo);
        }
        private void CreateRelationship(IEntity entity, string otherGuid, string otherType, string relationship)
        {
            var relationService = new RelationService(Client);
            relationService.CreateRelationship(entity, otherGuid, otherType, relationship);
        }
        public void IsA(Artefact artefact, params Category[] categories)
        {
            foreach (var category in categories)
            {
                CreateRelationship(artefact, category.Guid, Label.Entity.Category, Label.Relationship.IsA);
            }
        }
        public Relation HasRelation(IEntity entity, Data.Entities.Relation attribute)
        {
            /**
             * (entity)-[:HAS_ATTR]->(:Attribute)
             */
            Client.Cypher
                    .Match("(ent)", "(attr:" + Label.Entity.Relation + ")")
                    .Where((IEntity ent) => ent.Guid == entity.Guid)
                    .AndWhere((Relation attr) => attr.Guid == attribute.Guid)
                    .CreateUnique("ent-[:" + Label.Relationship.HasRelation + " { Guid: {guid} }]->attr")
                    .WithParams(new
                    {
                        guid = Guid.NewGuid().ToString()
                    })
                    .ExecuteWithoutResults();
            return attribute;
        }
        public Relation CreateRelation()
        {
            /**
             * (:Attribute)-[:ASSERTED_BY]->(CurrentUser)
             */
            var attribute = new Relation { Guid = Guid.NewGuid().ToString() };
            Client.Cypher
                .Merge("(attribute:" + Label.Entity.Relation + " { Guid: {guid} })")
                .OnCreate().Set("attribute = {attribute}")
                .WithParams(new
                {
                    guid = attribute.Guid,
                    attribute = attribute
                })
                .ExecuteWithoutResults();
            if (CurrentUser != null)
            {
                AssertedBy(attribute, CurrentUser);
            }
            return attribute;
        }
        public void AssertedBy(Relation relation, User _user)
        {
            Client.Cypher
            .Match("(attribute:" + Label.Entity.Relation + " { Guid: {attributeGuid} })",
                   "(user:" + Label.Entity.User + " { Guid: {userGuid} })")
            .WithParams(new
            {
                attributeGuid = relation.Guid,
                userGuid = _user.Guid
            })
            .CreateUnique("attribute-[:" + Label.Relationship.AssertedBy + " { Guid: {guid}, IsActive: {isActive} }]->user")
            .WithParams(new
            {
                guid = Guid.NewGuid().ToString(),
                isActive = true
            })
            .ExecuteWithoutResults();
        }
        /**
         *  match (a1:Agent {Name:"Leonardo da Vinci"})-[:HAS_ATTR]->(:Attribute)-[:IS_A]->(c1:Category)
            with c1, a1
            match (a2:Agent)-[:HAS_ATTR]->(:Attribute)-[:IS_A]->(c2:Category)
            where c2.Guid = c1.Guid and a2.Name <> a1.Name
            with a2, a1, collect(distinct(c2.Name)) as categories
            return a1.Name, a2.Name, categories, length(categories) as total order by total desc, a2.Name
         */
        public List<ArtefactsWithSameAttributes> FindSimilarArtefactsByTaxonomy(string artefactGuid, int? separation)
        {
            return FindSimilarArtefactsBy(artefactGuid, Label.Relationship.IsA, separation);
        }
        public List<ArtefactsWithSameAttributes> FindSimilarArtefactsByUsage(string artefactGuid, int? separation)
        {
            return FindSimilarArtefactsBy(artefactGuid, Label.Relationship.Uses, separation);
        }
        public List<ArtefactsWithSameAttributes> FindSimilarArtefactsByMotif(string artefactGuid, int? separation)
        {
            return FindSimilarArtefactsBy(artefactGuid, Label.Relationship.HasMotif, separation);
        }
        public List<ArtefactsWithSameAttributes> FindSimilarArtefactsByRepresents(string artefactGuid, int? separation)
        {
            return FindSimilarArtefactsBy(artefactGuid, Label.Relationship.Represents, separation);
        }
        public List<ArtefactsWithSameAttributes> FindSimilarArtefactsByRepresentsByAgent(string artefactGuid, int? separation)
        {
            return FindSimilarAgentsBy(artefactGuid, Label.Relationship.Represents, separation);
        }
        public List<ArtefactsWithSameAttributes> FindSimilarArtefactsByRepresentsByPlace(string artefactGuid, int? separation)
        {
            return FindSimilarPlacesBy(artefactGuid, Label.Relationship.Represents, separation);
        }
        //public void Dynamise(List<IGrouping<string, GenericAgentAgentHyperNode>> hyperNodesGrouping)
        //{
        //    foreach (var group in hyperNodesGrouping)
        //    {
        //        foreach (var item in group)
        //        {
        //            item.EntityDynamic = JsonConvert.DeserializeObject<dynamic>(item.EntityNode.Data);
        //        }
        //    }
        //}

        public ArtefactPublicOld FindArtefactPublicOld(string artefactGuid)
        {
            var details = Client.Cypher
                .Match("(artefact:" + Label.Entity.Artefact + " { Guid: {artefactGuid} })")
                .OptionalMatch("(artefact)-[:" + Label.Relationship.HasRelation + "]->(madeByRelation:" + Label.Entity.Relation + ")-[:" + Label.Relationship.MadeBy + "]->(madeBy:" + Label.Entity.Agent + ")")
                .OptionalMatch("(artefact)-[:" + Label.Relationship.HasRelation + "]->(:" + Label.Entity.Relation + ")-[:" + Label.Relationship.LocatedAt + "]->(locatedAt:" + Label.Entity.Place + ")")
                .OptionalMatch("(artefact)-[:" + Label.Relationship.HasRelation + "]->(:" + Label.Entity.Relation + ")-[:" + Label.Relationship.OwnedBy + "]->(ownedBy:" + Label.Entity.Agent + ")")
                .OptionalMatch("(artefact)-[:" + Label.Relationship.HasRelation + "]->(:" + Label.Entity.Relation + ")-[:" + Label.Relationship.IsA + "]->(attributes:" + Label.Entity.Category + ")")
                .OptionalMatch("(artefact)-[:" + Label.Relationship.HasRelation + "]->(:" + Label.Entity.Relation + ")-[:" + Label.Relationship.MadeFrom + "]->(madeFrom:" + Label.Entity.Category + ")")
                .OptionalMatch("(artefact)-[:" + Label.Relationship.HasImage + "]->(images:" + Label.Entity.Image + ")")
                .WithParams(new
                {
                    artefactGuid
                })
                .Return((
                    ICypherResultItem artefact,
                    ICypherResultItem attributes,
                    ICypherResultItem madeBy,
                    ICypherResultItem madeByRelation,
                    ICypherResultItem locatedAt,
                    ICypherResultItem ownedBy) => new
                    {
                        Artefact = artefact.As<Artefact>(),
                        Attributes = Return.As<IEnumerable<Category>>("collect(distinct(attributes))"),
                        Images = Return.As<IEnumerable<Image>>("collect(distinct(images))"),
                        MadeBy = madeBy.As<Agent>(),
                        MadeByRelation = madeByRelation.As<Relation>(),
                        LocatedAt = locatedAt.As<Place>(),
                        OwnedBy = ownedBy.As<Agent>(),
                        MadeFrom = Return.As<IEnumerable<Category>>("collect(distinct(madeFrom))")
                    })
                    .Results
                    .FirstOrDefault();

            var sameMotifs = Client.Cypher
                .Match("(artefact:" + Label.Entity.Artefact + " { Guid: {artefactGuid} })")
                .WithParams(new { artefactGuid })
                .Match("(artefact)-[:" + Label.Relationship.HasRelation + "]->(:" + Label.Entity.Relation + ")-[:" + Label.Relationship.HasMotif + "]->(motif:" + Label.Entity.Category + ")")
                .OptionalMatch("(artefact2:" + Label.Entity.Artefact + ")-[:" + Label.Relationship.HasRelation + "]->(:" + Label.Entity.Relation + ")-[:" + Label.Relationship.HasMotif + "]->(motif2:" + Label.Entity.Category + ")")
                .Where("motif.Guid = motif2.Guid and artefact.Guid <> artefact2.Guid")
                .With("artefact, artefact2, motif, motif2")
                .Match("(artefact2)-[:" + Label.Relationship.HasImage + "]->(image:" + Label.Entity.Image + ")")
                .Return((ICypherResultItem artefact) => new
                {
                    Artefact = artefact.As<Artefact>(),
                    ArtefactByMotif = Return.As<IEnumerable<ArtefactMotifImageTuple>>("collect({ Artefact: artefact2, Motif: motif2, Image: image })")
                })
                .Results
                .FirstOrDefault();

            var representsThings = Client.Cypher
                .Match("(artefact:" + Label.Entity.Artefact + " { Guid: {artefactGuid} })")
                .WithParams(new
                {
                    artefactGuid
                })
                .Match("(artefact)-[:" + Label.Relationship.HasRelation + "]->(:" + Label.Entity.Relation + ")-[:" + Label.Relationship.Represents + "]->(cat:" + Label.Entity.Category + ")")
                .OptionalMatch("(artefact2:" + Label.Entity.Artefact + ")-[:" + Label.Relationship.HasRelation + "]->(:" + Label.Entity.Relation + ")-[:" + Label.Relationship.Represents + "]->(cat2:" + Label.Entity.Category + ")")
                .Where("cat.Guid = cat2.Guid and artefact2.Guid <> artefact.Guid")
                .With("artefact, artefact2, cat, cat2")
                .Match("(artefact2)-[:" + Label.Relationship.HasImage + "]->(image:" + Label.Entity.Image + ")")
                .Return((ICypherResultItem artefact) => new
                    {
                        RepresentedCategories = Return.As<IEnumerable<Category>>("collect(distinct(cat))"),
                        ArtefactByCategory = Return.As<IEnumerable<ArtefactCategoryImageTuple>>("collect({ Artefact: artefact2, Category: cat2, Image: image })")
                    })
                    .Results
                    .FirstOrDefault();

            var representedCategories = Client.Cypher
                .Match("(artefact:" + Label.Entity.Artefact + " { Guid: {artefactGuid} })-[:" + Label.Relationship.HasRelation + "]->(:" + Label.Entity.Relation + ")-[:" + Label.Relationship.Represents + "]->(thing:" + Label.Entity.Category + ")")
                .WithParams(new { artefactGuid })
                .Return((ICypherResultItem thing) => thing.As<Category>())
                .Results
                .ToList();

            var representsAgents = Client.Cypher
                .Match("(artefact1:" + Label.Entity.Artefact + " { Guid: {artefactGuid} })")
                .WithParams(new
                {
                    artefactGuid
                })
                .Match("(artefact1)-[:" + Label.Relationship.HasRelation + "]->(:" + Label.Entity.Relation + ")-[:" + Label.Relationship.Represents + "]->(agent1:" + Label.Entity.Agent + ")")
                .OptionalMatch("(artefact2:" + Label.Entity.Artefact + ")-[:" + Label.Relationship.HasRelation + "]->(:" + Label.Entity.Relation + ")-[:" + Label.Relationship.Represents + "]->(agent2:" + Label.Entity.Agent + ")")
                .Where("agent1.Guid = agent2.Guid and artefact2.Guid <> artefact1.Guid")
                .With("artefact1, artefact2, agent1, agent2")
                .Match("(artefact2)-[:" + Label.Relationship.HasImage + "]->(image:" + Label.Entity.Image + ")")
                .Return(() => new
                {
                    RepresentedAgents = Return.As<IEnumerable<Agent>>("collect(distinct(agent1))"),
                    ArtefactByAgent = Return.As<IEnumerable<ArtefactAgentImageTuple>>("collect(distinct({ Artefact: artefact2, Agent: agent2, Image: image }))")
                })
                .Results
                .FirstOrDefault();

            var representsPlaces = Client.Cypher
                .Match("(artefact1:" + Label.Entity.Artefact + " { Guid: {artefactGuid} })")
                .WithParams(new
                {
                    artefactGuid
                })
                .Match("(artefact1)-[:" + Label.Relationship.HasRelation + "]->(:" + Label.Entity.Relation + ")-[:" + Label.Relationship.Represents + "]->(place1:" + Label.Entity.Place + ")")
                .OptionalMatch("(artefact2:" + Label.Entity.Artefact + ")-[:" + Label.Relationship.HasRelation + "]->(:" + Label.Entity.Relation + ")-[:" + Label.Relationship.Represents + "]->(place2:" + Label.Entity.Place + ")")
                .Where("place1.Guid = place2.Guid and artefact2.Guid <> artefact1.Guid")
                .With("artefact1, artefact2, place1, place2")
                .Match("(artefact2)-[:" + Label.Relationship.HasImage + "]->(image:" + Label.Entity.Image + ")")
                .Return(() => new
                {
                    RepresentedPlaces = Return.As<IEnumerable<Place>>("collect(distinct(place1))"),
                    ArtefactByPlace = Return.As<IEnumerable<ArtefactPlaceImageTuple>>("collect(distinct({ Artefact: artefact2, Place: place2, Image: image }))")
                })
                .Results
                .FirstOrDefault();

            var representedPlaces = Client.Cypher
                .Match("(artefact:" + Label.Entity.Artefact + " { Guid: {artefactGuid} })-[:" + Label.Relationship.HasRelation + "]->(:" + Label.Entity.Relation + ")-[:" + Label.Relationship.Represents + "]->(place:" + Label.Entity.Place + ")")
                .WithParams(new { artefactGuid })
                .Return((ICypherResultItem place) => place.As<Place>())
                .Results
                .ToList();

            var results = new ArtefactPublicOld
            {
                Artefact = details.Artefact,
                Attributes = details.Attributes,
                MadeBy = details.MadeBy,
                MadeByRelation = details.MadeByRelation,
                OwnedBy = details.OwnedBy,
                LocatedAt = details.LocatedAt,
                MadeFrom = details.MadeFrom != null ? details.MadeFrom.ToList() : new List<Category>(),
                Images = details.Images != null ? details.Images.ToList() : new List<Image>()
            };

            if (sameMotifs != null)
            {
                results.ArtefactByMotif = sameMotifs.ArtefactByMotif;
            }

            if (representsThings != null)
            {
                results.RepresentedCategories = representedCategories;
                results.ArtefactByCategory = representsThings.ArtefactByCategory;
            }

            if (representsAgents != null)
            {
                results.AgentsRepresented = representsAgents.RepresentedAgents;
                results.ArtefactByAgent = representsAgents.ArtefactByAgent;
            }

            if (representsPlaces != null)
            {
                results.RepresentedPlaces = representedPlaces;
                results.ArtefactByPlace = representsPlaces.ArtefactByPlace;
            }


            return results;
        }
        public List<ArtefactsRelatedByUsage> FindArtefactsRelatedByUsage(string artefactGuid)
        {
            var results = Client.Cypher
                .Match("(artefact:" + Label.Entity.Artefact + " { Guid: {artefactGuid} })-[:" + Label.Relationship.HasRelation + "]-(:" + Label.Entity.Relation + ")-[:" + Label.Relationship.Uses + "]->(technique:" + Label.Entity.Category + ")")
                .Match("(related:" + Label.Entity.Artefact + ")-[:" + Label.Relationship.HasRelation + "]->(:" + Label.Entity.Relation + ")-[:" + Label.Relationship.Uses + "]->(technique)")
                .OptionalMatch("(related)-[:" + Label.Relationship.HasImage + "]->(image:" + Label.Entity.Image + ")")
                .WithParams(new { artefactGuid })
                .Where("related.Guid <> source.Guid")
                .Return((ICypherResultItem artefact, ICypherResultItem images) => new ArtefactsRelatedByUsage
                {
                    Artefact = artefact.As<Artefact>(),
                    Images = Return.As<IEnumerable<Image>>("distinct(collect(image))")
                })
                .Results
                .ToList();
            return results;
        }
        public ArtefactImageTuple FindArtefactWithImage(string artefactGuid)
        {
            var query = Client.Cypher
                .Match("(artefact:Artefact { Guid: {artefactGuid} })").WithParams(new { artefactGuid })
                .Match("(artefact)-[:HAS_IMAGE]->(image:Image { Primary: true })")
                ;
            return query.Return(() => new ArtefactImageTuple
            {
                Artefact = Return.As<Artefact>("artefact"),
                Image = Return.As<Image>("image")
            })
            .Results
            .FirstOrDefault();
        }
        public IEnumerable<ArtefactImageTuple> FindRecommendedByArtefact(string artefactGuid)
        {
            var query = Client.Cypher
                .Match("(artefact:Artefact { Guid: {artefactGuid} })").WithParams(new { artefactGuid })
                .Match("(artefact)-[:HAS_RELATION]->(:Relation { Primary: true })-[:HAS_MOTIF|USES|HAS_GENRE]->(quality:Category)")
                .Match("(qualities:Category)-[:KIND_OF*0..]->(quality)")
                .Match("(user:User)-[:BOOKMARKED]->(artefact)")
                .Match("D=(user)-[:BOOKMARKED]->(artefact2:Artefact)-[:HAS_RELATION]->(:Relation { Primary: true })-[:HAS_MOTIF|USES|HAS_GENRE]->(qualities)")
                .Where("artefact.Guid <> artefact2.Guid")
                .Limit(5)
                .Match("(artefact2)-[:HAS_IMAGE]->(image:Image { Primary: true })")
                .With("count(qualities) as TotalQualities, artefact2, image")
                ;

            var result = query
                .Return(() => new ArtefactImageTuple
                {
                    Artefact = Return.As<Artefact>("artefact2"),
                    Image = Return.As<Image>("image")
                })
                .OrderBy("TotalQualities desc")
                .Results
                .ToList()
                .Take(5);

            return result;
        }
        public async Task<IEnumerable<ArtefactImageTuple>> FindSimilarAsync(string artefactGuid)
        {
            var relations = "HAS_MOTIF|USES|HAS_GENRE|IS_A";
            var query = Client.Cypher
                .Match("(a1:" + Label.Entity.Artefact + " { Guid: {artefactGuid} })").WithParams(new { artefactGuid })
                .Match("(a1)-[:HAS_RELATION]->(:Relation { Primary: true })-[:" + relations + "]->(:" + Label.Entity.Category + ")-[:" + Label.Relationship.KindOf + "*0..]->(c1:" + Label.Entity.Category + ")")
                .Match("p=(a2:" + Label.Entity.Artefact + ")-[:HAS_RELATION]->(:Relation { Primary: true })-[:" + relations + "]->(entity:" + Label.Entity.Category + ")-[:" + Label.Relationship.KindOf + "*0..]->(c1)")
                .Match("(a2)-[:" + Label.Relationship.HasImage + "]->(image:" + Label.Entity.Image + " { Primary: true })")
                .With("a1, a2, image, count(c1) as TotalCategories, length(p) as PathCost")
                .Where("a2.Guid <> a1.Guid and PathCost <= 2")
                ;

            var results1 = await query.Return(() => new ArtefactImageTuple
            {
                Artefact = Return.As<Artefact>("a2"),
                Image = Return.As<Image>("image")
            })
            .OrderBy("PathCost, TotalCategories desc")
            .ResultsAsync
            ;

            var relations2 = Label.Relationship.CopyOf + "|" + Label.Relationship.StudyFor + "|" + Label.Relationship.SimilarTo + "|" + Label.Relationship.InfluenceOn;
            var results2 = await Client.Cypher
                .Match("(a1:" + Label.Entity.Artefact + " { Guid: {artefactGuid} })").WithParams(new { artefactGuid })
                .Match("(a2:" + Label.Entity.Artefact + ")-[]->()-[:" + relations2 + "]->(a1)")
                .Match("(a2)-[:" + Label.Relationship.HasImage + "]->(image:" + Label.Entity.Image + " { Primary: true })")
                .Return(() => new ArtefactImageTuple
                {
                    Artefact = Return.As<Artefact>("a2"),
                    Image = Return.As<Image>("image")
                })
                .ResultsAsync
                ;

            var results3 = await Client.Cypher
                .Match("(a1:" + Label.Entity.Artefact + " { Guid: {artefactGuid} })").WithParams(new { artefactGuid })
                .Match("(a1)-[]->()-[:" + relations2 + "]->(a2:Artefact)")
                .Match("(a2)-[:" + Label.Relationship.HasImage + "]->(image:" + Label.Entity.Image + " { Primary: true })")
                .Return(() => new ArtefactImageTuple
                {
                    Artefact = Return.As<Artefact>("a2"),
                    Image = Return.As<Image>("image")
                })
                .ResultsAsync;

            return results2
                .Concat(results3)
                .Concat(results1)
                .Distinct(new ArtefactImageTupleComparer())
                ;
        }
        private List<ArtefactsWithSameAttributes> FindSimilarArtefactsBy(string artefactGuid, string relationType, int? separation)
        {
            return Client.Cypher
                .Match("(a1:" + Label.Entity.Artefact + " { Guid: {artefactGuid} })").WithParams(new { artefactGuid })
                .Match("(a1)-[:" + Label.Relationship.HasRelation + "]->(:" + Label.Entity.Relation + ")-[:" + relationType + "]->()-[:" + Label.Relationship.KindOf + "*0.." + (separation ?? 0) + "]->(c1:" + Label.Entity.Category + ")")
                .With("c1, a1")
                .Match("p=(a2:" + Label.Entity.Artefact + ")-[:" + Label.Relationship.HasRelation + "]->(:" + Label.Entity.Relation + ")-[r:" + relationType + "]->(entity)-[:" + Label.Relationship.KindOf + "*0.." + (separation ?? 0) + "]->(c2:" + Label.Entity.Category + ")")
                //.OptionalMatch("(a2)-[:HAS_IMAGE]->(image:Image { Primary: true })")
                .Where("c2.Guid = c1.Guid and a2.Guid <> a1.Guid")
                .With("a2, a1, c2, collect(type(r)) as rels, entity, count(distinct(c2)) as Total, length(p) as PathCost")
                .Return((ICypherResultItem a1, ICypherResultItem a2, ICypherResultItem c2) => new ArtefactsWithSameAttributes
                {
                    Artefact1 = a1.As<Artefact>(),
                    Artefact2 = a2.As<Artefact>(),
                    Categories = Return.As<IEnumerable<Category>>("collect(distinct(c2))"),
                    Entities = Return.As<IEnumerable<Entity>>("collect(distinct(entity))"),
                    RelationTypes = Return.As<IEnumerable<string>>("rels"),
                    Total = c2.CountDistinct(),
                    PathCost = Return.As<long>("PathCost")
                    //Image = Return.As<Image>("image")
                })
                .OrderBy("Total desc, a1.Name", "a2.Name")
                .Results
                .ToList();
        }
        private List<ArtefactsWithSameAttributes> FindSimilarAgentsBy(string artefactGuid, string relationType, int? separation)
        {
            return Client.Cypher
                .Match("(a1:" + Label.Entity.Artefact + " { Guid: {artefactGuid} })-[:" + Label.Relationship.HasRelation + "]->(:" + Label.Entity.Relation + ")-[:" + relationType + "]->(e1:Agent)")
                //.OptionalMatch("(e1)-[:" + Label.Relationship.HasRelation + "]->()-[:" + Label.Relationship.IsA + "]->()-[:" + Label.Relationship.KindOf + "*0.." + (separation ?? 0) + "]->(c1:" + Label.Entity.Category + ")")
                .WithParams(new
                {
                    artefactGuid
                })
                .With("e1, a1")
                .Match("p=(a2:" + Label.Entity.Artefact + ")-[:" + Label.Relationship.HasRelation + "]->(:" + Label.Entity.Relation + ")-[r:" + relationType + "]->(e2:Agent)")
                //.OptionalMatch("(e2)-[:" + Label.Relationship.HasRelation + "]->()-[:" + Label.Relationship.IsA + "]->()-[:" + Label.Relationship.KindOf + "*0.." + (separation ?? 0) + "]->(c2:" + Label.Entity.Category + ")")
                .Where("e2.Guid = e1.Guid and a2.Guid <> a1.Guid")
                .With("a2, a1, e2, collect(type(r)) as rels, count(distinct(e2)) as Total, length(p) as PathCost")
                .Return((ICypherResultItem a1, ICypherResultItem a2, ICypherResultItem e2) => new ArtefactsWithSameAttributes
                {
                    Artefact1 = a1.As<Artefact>(),
                    Artefact2 = a2.As<Artefact>(),
                    // Categories = Return.As<IEnumerable<Category>>("collect(distinct(c2))"),
                    Entities = Return.As<IEnumerable<Entity>>("collect(e2)"),
                    EntityTypes = Return.As<IEnumerable<string>>("collect(head(labels(e2)))"),
                    RelationTypes = Return.As<IEnumerable<string>>("rels"),
                    Total = e2.CountDistinct(),
                    PathCost = Return.As<long>("PathCost")
                })
                .OrderBy("Total desc, a1.Name", "a2.Name")
                .Results
                .ToList();
        }
        private List<ArtefactsWithSameAttributes> FindSimilarPlacesBy(string artefactGuid, string relationType, int? separation)
        {
            return Client.Cypher
                .Match("(a1:" + Label.Entity.Artefact + " { Guid: {artefactGuid} })-[:" + Label.Relationship.HasRelation + "]->(:" + Label.Entity.Relation + ")-[:" + relationType + "]->(e1:Place)")
                .WithParams(new
                {
                    artefactGuid
                })
                .With("e1, a1")
                .Match("p=(a2:" + Label.Entity.Artefact + ")-[:" + Label.Relationship.HasRelation + "]->(:" + Label.Entity.Relation + ")-[r:" + relationType + "]->(e2:Place)")
                .Where("e2.Guid = e1.Guid and a2.Guid <> a1.Guid")
                .With("a2, a1, e2, collect(type(r)) as rels, count(distinct(e2)) as Total, length(p) as PathCost")
                .Return((ICypherResultItem a1, ICypherResultItem a2, ICypherResultItem e2) => new ArtefactsWithSameAttributes
                {
                    Artefact1 = a1.As<Artefact>(),
                    Artefact2 = a2.As<Artefact>(),
                    Entities = Return.As<IEnumerable<Entity>>("collect(e2)"),
                    EntityTypes = Return.As<IEnumerable<string>>("collect(head(labels(e2)))"),
                    RelationTypes = Return.As<IEnumerable<string>>("rels"),
                    Total = e2.CountDistinct(),
                    PathCost = Return.As<long>("PathCost")
                })
                .OrderBy("Total desc, a1.Name", "a2.Name")
                .Results
                .ToList();
        }
        public List<ArtefactsRelatedToSimilarArtefacts> FindArtefactsRelatedToSimilarArtefacts(string artefactGuid, int? separation)
        {
            return Client.Cypher
                .Match("(a1:" + Label.Entity.Artefact + " { Guid: {artefactGuid} })-[:" + Label.Relationship.HasRelation + "]->(:" + Label.Entity.Relation + ")-[]->(other1:" + Label.Entity.Artefact + ")")
                .WithParams(new
                {
                    artefactGuid
                })
                .With("other1, a1")
                .Match("p=(a2:" + Label.Entity.Artefact + ")-[:" + Label.Relationship.HasRelation + "]->(:" + Label.Entity.Relation + ")-[r]->(other2:" + Label.Entity.Artefact + ")")
                //.Where("other2.Guid = other1.Guid and a2.Guid <> a1.Guid")
                .With("a2, a1, collect(type(r)) as rels, count(distinct(other2)) as Total, other2, length(p) as PathCost")
                .Return((ICypherResultItem a1, ICypherResultItem a2, ICypherResultItem other2) => new ArtefactsRelatedToSimilarArtefacts
                {
                    Artefact1 = a1.As<Artefact>(),
                    Artefact2 = a2.As<Artefact>(),
                    Artefacts = Return.As<IEnumerable<Artefact>>("collect(distinct(other2))"),
                    RelationTypes = Return.As<IEnumerable<string>>("rels"),
                    PathCost = Return.As<long>("PathCost")
                })
                .OrderBy("Total desc, a1.Name", "a2.Name")
                .Results
                .ToList();
        }
        public List<Artefact> All()
        {
            var people = Client.Cypher
                .Match("(thing:" + Label.Entity.Artefact + ")")
                .Return(thing => thing.As<Artefact>())
                .OrderBy("thing.Name")
                .Results.ToList();
            return people;
        }
        public void OwnedBy(Artefact artwork, Agent organisation)
        {
            Client.Cypher
                .Match("(a:" + Label.Entity.Artefact + ")", "(o:" + Label.Entity.Organisation + ")")
                .Where((Artefact a) => a.Guid == artwork.Guid)
                .AndWhere((Agent o) => o.Guid == organisation.Guid)
                .CreateUnique("a-[:" + Label.OwnedBy + "]->o")
                .ExecuteWithoutResults();
        }
        public void MadeOf(Artefact artwork, params Category[] categories)
        {
            foreach (var category in categories)
            {
                Client.Cypher
                    .Match("(a:" + Label.Entity.Artefact + ")",
                           "(g:" + Label.Entity.Category + ")")
                    .Where((Artefact a) => a.Guid == artwork.Guid)
                    .AndWhere((Category g) => g.Guid == category.Guid)
                    .CreateUnique("a-[:" + Label.MadeOf + "]->g")
                    .ExecuteWithoutResults();
            }
        }
        public Artefact FindArtefactByGuid(string guid)
        {
            var result = Client.Cypher
                .Match("(artwork:" + Label.Entity.Artefact + ")")
                .Where((Artefact artwork) => artwork.Guid == guid)
                .Return(artwork => artwork.As<Artefact>())
                .Results.FirstOrDefault();
            return result;
        }

        public List<object> FindOutgoingRelationships(string artefactGuid)
        {
            var attributes = Client.Cypher
                .Match("(artefact:" + Label.Entity.Artefact + " { Guid: {artefactGuid} })-[hasRelation:" + Label.Relationship.HasRelation + "]->(:" + Label.Entity.Relation + ")-[relationship]->(other)")
                .WithParams(new
                {
                    artefactGuid
                })
                .Return((ICypherResultItem artefact, ICypherResultItem other, ICypherResultItem relationship) =>
                    new ArtefactAttributeDynamic
                    {
                        Artefact = artefact.As<Artefact>(),
                        EntityType = Return.As<string>("head(labels(other))"),
                        EntityNode = other.As<Node<string>>(),
                        RelationshipType = relationship.Type(),
                        RelationshipNode = relationship.As<Node<string>>(),
                        HasRelationGuid = Return.As<string>("hasRelation.Guid")
                    })
                    .Results
                    .ToList();
            var factorised = Factorise(attributes);
            return factorised;
        }
        public List<object> FindIncomingRelationships(string artefactGuid)
        {
            var attributes = Client.Cypher
                .Match("(artefact:" + Label.Entity.Artefact + " { Guid: {artefactGuid} })<-[relationship]-(hasRelation:" + Label.Entity.Relation + ")<-[:" + Label.Relationship.HasRelation + "]-(other)")
                .WithParams(new
                {
                    artefactGuid
                })
                .Return((ICypherResultItem artefact, ICypherResultItem other, ICypherResultItem relationship) =>
                    new ArtefactAttributeDynamic
                    {
                        Artefact = artefact.As<Artefact>(),
                        EntityType = Return.As<string>("head(labels(other))"),
                        EntityNode = other.As<Node<string>>(),
                        RelationshipType = relationship.Type(),
                        RelationshipNode = relationship.As<Node<string>>(),
                        HasRelationGuid = Return.As<string>("hasRelation.Guid")
                    })
                    .Results
                    .ToList();
            var factorised = Factorise(attributes);
            return factorised;
        }
        public List<object> Factorise(List<ArtefactAttributeDynamic> list)
        {
            var results = new List<object>();
            foreach (var item in list)
            {
                item.EntityDynamic = JsonConvert.DeserializeObject<dynamic>(item.EntityNode.Data);
                item.RelationshipDynamic = JsonConvert.DeserializeObject<dynamic>(item.RelationshipNode.Data);
                if (item.EntityType == Label.Entity.Place)
                {
                    var attribute = new ArtefactAttributePlaceHyperNode
                    {
                        Artefact = item.Artefact,
                        Place = ToPlace(item.EntityDynamic),
                        Relationship = ToRelationship(item.RelationshipType, item.RelationshipDynamic),
                        HasRelationGuid = item.HasRelationGuid
                    };
                    results.Add(attribute);
                }
                if (item.EntityType == Label.Entity.Category)
                {
                    var attribute = new ArtefactAttributeCategoryHyperNode
                    {
                        Artefact = item.Artefact,
                        Category = ToCategory(item.EntityDynamic),
                        Relationship = ToRelationship(item.RelationshipType, item.RelationshipDynamic),
                        HasRelationGuid = item.HasRelationGuid
                    };
                    results.Add(attribute);
                }
                if (item.EntityType == Label.Entity.Agent)
                {
                    var attribute = new ArtefactAttributeAgentHyperNode
                    {
                        Artefact = item.Artefact,
                        Agent = ToAgent(item.EntityDynamic),
                        Relationship = ToRelationship(item.RelationshipType, item.RelationshipDynamic),
                        HasRelationGuid = item.HasRelationGuid
                    };
                    results.Add(attribute);
                }
                if (item.EntityType == Label.Entity.Artefact)
                {
                    var attribute = new ArtefactAttributeArtefactHyperNode
                    {
                        Artefact = item.Artefact,
                        OtherArtefact = ToArtefact(item.EntityDynamic),
                        Relationship = ToRelationship(item.RelationshipType, item.RelationshipDynamic),
                        HasRelationGuid = item.HasRelationGuid
                    };
                    results.Add(attribute);
                }
                if (item.EntityType == Label.Entity.Event)
                {
                    var attribute = new ArtefactAttributeEventHyperNode
                    {
                        Artefact = item.Artefact,
                        Event = ToEvent(item.EntityDynamic),
                        Relationship = ToRelationship(item.RelationshipType, item.RelationshipDynamic),
                        HasRelationGuid = item.HasRelationGuid
                    };
                    results.Add(attribute);
                }
            }
            return results;
        }
        public Data.Entities.Relationship ToRelationship(string relationshipType, dynamic node)
        {
            return new Data.Entities.Relationship
            {
                Guid = node.Guid,
                Type = relationshipType,
                IsActive = node.IsActive ?? false,
                Rank = node.Rank ?? 0
            };
        }
        public Event ToEvent(dynamic node)
        {
            return new Event
            {
                Milennium = node.Milennium,
                Proximity = node.Proximity,
                Year = node.Year,
                Month = node.Month,
                Day = node.Day,
                Hour = node.Hour,
                Minute = node.Minute,
                MilenniumEnd = node.MilenniumEnd,
                ProximityEnd = node.ProximityEnd,
                YearEnd = node.YearEnd,
                MonthEnd = node.MonthEnd,
                DayEnd = node.DayEnd,
                HourEnd = node.HourEnd,
                MinuteEnd = node.MinuteEnd
            };
        }
        public Category ToCategory(dynamic node)
        {
            return new Category
            {
                Guid = node.Guid,
                Name = node.Name,
                Article = node.Article,
                Aliases = node.Aliases,
                Description = node.Description,
                Plural = node.Plural,
                Subtitle = node.Tagline
            };
        }
        public Artefact ToArtefact(dynamic node)
        {
            return new Artefact
            {
                Guid = node.Guid,
                Name = node.Name,
                Article = node.Article,
                Aliases = ToStringArray(node.Aliases),
                Description = node.Description,
                Plural = node.Plural,
                Subtitle = node.Subtitle
            };
        }
        public Agent ToAgent(dynamic node)
        {
            return new Data.Agent
            {
                Guid = node.Guid,
                Name = node.Name,
                Article = node.Article,
                Aliases = ToStringArray(node.Aliases),
                FirstName = node.FirstName,
                LastName = node.LastName,
                Description = node.Description,
                Plural = node.Plural,
                Subtitle = node.Subtitle
            };
        }
        private string[] ToStringArray(dynamic value)
        {
            return value != null ? value.ToObject<string[]>() : new string[] { };
        }
        public Place ToPlace(dynamic node)
        {
            return new Place
            {
                Guid = node.Guid,
                Name = node.Name,
                Article = node.Article,
                Aliases = node.Aliases,
                Description = node.Description,
                Plural = node.Plural,
                Subtitle = node.Subtitle,
                Latitude = node.Latitude,
                Longitude = node.Longitude
            };
        }
    }
}
