﻿using Data.Entities;
using log4net;
using Neo4jClient;
using Neo4jClient.Cypher;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public interface IRelationService : INeo4jClientService
    {
        int DeleteAttribute(string relationshipGuid);
        Relation CreateRelation();
        void AssertedBy(Relation relation, User _user);
        Relation HasRelation(IEntity entity, Relation attribute);
        void Log(User _user, string targetGuid, string targetLabel, string action);
        void CreateRelationship(IEntity entity, string otherGuid, string otherType, string relationship);
        void SaveOrUpdate(Relation relation);
        void CreateRelationship(string sourceType, string sourceGuid, string destType, string destGuid, string relationshipType, string relationshipGuid = null);
        int DeleteRelationship(string relationType, string relationshipGuid);
    }
    public class RelationService : IRelationService
    {
        private readonly ILog Logger = LogManager.GetLogger("DefaultLogger");
        public User CurrentUser { get; set; }
        private readonly GraphClient Client;
        public RelationService(GraphClient client)
        {
            Client = client;
        }
        public int DeleteRelationship(string relationType, string relationshipGuid)
        {
            var deleted = Client.Cypher
                    .Match("(x)-[r:" + relationType + " { Guid: {relationshipGuid} }]->(y)")
                    .WithParams(new
                    {
                        relationshipGuid
                    })
                    .Delete("r")
                    .Return(r => Return.As<int>("count(r)"))
                    .Results
                    .FirstOrDefault();
            return deleted;
        }
        public void SaveOrUpdate(Relation relation)
        {
            if (relation.Guid == null)
            {
                relation.Guid = Guid.NewGuid().ToString();
            }
            Client.Cypher
                .Merge("(relation:" + Label.Entity.Relation + " { Guid: {guid} })")
                .OnCreate().Set("relation = {relation}")
                .OnMatch().Set("relation = {relation}")
                .WithParams(new
                {
                    guid = relation.Guid,
                    relation
                })
                .ExecuteWithoutResults();
        }
        public void CreateRelationship(string sourceType, string sourceGuid, string destType, string destGuid, string relationshipType, string relationshipGuid = null)
        {
            if (relationshipGuid == null)
            {
                relationshipGuid = Guid.NewGuid().ToString();
            }
            var actualDestType = destType == typeof(Entity).Name ? "" : ":" + destType;
            Client.Cypher
                .Match(
                    "(x:" + sourceType + " { Guid: {sourceGuid} })",
                    "(y" + actualDestType + " { Guid: {destGuid} })")
                .WithParams(new
                {
                    sourceGuid,
                    destGuid
                })
                .CreateUnique("x-[:" + relationshipType + " { Guid: {relationshipGuid} }]->y")
                .WithParams(new
                {
                    relationshipGuid
                })
                .ExecuteWithoutResults();
        }
        public void SetCurrentUser(User user)
        {
            CurrentUser = user;
        }
        public void CreateRelationship(IEntity entity, string otherGuid, string otherType, string relationship)
        {
            var relation = HasRelation(entity, CreateRelation());
            Client.Cypher
                .Match(
                    "(relation:" + Label.Entity.Relation + " { Guid: {relationGuid} })",
                    "(other:" + otherType + " { Guid: {otherGuid} })")
                .WithParams(new
                {
                    relationGuid = relation.Guid,
                    otherGuid
                })
                .CreateUnique("relation-[:" + relationship + " { Guid: {guid} }]->other")
                .WithParams(new
                {
                    guid = Guid.NewGuid().ToString()
                })
                .ExecuteWithoutResults();
        }
        public void Log(User _user, string targetGuid, string targetLabel, string action)
        {
            Client.Cypher
                .Match("(target:" + targetLabel + ")",
                       "(user:" + Label.Entity.User + ")")
                .Where((Entity target) => target.Guid == targetGuid)
                .AndWhere((User user) => user.Guid == _user.Guid)
                .Create("user-[:" + Label.Relationship.AuditFrom + " { Action: {action}, Date: {date} }]->target")
                .WithParams(new
                {
                    action = action,
                    date = DateTime.Now.ToUniversalTime().ToString()
                })
                .ExecuteWithoutResults();
        }
        public Relation CreateRelation()
        {
            /**
             * (:Attribute)-[:ASSERTED_BY]->(CurrentUser)
             */
            var attribute = new Relation { Guid = Guid.NewGuid().ToString() };
            Client.Cypher
                .Merge("(attribute:" + Label.Entity.Relation + " { Guid: {guid} })")
                .OnCreate().Set("attribute = {attribute}")
                .WithParams(new
                {
                    guid = attribute.Guid,
                    attribute = attribute
                })
                .ExecuteWithoutResults();
            if (CurrentUser != null)
            {
                AssertedBy(attribute, CurrentUser);
            }
            return attribute;
        }
        public void AssertedBy(Relation relation, User _user)
        {
            Client.Cypher
            .Match("(attribute:" + Label.Entity.Relation + " { Guid: {attributeGuid} })",
                   "(user:" + Label.Entity.User + " { Guid: {userGuid} })")
            .WithParams(new
            {
                attributeGuid = relation.Guid,
                userGuid = _user.Guid
            })
            .CreateUnique("attribute-[:" + Label.Relationship.AssertedBy + " { Guid: {guid}, IsActive: {isActive} }]->user")
            .WithParams(new
            {
                guid = Guid.NewGuid().ToString(),
                isActive = true
            })
            .ExecuteWithoutResults();
        }
        public Relation HasRelation(IEntity entity, Relation attribute)
        {
            /**
             * (entity)-[:HAS_ATTR]->(:Attribute)
             */
            Client.Cypher
                    .Match("(ent)", "(attr:" + Label.Entity.Relation + ")")
                    .Where((IEntity ent) => ent.Guid == entity.Guid)
                    .AndWhere((Relation attr) => attr.Guid == attribute.Guid)
                    .CreateUnique("ent-[:" + Label.Relationship.HasRelation + " { Guid: {guid} }]->attr")
                    .WithParams(new
                    {
                        guid = Guid.NewGuid().ToString()
                    })
                    .ExecuteWithoutResults();
            return attribute;
        }
        public int DeleteAttribute(string relationshipGuid)
        {
            try
            {
                var deleted = Client.Cypher
                    .Match("()-[r { Guid: {relationshipGuid} }]->()")
                    .WithParams(new
                    {
                        relationshipGuid = relationshipGuid
                    })
                    .Delete("r")
                    .Return(r => Return.As<int>("count(r)"))
                    .Results
                    .FirstOrDefault();
                return deleted;
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return 0;
            }
        }
    }
}
