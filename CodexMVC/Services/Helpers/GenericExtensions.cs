﻿using Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Helpers
{
    public static class CategoryAttributeHyperNodeExtensions
    {

        public static string EditAction(this CategoryAttributeHyperNode node)
        {
            switch (node.Relationship)
            {
                case Label.Relationship.KindOf: return "KindOf";
                case Label.Relationship.OppositeOf: return "OppositeOf";
                case Label.Relationship.SimilarTo: return "SimilarTo";
                default: return "";
            }
        }
    }
    public static class GenericExtensions
    {
        public static U Dot<T, U>(this T value, Func<T, U> getter, U defaultValue = default(U)) where T : class
        {
            return value == null ? defaultValue : getter(value);
        }
        public static bool IsNull<T>(this T value) where T : class
        {
            return value == null;
        }
    }
}
