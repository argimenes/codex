﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Helpers
{
    public static class CacheKey
    {
        public static class CategoryService
        {
            public static string AllWithAncestors = "CategoryService.AllWithAncestors";
            public static string AllSubcategoriesOfFamilialRelation = "CategoryService.AllSubcategoriesOf{ancestorName:FamilialRelation}";
            public static string AllSubcategoriesOfArtwork = "CategoryService.AllSubcategoriesOf{ancestorName:Artwork}";
            public static string AllSubcategoriesOfArtMovement = "CategoryService.AllSubcategoriesOf{ancestorName:ArtMovement}";
            public static string AllSubcategoriesOfArtGenre = "CategoryService.AllSubcategoriesOf{ancestorName:ArtGenres}";
            public static string AllSubcategoriesOfArtisticSubject = "CategoryService.AllSubcategoriesOf{ancestorName:ArtisticSubject}";
            public static string AllSubcategoriesOfOntologicalState = "CategoryService.AllSubcategoriesOf{ancestorName:OntologicalState}";
            public static string Disciplines = "CategoryService.Disciplines";
        }
        public static class AgentService
        {
            public static string AllWithAttributes = "AgentService.AllWithAttributes";
        }
        public static class ActService
        {
            public static string AllDocumentHyperNodes = "ActService.AllDocumentHyperNodes";
        }
        public static class PlaceService
        {
            public static string All = "PlaceService.All";
            public static string AllWithContainers = "PlaceService.AllWithContainers";
        }
        public static class ArtefactService
        {
            public static string AllMadeBy = "ArtefactService.AllMadeBy";
            public static string AllSources = "ArtefactService.AllSources";
        }
    }
}
