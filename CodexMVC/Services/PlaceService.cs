﻿using Data;
using Data.Entities;
using Neo4jClient;
using Neo4jClient.Cypher;
using Newtonsoft.Json;
using Services.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class YearActs
    {
        public string Milennium { get; set; }
        public int Year { get; set; }
        public int TotalActs { get; set; }
    }
    public class PlaceWithParents
    {
        public Place Place { get; set; }
        public int ArtworkTotal { get; set; }
        public IEnumerable<Category> PlaceTypes { get; set; }
        public IEnumerable<Place> Parents { get; set; }
    }
    public interface IPlaceService : INeo4jClientService
    {
        IEnumerable<ContextHyperNode> FindContextsByPlace(string placeGuid);
        Place SaveOrUpdate(Place place);
        Place FindPlaceByGuid(string guid);
        List<Place> All();
        List<Place> FindPlaces(string name);
        void IsA(Place entity, params Category[] categories);
        bool Delete(string placeGuid);
        void InsideOf(Place place, Place container);
        List<PlaceResult> AllWithContainers(int maxDepth = 2);
        Task<IEnumerable<AgentWithAgentTypes>> ArtistsWithArtefactsAtPlaceAsync(string placeGuid);

        Task<Category> FindPlaceTypeAsync(string guid);

        Task<IEnumerable<PlaceCategoryTuple>> FindContainersOfAsync(string guid);
        Task<IEnumerable<CollectionWithArtefacts>> CollectionsWithArtefactsAsync(string guid);
        IEnumerable<SubjectWithArefacts> FindSubjectsByPlace(string guid);
        IEnumerable<SubjectWithArefacts> FindMovementsByPlace(string guid);
        IEnumerable<SubjectWithArefacts> FindArtefactTypesByPlace(string guid);
        Task<IEnumerable<PlaceWithParents>> AllPlacesWithArtworks();
        Task<IEnumerable<ExhibitionDetails>> FindExhibitionsAtAsync(string guid);
        Task<IEnumerable<TourDetails>> FindToursAtAsync(string guid);
        bool PlaceExists(string guid);
    }
    public class SubjectWithArefacts
    {
        public Category Subject { get; set; }
        public IEnumerable<ArtistArtefactImageTuple> ArtefactsWithImages { get; set; }
    }
    public class CollectionWithArtefacts
    {
        public Artefact Collection { get; set; }
        public IEnumerable<ArtefactImageTuple> ArtefactsWithImages { get; set; }
        public Place Location { get; set; }
        public bool Exterior { get; set; }
    }
    public class ArtefactMarker
    {
        public Agent Artist { get; set; }
        public Artefact Artwork { get; set; }
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }
        public string Content { get; set; }
        public string ContainerName { get; set; }
    }
    public class ContainerWithArtefacts
    {
        public Artefact Container { get; set; }
        public int Depth { get; set; }
        public Place Location { get; set; }
        public Artefact TopContainer { get; set; }
        public IEnumerable<ArtistArtefactImageTuple> Artworks { get; set; }
    }
    public class AgentWithAgentTypes
    {
        public Agent Agent { get; set; }
        public IEnumerable<ArtistArtefactImageTuple> ArtefactWithImages { get; set; }
        public Category RepresentativeOf { get; set; }
        public int ArtefactTotal { get; set; }
        public IEnumerable<Category> AgentTypes { get; set; }
    }
    //
    public class ContainerWithArtefactsComparer : EqualityComparer<ContainerWithArtefacts>
    {
        public override bool Equals(ContainerWithArtefacts x, ContainerWithArtefacts y)
        {
            return x.Container.Guid == y.Container.Guid;
        }

        public override int GetHashCode(ContainerWithArtefacts obj)
        {
            return base.GetHashCode();

        }
    }
    public class AgentWithAgentTypesComparer : EqualityComparer<AgentWithAgentTypes>
    {
        public override bool Equals(AgentWithAgentTypes x, AgentWithAgentTypes y)
        {
            return x.Agent.Guid == y.Agent.Guid;
        }

        public override int GetHashCode(AgentWithAgentTypes obj)
        {
            return base.GetHashCode();

        }
    }
    public class ImageComparer : EqualityComparer<Image>
    {
        public override bool Equals(Image x, Image y)
        {
            return x.Guid == y.Guid;
        }

        public override int GetHashCode(Image obj)
        {
            return base.GetHashCode();

        }
    }
    public class EntityImageComparer : EqualityComparer<EntityImage>
    {
        public override bool Equals(EntityImage x, EntityImage y)
        {
            return x.Entity.Guid == y.Entity.Guid;
        }

        public override int GetHashCode(EntityImage obj)
        {
            return base.GetHashCode();

        }
    }
    public class RelationCategoryTupleComparer : EqualityComparer<RelationCategoryTuple>
    {
        public override bool Equals(RelationCategoryTuple x, RelationCategoryTuple y)
        {
            return x.Category.Guid == y.Category.Guid;
        }

        public override int GetHashCode(RelationCategoryTuple obj)
        {
            return base.GetHashCode();

        }
    }
    public class ArtefactMarkerComparer : EqualityComparer<ArtefactMarker>
    {
        public override bool Equals(ArtefactMarker x, ArtefactMarker y)
        {
            return x.Artwork.Guid == y.Artwork.Guid;
        }

        public override int GetHashCode(ArtefactMarker obj)
        {
            return base.GetHashCode();

        }
    }
    //
    public class ArtefactAgentImageTupleComparer : EqualityComparer<ArtefactAgentImageTuple>
    {
        public override bool Equals(ArtefactAgentImageTuple x, ArtefactAgentImageTuple y)
        {
            return x.Artefact.Guid == y.Artefact.Guid;
        }

        public override int GetHashCode(ArtefactAgentImageTuple obj)
        {
            return base.GetHashCode();

        }
    }
    public class AgentComparer : EqualityComparer<Agent>
    {
        public override bool Equals(Agent x, Agent y)
        {
            return x.Guid == y.Guid;
        }

        public override int GetHashCode(Agent obj)
        {
            return base.GetHashCode();

        }
    }
    public class RelationAgentTupleComparer : EqualityComparer<RelationAgentTuple>
    {
        public override bool Equals(RelationAgentTuple x, RelationAgentTuple y)
        {
            return x.Agent.Guid == y.Agent.Guid;
        }

        public override int GetHashCode(RelationAgentTuple obj)
        {
            return base.GetHashCode();

        }
    }
    public class ArtefactImageTupleComparer : EqualityComparer<ArtefactImageTuple>
    {
        public override bool Equals(ArtefactImageTuple x, ArtefactImageTuple y)
        {
            return x.Artefact.Guid == y.Artefact.Guid;
        }

        public override int GetHashCode(ArtefactImageTuple obj)
        {
            return base.GetHashCode();

        }
    }
    //public class EntityImageComparer : EqualityComparer<EntityImage>
    //{
    //    public override bool Equals(EntityImage x, EntityImage y)
    //    {
    //        return x.Entity.Guid == y.Entity.Guid;
    //    }

    //    public override int GetHashCode(EntityImage obj)
    //    {
    //        return base.GetHashCode();

    //    }
    //}
    public class IEntityComparer : EqualityComparer<IEntity>
    {
        public override bool Equals(IEntity x, IEntity y)
        {
            return x.Guid == y.Guid;
        }

        public override int GetHashCode(IEntity obj)
        {
            return base.GetHashCode();

        }
    }
    public class PlaceCategoryTupleComparer : EqualityComparer<PlaceCategoryTuple>
    {
        public override bool Equals(PlaceCategoryTuple x, PlaceCategoryTuple y)
        {
            return x.Place.Guid == y.Place.Guid;
        }

        public override int GetHashCode(PlaceCategoryTuple obj)
        {
            return base.GetHashCode();

        }
    }
    public class ArtistArtefactImageTupleComparer : EqualityComparer<ArtistArtefactImageTuple>
    {
        public override bool Equals(ArtistArtefactImageTuple x, ArtistArtefactImageTuple y)
        {
            return x.Artefact.Guid == y.Artefact.Guid;
        }

        public override int GetHashCode(ArtistArtefactImageTuple obj)
        {
            return base.GetHashCode();

        }
    }
    //
    public class ArtistWithArtefactImage
    {
        public Agent Artist { get; set; }
        public IEnumerable<ArtefactImageTuple> ArtefactWithImages { get; set; }
    }
    public class ArtefactImageTuple
    {
        public Artefact Artefact { get; set; }
        public Image Image { get; set; }
    }
    public class ArtistArtefactImageTuple : ArtefactImageTuple
    {
        public Artefact TopContainer { get; set; }
        public Artefact Container { get; set; }
        public Place Location { get; set; }
        public Agent Artist { get; set; }
        public Category ArtefactType { get; set; }
        public Category Subject { get; set; }
        public Category Motif { get; set; }
        public Category Genre { get; set; }
        public Category ArtistRepresentativeOf { get; set; }
        public string ToEntityJsonDisplay()
        {
            var artefactType = ArtefactType != null ? ArtefactType.Name.ToLower() : "";
            var artistName = Artist != null ? " by " + Artist.FullName2 : "";
            return "<b>" + Artefact.ToDisplay() + "</b> <small>" + artefactType + artistName;
        }
    }
    public class ArtistWithRepresentativeArtwork
    {
        public string Relationship { get; set; }
        public PersonalDetails PersonalDetails { get; set; }
        public IEnumerable<ArtefactImageTuple> Artworks { get; set; }
    }
    public class MotifWithArtists
    {
        public Category Motif { get; set; }
        public IEnumerable<Agent> Artists { get; set; }
    }
    public class PlaceService : IPlaceService
    {
        public User CurrentUser { get; set; }
        private readonly GraphClient Client;
        private readonly ICommonService commonService;
        public PlaceService(
            GraphClient client,
            ICommonService _commonService
            )
        {
            Client = client;
            commonService = _commonService;
        }

        public bool PlaceExists(string placeGuid)
        {

            var query = Client.Cypher
                .Match("(place:Place { Guid: {placeGuid} })").WithParams(new { placeGuid })
                ;

            var result = query
                .Return(() => Return.As<Place>("place"))
                .Results
                .FirstOrDefault()
                ;

            return result != null;
        }

        public async Task<IEnumerable<TourDetails>> FindToursAtAsync(string placeGuid)
        {

            var query = Client.Cypher
               .Match("(place:Place { Guid: {placeGuid} })").WithParams(new { placeGuid })
               .Match("(location:Place)-[:INSIDE_OF*0..]->(place)")
               .Match("(tour)-[:STOPS_AT]->(location)-[:INSIDE_OF*1..]->(inside:Place)")
               .Match("(tour)-[:HAS_DETAIL]->(detail:Detail)")
               .OptionalMatch("(tour:Artefact)-[:HAS_RELATION]->()-[:" + Label.Relationship.Covers + "]->(artist)")
               .OptionalMatch("(tour)-[:STOPS_IN]->(collection:Artefact)")
               .OptionalMatch("(tour)-[:HAS_RELATION]->()-[:" + Label.Relationship.TourFeatures + "]->(artefact:Artefact)")
               .OptionalMatch("(artefact)-[:HAS_IMAGE]->(image:Image { Primary: true })")
               .With("{ Artist: artist, ArtefactWithImages: collect(distinct { Artefact: artefact, Image: image }) } as Artworks, tour, detail, collection, location, inside")
               ;

            var results = await query
                .Return(() => new TourDetails
                {
                    Tour = Return.As<Artefact>("tour"),
                    Detail = Return.As<TourDetail>("detail"),
                    Location = Return.As<Place>("location"),
                    StopsIn = Return.As<IEnumerable<Artefact>>("collect(distinct(collection))"),
                    Inside = Return.As<IEnumerable<Place>>("collect(distinct(inside))"),
                    Artworks = Return.As<IEnumerable<ArtistWithArtefactImage>>("collect(distinct(Artworks))")
                })
                .ResultsAsync
                ;

            return results;

        }

        public async Task<IEnumerable<ExhibitionDetails>> FindExhibitionsAtAsync(string placeGuid)
        {

            var query = Client.Cypher
                .Match("(place:Place { Guid: {placeGuid} })").WithParams(new { placeGuid })
                .Match("(places:Place)-[:INSIDE_OF*0..]->(place)")
                .Match("(collection:Artefact)-[:LOCATED_AT]->(places)")
                .Match("(exhibition:Artefact)-[:TAKES_PLACE_IN]->(collection)")
                .Match("(places)-[:INSIDE_OF*1..]->(inside:Place)")
                .OptionalMatch("(exhibition)-[:HAS_DETAIL]->(detail:EventDetail)")
                .OptionalMatch("(artefact:Artefact)-[:HAS_RELATION]->()-[:EXHIBITED_IN]->(exhibition)")
                .OptionalMatch("(artefact)-[:HAS_RELATION]->()-[:MADE_BY]->(artist:Agent)")
                .OptionalMatch("(artefact)-[:HAS_IMAGE]->(image:Image { Primary: true })")
                .With("{ Artist: artist, ArtefactWithImages: collect(distinct { Artefact: artefact, Image: image }) } as Artworks, exhibition, detail, collection, places, inside")
                ;

            var results = await query
                .Return(() => new ExhibitionDetails
                {
                    Exhibition = Return.As<Artefact>("exhibition"),
                    Detail = Return.As<EventDetail>("detail"),
                    Collection = Return.As<Artefact>("collection"),
                    Location = Return.As<Place>("places"),
                    Inside = Return.As<IEnumerable<Place>>("collect(distinct(inside))"),
                    Artworks = Return.As<IEnumerable<ArtistWithArtefactImage>>("collect(distinct(Artworks))")
                })
                .ResultsAsync
                ;

            return results;

        }

        public async Task<IEnumerable<PlaceWithParents>> AllPlacesWithArtworks()
        {

            var query = Client.Cypher
                .Match("(country:Category { Name: 'Country' })")
                .Match("(countries:Place)-[:IS_A]->(country)")
                .Match("p=(place:Place)-[:INSIDE_OF*1..]->(countries)")
                .Match("(container:Artefact)-[:LOCATED_AT]->(place)")
                .Match("art=(artefact:Artefact)-[:LOCATED_IN*0..]->(container)")
                .Match("(artefact)-[:HAS_IMAGE]->(image:Image)")
                .OptionalMatch("(place)-[:IS_A]->(placeType:Category)")
                .With("extract(n IN nodes(p) | n) as Parents, count(artefact) as ArtworkTotal, place, placeType")
                ;

            var results = await query
                .Return(() => new PlaceWithParents {
                    Place = Return.As<Place>("distinct(place)"),
                    ArtworkTotal = Return.As<int>("ArtworkTotal"),
                    PlaceTypes = Return.As<IEnumerable<Category>>("collect(distinct(placeType))"),
                    Parents = Return.As<IEnumerable<Place>>("Parents")
                })
                .OrderBy("Place.Name")
                .ResultsAsync
                ;

            return results;

        }

        public IEnumerable<SubjectWithArefacts> FindArtefactTypesByPlace(string placeGuid)
        {
            var query = Client.Cypher
              .Match("(place:Place { Guid: {placeGuid} })").WithParams(new { placeGuid })
              .Match("(locations:Place)-[:INSIDE_OF*0..]->(place)")
              .Match("(artefact)-[:LOCATED_IN*0..]->()-[:LOCATED_AT]->(locations)")
              .Match("(artefact)-[:HAS_RELATION]->()-[:MADE_BY]->(artist:Agent)")
              .Match("(artefact)-[:HAS_RELATION]->()-[:IS_A]->(artefactType:Category)")
              .Match("(artefact)-[:HAS_IMAGE]->(image:Image { Primary: true })")
              ;
            var results = query.Return(() => new SubjectWithArefacts
            {
                Subject = Return.As<Category>("artefactType"),
                ArtefactsWithImages = Return.As<IEnumerable<ArtistArtefactImageTuple>>("collect(distinct({ Artist: artist, Artefact: artefact, Image: image }))")
            })
                .Results
                .ToList();
            return results;
        }
        public IEnumerable<SubjectWithArefacts> FindMovementsByPlace(string placeGuid)
        {
            var query = Client.Cypher
               .Match("(place:Place { Guid: {placeGuid} })").WithParams(new { placeGuid })
               .Match("(locations:Place)-[:INSIDE_OF*0..]->(place)")
               .Match("(artefact)-[:LOCATED_IN*0..]->()-[:LOCATED_AT]->(locations)")
               .Match("(artefact)-[:HAS_RELATION]->()-[:MADE_BY]->(artist:Agent)")
               .Match("(artist)-[:HAS_RELATION]->()-[:REPRESENTATIVE_OF]->(movement:Category)")
               .Match("(artefact)-[:HAS_IMAGE]->(image:Image { Primary: true })")
               ;
            var results = query.Return(() => new SubjectWithArefacts
            {
                Subject = Return.As<Category>("movement"),
                ArtefactsWithImages = Return.As<IEnumerable<ArtistArtefactImageTuple>>("collect(distinct({ Artist: artist, Artefact: artefact, Image: image }))")
            })
                .Results
                .ToList();
            return results;
        }
        public IEnumerable<SubjectWithArefacts> FindSubjectsByPlace(string placeGuid)
        {
            var query = Client.Cypher
               .Match("(place:Place { Guid: {placeGuid} })").WithParams(new { placeGuid })
               .Match("(locations:Place)-[:INSIDE_OF*0..]->(place)")
               .Match("(artefact)-[:LOCATED_IN*0..]->()-[:LOCATED_AT]->(locations)")
               .Match("(artefact)-[:HAS_RELATION]->()-[:MADE_BY]->(artist:Agent)")
               .Match("(artefact)-[:HAS_RELATION]->()-[:HAS_SUBJECT]->(subject:Category)")
               .Match("(artefact)-[:HAS_IMAGE]->(image:Image { Primary: true })")
               ;
            var results = query.Return(() => new SubjectWithArefacts
                {
                    Subject = Return.As<Category>("subject"),
                    ArtefactsWithImages = Return.As<IEnumerable<ArtistArtefactImageTuple>>("collect(distinct({ Artist: artist, Artefact: artefact, Image: image }))")
                })
                .Results
                .ToList();
            return results;
        }

        public async Task<IEnumerable<CollectionWithArtefacts>> CollectionsWithArtefactsAsync(string placeGuid)
        {
            var query = Client.Cypher
                .Match("(place:Place { Guid: {placeGuid} })").WithParams(new { placeGuid })
                .Match("(locations:Place)-[:INSIDE_OF*0..]->(place)")
                .Match("(collection:Artefact)-[:LOCATED_AT]->(locations)")
                .Match("(artefact)-[:LOCATED_IN*0..]->(collection)")
                .Match("(artefact)-[:HAS_IMAGE]->(image:Image { Primary: true })")
                .Match("(exterior:Category { Name: 'Exterior' })")
                .OptionalMatch("ext=(collection)-[]->()-[:HAS_PHYSICAL_PROPERTY_OF]->(exterior)")
                ;
            var results = await query
                .Return(() => new CollectionWithArtefacts
                {
                    Collection = Return.As<Artefact>("collection"),
                    Location = Return.As<Place>("locations"),
                    ArtefactsWithImages = Return.As<IEnumerable<ArtefactImageTuple>>("collect(distinct({ Artefact: artefact, Image: image }))"),
                    Exterior = Return.As<bool>("length(ext) > 0")
                })
                .ResultsAsync;

            return results;
        }

        public async Task<IEnumerable<PlaceCategoryTuple>> FindContainersOfAsync(string placeGuid)
        {
            var query = Client.Cypher
                .Match("(place:Place { Guid: {placeGuid} })").WithParams(new { placeGuid })
                .Match("(place)-[:INSIDE_OF*0..]->(container:Place)")
                .OptionalMatch("(container)-[:IS_A]->(containerType:Category)")
                ;

            var results = await query
                .Return(() => new PlaceCategoryTuple
                {
                    Place = Return.As<Place>("container"),
                    Category = Return.As<Category>("containerType")
                })
                .ResultsAsync
                ;

            return results;
        }

        public async Task<Category> FindPlaceTypeAsync(string placeGuid)
        {
            var query = Client.Cypher
                .Match("(place:Place { Guid: {placeGuid} })").WithParams(new { placeGuid })
                .Match("(place)-[:IS_A]->(placeType:Category)")
                ;

            var results = await query.Return(() => Return.As<Category>("placeType"))
                .ResultsAsync
                ;

            return results.FirstOrDefault();
        }

        public async Task<IEnumerable<AgentWithAgentTypes>> ArtistsWithArtefactsAtPlaceAsync(string placeGuid)
        {

            var query = Client.Cypher
                .Match("(place:Place { Guid: {placeGuid} })").WithParams(new { placeGuid })
                .Match("(places:Place)-[:INSIDE_OF*0..]->(place)")
                .Match("(container:Artefact)-[:LOCATED_AT]->(places)")
                .Match("(artefact:Artefact)-[:LOCATED_IN*0..]->(container)")
                .Match("(artefact)-[:HAS_RELATION]->()-[:MADE_BY]->(agent:Agent)")
                .Match("(agent)-[:HAS_RELATION]->()-[:IS_A]->(agentType:Category)")
                .Match("(artefact)-[:HAS_IMAGE]->(image:Image { Primary: true })")
                .Match("(artefact)-[:HAS_RELATION]->()-[:IS_A]->(artefactType:Category)")
                .OptionalMatch("(agent)-[:" + Label.Relationship.HasRelation + "]->()-[:" + Label.Relationship.RepresentativeOf + "]->(representativeOf:Category)")
                .OptionalMatch("(artefact)-[:" + Label.Relationship.HasRelation + "]->()-[:" + Label.Relationship.HasSubject + "]->(subject:Category)")
                .OptionalMatch("(artefact)-[:" + Label.Relationship.HasRelation + "]->()-[:" + Label.Relationship.HasGenre + "]->(genre:Category)")
                .With("agent, representativeOf, collect(distinct({ Artefact: artefact, Artist: agent, Image: image, Container: container, Location: places, ArtefactType: artefactType, Subject: subject, Genre: genre })) as artefactsWithImages, collect(distinct(agentType)) as agentTypes")
                ;

            var results = await query
                .Return(() => Return.As<AgentWithAgentTypes>("distinct { Agent: agent, RepresentativeOf: representativeOf, AgentTypes: agentTypes, ArtefactWithImages: artefactsWithImages, ArtefactTotal: length(artefactsWithImages) }"))
                .ResultsAsync
                ;

            return results;
        }

        public IEnumerable<ContextHyperNode> FindContextsByPlace(string placeGuid)
        {
            var query = Client.Cypher
                .Match("(place:" + Label.Entity.Place + " { Guid: {placeGuid} })").WithParams(new { placeGuid })
                .Match("(inside:" + Label.Entity.Place + ")-[:" + Label.Relationship.InsideOf + "*0..4]->(place)")
                .Match("(context:" + Label.Entity.Context + ")-[contextAt:" + Label.Relationship.ContextAt + "]->(inside)")
                .Match("(artefact:" + Label.Entity.Artefact + ")-[hasContext:" + Label.Relationship.HasContext + "]->(context:" + Label.Entity.Context + ")")
                .Match("(context)-[contextFor:" + Label.Relationship.ContextFor + "]->(subject:" + Label.Entity.Category + ")")
                .OptionalMatch("(artefact)-[:" + Label.Relationship.HasImage + "]->(artefactImage:" + Label.Entity.Image + " { Primary: true })")
                .OptionalMatch("(artefact)-[:" + Label.Relationship.HasRelation + "]->()-[:" + Label.Relationship.MadeBy + "]->(madeBy:" + Label.Entity.Agent + ")");
            var results = query.Return(() => new ContextHyperNode
            {
                Context = Return.As<Context>("context"),
                Artefact = Return.As<Artefact>("artefact"),
                Subject = Return.As<Category>("subject"),
                Place = Return.As<Place>("inside"),
                HasContextGuid = Return.As<string>("hasContext.Guid"),
                ContextAtGuid = Return.As<string>("contextAt.Guid"),
                ContextForGuid = Return.As<string>("contextFor.Guid"),
                ArtefactImage = Return.As<Image>("artefactImage"),
                MadeBy = Return.As<Agent>("head(collect(madeBy))")
            })
            .Results
            .ToList();
            return results;
        }
        public List<PlaceResult> AllWithContainers(int maxDepth = 4)
        {
            var result = Client.Cypher
                .Match("(place:" + Label.Entity.Place + ")-[:" + Label.InsideOf + "*0.." + maxDepth + "]->(container:" + Label.Entity.Place + ")")
                .Return((ICypherResultItem place, ICypherResultItem parent) => new PlaceResult
                {
                    Place = place.As<Place>(),
                    Containers = Return.As<IEnumerable<Place>>("collect(distinct(container))")
                }).OrderBy("place.Name").Results.ToList();
            return result;
        }
        public void InsideOf(Place place, Place container)
        {
            var relationshipGuid = Guid.NewGuid().ToString();
            Client.Cypher
                    .Match(
                        "(place:" + Label.Entity.Place + " { Guid: {placeGuid} })",
                        "(container:" + Label.Entity.Place + " { Guid: {containerGuid} })"
                    )
                    .WithParams(new
                    {
                        placeGuid = place.Guid,
                        containerGuid = container.Guid
                    })
                    .CreateUnique("place-[:" + Label.InsideOf + " { Guid: {relationshipGuid} }]->container")
                    .WithParams(new
                    {
                        relationshipGuid
                    })
                    .ExecuteWithoutResults();
        }
        public void IsA(Place place, params Category[] categories)
        {
            /**
             * (place)-[:IS_A]->(category)
             */
            foreach (var category in categories)
            {
                var relationshipGuid = Guid.NewGuid().ToString();
                Client.Cypher
                    .Match(
                        "(agent:" + Label.Entity.Place + " { Guid: {agentGuid} })",
                        "(cat:" + Label.Entity.Category + " { Guid: {catGuid} })")
                    .WithParams(new
                    {
                        agentGuid = place.Guid,
                        catGuid = category.Guid
                    })
                    .CreateUnique("agent-[:" + Label.IsA + " { Guid: {relationshipGuid} }]->cat")
                    .WithParams(new
                    {
                        relationshipGuid
                    })
                    .ExecuteWithoutResults();
            }
        }
        public void SetCurrentUser(User user)
        {
            CurrentUser = user;
        }
        public Place SaveOrUpdate(Place place)
        {
            commonService.SaveOrUpdate(place, Label.Entity.Place);
            Cache.RemoveFromCache(CacheKey.PlaceService.All);
            return place;
        }
        public bool Delete(string placeGuid)
        {
            try
            {
                /**
                 * Delete the attribute hypernodes.
                 */
                Client.Cypher
                    .Match("(p:" + Label.Entity.Place + " { Guid: {placeGuid} })-[r1:" + Label.Relationship.HasRelation + "]->(attribute:" + Label.Entity.Relation + ")-[r2]-()")
                    .WithParams(new
                    {
                        placeGuid = placeGuid
                    })
                    .Delete("r1, r2, attribute")
                    .ExecuteWithoutResults();
                /**
                 * Delete any other direct relationships and the node itself.
                 */
                Client.Cypher
                    .Match(
                        "(p:" + Label.Entity.Place + " { Guid: {placeGuid} })"
                    )
                    .OptionalMatch("(p)-[r]-()")
                    .WithParams(new
                    {
                        placeGuid = placeGuid
                    })
                    .Delete("r, p")
                    .ExecuteWithoutResults();
                Cache.RemoveFromCache(CacheKey.PlaceService.All);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public Place FindPlaceByGuid(string guid)
        {
            var place = Client.Cypher
                .Match("(p:" + Label.Place + ")")
                .Where((Place p) => p.Guid == guid)
                .Return(p => p.As<Place>())
                .Results.FirstOrDefault();
            return place;
        }
        public List<Place> FindPlaces(string name)
        {
            var place = Client.Cypher
                .Match("(p:" + Label.Place + ")")
                .Where("p.Name =~ '(?i)" + name + ".*'")
                .Return(p => p.As<Place>())
                .Results.ToList();
            return place;
        }
        public List<Place> All()
        {
            var people = Client.Cypher
                .Match("(p:" + Label.Place + ")")
                .Return(p => p.As<Place>())
                .OrderBy("p.Name")
                .Results.ToList();
            return people;
        }
    }
}
